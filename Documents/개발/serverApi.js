var host = "http://115.68.104.30:8888";

function httpPost(_url, _paramId) {
	var params = JSON.parse($('#' + _paramId).val());
	$.post(host + _url, params, function(_data) {
		var html = "";
		html += "<p>요청</p>";
		html += "<div>URL : " + (host + _url) + "</div>";
		html += "<div>PARAMS : " + JSON.stringify(params).replace(/\\/g, "") + "</div>";
		html += "<p>응답</p>";
		html += "<div>" + JSON.stringify(_data) + "</div>";
		$('#ret').html(html);
	}, "json");
}

function httpGet(_url, _paramId) {
	var params = JSON.parse($('#' + _paramId).val());
	$.get(host + _url, params, function(_data) {
		var html = "";
		html += "<p>요청</p>";
		html += "<div>URL : " + (host + _url) + "</div>";
		html += "<div>PARAMS : " + JSON.stringify(params).replace(/\\/g, "") + "</div>";
		html += "<p>응답</p>";
		html += "<div>" + JSON.stringify(_data) + "</div>";
		$('#ret').html(html);
	}, "json");
}
