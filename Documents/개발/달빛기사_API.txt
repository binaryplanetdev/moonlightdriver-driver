taxi.js
server.post('/taxi/:taxiId/register', registerTaxi); // 택시카풀 동승 신청 
server.post('/taxi', createTaxi); // 택시카풀 생성 
server.post('/taxi/list', taxiList); // 택시카풀 목록 불러오기 
server.post('/taxi/:taxiId/get', getTaxi); // (현재 미사용중이나 사용해야 할 것으로 보임) 
server.post('/taxi/:taxiId/accept', acceptTaxi); // 동승신청 수락
server.post('/taxi/:taxiId/decline', declineTaxi); // 동승신청 거부
server.post('/driver/update', updateStatus); // 기사의 현재 위치 업데이트 (현재 사용하지 않고있으나 사용해야 함)
server.get('/driver/find', findStatus); // 주변 기사 검색 (현재 사용하지 않고 있으나 사용해야 함)

 
driver.js
server.post('/driver/register', registerDriver); // 회원가입
server.post('/driver/:driverId/edit', EditDriver); // 회원정보 수정, 회원정보를 불러올 때도 사용됨


file.js
server.get('/file/:fileTime/:fileName', sendFile); // 회원 프로필 사진 얻기 
server.post('/file', uploadFile); // 회원 프로필 사진 등록 


bus.js
server.post('/nosun', recordNosun);
server.get('/nosun/:nosunId/get', checkNosun);
(위 둘은 버스 운행시간을 서버에 저장하고 운행시간을 확인하기 위해 썼던 것인데, 현재는 쓰지 않음) 
(이 파일 내의 나머지 것들은 셔틀노선 수집앱쪽을 확인해보셔야 합니다.)
 

bus_new.js
server.get("/busnew/:busId/comment", getComment); // 셔틀버스에 달려있는 코멘트 불러오기 
server.get('/busnew2', busList2); // 셔틀 목록 불러오기 
(이 둘 이외의 것들은 셔틀버스 노선 수집앱쪽에서 사용되고 있을겁니다.)


#
bus_new.js
server.post("/busnew/:busId/comment", addComment);
내 주변 셔틀 목록을 불러오는 것이 목적이므로
각 노선의 이동경로 포인트들은 필요 없는 상황입니다.

이렇게 되면 추가로
셔틀 노선을 하나 선택했을 때 이동경로 point들을 보내줄 api가 필요합니다.

bus_new.js
server.get("/busnew/:busId/comment", getComment);
채팅창에 처음 참여하는 사람의 경우
지난 채팅목록을 다 보여주든가 최근 몇개만 보여주든가 해야하는데
parameter로 준비된 것이
len,  pageNo 입니다.
전체 채팅 개수를 result에 항상 내려보내주는 것도 좋을 것 같고..
협의가 필요한 부분이네요.

#
1. 푸시를 위한 id와 기기 폰번호를 서버에 자동 저장
(참고 : 예전에 회원가입과 푸시 보내는 부분이 구현된 적이 있습니다.)
푸시 부분이 제 개인계정key가 들어가있어서
사장님께 구글개발자 사이트 가입 요청드리고, 
그 계정으로 key 만들어서 알려드리겠습니다.

2. 채팅 참여중 여부
참여중인 기사들에게만 푸시를 보내기 위해서
어떤 노선의 채팅이 참여중인지를 저장해둘 api 필요

3. 내 주변 셔틀 정류장
내 주변의 셔틀 정류장 목록을 보여주기 위한 api 필요