<?php
	header("Content-Type:text/html; charset=utf-8");
		
	try {
		require_once ("../inc/config.php");
		require_once_classes(array("CDatabaseManager"));
	
		$retResult = array("result" => "OK", "msg" => "SUCCESS", "data" => array());
			
		$database_manager = new CDatabaseManager();
		$database = $database_manager->getDb();
		
		$type = $_POST["type"];
		$retCount = 0;
		
		if($type == "update_crackdown_data") {
			$crackdown_data = $_POST["crackdown_data"];
			
			if(!isset($crackdown_data) || empty($crackdown_data)) {
				echo json_encode($retResult);
				exit;
			}
			
			$crackdown_data = json_decode($crackdown_data, true);
			
			$crackdowns = $database->crackdowns;
			$counters = $database->counters;
			
			$lastUpdatedTimeCounter = $counters->findOne(array("_id" => "crackdown_last_update_time"));

			$lastUpdatedTime = 0;
			if(isset($lastUpdatedTimeCounter)) {
				$lastUpdatedTime = $lastUpdatedTimeCounter["seq"];
			}
			
			$updatedTime = time() * 1000;
			foreach ($crackdown_data as $row) {
				if($lastUpdatedTime >= $row["createTime"]) {
					continue;
				}
				
				$createTime = date("G", ($row["createTime"] / 1000));
				if($createTime > 5 && $createTime < 19) {
					continue;
				}

				$newData = array(
					"userId" => "",
					"registrant" => getRandPhoneNumber(),
					"phone" => "",
					"reportType" => "normal",
					"picture" => "",
					"dislikeCount" => floatval(0),
					"likeCount" => floatval(0),
					"commentCount" => floatval(0),
					"from" => $row["from"],
					"sido" => $row["sido"], 
					"sigugun" => $row["sigugun"],
					"locations" => array("type" => "Point", "coordinates" => array($row["longitude"], $row["latitude"])),
					"address" => $row["address"],
					"seq" => floatval($row["seq"]),
					"createTime" => floatval($row["createTime"]),
					"updatedTime" => floatval($updatedTime)
				);
				
				$crackdowns->insert($newData);
				
				$retCount++;
			}
			
			$counters->update(array("_id" => "crackdown_last_update_time"), array('$set' => array('seq' => floatval($updatedTime))));
			
			$retResult["retCount"] = $retCount;
		} else {
			$retResult = array("result" => "ERROR", "msg" => "정상적인 호출이 아닙니다.");
		}
	} catch (Exception $e) {
		$retResult["result"] = "ERROR";
		$retResult["msg"] = $e->getMessage();
	}
	
	echo json_encode($retResult);
	exit;
	
	function getRandPhoneNumber() {
		$randomNumber = "";
		for($i = 0; $i < 8; $i++) {
			if($i == 1 || $i == 2) {
				$randomNumber .= "X";
			} else {
				$randomNumber .= mt_rand(0, 9);
			}
			
			if($i == 3) {
				$randomNumber .= "-";
			}
		}
		
		return $randomNumber;
	}
?>
