<?php
	header("Content-Type:text/html; charset=utf-8");
	
	try {
		require_once ("../inc/config.php");
		require_once_classes(array("CDatabaseManager"));
	
		$retResult = array("result" => "OK");
			
		$database_manager = new CDatabaseManager();
		$database = $database_manager->getDb();

		$type = $_POST["type"];
		$retCount = 0;
		
		if($type == "update_shuttle_position") {
			$shuttle_position_data = $_POST["shuttle_position_data"];
			
			if(isset($shuttle_position_data) && !empty($shuttle_position_data)) {
				$shuttle_position_data = json_decode($shuttle_position_data, true);
			}
			
			if(isset($shuttle_position_data["CarGpsList"])) {
				$shuttlerealtimepositions = $database->shuttlerealtimepositions;
				
				foreach ($shuttle_position_data["CarGpsList"] as $row) {
					$shuttlerealtimepositions->update(
						array('categoryName' => $row["UserID"]),
						array(
							'categoryName' => $row["UserID"],
							'updateDate' => time() * 1000,
							'locations' => array('type' => 'Point', 'coordinates' => array($row['Lon'], $row['Lat']))),
						array('upsert' => true)
					);
				
					$retCount++;
				}
					
				$retResult["retCount"] = $retCount;
			}
		} else {
			$retResult = array("result" => "ERROR");
		}
	} catch (Exception $e) {
		$retResult["result"] = "ERROR";
		$retResult["message"] = $e->getMessage();
	}
	
	echo json_encode($retResult);
	exit;
?>
