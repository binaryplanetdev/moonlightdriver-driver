<?php
	class CNoticeManager {		
		function CNoticeManager() {}
		
		function getNoticeList($_notices) {
			$notice_list = $_notices->find()->sort(array('seq' => -1));
			
			$ret_notice_list = array();
			foreach ($notice_list as $row) {
				$notice_id = strval($row["_id"]);
				
				$row["notice_id"] = $notice_id;
				$row["startDate"] = date("Y-m-d H:i:s", ($row["startDate"] / 1000));
				$row["endDate"] = date("Y-m-d H:i:s", ($row["endDate"] / 1000));
				$row["createdDate"] = date("Y-m-d H:i:s", ($row["createdDate"] / 1000));
				
				$ret_notice_list[$notice_id] = $row;
			}
			
			return $ret_notice_list;
		}
		
		function getNoticeInfo($_notices, $_notice_id) {
			$ret_notice_info = $_notices->findOne(array('_id' => new MongoId($_notice_id)));
			
			$notice_id = strval($ret_notice_info["_id"]);
			
			$ret_notice_info["notice_id"] = $notice_id;
			
			return $ret_notice_info;
		}
		
		function getNextNoticeSeq($_notices) {
			$next_seq_cursor = $_notices->find(array(), array('seq' => 1))->sort(array('seq' => -1))->limit(1);
			$next_seq = 0;
			foreach ($next_seq_cursor as $row) {
				$next_seq = intval($row["seq"]) + 1;
			}
		
			return $next_seq;
		}
		
		function addNotice($_notices, $_title, $_contents, $_image, $_link, $_startDate, $_endDate) {
			$startDate = strtotime($_startDate) * 1000;
			$endDate = strtotime($_endDate) * 1000;
			
			$seq = $this->getNextNoticeSeq($_notices);
			
			$newData = array(
				'seq' => $seq,
				'title' => $_title,
				'contents' => $_contents,
				'link' => $_link,
				'image' => $_image,
				'startDate' => $startDate,
				'endDate' => $endDate,
				'createdDate' => time() * 1000,
				'updatedDate' => time() * 1000
			);
			
			$ret = $_notices->insert($newData);
			
			if(isset($newData["_id"])) {
				$newData["notice_id"] = strval($newData["_id"]);
				$newData["startDate"] = date("Y-m-d H:i:s", ($newData["startDate"] / 1000));
				$newData["endDate"] = date("Y-m-d H:i:s", ($newData["endDate"] / 1000));
				$newData["createdDate"] = date("Y-m-d H:i:s", ($newData["createdDate"] / 1000));
			}
			
			return $newData;
		}
		
		function updateNotice($_notices, $_noticeId, $_title, $_contents, $_image, $_link, $_startDate, $_endDate) {
			$startDate = strtotime($_startDate) * 1000;
			$endDate = strtotime($_endDate) * 1000;
			
			$updateData = array(
				'title' => $_title,
				'contents' => $_contents,
				'link' => $_link,
				'image' => $_image,
				'startDate' => $startDate,
				'endDate' => $endDate,
				'updatedDate' => time() * 1000
			);
			
			$ret = $_notices->update(array('_id' => new MongoId($_noticeId)), array('$set' => $updateData));
			
			$updateData["notice_id"] = $_noticeId;
			$updateData["startDate"] = date("Y-m-d H:i:s", ($updateData["startDate"] / 1000));
			$updateData["endDate"] = date("Y-m-d H:i:s", ($updateData["endDate"] / 1000));

			return $updateData;
		}
		
		function deleteNotice($_notices, $_noticeId) {
			$_notices->remove(array("_id" => new MongoId($_noticeId)));
		}
	}
?>