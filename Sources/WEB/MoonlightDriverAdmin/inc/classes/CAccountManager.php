<?php 
	class CAccountManager {
		function CAccountManager() {}
		
		function getAccountList($_accounts) {
			$account_list = $_accounts->find()->sort(array('id' => 1));
				
			global $ROLE;
			
			$ret_account_list = array();
			foreach ($account_list as $row) {
				$account_id = strval($row["_id"]);
		
				$row["account_id"] = $account_id;
				$row["id"] = $row["id"];
				$row["name"] = $row["name"];
				$row["phone"] = $row["phone"];
				$row["email"] = $row["email"];
				$row["level"] = $row["level"];
				$row["role"] = $ROLE[$row["level"]];
				$row["createdDate"] = date("Y-m-d H:i:s", ($row["createdDate"] / 1000));
				$row["updatedDate"] = date("Y-m-d H:i:s", ($row["updatedDate"] / 1000));
		
				$ret_account_list[$account_id] = $row;
			}
			
			return $ret_account_list;
		}
		
		function getAccountInfo($_accounts, $_account_id) {
			$ret_account_info = $_accounts->findOne(array('_id' => new MongoId($_account_id)));
				
			if(isset($ret_account_info)) {
				$account_id = strval($ret_account_info["_id"]);
				
				global $ROLE;
				
				$ret_account_info["account_id"] = $account_id;
				$ret_account_info["role"] = $ROLE[$ret_account_info["level"]];
				$ret_account_info["createdDate"] = date("Y-m-d H:i:s", ($ret_account_info["createdDate"] / 1000));
				$ret_account_info["updatedDate"] = date("Y-m-d H:i:s", ($ret_account_info["updatedDate"] / 1000));
			}
				
			return $ret_account_info;
		}
		
		function isDuplicatedId($_accounts, $_id, $_account_id) {
			$account_list = $_accounts->find(array("id" => $_id));
			
			$count = 0;
			foreach ($account_list as $row) {
				$account_id = strval($row["_id"]);
				if(isset($_account_id)) {
					if($account_id != $_account_id) {
						$count++;
					}
				} else {
					$count++;
				}
			}
			
			if($count > 0) {
				return true;
			} else {
				return false;
			}
		}
		
		function addAccount($_accounts, $_id, $_pw, $_name, $_phone, $_email, $_level, $_ynPush) {
			$newData = array(
				'id' => $_id,
				'pw' => md5($_pw),
				'name' => $_name,
				'phone' => $_phone,
				'email' => $_email,
				'level' => $_level,
				'ynPush' => $_ynPush,
				'createdDate' => time() * 1000,
				'updatedDate' => time() * 1000
			);
			
			$ret = $_accounts->insert($newData);
				
			if(isset($newData["_id"])) {
				global $ROLE;
				
				$newData["account_id"] = strval($newData["_id"]);
				$newData["role"] = $ROLE[$newData["level"]];
				$newData["createdDate"] = date("Y-m-d H:i:s", ($newData["createdDate"] / 1000));
				$newData["updatedDate"] = date("Y-m-d H:i:s", ($newData["updatedDate"] / 1000));
			}
				
			return $newData;
		}
		
		function updateAccount($_accounts, $_account_id, $_id, $_pw, $_name, $_phone, $_email, $_level, $_ynPush) {
			$updateData = array(
				'id' => $_id,
				'name' => $_name,
				'phone' => $_phone,
				'email' => $_email,
				'level' => $_level,
				'ynPush' => $_ynPush,
				'updatedDate' => time() * 1000
			);
			
			if(isset($_pw) && !empty($_pw)) {
				$updateData["pw"] = md5($_pw);
			}
				
			$ret = $_accounts->update(array('_id' => new MongoId($_account_id)), array('$set' => $updateData));
				
			return $this->getAccountInfo($_accounts, $_account_id);
		}
		
		function deleteAccount($_accounts, $_account_id) {
			$_accounts->remove(array("_id" => new MongoId($_account_id)));
		}
		
		function updateAccountYnPush($_accounts, $_account_id, $_ynPush) {
			$updateData = array(
				'ynPush' => $_ynPush,
				'updatedDate' => time() * 1000
			);
				
			$ret = $_accounts->update(array('_id' => new MongoId($_account_id)), array('$set' => $updateData));
		}
	}
?>