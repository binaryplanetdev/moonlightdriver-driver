<?php
	class CCallManager {		
		function CCallManager() {}
		
		function getCallList($_database) {
			$call_list = $_database->find()->sort(array('_id' => -1));
			
			global $CALL_STATUS;
			$ret_call_list = array();
			foreach ($call_list as $row) {
				$call_id = strval($row["_id"]);
				
				$row["call_id"] = $call_id;
				$row["status_text"] = $CALL_STATUS[$row["status"]]["status_text"];
				$row["createdAt"] = date("Y-m-d H:i:s", $row["createdAt"]->sec);
				
				$ret_call_list[$call_id] = $row;
			}
			
			return $ret_call_list;
		}
		
		function getCallInfo($_database, $_call_id) {
			$ret_call_info = $_database->findOne(array('_id' => new MongoId($_call_id)));
			
			$call_id = strval($ret_call_info["_id"]);
			
			global $CALL_STATUS;
			$ret_call_info["call_id"] = $call_id;
			$ret_call_info["status_text"] = $CALL_STATUS[$ret_call_info["status"]]["status_text"];
			$ret_call_info["createdAt"] = date("Y-m-d H:i:s", $ret_call_info["createdAt"]->sec);
			
			return $ret_call_info;
		}
	}
?>