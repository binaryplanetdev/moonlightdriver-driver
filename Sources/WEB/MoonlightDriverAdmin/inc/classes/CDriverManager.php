<?php 
	class CDriverManager {
		function CDriverManager() {}
		
		function getDriverList($_drivers) {
			$driver_list = $_drivers->find()->sort(array('id' => 1));
				
			$ret_driver_info = array();
			foreach ($driver_list as $row) {
				$driver_id = strval($row["_id"]);
		
				$retData = array();
				$retData["driver_id"] = $driver_id;
				$retData["name"] = isset($row["name"]) ? $row["name"] : "";
				$retData["phone"] = isset($row["phone"]) ? $row["phone"] : "";
				$retData["star"] = isset($row["star"]) ? $row["star"] : 0;
				$retData["starnum"] = isset($row["starnum"]) ? $row["starnum"] : 0;
				$retData["point"] = isset($row["point"]) ? $row["point"] : 0;
				$retData["version"] = isset($row["version"]) ? $row["version"] : "";
				$retData["ynLogin"] = isset($row["ynLogin"]) ? $row["ynLogin"] : "N";
				$retData["createdDate"] = isset($row["createdDate"]) ? date('Y-m-d H:i:s', $row["createdDate"]->sec) : "";
				$retData["lastLoginDate"] = isset($row["lastLoginDate"]) ? date('Y-m-d H:i:s', $row["lastLoginDate"]->sec) : "";
		
				$ret_driver_info[$driver_id] = $retData;
			}
			
			return $ret_driver_info;
		}
		
		function getDriverInfo($_drivers, $_driver_id) {
			$ret_driver_info = $_drivers->findOne(array('_id' => new MongoId($_driver_id)));
			
			if(isset($ret_driver_info)) {
				$driver_id = strval($ret_driver_info["_id"]);
				
				$ret_driver_info["driver_id"] = $driver_id;
			}
				
			return $ret_driver_info;
		}
		
		function getNearDriverList($_taxifinds, $_lat, $_lng, $_range) {
			$limitTime = (time() * 1000) - (60 * 60 * 1000);
			$retDriverList = array();
			
			$driverList = $_taxifinds->aggregate(
				array(
					'$geoNear' => array(
						'near' => array(
							'type' => 'Point',
							'coordinates' => array($_lng, $_lat)
						),
						'distanceField' => 'dist.distance',
						"maxDistance" => $_range,
						"spherical" => true,
						"limit" => 100000
					)
				),
				array(
					'$match' => array(
						'date' => array('$gt' => $limitTime)
					)
				),
				array(
					'$group' => array(
						'_id' => array(
							'driverId' => '$driverId',
							'phone' => '$phone',
							'location' => '$location'
						),
						'minDistance' => array(
							'$min' => '$dist.distance'
						)
					)
				),
				array(
					'$sort' => array(
						'minDistance' => 1
					)
				)
			);
			
			foreach ($driverList["result"] as $row) {
				$retDriverList[] = array(
					"driverId" => $row["_id"]["driverId"],
					"phone" => $row["_id"]["phone"],
					"location" => array("lat" => $row["_id"]["location"]["coordinates"][1], "lng" => $row["_id"]["location"]["coordinates"][0]),
					"stationType" => "driver"
				);
			}
			
			return $retDriverList;
		}
		
		function getRegIdList($_drivers, $_recv_user_type, $_phones, $_skip, $_limit) {
			if($_recv_user_type == "selected") {
				$driver_list = $_drivers->find(array('phone' => array('$in' => $_phones)), array("gcmId" => 1))->skip($_skip)->limit($_limit);
			} else if($_recv_user_type == "all") {
				$driver_list = $_drivers->find(array(), array("gcmId" => 1))->skip($_skip)->limit($_limit);
			} else {
				return null;
			}
			
			if(!isset($driver_list)) {
				return null;
			}
						
			$ret_driver_list = array();
			foreach ($driver_list as $row) {
				$ret_driver_list[] = $row["gcmId"];
			}
				
			return $ret_driver_list;
		}
		
		function getLastestAppVersion($_versions) {
			$ret_version_info = $_versions->find(array('isValid' => true))->sort(array("version" => -1))->limit(1);
			
			if(isset($ret_version_info)) {
				foreach ($ret_version_info as $row) {
					return $row["version"];
				}
			}
		
			return "";
		}
		
		function getAppVersionList($_versions) {
			$ret_version_list = array();
			$version_list = $_versions->find()->sort(array("version" => -1));
				
			if(isset($version_list)) {
				foreach ($version_list as $row) {
					$ret_version_list[] = array(
						"version" => $row["version"],
						"isValid" => $row["isValid"]
					);
				}
			}
		
			return $ret_version_list;
		}
	}
?>