<?php
	class CQuestionManager {		
		function CQuestionManager() {}
		
		function getQuestionList($_questions) {
			$question_list = $_questions->find()->sort(array('seq' => -1));
			
			$ret_question_list = array();
			foreach ($question_list as $row) {
				$question_id = strval($row["_id"]);
				
				$row["question_id"] = $question_id;
				$row["updatedDate"] = date("Y-m-d H:i:s", ($row["updatedDate"] / 1000));
				$row["createdDate"] = date("Y-m-d H:i:s", ($row["createdDate"] / 1000));
				
				$ret_question_list[$question_id] = $row;
			}
			
			return $ret_question_list;
		}
		
		function getQuestionAnswerList($_questionanswers, $_question_ids) {
			$question_answer_list = $_questionanswers->find(array('questionId' => array('$in' => $_question_ids)))->sort(array("_id" => 1, "seq" => -1));
			
			$question_answer_info_list = array();
			foreach ($question_answer_list as $row) {
				$question_id = $row["questionId"];
				$question_answer_id = strval($row["_id"]);
				
				$row["question_answer_id"] = $question_answer_id;
				$row["createdDate"] = date("Y-m-d H:i:s", ($row["createdDate"] / 1000));
			
				if(!isset($question_answer_info_list[$question_id])) {
					$question_answer_info_list[$question_id] = array();
				}
							
				$question_answer_info_list[$question_id][$question_answer_id] = $row;
			}
			
			return $question_answer_info_list;
		}
		
		function getDriverList($_drivers, $_driver_ids) {
			$driver_list = $_drivers->find(array('_id' => array('$in' => $_driver_ids)));
			
			$driver_info_list = array();
			foreach ($driver_list as $row) {
				$driver_id = strval($row["_id"]);
				$row["driver_id"] = $driver_id;
				
				$driver_info_list[$driver_id] = $row;
			}
			
			return $driver_info_list;
		}
		
		function getDriverInfo($_drivers, $_driver_id) {
			$driver_info = $_drivers->findOne(array('_id' => new MongoId($_driver_id)));
				
			$driver_id = strval($driver_info["_id"]);
			
			$driver_info["driver_id"] = $driver_id;
			
			return $driver_info;
		}
		
		function getQuestionAnswerInfo($_questionanswers, $_question_answer_id) {
			$answer_info = $_questionanswers->findOne(array('_id' => new MongoId($_question_answer_id)));
		
			$question_answer_id = strval($answer_info["_id"]);
				
			$answer_info["question_answer_id"] = $question_answer_id;
			$answer_info["createdDate"] = date("Y-m-d H:i:s", ($answer_info["createdDate"] / 1000));
				
			return $answer_info;
		}
		
		function getQuestionInfo($_questions, $_question_id) {
			$ret_question_info = $_questions->findOne(array('_id' => new MongoId($_question_id)));
			
			$question_id = strval($ret_question_info["_id"]);
			
			$ret_question_info["question_id"] = $question_id;
			$ret_question_info["createdDate"] = date("Y-m-d H:i:s", ($ret_question_info["createdDate"] / 1000));
			
			return $ret_question_info;
		}
		
		function getQuestionAnswerCount($_questionanswers, $_questionId) {
			$answer_count = $_questionanswers->count(array('questionId' => $_questionId));
			
			return $answer_count;
		}
		
		function getNextAnswerSeq($_questionanswers, $_questionId) {
			$next_seq_cursor = $_questionanswers->find(array('questionId' => $_questionId), array('seq' => 1))->sort(array('seq' => -1))->limit(1);
			$next_seq = 0;
			foreach ($next_seq_cursor as $row) {
				$next_seq = intval($row["seq"]) + 1;
			}

			return $next_seq;
		}
		
		function insertNewAnswer($_questionanswers, $_questionId, $_answer) {
			$next_seq = $this->getNextAnswerSeq($_questionanswers, $_questionId);
			
			$newData = array(
				"questionId" => $_questionId,
				"seq" => $next_seq,
				"answer" => $_answer,
				"createdDate" => (time() * 1000)
			);
			
			$ret = $_questionanswers->insert($newData);
			
			$newData["createdDate"] = date("Y-m-d H:i:s", ($newData["createdDate"] / 1000));
						
			return $newData;
		}
		
		function updateAnswer($_questionanswers, $_questionAnswerId, $_answer) {
			$updateData = array(
				"answer" => $_answer
			);
				
			$ret = $_questionanswers->update(array('_id' => new MongoId($_questionAnswerId)), array('$set' => $updateData));
				
			$updateData = $this->getQuestionAnswerInfo($_questionanswers, $_questionAnswerId);
		
			return $updateData;
		}
		
		function updateAnswerCount($_questions, $_questionanswers, $_questionId) {
			$answer_count = $this->getQuestionAnswerCount($_questionanswers, $_questionId);
			
			$updateData = array('$set' => array('answerCount' => $answer_count));
			$_questions->update(array('_id' => new MongoId($_questionId)), $updateData);
			
			return $answer_count;
		}
		
		function deleteQuestion($_questions, $_questionanswers, $_questionId) {
			$_questionanswers->remove(array("questionId" => $_questionId));
			$_questions->remove(array("_id" => new MongoId($_questionId)));
		}
	}
?>