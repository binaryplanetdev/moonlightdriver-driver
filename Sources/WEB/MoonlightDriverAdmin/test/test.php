<?php
	header("Content-Type:text/html; charset=utf-8");
	
	try {
		require_once ("../inc/config.php");
		require_once_classes(array("CDatabaseManager"));
	
		$database_manager = new CDatabaseManager();
		$database = $database_manager->getDb();
		
		$_logtaxifinds = $database->_logtaxifinds;
		$_logtaxifinds->aggregate(
			array(
				'$match' => array(
					'logDate' => array('$gt' => $_stationCodes),
					'weekTag' => strval($dayOfWeek)
				)
			),
			array(
				'$group' => array(
					'_id' => array(
						'stationCode' => '$stationCode'
					),
					'firstTime' => array(
						'$min' => '$firstTime'
					),
					'lastTime' => array(
						'$max' => '$lastTime'
					)
				)
			)
		);
		
		foreach ($crackdown_data as $row) {
			$row["updatedTime"] = time() * 1000;
			$crackdowns->insert($row);
		}
	} catch (Exception $e) {
		echo "<pre>";
		print_r($e);
	}
	
	exit;
?>
