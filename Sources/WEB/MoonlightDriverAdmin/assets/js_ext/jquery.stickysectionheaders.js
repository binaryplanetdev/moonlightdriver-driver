/*!
 * Sticky Section Headers
 *
 * Copyright (c) 2013 Florian Plank (http://www.polarblau.com/)
 * Dual licensed under the MIT (MIT-LICENSE.txt)
 * and GPL (GPL-LICENSE.txt) licenses.
 *
 * USAGE:
 *
 * $('#container').stickySectionHeaders({
 *   stickyClass      : 'sticky',
 *   headlineSelector : 'strong'
 * });
 *
 */

(function($) {
	$.fn.stickySectionHeaders = function(options) {
		var settings = $.extend({
			stickyClass     : 'sticky',
			headlineSelector: 'strong'
		}, options);
		
		return $(this).each(function() {
			var $this = $(this);
			$(this).find('ul:first').bind('scroll.sticky', function(e) {
				$(this).find('> li').each(function() {
					var $this = $(this);
					var top = $this.position().top;
					var height = $this.outerHeight();
					var $head = $this.find(settings.headlineSelector);
					var headHeight = $head.outerHeight();
					
					if (top < 0) {
						$this.addClass(settings.stickyClass).css('paddingTop', headHeight);
						$head.css({
							'top'  : (height + top < headHeight) ? (headHeight - (top + height)) * -1 : '',
							'width': $this.outerWidth()
						});
					} else {
						$this.removeClass(settings.stickyClass).css('paddingTop', '');
					}
				});
			});
		});
	};
})(jQuery);
