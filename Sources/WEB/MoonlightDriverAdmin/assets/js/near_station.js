var shuttleLineColorList;
var driver_current_position_marker;

function initializeMap() {
	getShuttleColorList();
}

function getShuttleColorList() {
	var params = {};
	params.type = "shuttle_line_color_list";
	
	shuttleLineColorList = {};
	
	$.post("/ajax/ajax.php", params, function(_ret_data) {
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			shuttleLineColorList = ret_data.data;
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function searchDriver() {
	var search_text = $.trim($("#driver_search").val());
	if(search_text.length < 3) {
		alert("3자이상 입력하세요.");
		return;
	}
	
	var params = {};
	params.type = "search_driver";
	params.search_text = search_text;
	
	$.post("/ajax/ajax.php", params, function(_ret_data) {
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			if(ret_data.data && map) {
				var center = new google.maps.LatLng(ret_data.data.location.coordinates[1], ret_data.data.location.coordinates[0]);
				
				showMarker(center, ret_data.data);
				
				map.panTo(center);
			} else {
				alert("결과가 없습니다.");
			}
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function showMarker(_position, _driver_data) {
	if(driver_current_position_marker) {
		driver_current_position_marker.setMap(null);
	}
	
	driver_current_position_marker = new google.maps.Marker({
		position: _position,
		icon: "/assets/images/pin_driver_40.png",
		map: map
	});

	var driver_current_position_infowindow = new google.maps.InfoWindow({
		disableAutoPan: true,
		content: _driver_data.name + "기사님(" + _driver_data.phone + ")의 현위치<br/>(최종 업데이트 : " + _driver_data.date + ")"
	});

	driver_current_position_marker.addListener('click', function() {
		driver_current_position_infowindow.open(map, driver_current_position_marker);
	});

	driver_current_position_infowindow.open(map, driver_current_position_marker);
}