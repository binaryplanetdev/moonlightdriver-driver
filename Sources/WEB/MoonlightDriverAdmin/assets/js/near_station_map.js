var map;
var LIMIT_BOUNDS_NE = { lat : 83.91177715928563, lng : -175.8688546344638 };
var LIMIT_BOUNDS_SW = { lat : -43.27909581852835, lng : 57.56865844130516 };
var isHidePopupLayer = true;
var isHideSecondPopupLayer = true;
var distance_select = [1000, 2000, 3000, 4000, 5000];
var selected_distance_index = 0;
var running_station_list;
var shuttle_line_color_list;
var isShowOutback = false;
var station_info_window;
var station_info_window_element;
var stationMarkerList = [];
var shuttleLineList = [];
var outbackPolygon;
var lastCenter;
var DEFAULT_ZOOM = 18;

function initMap() {
	if(!map_center) {
		map_center = new google.maps.LatLng(37.566702, 126.978309);
	}
	
	map = new google.maps.Map(document.getElementById('map'), {
		center: map_center,
		zoom: DEFAULT_ZOOM
	});
	
	station_info_window = new google.maps.InfoWindow({
		content: ""
	});
	
	google.maps.event.addListener(map, 'idle', function() {
		if(map.getZoom() < 15) {
			removeAllObjectOnMap();			
			return;
		}
		
		if(lastCenter != null) {
			var distance = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(lastCenter.lat(), lastCenter.lng()), new google.maps.LatLng(map.getCenter().lat(), map.getCenter().lng()));
			
			if(distance <= 500) {
				return;
			}
		}
		
		getNearStationList(map.getCenter());
		
		lastCenter = map.getCenter();
	});
	
	initializeMap();
}

$(function() {
	$("body").on("click", function() {
		if(isHidePopupLayer) {
			hidePopupLayer();
		}
		
		if(isHideSecondPopupLayer) {
			hideSecondPopupLayer();
		}
		
		isHidePopupLayer = true;
		isHideSecondPopupLayer = true;
	});
	
	$("#modal_popup_close").on("click", function() {
		hidePopupLayer();
		hideSecondPopupLayer();
	});
	
	$("#second_modal_popup_close").on("click", function() {
		hideSecondPopupLayer();
	});
	
	$('#btnStationList').on("click", function() {
		isHidePopupLayer = false;
		
		adjustPopupPosition($(this));
		
		showStationList();
	});
	
	$('#btnShuttleLineColor').on("click", function() {
		isHidePopupLayer = false;
		
		adjustPopupPosition($(this));
		
		showShuttleLineColor();
	});
	
	$("#btnShowOutback").on("click", function() {
		isShowOutback = !isShowOutback;
		
		changeBtnOutbackShowText();
		
		removeBackgroundPolygon();
		
		if(isShowOutback) {
			showOutbackPolygon();
		}
	});
	
	$('#station_list').stickySectionHeaders({
		stickyClass     : 'sticky',
		headlineSelector: 'strong'
	});
	
	$("#popup_layer").on("click", function() {
		isHidePopupLayer = false;
		isHideSecondPopupLayer = false;
	});
	
	$("#second_popup_layer").on("click", function() {
		isHideSecondPopupLayer = false;
		isHidePopupLayer = false;
	});
		
	changeBtnOutbackShowText();
});

function showOutbackPolygon() {
	removeBackgroundPolygon();
	
	var backgroundCoordinates = [
		{ lat : LIMIT_BOUNDS_SW.lat, lng : LIMIT_BOUNDS_NE.lng },
		LIMIT_BOUNDS_NE,
		{ lat : LIMIT_BOUNDS_NE.lat, lng : LIMIT_BOUNDS_SW.lng },
		LIMIT_BOUNDS_SW
	];
		
	var stationMarkerCircleList = [];
	
	var unionPoly = null;
	if(running_station_list) {
		unionPoly = getUnionStationPolygon(running_station_list);
	}
	
	if(unionPoly != null) {
		if(Array.isArray(unionPoly)) {
			$.each(unionPoly, function(_union_poly_key, _union_poly) {
				var paths = _union_poly.getPaths();
				
				for(var i = 0; i < paths.getLength(); i++) {
					var unionPaths = [];
					var path = paths.getAt(i);

					for(var j = 0; j < path.getLength(); j++) {
						unionPaths.push(path.getAt(j));
					}
					
					stationMarkerCircleList.push(unionPaths);
				}
			});
		} else {
			var paths = unionPoly.getPaths();
			
			for(var i = 0; i < paths.getLength(); i++) {
				var unionPaths = [];
				var path = paths.getAt(i);
				
				for(var j = 0; j < path.getLength(); j++) {
					unionPaths.push(path.getAt(j));
				}
				
				stationMarkerCircleList.push(unionPaths);
			}
			
		}
	}
	
	stationMarkerCircleList.unshift(backgroundCoordinates);
	
	outbackPolygon = new google.maps.Polygon({
		paths: stationMarkerCircleList,
		strokeWeight: 0,
		fillColor: '#FF0000',
		fillOpacity: 0.35,
		map: map
	});
}

function getUnionStationPolygon(_station_list) {
	var geometries = [];
	$.each(_station_list, function(_station_type, _list_data) {
		if(_list_data.length > 0) {
			$.each(_list_data, function(k, v) {
				if(v.is_valid) {
					geometries.push(getGeometry(v.location));
				}
			});
		}
	});
	
	return cascadeUnion(geometries);
}

function getGeometry(_location) {
	var path = getCirclePoints(_location, 1000, 1);
	var wtk = getPointsToWTK(path);
	
	var wktReader = new jsts.io.WKTReader();
	
    return wktReader.read(wtk);
}

function cascadeUnion(_geometries) {
	var wicket = new Wkt.Wkt();
	var dissolvedGeometry = jsts.operation.union.CascadedPolygonUnion.union(_geometries);
	
	var wktWriter = new jsts.io.WKTWriter();
	var wkt = wktWriter.write(dissolvedGeometry);
	wicket.read(wkt);
	
	var polyOptions = {
		strokeWeight: 0,
		fillColor: '#FF0000',
		fillOpacity: 0.35
	};
	
	return wicket.toObject(polyOptions);
}

function getPointsToWTK(_point) {
	var poly = this;
	var wkt = "POLYGON((";
	
	for(var i = 0; i < _point.length; i++) {
		wkt += _point[i].lng().toString() + " " + _point[i].lat().toString() + ",";
	}
	
	wkt += _point[0].lng().toString() + " " + _point[0].lat().toString();
	
	wkt += "))";
	
	return wkt;
};

function changeBtnOutbackShowText() {	
	if(isShowOutback) {
		$("#btnShowOutback").css({
			"background-color" : "#f0ad4e",
			"color" : "#fff",
			"font-weight" : "bold"
		});
		
		$("#btnShowOutback").text("오지 ON");
	} else {
		$("#btnShowOutback").css({
			"background-color" : "#fff",
			"color" : "#f0ad4e",
			"font-weight" : "bold"
		});
		
		$("#btnShowOutback").text("오지 OFF");
	}
}

function showRouteListByStation(_station_data) {
	initSecondPopupLayer();
	
	var contentHtml = "";
	
	contentHtml += "<div class='row'>";
	contentHtml += "<div id='bus_route_list' class='station_list'></div>";
	contentHtml += "</div>";
		
	$("#second_modal_popup_label").html(_station_data.station_name + " [" + _station_data.ars_id+ "]");
	$("#second_modal_popup_content").html(contentHtml);
	$("#second_modal_popup_footer").hide();
		
	$('#second_popup_layer').show();
	
	if(_station_data.station_type == "bus") {
		getBusRouteListByStation(_station_data.station_id);
	} else if(_station_data.station_type == "shuttle") {
		getShuttleRouteListByStation(_station_data.station_id);
	}
	
	adjustListViewSize($('#second_popup_layer'), $("#bus_route_list > ul"), 85);
}

function getBusRouteListByStationHtml(_route_data) {
	_route_data.sort(function(a, b) {
		return a.busRouteTypeName - b.busRouteTypeName;
	});
	
	var route_list = _.groupBy(_route_data, function(route_data){ 
		return route_data.busRouteTypeName;
	});
	
	var contentHtml = "";
	contentHtml += "<ul>";
	
	$.each(route_list, function(key, data) {
		contentHtml += "<li>";
		contentHtml += "<strong>" + key + "</strong>";
		contentHtml += "<ul>";
		
		$.each(data, function(key2, data2) {
			contentHtml += "<li data='' class='bus_route_info'>";
			contentHtml += "<div class='bus_route_name_box'>";
			contentHtml += "<div class='bus_route_name'>" + data2.busRouteName + "</div>";
			contentHtml += "<div class='bus_route_next_info_box'>";
			contentHtml += "<div class='pull-left'>" + (data2.direction.length > 0 ? data2.direction + " 방면" : "") + "</div>";
			contentHtml += "<div class='pull-right'></div>";
			contentHtml += "</div>";
			contentHtml += "<div class='bus_route_next_info_box'>";
			contentHtml += "<div class='pull-left'>다음 : " + data2.nextStation + "</div>";
			contentHtml += "<div class='pull-right'></div>";
			contentHtml += "</div>";
			contentHtml += "</div>";
			contentHtml += "<div class='bus_route_more'>";
			contentHtml += "<img src='/assets/images/list_more.png'/>";
			contentHtml += "</div>";
			contentHtml += "<div class='clear_both'></div>";
			contentHtml += "</li>";
		});
		
		contentHtml += "</ul>";
		contentHtml += "</li>";
	});
	
	contentHtml += "</ul>";
	
	$("#bus_route_list").html(contentHtml);
	
	adjustListViewSize($('#second_popup_layer'), $("#bus_route_list > ul"), 85);
	
	$('#bus_route_list').stickySectionHeaders({
		stickyClass     : 'sticky',
		headlineSelector: 'strong'
	});
	
	$('#second_popup_layer').css({"min-width" : 550});
}

function getShuttleRouteListByStationHtml(_route_data) {
	var contentHtml = "";
	contentHtml += "<ul>";
	contentHtml += "<li>";
	contentHtml += "<strong>대리셔틀</strong>";
	contentHtml += "<ul>";
	
	$.each(_route_data, function(key, data) {
		contentHtml += "<li data='' class='bus_route_info'>";
		contentHtml += "<div class='bus_route_name_box'>";
		contentHtml += "<div class='bus_route_name'>" + data.name + "</div>";
		contentHtml += "<div class='shuttle_route_desc_box'>" + data.desc + "</div>";
		contentHtml += "</div>";
		contentHtml += "<div class='bus_route_more'>";
		contentHtml += "<img src='/assets/images/list_more.png'/>";
		contentHtml += "</div>";
		contentHtml += "<div class='clear_both'></div>";
		contentHtml += "</li>";
	});
	
	contentHtml += "</ul>";
	contentHtml += "</li>";
	contentHtml += "</ul>";
	
	$("#bus_route_list").html(contentHtml);
	
	adjustListViewSize($('#second_popup_layer'), $("#bus_route_list > ul"), 85);
	
	$('#bus_route_list').stickySectionHeaders({
		stickyClass     : 'sticky',
		headlineSelector: 'strong'
	});
	
	$('#second_popup_layer').css({"min-width" : 550});
}

function getBusRouteListByStation(_station_id) {
	var params = {};
	params.type = "get_bus_route_list_by_station";
	params.station_id = _station_id;
	
	$.post("/ajax/ajax.php", params, function(_ret_data) {
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			getBusRouteListByStationHtml(ret_data.data);
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function getShuttleRouteListByStation(_station_id) {
	var params = {};
	params.type = "get_shuttle_route_list_by_station";
	params.station_id = _station_id;
	
	$.post("/ajax/ajax.php", params, function(_ret_data) {
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			getShuttleRouteListByStationHtml(ret_data.data);
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function showShuttleLineColor() {
	initPopupLayer();
	
	var contentHtml = "";
	
	contentHtml += "<div class='row'>";
	contentHtml += "<div id='shuttle_line_color_list'>";
	contentHtml += "</div>";
	contentHtml += "</div>";
	
	$("#modal_popup_label").text("셔틀 노선색");
	$("#modal_popup_content").html(contentHtml);
	$("#modal_popup_footer").hide();
	
	getShuttleLineColorListHtml();
}

function getShuttleLineColorListHtml() {
	var contentHtml = "";
	contentHtml += "<div class='list-group'>";
	
	$.each(shuttle_line_color_list, function(k, v) {
		contentHtml += "<a href='#' class='list-group-item'>";
		contentHtml += "<span class='shuttle_color' style='background-color: " + shuttleLineColorList[v.busId].lineColor + "'></span>";
		contentHtml += "<span class='shuttle_name'>" + v.name + "</span>";
		contentHtml += "</a>";
	});
		
	contentHtml += "</div>";
	
	$("#shuttle_line_color_list").html(contentHtml);
	
	$('#popup_layer').show();
	
	adjustListViewSize($('#popup_layer'), $("#shuttle_line_color_list"), 30);
	
	$('#popup_layer').css({"min-width" : 250});
}

function showStationList() {
	initPopupLayer();
	
	var contentHtml = "";
	
	contentHtml += "<div class='row'>";
	contentHtml += "<form class='form-horizontal'>";
	contentHtml += "<div class='form-group form-group-md'>";
	contentHtml += "<label class='col-sm-2 control-label' for='select_distance'>검색범위</label>";
	contentHtml += "<div class='col-sm-5'>";
	contentHtml += "<select id='select_distance' class='form-control'></select>";
	contentHtml += "</div>";
	contentHtml += "</div>";
	contentHtml += "</form>";
	contentHtml += "<div id='station_list' class='station_list'></div>";
	contentHtml += "</div>";
	
	$("#modal_popup_label").text("가까운 정류장");
	$("#modal_popup_content").html(contentHtml);
	$("#modal_popup_footer").hide();
	
	setDistanceSelect();
	
	getStationListHtml();
	
	$("#select_distance").off("change").on("change", function() {
		selected_distance_index = $(this).val();
		getStationListHtml();
	});
	
	$('#popup_layer').show();
	
	adjustListViewSize($('#popup_layer'), $("#station_list > ul"), 85);
}

function setDistanceSelect() {
	var html = "";
	$.each(distance_select, function(k, v) {
		html += "<option value='" + k + "' " + (k == selected_distance_index ? "selected" : "") + ">" + (v / 1000) + "km</option>";		
	});
	
	$("#select_distance").html(html);
}

function adjustPopupPosition(_btn_clicked) {
	var offset = _btn_clicked.offset();
	var width = _btn_clicked.outerWidth(true);
	var height = _btn_clicked.outerHeight(true);
	
	var top = (offset.top + (height / 2)) - 30;
	var left = offset.left + width + 20;
		
	$('#popup_layer').css({top: top, left: left});
}

function adjustSecondPopupPosition(_btn_clicked) {
	var offset = _btn_clicked.offset();
	var width = _btn_clicked.outerWidth(true);
	var height = _btn_clicked.outerHeight(true);
	
	var top = offset.top;
	var left = offset.left + width + 20;
	
	$('#second_popup_layer').css({top: top, left: left});
}

function adjustListViewSize(_popup_layer, _list_view, _paddings) {
	var body_height = $("body").height();
	var header_height = $('.modal-header').outerHeight(true);
	var content_padding = parseInt($("#modal_popup_content").css("padding").replace("px", ""));
	var offset = _popup_layer.offset();
	
	var height = body_height - offset.top - header_height - (content_padding * 2) - _paddings;
	
	_list_view.css({"max-height": height});
}

function hidePopupLayer() {
	$('#popup_layer').hide();
	initPopupLayer();
}

function initPopupLayer() {
	$("#modal_popup_label").text("");
	$("#modal_popup_content").html("");
	$("#modal_popup_footer").html("");
}

function hideSecondPopupLayer() {
	$('#second_popup_layer').hide();
	initSecondPopupLayer();
}

function initSecondPopupLayer() {
	$("#second_modal_popup_label").text("");
	$("#second_modal_popup_content").html("");
	$("#second_modal_popup_footer").html("");
}

function getIndexByStationType(_station_type) {
	switch(_station_type) {
		case "shuttle" : 
			return 0;
		case "subway" :
			return 1;
		case "carpool" : 
			return 2;
		case "bus" :
			return 3;
		default :
			return 4;
	}
}

function getHeaderTitleByStationType(_station_type) {
	switch(_station_type) {
		case "shuttle" : 
			return "대리셔틀";
		case "subway" :
			return "지하철역";
		case "carpool" : 
			return "택시카풀 등록자";
		case "bus" :
			return "시내버스 / 마을버스 / 심야 정거장";
		default :
			return "기타";
	}
}

function getStationListHtml() {
	var showing_shuttle_station_list = getStationListInSelectedDistance();
	
	var contentHtml = "";
	contentHtml += "<ul>";
	
	if(showing_shuttle_station_list) {
		$.each(showing_shuttle_station_list, function(_station_type, _list_data) {
			if(_list_data.length > 0) {
				contentHtml += "<li>";
				contentHtml += "<strong>" + getHeaderTitleByStationType(_station_type) + "</strong>";
				contentHtml += "<ul>";
				
				$.each(_list_data, function(k, v) {
					var distance;
					if(v.distance < 1000) {
						distance = v.distance.toFixed(2) + "m";
					} else {
						distance = ((v.distance / 1000).toFixed(2));
						distance += "km";
					}
					
					var data = _station_type + "_" + v.ars_id + "_" + v.station_name + "_" + v.station_id;
					
					contentHtml += "<li data='" + data + "' class='station_info'>";
					contentHtml += "<div class='station_name_box'>";
					contentHtml += "<div class='station_name'>" + v.station_name + "</div>";
					contentHtml += "<div class='station_arsid_box'><span style='margin-right: 10px;'>[" + v.ars_id + "]</span> | <span class='station_distance'>" + distance + "</span></div>";
					contentHtml += "</div>";
					contentHtml += "<div class='station_info_more'>";
					contentHtml += "<img src='/assets/images/list_more.png'/>";
					contentHtml += "</div>";
					contentHtml += "<div class='clear_both'></div>";
					contentHtml += "</li>";
				});
				
				contentHtml += "</ul>";
				contentHtml += "</li>";
			}
		});
	}
	
	contentHtml += "</ul>";
	
	$("#station_list").html(contentHtml);
	
	adjustListViewSize($('#popup_layer'), $("#station_list > ul"), 85);
	
	$('#station_list').stickySectionHeaders({
		stickyClass     : 'sticky',
		headlineSelector: 'strong'
	});
	
	$('#popup_layer').css({"min-width" : 550});
	
	$(".station_info").off("click").on("click", function() {
		var data = $(this).attr("data").split("_");		
		var station_data = {
			station_id : data[3],
			ars_id : data[1],
			station_name : data[2],
			station_type : data[0]
		};
		
		adjustSecondPopupPosition($('#popup_layer'));
		
		if(station_data.station_type == "bus" || station_data.station_type == "shuttle") {
			showRouteListByStation(station_data);
		} else if(station_data.station_type == "subway") {
			window.open("http://map.daum.net/subway/subwayStationInfo?type=4&stationId=SES" + station_data.station_id, "_blank", "width=680,height=690");
		}
	});
}

function getStationListInSelectedDistance() {
	var showing_shuttle_station_list = null;
	
	if(running_station_list) {
		showing_shuttle_station_list = {};
		$.each(running_station_list, function(_station_type, _list_data) {
			showing_shuttle_station_list[_station_type] = [];
			
			if(_list_data.length > 0) {
				$.each(_list_data, function(k, v) {
					if(v.distance <= distance_select[selected_distance_index]) {
						showing_shuttle_station_list[_station_type].push(v);
					}
				});
			}
			
			showing_shuttle_station_list[_station_type].sort(function(a, b) {
				return a.distance - b.distance;
			});
		});
	}
		
	return showing_shuttle_station_list;
}

function getNearStationList(_center_position) {
	var params = {};
	params.type = "get_near_station";
	params.lat = _center_position.lat();
	params.lng = _center_position.lng();
	params.range = 5000;
		
	$.post("/ajax/ajax.php", params, function(_ret_data) {
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			var shuttleRunningBusIdList = getRunningStations(ret_data.data.shuttleInfoList);
			getShuttleLineColorList(ret_data.data.shuttleInfoList, shuttleRunningBusIdList);
			showStationMarker(ret_data.data, shuttleRunningBusIdList);
			drawShuttleLine(ret_data.data.shuttleLineList, shuttleRunningBusIdList);
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function getShuttleLineColorList(_data, _shuttleRunningBusIdList) {
	shuttle_line_color_list = [];
	
	$.each(_data, function(key, val) {
		if(_shuttleRunningBusIdList.indexOf(val.busId) >= 0) {
			shuttle_line_color_list.push(val);
		}
	});
}

function getRunningStations(_shuttleInfo) {
	var shuttleRunningBusId = [];
	$.each(_shuttleInfo, function(key, val) {
		var isRunning = checkIsShuttleLineRunning(val);
		if(isRunning) {
			shuttleRunningBusId.push(val.busId);
		}
	});
	
	return shuttleRunningBusId;
}

function checkIsShuttleLineRunning(_shuttleInfo) {
	var isValidTime = false;
	var dayOfWeek = new Date().getDay();
	
	var firstTime = _shuttleInfo.runFirstTime;
	var lastTime = _shuttleInfo.runLastTime;
	var isHoliday = checkHoliday(_shuttleInfo.holidayDate);
	if(isHoliday) {
		dayOfWeek == 0;
	}

	if(dayOfWeek == 0) {
		firstTime = _shuttleInfo.sundayRunFirstTime;
		lastTime = _shuttleInfo.sundayRunLastTime;
	} else if(dayOfWeek == 6) {
		firstTime = _shuttleInfo.saturdayRunFirstTime;
		lastTime = _shuttleInfo.saturdayRunLastTime;
	}
	
	if(!firstTime || firstTime.length <= 0 || !lastTime || lastTime.length <= 0) {
		return false;
	}
	
	return checkIsRunningStation(firstTime, lastTime);
}

function checkIsShuttleStationRunning(_shuttleInfo) {
	var isValidTime = false;
	var dayOfWeek = new Date().getDay();
	
	var firstTime = _shuttleInfo.firstTime;
	var lastTime = _shuttleInfo.lastTime;
	var isHoliday = checkHoliday(_shuttleInfo.holidayDate);
	if(isHoliday) {
		dayOfWeek == 0;
	}

	if(dayOfWeek == 0) {
		firstTime = _shuttleInfo.sundayFirstTime;
		lastTime = _shuttleInfo.sundayLastTime;
	} else if(dayOfWeek == 6) {
		firstTime = _shuttleInfo.saturdayFirstTime;
		lastTime = _shuttleInfo.saturdayLastTime;
	}
	
	if(!firstTime || firstTime.length <= 0 || !lastTime || lastTime.length <= 0) {
		return false;
	}
	
	return checkIsRunningStation(firstTime, lastTime);
}

function checkHoliday(_holidayDate) {
	if(!_holidayDate || _holidayDate.length <= 0) {
		return false;
	}
	
	var now = new Date();
	var nowDate = now.getFullYear() + "-";
	nowDate += ((now.getMonth + 1) < 10 ? '0' + (now.getMonth + 1) : now.getMonth + 1) + "-";
	nowDate += now.getDate() < 10 ? '0' + now.getDate() : now.getDate();
	
	var holidayList = _holidayDate.split(",");
	if(holidayList.indexOf(nowDate) == -1) {
		return false;
	}
	
	return true;
}

function removeBackgroundPolygon() {
	if(outbackPolygon) {
		outbackPolygon.setMap(null);
		outbackPolygon = null;
	}
}

function removeAllMarkers() {
	$.each(stationMarkerList, function(k, v) {
		v.setMap(null);
	});
	
	stationMarkerList = [];
	running_station_list = {};
}

function removeAllShuttlLines() {
	$.each(shuttleLineList, function(k, v) {
		v.setMap(null);
	});
	
	shuttleLineList = [];
}

function removeAllObjectOnMap() {
	removeBackgroundPolygon();
	removeAllMarkers();
	removeAllShuttlLines();
	
	isShowOutback = false;
	changeBtnOutbackShowText();
	
	lastCenter = null;
	
	$('#btnStationList').attr("disabled", true);
	$('#btnShuttleLineColor').attr("disabled", true);
}

var polygons = null;
function showStationMarker(_data, _shuttleRunningBusIdList) {
	removeAllMarkers();
	
	removeAllShuttlLines();
	
	var isValid;
	var stationData;
	var dayOfWeek = new Date().getDay();
	
	running_station_list = {};
	running_station_list.shuttle = [];
	$.each(_data.shuttleMarkerList, function(k, v) {
		if(_shuttleRunningBusIdList.indexOf(v.busId) >= 0) {
			isValid = checkIsShuttleStationRunning(v);
			stationData = {
				station_id : v.arsId,
				ars_id : v.arsId,
				station_name : v.text,
				location: v.location,
				station_type : "shuttle",
				distance : v.distance,
				is_valid : isValid
			};
			
			running_station_list.shuttle.push(stationData);
			stationMarkerList.push(addStationMarker(stationData));
		}
	});
	
	running_station_list.carpool = [];
	$.each(_data.carpoolRegistrantList, function(k, v) {
		stationData = {
			station_id : v.taxiId,
			ars_id : v.taxiId,
			station_name : "등록자 " + v.registrant + " 기사님",
			location: v.location,
			station_type : "carpool",
			distance : v.distance,
			is_valid : true
		};
		
		running_station_list.carpool.push(stationData);
		stationMarkerList.push(addStationMarker(stationData));
	});
	
	running_station_list.subway = [];
	$.each(_data.subwayStationList, function(k, v) {
		isValid = checkIsRunningStation(v.firstTime, v.lastTime);
		stationData = {
			station_id : (v.stationCodeDaum != null && v.stationCodeDaum.length > 0) ? v.stationCodeDaum : v.stationCode,
			ars_id : (v.stationCodeDaum != null && v.stationCodeDaum.length > 0) ? v.stationCodeDaum : v.stationCode,
			station_name : v.stationName,
			location: v.location,
			station_type : "subway",
			distance : v.distance,
			is_valid : isValid
		};
		
		running_station_list.subway.push(stationData);
		stationMarkerList.push(addStationMarker(stationData));
	});
	
	running_station_list.bus = [];
	$.each(_data.busStationList, function(k, v) {
		isValid = checkIsRunningStation(v.startTime, v.endTime);
		stationData = {
			station_id : v.stationId,
			ars_id : v.arsId,
			station_name : v.stationName,
			location: v.location,
			station_type : "bus",
			distance : v.distance,
			is_valid : isValid
		};
		
		running_station_list.bus.push(stationData);
		stationMarkerList.push(addStationMarker(stationData));
	});
	
	$.each(_data.driverList, function(k, v) {
		stationData = {
			station_id : v.driverId,
			ars_id : v.driverId,
			station_name : v.phone,
			location: v.location,
			station_type : "driver",
			distance : 0,
			is_valid : true
		};
		
		stationMarkerList.push(addStationMarker(stationData));
	});	
	
	if(stationMarkerList.length <= 0) {
		$('#btnStationList').attr("disabled", true);
	} else {
		$('#btnStationList').attr("disabled", false);
	}
	
//	console.log("station count : " + stationMarkerList.length);
	
	if(isShowOutback) {
		showOutbackPolygon();
	}
}

function addStationMarker(_stationData) {
	var zoom = map.getZoom();
	
	var marker = new google.maps.Marker({
		position: _stationData.location,
		map: map,
		icon: getMarkerIcon(_stationData.station_type, zoom, _stationData.is_valid),
		station_data : _stationData
	});
	
	marker.addListener('click', function() {
		setInfowindow(marker);
	});
	
	return marker;
}

function getInfoWindowContents(_stationData) {
	var info_window_content = "<div id='info_window_content' style='cursor: pointer;'>";
	
	if(_stationData.station_type == "shuttle") {
		info_window_content += "[대리셔틀]";
	} else if(_stationData.station_type == "carpool") {
		info_window_content += "[택시카풀]";
	} else if(_stationData.station_type == "bus") {
		info_window_content += "[버스]";
	} else if(_stationData.station_type == "subway") {
		info_window_content += "[지하철]";
	}
	
	info_window_content += _stationData.station_name + "</div>";
	
	return info_window_content;
}

function setInfowindow(_marker) {
	var station_data = _marker.station_data;
	station_info_window.setContent(getInfoWindowContents(station_data));
	station_info_window.open(map, _marker);
	
	var info_root = $("#info_window_content").parent("div").parent("div").parent("div").parent("div");
	info_root.css({cursor: "pointer"});
	info_root.off("click").on("click", function() {
		isHideSecondPopupLayer = false;
		
		if(station_data.station_type == "bus" || station_data.station_type == "shuttle") {
			adjustSecondPopupPosition($('#btnStationList'));
			showRouteListByStation(station_data);
		} else if(station_data.station_type == "subway") {
			window.open("http://map.daum.net/subway/subwayStationInfo?type=4&stationId=SES" + station_data.station_id, "_blank", "width=680,height=690");
		}
	});
}

function drawShuttleLine(_data, _shuttleRunningBusIdList) {
	var list_count = 0;
	
	$.each(_data, function(k, v) {
		if(_shuttleRunningBusIdList.indexOf(v._id.busId) >= 0) {
			v.shuttle_point.sort(function(a, b) {
				return a.index - b.index;
			});
			
			var isDrawLine = false;
			var prevPointIndex = null;
			var shuttleLinePoints = [];
			
			var shuttleLineColor = shuttleLineColorList[v._id.busId].lineColor;
			$.each(v.shuttle_point, function(k, shuttle_point_info) {
				if(prevPointIndex != null && (prevPointIndex + 1) != shuttle_point_info.index) {
					drawShuttlePolyLine(shuttleLineColor, shuttleLinePoints);
					shuttleLinePoints = [];
				} else {
					shuttleLinePoints.push({lat : shuttle_point_info.locations.coordinates[1], lng : shuttle_point_info.locations.coordinates[0]});
				}
			});
			
			drawShuttlePolyLine(shuttleLineColor, shuttleLinePoints);
			list_count++;
		}
	});
	
	if(list_count == 0) {
		$('#btnShuttleLineColor').attr("disabled", true);
	} else {
		$('#btnShuttleLineColor').attr("disabled", false);
	}
}

function drawShuttlePolyLine(_color, _points) {
	var shuttlePath = new google.maps.Polyline({
		path: _points,
		geodesic: true,
		strokeColor: _color,
		strokeOpacity: 1.0,
		strokeWeight: 5,
		icons: [{
			icon: {
				path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
			},
			strokeColor: _color,
            fillColor: _color,
            fillOpacity: 1,
			repeat: '200px',
			offset: '100%'
		}],
		map: map
	});
	
	shuttleLineList.push(shuttlePath);
}

function getCirclePoints(_point, _radius, _direction) {
	var d2r = Math.PI / 180;   // degrees to radians
	var r2d = 180 / Math.PI;   // radians to degrees
	var earthsradius = 3963; // 3963 is the radius of the earth in miles
	var points = 50;
	var start = 0;
	var end = 0;
	
	var radius = (_radius / 1000) / 1.61;
	
	var rlat = (radius / earthsradius) * r2d;
	var rlng = rlat / Math.cos(_point.lat * d2r);
	
	var retCirclePoints = [];
	
	if (_direction == 1) {
		start = 0;
		end = points + 1;
	} else {
		start = points + 1;
		end = 0;
	}
	
	for (var i = start; (_direction == 1 ? i < end : i > end); i = i + _direction) {
		var theta = Math.PI * (i / (points / 2));
		ey = _point.lng + (rlng * Math.cos(theta));
		ex = _point.lat + (rlat * Math.sin(theta));
		
		retCirclePoints.push(new google.maps.LatLng(ex, ey));
	}
	
	return retCirclePoints;
}

function checkIsRunningStation(_firstTime, _lastTime) {
	if(!_firstTime || !_lastTime) {
		return true;
	}

	var isValidTime = false;

	var hours = new Date().getHours();
	var minutes = new Date().getMinutes();

	var firstTimeSplit = _firstTime.split(":");
	var firstTimeHours = parseInt(firstTimeSplit[0]);
	var firstTimeMinutes = parseInt(firstTimeSplit[1]);

	var lastTimeSplit = _lastTime.split(":");
	var lastTimeHours = parseInt(lastTimeSplit[0]);
	var lastTimeMinutes = parseInt(lastTimeSplit[1]);

	if((firstTimeHours < hours || (firstTimeHours == hours && firstTimeMinutes <= minutes))
			&& (lastTimeHours > hours || (lastTimeHours == hours && lastTimeMinutes >= minutes))) {
		isValidTime = true;
	}

	if(!isValidTime) {
		hours = hours + 24;

		if((firstTimeHours < hours || (firstTimeHours == hours && firstTimeMinutes <= minutes))
				&& (lastTimeHours > hours || (lastTimeHours == hours && lastTimeMinutes >= minutes))) {
			isValidTime = true;
		}
	}

	return isValidTime;
}

function getMarkerIcon(_type, _zoom, _isValid) {
	var icon_url = "/assets/images/";
	switch(_type) {
		case 'shuttle' : 
			if(_isValid) {
				icon_url += "pin_shuttle_15.png";
			} else {
				icon_url += "pin_shuttle_15_disabled.png";
			}
			break;
		case 'bus' : 
			if(_isValid) {
				icon_url += "pin_bus_15.png";
			} else {
				icon_url += "pin_bus_15_disabled.png";
			}
			break;
		case 'subway' : 
			if(_isValid) {
				icon_url += "pin_subway_15.png";
			} else {
				icon_url += "pin_subway_15_disabled.png";
			}
			break;
		case 'carpool' : 
			icon_url += "pin_taxi_registrant_15.png";
			break;
		case 'driver' : 
			icon_url += "pin_driver_40.png";
			break;
		default : 
			icon_url = "";
			break;
	}
	
	return icon_url;
}
