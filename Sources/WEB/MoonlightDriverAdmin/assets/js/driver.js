var list_table;
var rows_selected = [];
var selected_recv_user = [];

$(document).ready(function (){
	rows_selected = [];
	selected_recv_user = [];
	
	list_table = $('#driver_list').DataTable({
		columnDefs: [{
			targets: 0,
			searchable: false,
			orderable: false,
			width: '1%',
			className: 'dt-body-center',
			render: function (data, type, full, meta) {
				return '<input type="checkbox" value="' + full[3] + '">';
			}
		}],
		initComplete: function () {
			var html = "";
			html += "<div style='margin-top: 10px;'>";
			html += "	<button class='btn btn-primary btnShowPushPopup'>선택 푸시 보내기</button>";
			html += "	<button class='btn btn-danger btnShowAllPushPopup'>전체 푸시 보내기</button>";
			html += "</div>";
			
			$('#driver_list_length').append(html);
			
			html = "";
			html += "<div style='margin-top: 10px;'>";
			html += "	<label>앱 버전별 사용자 선택 : ";
			html += "		<select class='form-control input-sm' id='app_version_filter'>";
			html += "			<option value='all'>전체</option>";
			
			$.each(version_list, function(key, val) {
				html += "			<option value='" + val.version + "'>" + val.version + "</option>";
			});
			
			html += "			<option value='old'>구버전 사용자</option>";
			html += "		</select>";
			html += "	</label>";
			html += "</div>";
			
			$('#driver_list_filter').append(html);

			$(".btnShowPushPopup").off("click").on("click", function() {
				showPushPopup("selected");
			});
			
			$(".btnShowAllPushPopup").off("click").on("click", function() {
				showPushPopup("all");
			});
			
			$("#app_version_filter").off("change").on("change", function() {
				var selected = $("#app_version_filter option:selected").val();
				if(selected == "all") {
					list_table.columns(6).search('').draw();
				} else if(selected == "old") {
					list_table.columns(6).search('old').draw();
				} else {
					list_table.columns(6).search(selected).draw();	
				}
			});
		},
		order: [[ 1, "desc" ]],
		rowCallback: function(row, data, dataIndex){
			// Get row ID
			var rowId = data[0];
			
			// If row ID is in the list of selected row IDs
			if($.inArray(rowId, rows_selected) !== -1){
				$(row).find('input[type="checkbox"]').prop('checked', true);
				$(row).addClass('selected');
			}
		},
		drawCallback: function() {
			
		}
	});

	$.each(driver_list, function(key, val) {
		var new_node = list_table.row.add([
			val.driver_id,
			val.createdDate,
			val.name,
			val.phone,
			val.star,
			val.point,
			val.version ? val.version : "old",
			val.ynLogin && val.ynLogin == "Y" ? "접속중" : "",
			val.lastLoginDate
		]).node();

		$(new_node).attr("id", val.driver_id);
	});

	list_table.draw();

	$('#driver_list tbody').on('click', 'input[type="checkbox"]', function(e){
		var $row = $(this).closest('tr');

		// Get row data
		var data = list_table.row($row).data();

		// Get row ID
		var rowId = data[0];

		// Determine whether row ID is in the list of selected row IDs
		var index = $.inArray(rowId, rows_selected);

		// If checkbox is checked and row ID is not in list of selected row IDs
		if(this.checked && index === -1) {
			rows_selected.push(rowId);

			// Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
		} else if (!this.checked && index !== -1) {
			rows_selected.splice(index, 1);
		}

		if(this.checked){
			$row.addClass('selected');
			selected_recv_user.push(data[3]);
		} else {
			$row.removeClass('selected');
			selected_recv_user = _.without(selected_recv_user, data[3]);
		}

		// Update state of "Select all" control
		updateDataTableSelectAllCtrl(list_table);

		// Prevent click event from propagating to parent
		e.stopPropagation();
	});

	// Handle click on table cells with checkboxes
	$('#driver_list').on('click', 'tbody td, thead th:first-child', function(e){
		$(this).parent().find('input[type="checkbox"]').trigger('click');
	});

	// Handle click on "Select all" control
	$('thead input[name="select_all"]', list_table.table().container()).on('click', function(e){
		if(this.checked){
			$('#driver_list tbody input[type="checkbox"]:not(:checked)').trigger('click');
		} else {
			$('#driver_list tbody input[type="checkbox"]:checked').trigger('click');
		}

		// Prevent click event from propagating to parent
		e.stopPropagation();
	});
	
	list_table.on('draw', function(){
		// Update state of "Select all" control
		updateDataTableSelectAllCtrl(list_table);
	});
});

function showPushPopup(_mode) {
	$("#modal_popup_label").text("푸시 보내기");
	
	$("#modal_popup_content").html(getPushContentHtml());
	
	var footerHtml = "";
	footerHtml += "<button type='button' id='btnSendPush' class='btn btn-primary'>보내기</button>";
	footerHtml += "<button type='button' class='btn btn-default' data-dismiss='modal'>취소</button>";
	
	$("#modal_popup_footer").html(footerHtml);
		
	$("#btnSendPush").off("click").on("click", function() {
		sendPush();
	});
	
	if(_mode == "selected") {
//		var recv_user = getRecvUserList(list_table);
		$("#recv_user").text(selected_recv_user.join());
		$("#selected_recv_user_container").show();
		$("#all_recv_user_container").hide();
	} else {
		$("#selected_recv_user_container").hide();
		$("#all_recv_user_container").show();
	}
	
	$("#recv_user_type").val(_mode);
	$("#push_message_length").text(MAX_LENGTH + "Bytes");
	
	$("#modal_popup").modal('show');
	
	$("#push_message").off("keyup").on("keyup", function() {
		var push_message = $.trim($(this).val());
		var stringByteLength = MAX_LENGTH - getStringBytes(push_message);
		
		$("#push_message_length").text(stringByteLength + "Bytes");
	});
	
	$('#sendPushForm').ajaxForm({
		url: AJAX_URL,
		dataType:  'json',
		type: "POST",
		beforeSubmit: function (data, form, option) {
			var push_title = $.trim($("#push_title").val());
			var push_message = $.trim($("#push_message").val());
			var recv_user = $.trim($("#recv_user").val());
			var recv_user_type = $.trim($("#recv_user_type").val());
			
			if(push_title.length <= 0) {
				alert("제목을 입력해주세요.");
				$("#push_title").focus();
				return false;
			}
			
			if(push_message.length <= 0) {
				alert("내용을 입력해주세요.");
				$("#push_message").focus();
				return false;
			} else {
				var stringByteLength = getStringBytes(push_message);
				if(stringByteLength > MAX_LENGTH) {
					alert("메시지 최대 길이를 초과했습니다.");
					$("#push_message").focus();
					return false;
				}
			}
			
			if(recv_user_type == "selected" && recv_user.length <= 0) {
				alert("받는사람을 선택해 주세요.");
				$("#recv_user").focus();
				return false;
			}
			
			
            return true;
        },
        success: function(ret_data){
            if(ret_data.result == "OK") {
    			alert("총 " + ret_data.data + "명에게 전송 되었습니다.");
    			$("#modal_popup").modal('hide');
    		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
    			alert(ret_data.msg);
    			location.href = "/";
    		} else {
    			alert(ret_data.msg);
    		}
        },
        error: function(){
            //에러발생을 위한 code페이지
        }                               
    });
}

function getStringBytes(_message, b, i, c) {
	for(b = i = 0; c = _message.charCodeAt(i++); b += c >> 11 ? 3 : c >> 7 ? 2 : 1);
    return b;
}

function getPushContentHtml() {
	var html = "";
	
	html += "<div class='row'>";
	html += "	<form class='form-horizontal' id='sendPushForm'>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='push_title'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>제목</label>";
	html += "			<div class='col-sm-8'><input type='text' class='form-control' id='push_title' name='push_title'/></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='push_message'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>내용</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<textarea class='form-control' id='push_message' name='push_message' rows='10'/>";
	html += "				<p class='help-block' id='push_message_length'></p>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='recv_user'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>받는 사람</label>";
	html += "			<div class='col-sm-8' id='selected_recv_user_container'>";
	html += "				<textarea class='form-control' id='recv_user' name='recv_user' rows='10'/>";
	html += "				<p class='help-block'>콤마(,)로 구분된 전화번호.</p>";
	html += "				<p class='help-block'>e.g. 01012345678,+821012345678,01012345678</p>";
	html += "			</div>";
	html += "			<div class='col-sm-8' id='all_recv_user_container'>";
	html += "				<textarea class='form-control' rows='1' readOnly>전체 사용자</textarea>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='push_img'>이미지</label>";
	html += "			<div class='col-sm-8'><input type='file' name='push_img' id='push_img'/></div>";
	html += "		</div>";
	html += "		<input type='hidden' name='type' value='send_push'>";
	html += "		<input type='hidden' id='recv_user_type' name='recv_user_type'>";
	html += "	</form>";
	html += "</div>";
	
	return html;
}

function sendPush() {
	$('#sendPushForm').submit();
}

function getRecvUserList(table) {
	var recv_user = [];
	
	table.rows().every(function (_rowIdx, _tableLoop, _rowLoop) {
		var data = this.data();
		var rowId = data[0];
				
		// If row ID is in the list of selected row IDs
		if($.inArray(rowId, rows_selected) !== -1) {
			recv_user.push(data[3]);
		}
	});
	
	return recv_user;
}

function updateDataTableSelectAllCtrl(table){
	var $table             = table.table().node();
	var $chkbox_all        = $('tbody input[type="checkbox"]', $table);
	var $chkbox_checked    = $('tbody input[type="checkbox"]:checked', $table);
	var chkbox_select_all  = $('thead input[name="select_all"]', $table).get(0);
	
	// If none of the checkboxes are checked
	if($chkbox_checked.length === 0) {
		chkbox_select_all.checked = false;
		if('indeterminate' in chkbox_select_all){
			chkbox_select_all.indeterminate = false;
		}
		
		// If all of the checkboxes are checked
	} else if ($chkbox_checked.length === $chkbox_all.length) {
		chkbox_select_all.checked = true;
		if('indeterminate' in chkbox_select_all) {
			chkbox_select_all.indeterminate = false;
		}
		
		// If some of the checkboxes are checked
	} else {
		chkbox_select_all.checked = true;
		if('indeterminate' in chkbox_select_all) {
			chkbox_select_all.indeterminate = true;
		}
	}
}
