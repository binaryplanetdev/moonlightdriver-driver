function showAccountDetailPopup(_data) {
	var mode = _data ? "edit" : "create";
	
	if(mode == "edit") {
		$("#modal_popup_label").text("관리자 계정 상세보기");
	} else {
		$("#modal_popup_label").text("관리자 등록");
	}
	
	$("#modal_popup_content").html(getAccountContentsHtml(mode));
	
	var footerHtml = "";
	
	initRoleLevelData();
	
	if(mode == "edit") {
		$("#id").val(_data.id);
		$("#name").val(_data.name);
		$("#phone").val(_data.phone);
		$("#email").val(_data.email);
		$("#role_level").val(_data.level);
		
		if(_data.ynPush == "Y") {
			$("input:checkbox[id='ynPush']").attr("checked", true);
		} else {
			$("input:checkbox[id='ynPush']").attr("checked", false);
		}
		
		footerHtml += "<button type='button' id='btnEdit' class='btn btn-primary'>수정</button>";
	} else {
		$("input:checkbox[id='ynPush']").attr("checked", true);
		
		footerHtml += "<button type='button' id='btnAdd' class='btn btn-primary'>등록</button>";
	}
	
	footerHtml += "<button type='button' class='btn btn-default' data-dismiss='modal'>닫기</button>";
	$("#modal_popup_footer").html(footerHtml);
	
	$("#modal_popup").modal('show');
	
	if(mode == "edit") {
		$("#btnEdit").off("click").on("click", function() {
			editAccount(_data);
		});
	} else {
		$("#btnAdd").off("click").on("click", function() {
			addAccount();
		});
	}
}

function initRoleLevelData() {
	var html = "";
	
	html += "<option value='0'>권한 선택</option>";
	$.each(role_level, function(_key, _val) {
		html += "<option value='" + _key + "'>" + _val + "</option>";
	});
	
	$("#role_level").html(html);
}

function getAccountContentsHtml(_mode) {
	var html = "";
	
	html += "<div class='row'>";
	html += "	<form class='form-horizontal'>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-2 control-label' for='id'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>아이디</label>";
	html += "			<div class='col-sm-9'><input type='text' class='form-control' id='id'/></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-2 control-label' for='pw'>";

	if(_mode == "create") {
		html += "				<span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>";
	}
	
	html += "				비밀번호";
	html += "			</label>";
	html += "			<div class='col-sm-9'><input type='password' class='form-control' id='pw'/></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-2 control-label' for='name'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>이름</label>";
	html += "			<div class='col-sm-9'><input type='text' class='form-control' id='name'/></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-2 control-label' for='phone'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>휴대폰</label>";
	html += "			<div class='col-sm-9'><input type='text' class='form-control' id='phone'></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-2 control-label' for='email'>이메일</label>";
	html += "			<div class='col-sm-9'><input type='text' class='form-control' id='email'></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-2 control-label' for='role_level'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>권한</label>";
	html += "			<div class='col-sm-9'>";
	html += "				<select class='form-control' id='role_level'></select>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-2 control-label' for='ynPush'>푸시 알림</label>";
	html += "			<div class='col-sm-9'><div class='checkbox'><label><input type='checkbox' id='ynPush' name='ynPush' value='Y'></label></div></div>";
	html += "		</div>";
	html += "	</form>";
	html += "</div>";
	
	return html;
}

function editAccount(_account_info) {
	var params = {};
	params.type = "edit_account";
	params.account_id = _account_info.account_id;
	params.id = $.trim($("#id").val());
	params.pw = $.trim($("#pw").val());
	params.name = $.trim($("#name").val());
	params.phone = $.trim($("#phone").val());
	params.email = $.trim($("#email").val());
	params.level = $.trim($("#role_level").val());
	params.ynPush = $("input:checkbox[id='ynPush']").is(":checked") ? "Y" : "N";
	
	if(params.id.length <= 0) {
		alert("아이디를 입력해주세요.");
		return;
	}
	
	if(params.name.length <= 0) {
		alert("이름을 입력해주세요.");
		return;
	}
	
	if(params.phone.length <= 0) {
		alert("휴대폰 번호를 입력해주세요.");
		return;
	}
	
	if(params.level == 0 || params.level.length <= 0) {
		alert("권한을 입력해주세요.");
		return;
	}
	
	$.post(AJAX_URL, params, function(_ret_data) {
		$("#modal_popup").modal('hide');
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			var ynPushHtml = "<input type='checkbox' class='btnYnPush' " + (ret_data.data.ynPush == "Y" ? "checked" : "") + " />";
			var deleteHtml = "<button type='button' class='btn btn-primary btn-sm btn_delete_account'>삭제</button>";
			
			account_list[_account_info.account_id].id = ret_data.data.id;
			account_list[_account_info.account_id].name = ret_data.data.name;
			account_list[_account_info.account_id].phone = ret_data.data.phone;
			account_list[_account_info.account_id].email = ret_data.data.email;
			account_list[_account_info.account_id].role = ret_data.data.role;
			account_list[_account_info.account_id].level = ret_data.data.level;
			account_list[_account_info.account_id].ynPush = ret_data.data.ynPush;
			account_list[_account_info.account_id].createdDate = ret_data.data.createdDate;
			
			list_table.row($('#' + _account_info.account_id)).data([
				account_list[_account_info.account_id].id,
				account_list[_account_info.account_id].name,
				account_list[_account_info.account_id].phone,
				account_list[_account_info.account_id].email,
				account_list[_account_info.account_id].role,
				ynPushHtml,
				account_list[_account_info.account_id].createdDate,
				deleteHtml
			]).draw(false);
			
			alert("수정 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function addAccount() {
	var params = {};
	params.type = "add_account";
	params.id = $.trim($("#id").val());
	params.pw = $.trim($("#pw").val());
	params.name = $.trim($("#name").val());
	params.phone = $.trim($("#phone").val());
	params.email = $.trim($("#email").val());
	params.level = $.trim($("#role_level").val());
	params.ynPush = $("input:checkbox[id='ynPush']").is(":checked") ? "Y" : "N";
	
	if(params.id.length <= 0) {
		alert("아이디를 입력해주세요.");
		return;
	}
	
	if(params.pw.length <= 0) {
		alert("비밀번호를 입력해주세요.");
		return;
	}
	
	if(params.name.length <= 0) {
		alert("이름을 입력해주세요.");
		return;
	}
	
	if(params.phone.length <= 0) {
		alert("휴대폰 번호를 입력해주세요.");
		return;
	}
	
	if(params.level == 0 || params.level.length <= 0) {
		alert("권한을 입력해주세요.");
		return;
	}
	
	$.post(AJAX_URL, params, function(_ret_data) {
		$("#modal_popup").modal('hide');
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			var account_id = ret_data.data.account_id;
			account_list[account_id] = ret_data.data;
			
			var ynPushHtml = "<input type='checkbox' class='btnYnPush' " + (account_list[account_id].ynPush == "Y" ? "checked" : "") + " />";
			var deleteHtml = "<button type='button' class='btn btn-primary btn-sm btn_delete_account'>삭제</button>";

			var new_node = list_table.row.add([
				account_list[account_id].id,
				account_list[account_id].name,
				account_list[account_id].phone,
				account_list[account_id].email,
				account_list[account_id].role,
				ynPushHtml,
				account_list[account_id].createdDate,
				deleteHtml
			]).draw(false).node();
			
			$(new_node).attr("id", account_id);
			
			alert("등록 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function deleteAccount(_account_id) {
	var params = {};
	params.type = "delete_account";
	params.account_id = _account_id;
	
	$.post(AJAX_URL, params, function(_ret_data) {
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			list_table.row($('#' + _account_id)).remove().draw(false);
			alert("삭제 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function changeAccountYnPush(_account_id, _yn_push) {
	var params = {};
	params.type = "change_account_ynpush";
	params.account_id = _account_id;
	params.ynPush = _yn_push;
		
	$.post(AJAX_URL, params, function(_data) {
		var ret_data = JSON.parse(_data);
		if(ret_data.result == "OK") {
			account_list[_account_id].ynPush = _yn_push;
			
			var ynPushHtml = "<input type='checkbox' class='btnYnPush' " + (account_list[_account_id].ynPush == "Y" ? "checked" : "") + " />";
			var deleteHtml = "<button type='button' class='btn btn-primary btn-sm btn_delete_account'>삭제</button>";
			
			list_table.row($('#' + _account_id)).data([
				account_list[_account_id].id,
				account_list[_account_id].name,
				account_list[_account_id].phone,
				account_list[_account_id].email,
				account_list[_account_id].role,
				ynPushHtml,
				account_list[_account_id].createdDate,
				deleteHtml
			]).draw(false);
			
			alert("변경 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			list_table.row($('#' + _account_id)).invalidate().draw(false);
			alert(ret_data.msg);
			location.href = LOGIN_URL;
		} else {
			list_table.row($('#' + _account_id)).invalidate().draw(false);
			alert(ret_data.msg);
		}
	}, "text");
}
