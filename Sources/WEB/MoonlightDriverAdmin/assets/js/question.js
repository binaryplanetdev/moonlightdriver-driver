function showMapPopup(_question_id) {
	window.open("/pages/question_position_map.php?question_id=" + _question_id);
}

function showQuestionAnswerPopup(_isEdit, _data) {
	if(_isEdit) {
		$("#second_modal_popup_label").text("문의사항 답변보내기");
	} else {
		$("#second_modal_popup_label").text("문의사항 답변 상세보기");
	}
	
	$("#second_modal_popup_content").html(getQuestionAnswerContentsHtml(_isEdit, _data));
	$("#second_modal_popup_footer").html(getQuestionAnswerFooterHtml(_isEdit));
	
	$("#second_modal_popup").modal('show');
	
	if(_isEdit) {
		$("#btnSaveAnswer").on('click', function() {
			var params = {};
			params.type = "save_question_answer";
			params.question_id = _data.question_id;
			params.answer = $.trim($("#answer").val());
			
			if(params.answer.length <= 0) {
				alert("답변을 입력해 주세요.");
				return;
			}
			
			$.post("/ajax/ajax.php", params, function(_ret_data) {
				var ret_data = JSON.parse(_ret_data);
				if(ret_data.result == "OK") {
					var question_id = ret_data.data.question_answer.questionId;
					question_list[question_id].answerCount = ret_data.data.question_answer_count;
					
					if(!question_answer_list[question_id]) {
						question_answer_list[question_id] = {};
					}
					
					question_answer_list[question_id][ret_data.data.question_answer.question_answer_id] = {
						questionId : ret_data.data.question_answer.questionId,
						question_answer_id : ret_data.data.question_answer.question_answer_id,
						seq : ret_data.data.question_answer.seq,
						answer : ret_data.data.question_answer.answer,
						createdDate : ret_data.data.question_answer.createdDate
					};
					
					var showAnswerHtml = "<button type='button' class='btn btn-primary btn-sm show_answer'>답변보기(" + question_list[question_id].answerCount + ")</button>";
					
					list_table.row($('#' + question_id)).data([
						question_list[question_id].seq,
						question_list[question_id].title,
						question_list[question_id].contents,
						driver_list[question_list[question_id].driverId].name,
						question_list[question_id].phone,
						showAnswerHtml,
						question_list[question_id].createdDate,
						mapPopUpHtml
					]).draw(false);
					
					sendPush(question_id);
				} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
					alert(ret_data.msg);
					location.href = "/";
				} else {
					alert(ret_data.msg);
				}
			}, "text");
		});
		
		$('#second_modal_popup').on('hidden.bs.modal', function () {
			if(isShowQuestionPopup) {
				$('#modal_popup').modal('show');
				isShowQuestionPopup = false;
			}
		});
	} else {
		$("#btnSaveChanges").on('click', function() {
			var params = {};
			params.type = "update_question_answer";
			params.question_id = _data.questionId;
			params.question_answer_id = _data.question_answer_id;
			params.answer = $.trim($("#answer").val());
			
			if(params.answer.length <= 0) {
				alert("답변을 입력해 주세요.");
				return;
			}
			
			$.post("/ajax/ajax.php", params, function(_ret_data) {
				var ret_data = JSON.parse(_ret_data);
				if(ret_data.result == "OK") {
					var question_id = ret_data.data.question_answer.questionId;
					question_list[question_id].answerCount = ret_data.data.question_answer_count;
					
					if(!question_answer_list[question_id]) {
						question_answer_list[question_id] = {};
					}
					
					question_answer_list[question_id][ret_data.data.question_answer.question_answer_id] = {
						questionId : ret_data.data.question_answer.questionId,
						question_answer_id : ret_data.data.question_answer.question_answer_id,
						seq : ret_data.data.question_answer.seq,
						answer : ret_data.data.question_answer.answer,
						createdDate : ret_data.data.question_answer.createdDate
					};
					
					var showAnswerHtml = "<button type='button' class='btn btn-primary btn-sm show_answer'>답변보기(" + question_list[question_id].answerCount + ")</button>";
					
					list_table.row($('#' + question_id)).data([
						question_list[question_id].seq,
						question_list[question_id].title,
						question_list[question_id].contents,
						driver_list[question_list[question_id].driverId].name,
						question_list[question_id].phone,
						showAnswerHtml,
						question_list[question_id].createdDate,
						mapPopUpHtml,
						deleteHtml
					]).draw(false);
					
					sendPush(question_id);
				} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
					alert(ret_data.msg);
					location.href = "/";
				} else {
					alert(ret_data.msg);
				}
			}, "text");
		});
	}
}

function deleteQuestion(_question_id) {
	var params = {};
	params.type = "delete_question";
	params.question_id = _question_id;
	
	$.post("/ajax/ajax.php", params, function(_ret_data) {
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			list_table.row($('#' + _question_id)).remove().draw(false);
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function sendPush(_questionId) {
	var message = "문의사항에 대한 답변이 등록되었습니다.";
	$.get("http://115.68.104.30:8888/question/push?questionId=" + _questionId + "&message=" + message, function(_data) {
		$("#second_modal_popup").modal('hide');
		
		var ret_data = JSON.parse(_data);
		if(ret_data.result.code == 100) {
			alert("답변이 등록 되었습니다.");
		} else {
			alert(ret_data.result.detail);
		}
	}, "text");
}

function getQuestionAnswerContentsHtml(_isEdit, _data) {
	var html = "";
	
	html += "<div class='row'>";
	html += "	<form class='form-horizontal'>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-2 control-label' for='answer'>답변</label>";
	html += "			<div class='col-sm-9'><textarea class='form-control' id='answer' rows='25'>" + (_isEdit ? "" : _data.answer) + "</textarea></div>";
	html += "		</div>";
	html += "	</form>";
	html += "</div>";
	
	return html;
}

function getQuestionAnswerFooterHtml(_isEdit) {
	var html = "";
	
	if(_isEdit) {
		html += "<button type='button' id='btnSaveAnswer' class='btn btn-primary'>등록</button>";
	} else {
		html += "<button type='button' id='btnSaveChanges' class='btn btn-primary'>수정</button>";
	}
	
	html += "<button type='button' class='btn btn-default' data-dismiss='modal'>닫기</button>";
	
	return html;
}

function showQuestionDetailPopup(_data) {
	$("#modal_popup_label").text("문의사항 상세보기");
	$("#modal_popup_content").html(getQuestionDetailContentsHtml());
	$("#modal_popup_footer").html(getQuestionDetailFooterHtml());
	
	$("#title").text(_data.title);
	$("#contents").text(_data.contents);
	$("#register_name").val(driver_list[_data.driverId].name);
	$("#register_phone").val(_data.phone);
	$("#register_date").val(_data.createdDate);
	
	$("#modal_popup").modal('show');
	
	$('#btnShowAnswerPopup').on('click', function() {
		isShowQuestionPopup = true;
		$("#modal_popup").modal('hide');
		showQuestionAnswerPopup(true, _data);
	});
}

function getQuestionDetailContentsHtml() {
	var html = "";
	
	html += "<div class='row'>";
	html += "<form class='form-horizontal'>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-3 control-label' for='register_name'>등록자 이름</label>";
	html += "<div class='col-sm-8'><input type='text' class='form-control' id='register_name' readonly/></div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-3 control-label' for='register_phone'>등록자 전화번호</label>";
	html += "<div class='col-sm-8'><input type='text' class='form-control' id='register_phone' readonly/></div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-3 control-label' for='title'>제목</label>";
	html += "<div class='col-sm-8'><textarea class='form-control' id='title' rows='3' readonly></textarea></div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-3 control-label' for='contents'>내용</label>";
	html += "<div class='col-sm-8'><textarea class='form-control' id='contents' rows='20' readonly></textarea></div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-3 control-label' for='register_date'>등록일자</label>";
	html += "<div class='col-sm-8'><input type='text' class='form-control' id='register_date' readonly/></div>";
	html += "</div>";
	html += "</form>";
	html += "</div>";
	
	return html;
}

function getQuestionDetailFooterHtml() {
	var html = "";
	
	html += "<button type='button' id='btnShowAnswerPopup' class='btn btn-primary'>답변보내기</button>";
	html += "<button type='button' class='btn btn-default' data-dismiss='modal'>닫기</button>";
	
	return html;
}

function getAnswerRowHtml(_question_id) {
	var html = "";
	html += "<table class='table table-striped table-bordered table-hover question_answer_list'>";
	html += "<thead>";
	html += "<tr>";
	html += "	<th>#</th>";
	html += "	<th>답변내용</th>";
	html += "	<th>답변일자</th>";
	html += "</tr>";
	html += "</thead>";
	html += "<tbody>";
	
	if(question_answer_list[_question_id]) {
		$.each(question_answer_list[_question_id], function(_question_answer_id, _quesiton_answer) {
			html += "<tr id='" + _question_id + "_" + _question_answer_id + "'>";
			html += "	<td>" + _quesiton_answer.seq + "</td>";
			html += "	<td class='concat'>" + _quesiton_answer.answer + "</td>";
			html += "	<td>" + _quesiton_answer.createdDate + "</td>";
			html += "</tr>";
		});
	}
	
	html += "</tbody>";
	html += "</table>";
	
	return html;
}

function makeDateString(_dateTime) {
	var dateTime = new Date(_dateTime);
	var year = dateTime.getFullYear();
	var month = dateTime.getMonth() + 1;
	if(month < 10) {
		month = "0" + month;
	}
	var date = dateTime.getDate();
	if(date < 10) {
		date = "0" + date;
	}
	var hours = dateTime.getHours();
	if(hours < 10) {
		hours = "0" + hours;
	}
	var minutes = dateTime.getMinutes();
	if(minutes < 10) {
		minutes = "0" + minutes;
	}
	var seconds = dateTime.getSeconds();
	if(seconds < 10) {
		seconds = "0" + seconds;
	}
	
	return year + "-" + month + "-" + date + " " + hours + ":" + minutes + ":" + seconds;
}
