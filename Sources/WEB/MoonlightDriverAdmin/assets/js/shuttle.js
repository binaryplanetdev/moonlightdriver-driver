var shuttle_points_map_window;
var new_points_list;
var is_update_points_list;

function showShuttleDetailPopup(_mode, _shuttle_info) {
	is_update_points_list = false;
	
	$("#modal_popup_content").html(getShuttleDetailContentsHtml());
	
	setCategory();
	
	var footerHtml = "";
	
	if(_mode == "edit") {
		$("#org_shuttle_wrapper").hide();
		$("#name").val(_shuttle_info.name);
		$("#desc").val(_shuttle_info.desc);
		$("#category").val(_shuttle_info.category);
		$("#route_type").val(_shuttle_info.routeType);
		$("#phone").val(_shuttle_info.phone);
		$("#first_time").val(_shuttle_info.firstTime);
		$("#last_time").val(_shuttle_info.lastTime);
		$("#saturday_first_time").val(_shuttle_info.saturdayFirstTime);
		$("#saturday_last_time").val(_shuttle_info.saturdayLastTime);
		$("#sunday_first_time").val(_shuttle_info.sundayFirstTime);
		$("#sunday_last_time").val(_shuttle_info.sundayLastTime);
		$("#run_first_time").val(_shuttle_info.runFirstTime);
		$("#run_last_time").val(_shuttle_info.runLastTime);
		$("#saturday_run_first_time").val(_shuttle_info.saturdayRunFirstTime);
		$("#saturday_run_last_time").val(_shuttle_info.saturdayRunLastTime);
		$("#sunday_run_first_time").val(_shuttle_info.sundayRunFirstTime);
		$("#sunday_run_last_time").val(_shuttle_info.sundayRunLastTime);
		$("#holiday_date").val(_shuttle_info.holidayDate);
		$("#term").val(_shuttle_info.term);
		$("#total_time").val(_shuttle_info.totalTime);
		$("#shuttle_points").val(_shuttle_info.pointCount);
		$("#shuttle_line_color").val(_shuttle_info.lineColor);
		$("#yn_enabled").val(_shuttle_info.ynEnabled);
		
		$("#modal_popup_label").text("셔틀 노선 상세보기");

		footerHtml += "<button type='button' id='btnSaveChanges' class='btn btn-primary'>수정</button>";
	} else {
		$("#org_shuttle_wrapper").show();
		$("#modal_popup_label").text("셔틀 노선 등록");
		footerHtml += "<button type='button' id='btnAddShuttle' class='btn btn-primary'>추가</button>";
	}
	
	footerHtml += "<button type='button' class='btn btn-default' data-dismiss='modal'>닫기</button>";
	$("#modal_popup_footer").html(footerHtml);
	
	if(_mode == "edit") {
		$('#btnSaveChanges').off("click").on("click", function() {
			editShuttle(_shuttle_info.shuttle_id);
		});
	} else {
		setOrgShuttleList();
		
		$('#btnAddShuttle').off("click").on("click", function() {
			addShuttle();
		});
		
		$("#org_shuttle").off("change").on("change", function() {
			var selected_shuttle_id = $.trim($(this).val());
			if(shuttle_list[selected_shuttle_id]) {
				var params = {};
				params.type = "get_shuttle_points";
				params.shuttle_id = shuttle_list[selected_shuttle_id].shuttle_id;
				
				$.post(AJAX_URL, params, function(_data) {
					var ret_data = JSON.parse(_data);
					if(ret_data.result == "OK") {
						new_points_list = ret_data.data.point_list;
						is_update_points_list = true;
						
						$("#name").val(shuttle_list[selected_shuttle_id].name);
						$("#desc").val(shuttle_list[selected_shuttle_id].desc);
						$("#category").val(shuttle_list[selected_shuttle_id].category);
						$("#route_type").val(shuttle_list[selected_shuttle_id].routeType);
						$("#phone").val(shuttle_list[selected_shuttle_id].phone);
						$("#first_time").val(shuttle_list[selected_shuttle_id].firstTime);
						$("#last_time").val(shuttle_list[selected_shuttle_id].lastTime);
						$("#saturday_first_time").val(shuttle_list[selected_shuttle_id].saturdayFirstTime);
						$("#saturday_last_time").val(shuttle_list[selected_shuttle_id].saturdayLastTime);
						$("#sunday_first_time").val(shuttle_list[selected_shuttle_id].sundayFirstTime);
						$("#sunday_last_time").val(shuttle_list[selected_shuttle_id].sundayLastTime);
						$("#run_first_time").val(shuttle_list[selected_shuttle_id].runFirstTime);
						$("#run_last_time").val(shuttle_list[selected_shuttle_id].runLastTime);
						$("#saturday_run_first_time").val(shuttle_list[selected_shuttle_id].saturdayRunFirstTime);
						$("#saturday_run_last_time").val(shuttle_list[selected_shuttle_id].saturdayRunLastTime);
						$("#sunday_run_first_time").val(shuttle_list[selected_shuttle_id].sundayRunFirstTime);
						$("#sunday_run_last_time").val(shuttle_list[selected_shuttle_id].sundayRunLastTime);
						$("#holiday_date").val(shuttle_list[selected_shuttle_id].holidayDate);
						$("#term").val(shuttle_list[selected_shuttle_id].term);
						$("#total_time").val(shuttle_list[selected_shuttle_id].totalTime);
						$("#shuttle_points").val(shuttle_list[selected_shuttle_id].pointCount);
						$("#shuttle_line_color").val(shuttle_list[selected_shuttle_id].lineColor);
						$("#yn_enabled").val(shuttle_list[selected_shuttle_id].ynEnabled);
						
						$(".shuttle_line_color").colorpicker('setValue', shuttle_list[selected_shuttle_id].lineColor);
					} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
						alert(ret_data.msg);
						location.href = LOGIN_URL;
					} else {
						alert(ret_data.msg);
					}
				}, "text");
			}
		});
	}
	
	$(".shuttle_line_color").colorpicker({
		format: "hex"
	});
	
	$(".datepicker").datepicker({
		language: "kr",
		format: "yyyy-mm-dd",
	    multidate: true,
	    todayHighlight: true
	});
	
	$('.datepicker').datepicker().on("changeDate", function(e) {
		setHolidayDateList();
	});
	
	setHolidayDateList();
	
	$("#btnSelectShuttlePoints").off("click").on("click", function() {
		var url = "/pages/shuttle_points_map.php?m=e";
		
		if(_mode == "edit") {
			url += "&shuttle_id=" + _shuttle_info.shuttle_id;
		} else {
			var org_shuttle_id = $.trim($("#org_shuttle").val());
			if(org_shuttle_id != -1) {
				url += "&shuttle_id=" + org_shuttle_id;
			}
		}
		
		showShuttlePointsMap(url);
	});
	
	new_points_list = [];
	
	$("#modal_popup").modal('show');
}

function setHolidayDateList() {
	var holiday_list = $.trim($("#holiday_date").val());
	
	if(!holiday_list || holiday_list.length <= 0) {
		return;
	}
	
	var list_array = holiday_list.split(",");
	
	var html = "";
	$.each(list_array, function(key, val) {
		html += "<tr>";
		html += "	<td>" + val + "</td>";
		html += "	<td><button class='btn btn-danger btnDeleteHoliday' data-date='" + val + "'>삭제</button></td>";
		html += "</tr>";
	});
	
	$("#holiday_list_body").html(html);
	
	$(".btnDeleteHoliday").off("click").on("click", function(event) {
		event.preventDefault ? event.preventDefault() : (event.returnValue = false);
		
		var deleteDate = $(this).attr("data-date");
		var holiday_list = $.trim($("#holiday_date").val());
		
		if(!holiday_list || holiday_list.length <= 0) {
			return;
		}
		
		var list_array = holiday_list.split(",");
		list_array = _.without(list_array, deleteDate);
		
		$("#holiday_date").val(list_array.join(","));
		$('.datepicker').datepicker('update');
		
		$(this).parent("td").parent("tr").remove();
	});
}

function setOrgShuttleList() {
	var html = "";
	
	html += "<option value='-1'>원본 셔틀 노선 선택</option>";	
	$.each(shuttle_list, function(_key, _val) {
		html += "<option value='" + _key + "'>" + _val.name + "</option>";	
	});
	
	$("#org_shuttle").html(html);
}

function addNewPointsList(_list) {
	new_points_list = _list;
	$("#shuttle_points").val(new_points_list.length);
	is_update_points_list = true;
}

function updatePointsList(_shuttle_id, _list) {
	var params = {};
	params.type = "edit_shuttle_points";
	params.shuttle_id = _shuttle_id;
	params.pointCount = _list.length;
	params.new_points_list = JSON.stringify(_list);
	
	$.post(AJAX_URL, params, function(_data) {
		var ret_data = JSON.parse(_data);
		if(ret_data.result == "OK") {
			shuttle_list[_shuttle_id].pointCount = ret_data.data.pointCount;
			
			var ynEnabledHtml = "<input type='checkbox' class='btnYnEnabledToggle' " + ((shuttle_list[_shuttle_id].ynEnabled == "Y") ? "checked" : "") + "/>";
			var showPointsHtml = "<button type='button' class='btn btn-info showPoints'>" + shuttle_list[_shuttle_id].pointCount + "</button>";
			
			list_table.row($('#' + _shuttle_id)).data([
				shuttle_list[_shuttle_id].name,
				shuttle_list[_shuttle_id].desc,
				shuttle_list[_shuttle_id].category_name + "(" + shuttle_list[_shuttle_id].category + ")",
				shuttle_list[_shuttle_id].routeType == "shuttle" ? "대리셔틀" : "심야버스",
				shuttle_list[_shuttle_id].phone,
				shuttle_list[_shuttle_id].firstTime,
				shuttle_list[_shuttle_id].lastTime,
				shuttle_list[_shuttle_id].term,
				shuttle_list[_shuttle_id].totalTime,
				showPointsHtml,
				shuttle_list[_shuttle_id].ynEnabled == "Y" ? "사용중" : "사용안함",
				ynEnabledHtml,
				deleteShuttleHtml
			]).draw(false);
			
			alert("수정 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = LOGIN_URL;
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function showShuttlePointsMap(_url) {
	if(shuttle_points_map_window && shuttle_points_map_window.closed == false) {
		shuttle_points_map_window.close();
	}
	
	shuttle_points_map_window = window.open(_url, "shuttle_points_map_window");
}

function getShuttleDetailContentsHtml() {
	var html = "";
	
	html += "<div class='row'>";
	html += "	<form class='form-horizontal'>";
	html += "		<div class='form-group form-group-md' id='org_shuttle_wrapper'>";
	html += "			<label class='col-md-3 control-label' for='org_shuttle'>원본 셔틀 노선</label>";
	html += "			<div class='col-md-8'><select class='form-control' id='org_shuttle'/></select></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-md-3 control-label' for='name'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>노선 명</label>";
	html += "			<div class='col-md-8'><input type='text' class='form-control' id='name'/></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-md-3 control-label' for='desc'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>노선 구간</label>";
	html += "			<div class='col-md-8'><textarea class='form-control' id='desc' rows='3'></textarea></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-md-3 control-label' for='category'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>카테고리</label>";
	html += "			<div class='col-md-8'><select class='form-control' id='category'></select></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-md-3 control-label' for='shuttle_line_color'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>노선 색상</label>";
	html += "			<div class='col-md-8'>";
	html += "				<div class='input-group shuttle_line_color'>";
	html += "					<input type='text' class='form-control' id='shuttle_line_color'/>";
	html += "					<span class='input-group-addon'><i></i></span>";
	html += "				</div>";
	html += "				<p class='help-block'>e.g. #34fe35</p>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-md-3 control-label' for='route_type'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>셔틀/심야 구분</label>";
	html += "			<div class='col-md-8'>";
	html += "				<select class='form-control' id='route_type'>";
	html += "					<option value='shuttle'>대리셔틀</option>";
	html += "					<option value='nightbus'>심야버스</option>";
	html += "				</select>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-md-3 control-label' for='phone'>셔틀 기사 휴대폰</label>";
	html += "			<div class='col-md-8'><input type='text' class='form-control' id='phone'/></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-md-3 control-label' for='first_time'>첫/막차 설정</label>";
	html += "			<div class='col-md-8'>";
	html += "				<div class='panel panel-info' style='margin-top: 5px;'>";
	html += "					<div class='panel-heading'>평일 첫차/막차 시간</div>";
	html += "					<div class='panel-body'>";
	html += "						<div class='row'>";
	html += "							<div class='col-md-5' style='padding: 0px;'>";
	html += "								<input type='text' class='form-control' id='first_time'/>";
	html += "								<p class='help-block'>첫차 시간(e.g. 00:30)</p>";
	html += "							</div>";
	html += "							<div class='col-md-2' style='line-height: 30px; text-align: center; padding: 0px;'>-</div>";
	html += "							<div class='col-md-5' style='padding: 0px;'>";
	html += "								<input type='text' class='form-control' id='last_time'/>";
	html += "								<p class='help-block'>막차 시간(e.g. 00:30)</p>";
	html += "							</div>";
	html += "						</div>";
	html += "					</div>";
	html += "				</div>";
	html += "				<div class='panel panel-info' style='margin-top: 5px;'>";
	html += "					<div class='panel-heading'>토요일 첫차/막차 시간</div>";
	html += "					<div class='panel-body'>";
	html += "						<div class='row'>";
	html += "							<div class='col-md-5' style='padding: 0px;'>";
	html += "								<input type='text' class='form-control' id='saturday_first_time'/>";
	html += "								<p class='help-block'>첫차 시간(e.g. 00:30)</p>";
	html += "							</div>";
	html += "							<div class='col-md-2' style='line-height: 30px; text-align: center; padding: 0px;'>-</div>";
	html += "							<div class='col-md-5' style='padding: 0px;'>";
	html += "								<input type='text' class='form-control' id='saturday_last_time'/>";
	html += "								<p class='help-block'>막차 시간(e.g. 00:30)</p>";
	html += "							</div>";
	html += "						</div>";
	html += "					</div>";
	html += "				</div>";
	html += "				<div class='panel panel-info' style='margin-top: 5px;'>";
	html += "					<div class='panel-heading'>일요일 첫차/막차 시간</div>";
	html += "					<div class='panel-body'>";
	html += "						<div class='row'>";
	html += "							<div class='col-md-5' style='padding: 0px;'>";
	html += "								<input type='text' class='form-control' id='sunday_first_time'/>";
	html += "								<p class='help-block'>첫차 시간(e.g. 00:30)</p>";
	html += "							</div>";
	html += "							<div class='col-md-2' style='line-height: 30px; text-align: center; padding: 0px;'>-</div>";
	html += "							<div class='col-md-5' style='padding: 0px;'>";
	html += "								<input type='text' class='form-control' id='sunday_last_time'/>";
	html += "								<p class='help-block'>막차 시간(e.g. 00:30)</p>";
	html += "							</div>";
	html += "						</div>";
	html += "					</div>";
	html += "				</div>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-md-3 control-label' for='run_first_time'>운행시간 설정</label>";
	html += "			<div class='col-md-8'>";
	html += "				<div class='panel panel-success' style='margin-top: 5px;'>";
	html += "					<div class='panel-heading'>평일 운행시간</div>";
	html += "					<div class='panel-body'>";
	html += "						<div class='row'>";
	html += "							<div class='col-md-5' style='padding: 0px;'>";
	html += "								<input type='text' class='form-control' id='run_first_time'/>";
	html += "								<p class='help-block'>시작 시간(e.g. 00:30)</p>";
	html += "							</div>";
	html += "							<div class='col-md-2' style='line-height: 30px; text-align: center; padding: 0px;'>-</div>";
	html += "							<div class='col-md-5' style='padding: 0px;'>";
	html += "								<input type='text' class='form-control' id='run_last_time'/>";
	html += "								<p class='help-block'>종료 시간(e.g. 00:30)</p>";
	html += "							</div>";
	html += "						</div>";
	html += "					</div>";
	html += "				</div>";
	html += "				<div class='panel panel-success' style='margin-top: 5px;'>";
	html += "					<div class='panel-heading'>토요일 운행시간</div>";
	html += "					<div class='panel-body'>";
	html += "						<div class='row'>";
	html += "							<div class='col-md-5' style='padding: 0px;'>";
	html += "								<input type='text' class='form-control' id='saturday_run_first_time'/>";
	html += "								<p class='help-block'>시작 시간(e.g. 00:30)</p>";
	html += "							</div>";
	html += "							<div class='col-md-2' style='line-height: 30px; text-align: center; padding: 0px;'>-</div>";
	html += "							<div class='col-md-5' style='padding: 0px;'>";
	html += "								<input type='text' class='form-control' id='saturday_run_last_time'/>";
	html += "								<p class='help-block'>종료 시간(e.g. 00:30)</p>";
	html += "							</div>";
	html += "						</div>";
	html += "					</div>";
	html += "				</div>";
	html += "				<div class='panel panel-success' style='margin-top: 5px;'>";
	html += "					<div class='panel-heading'>일요일 운행시간</div>";
	html += "					<div class='panel-body'>";
	html += "						<div class='row'>";
	html += "							<div class='col-md-5' style='padding: 0px;'>";
	html += "								<input type='text' class='form-control' id='sunday_run_first_time'/>";
	html += "								<p class='help-block'>시작 시간(e.g. 00:30)</p>";
	html += "							</div>";
	html += "							<div class='col-md-2' style='line-height: 30px; text-align: center; padding: 0px;'>-</div>";
	html += "							<div class='col-md-5' style='padding: 0px;'>";
	html += "								<input type='text' class='form-control' id='sunday_run_last_time'/>";
	html += "								<p class='help-block'>종료 시간(e.g. 00:30)</p>";
	html += "							</div>";
	html += "						</div>";
	html += "					</div>";
	html += "				</div>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-md-3 control-label' for='holiday_date'>공휴일 설정</label>";
	html += "			<div class='col-md-8'>";
	html += "				<div class='input-group date datepicker'>";
	html += "					<input type='text' class='form-control' id='holiday_date' readOnly><span class='input-group-addon'><i class='glyphicon glyphicon-th'></i></span>";
	html += "				</div>";
	html += "				<div class='panel panel-danger' style='margin-top: 5px;'>";
	html += "					<div class='panel-heading'>공휴일 리스트</div>";
	html += "					<div class='panel-body'>";
	html += "						<div class='table-responsive' style='min-height: 0px; max-height: 200px; overflow-y: auto;'>";
	html += "							<table class='table table-striped'>";
	html += "								<thead>";
	html += "									<tr>";
	html += "										<th>공휴일</th>";
	html += "										<th>삭제</th>";
	html += "									</tr>";
	html += "								</thead>";
	html += "								<tbody id='holiday_list_body'></tbody>";
	html += "							</table>";
	html += "						</div>";
	html += "					</div>";
	html += "				</div>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-md-3 control-label' for='term'>배차간격(분)</label>";
	html += "			<div class='col-md-8'><input type='text' class='form-control' id='term'/></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-md-3 control-label' for='total_time'>총소요시간(분)</label>";
	html += "			<div class='col-md-8'><input type='text' class='form-control' id='total_time'/></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-md-3 control-label' for='shuttle_points'>노선 정류장</label>";
	html += "			<div class='col-md-5'>";
	html += "				<input type='text' class='form-control' id='shuttle_points' readonly/>";
	html += "			</div>";
	html += "			<div class='col-md-3'>";
	html += "				<button type='button' class='btn btn-primary' id='btnSelectShuttlePoints'>정류장 선택</button>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-md-3 control-label' for='yn_enabled'>상태</label>";
	html += "			<div class='col-md-8'>";
	html += "				<select class='form-control' id='yn_enabled'>";
	html += "					<option value='Y'>사용중</option>";
	html += "					<option value='N'>사용안함</option>";
	html += "				</select>";
	html += "			</div>";
	html += "		</div>";
	html += "	</form>";
	html += "</div>";
	
	return html;
}

function setCategory() {
	var html = "";
	$.each(category_list, function(key, val) {
		html += "<option value='" + key + "'>" + val.category_name + "(" + val.index + ")</option>";
	});
	
	$("#category").html(html);
}

function addShuttle() {
	var params = {};
	params.type = "add_shuttle";
	params.name = $.trim($("#name").val());
	params.desc = $.trim($("#desc").val());
	params.category = $.trim($("#category").val());
	params.lineColor = $.trim($("#shuttle_line_color").val());
	params.routeType = $.trim($("#route_type").val());
	params.phone = $.trim($("#phone").val());
	params.term = $.trim($("#term").val());
	params.totalTime = $.trim($("#total_time").val());
	params.ynEnabled = $.trim($("#yn_enabled").val());
	params.firstTime = $.trim($("#first_time").val());
	params.lastTime = $.trim($("#last_time").val());
	params.saturdayFirstTime = $.trim($("#saturday_first_time").val());
	params.saturdayLastTime = $.trim($("#saturday_last_time").val());
	params.sundayFirstTime = $.trim($("#sunday_first_time").val());
	params.sundayLastTime = $.trim($("#sunday_last_time").val());
	params.runFirstTime = $.trim($("#run_first_time").val());
	params.runLastTime = $.trim($("#run_last_time").val());
	params.saturdayRunFirstTime = $.trim($("#saturday_run_first_time").val());
	params.saturdayRunLastTime = $.trim($("#saturday_run_last_time").val());
	params.sundayRunFirstTime = $.trim($("#sunday_run_first_time").val());
	params.sundayRunLastTime = $.trim($("#sunday_run_last_time").val());
	params.holidayDate = $.trim($("#holiday_date").val());
	params.pointCount = 0;
	params.ynUpdatePointsList = "N";
	
	if(is_update_points_list) {
		params.ynUpdatePointsList = "Y";
		params.pointCount = new_points_list.length;	
		params.new_points_list = JSON.stringify(new_points_list);
	}
	
	if(params.name.length <= 0) {
		alert("셔틀 노선명을 입력해주세요.");
		$("#name").focus();
		return;
	}
	
	if(params.desc.length <= 0) {
		alert("셔틀 노선 구간 설명을 입력해주세요.");
		$("#desc").focus();
		return;
	}
	
	if(params.category.length <= 0) {
		alert("카테고리를 선택해주세요.");
		$("#category").focus();
		return;
	}
	
	if(params.lineColor.length <= 0) {
		alert("셔틀 노선 색상을 입력해주세요.");
		$("#shuttle_line_color").focus();
		return;
	}
	
	if(params.routeType.length <= 0) {
		alert("대리셔틀/심야버스 구분을 선택해주세요.");
		$("#route_type").focus();
		return;
	}
	
	if(params.firstTime.length <= 0) {
		params.firstTime = "00:30";
	}
	
	if(params.lastTime.length <= 0) {
		params.lastTime = "02:30";
	}
	
	if(params.term.length <= 0) {
		params.term = 0;
	}
	
	if(params.totalTime.length <= 0) {
		params.totalTime = 0;
	}
		
	$.post(AJAX_URL, params, function(_data) {
		$("#modal_popup").modal('hide');
		
		var ret_data = JSON.parse(_data);
		if(ret_data.result == "OK") {
			shuttle_list[ret_data.data.shuttle_id] = ret_data.data;
			
			var ynEnabledHtml = "<input type='checkbox' class='btnYnEnabledToggle' " + ((ret_data.data.ynEnabled == "Y") ? "checked" : "") + "/>";
			var showPointsHtml = "<button type='button' class='btn btn-info showPoints'>" + ret_data.data.pointCount + "</button>";
			
			var new_node = list_table.row.add([
				ret_data.data.name,
				ret_data.data.desc,
				ret_data.data.category_name + "(" + ret_data.data.category + ")",
				ret_data.data.routeType == "shuttle" ? "대리셔틀" : "심야버스",
				ret_data.data.phone,
				ret_data.data.firstTime,
				ret_data.data.lastTime,
				ret_data.data.term,
				ret_data.data.totalTime,
				showPointsHtml,
				ret_data.data.ynEnabled == "Y" ? "사용중" : "사용안함",
				ynEnabledHtml,
				deleteShuttleHtml
			]).draw(false).node();
			
			$(new_node).attr("id", ret_data.data.shuttle_id);
			alert("추가 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = LOGIN_URL;
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function editShuttle(_shuttle_id) {
	var params = {};
	params.type = "edit_shuttle";
	params.shuttle_id = _shuttle_id;
	params.name = $.trim($("#name").val());
	params.desc = $.trim($("#desc").val());
	params.category = $.trim($("#category").val());
	params.lineColor = $.trim($("#shuttle_line_color").val());
	params.routeType = $.trim($("#route_type").val());
	params.phone = $.trim($("#phone").val());
	params.term = $.trim($("#term").val());
	params.totalTime = $.trim($("#total_time").val());
	params.ynEnabled = $.trim($("#yn_enabled").val());
	params.firstTime = $.trim($("#first_time").val());
	params.lastTime = $.trim($("#last_time").val());
	params.saturdayFirstTime = $.trim($("#saturday_first_time").val());
	params.saturdayLastTime = $.trim($("#saturday_last_time").val());
	params.sundayFirstTime = $.trim($("#sunday_first_time").val());
	params.sundayLastTime = $.trim($("#sunday_last_time").val());
	params.runFirstTime = $.trim($("#run_first_time").val());
	params.runLastTime = $.trim($("#run_last_time").val());
	params.saturdayRunFirstTime = $.trim($("#saturday_run_first_time").val());
	params.saturdayRunLastTime = $.trim($("#saturday_run_last_time").val());
	params.sundayRunFirstTime = $.trim($("#sunday_run_first_time").val());
	params.sundayRunLastTime = $.trim($("#sunday_run_last_time").val());
	params.holidayDate = $.trim($("#holiday_date").val());
	params.pointCount = shuttle_list[_shuttle_id].pointCount;
	params.ynUpdatePointsList = "N";
	
	if(is_update_points_list) {
		params.ynUpdatePointsList = "Y";
		params.pointCount = new_points_list.length;	
		params.new_points_list = JSON.stringify(new_points_list);
	}
	
	if(params.name.length <= 0) {
		alert("셔틀 노선명을 입력해주세요.");
		$("#name").focus();
		return;
	}
	
	if(params.desc.length <= 0) {
		alert("셔틀 노선 구간 설명을 입력해주세요.");
		$("#desc").focus();
		return;
	}
	
	if(params.category.length <= 0) {
		alert("카테고리를 선택해주세요.");
		$("#category").focus();
		return;
	}
	
	if(params.lineColor.length <= 0) {
		alert("셔틀 노선 색상을 입력해주세요.");
		$("#shuttle_line_color").focus();
		return;
	}
	
	if(params.routeType.length <= 0) {
		alert("대리셔틀/심야버스 구분을 선택해주세요.");
		$("#route_type").focus();
		return;
	}
	
	if(params.firstTime.length <= 0) {
		params.firstTime = "00:30";
	}
	
	if(params.lastTime.length <= 0) {
		params.lastTime = "02:30";
	}
	
	if(params.term.length <= 0) {
		params.term = 0;
	}
	
	if(params.totalTime.length <= 0) {
		params.totalTime = 0;
	}
		
	$.post(AJAX_URL, params, function(_data) {
		$("#modal_popup").modal('hide');
		
		var ret_data = JSON.parse(_data);
		if(ret_data.result == "OK") {
			shuttle_list[_shuttle_id] = ret_data.data;
			
			var ynEnabledHtml = "<input type='checkbox' class='btnYnEnabledToggle' " + ((shuttle_list[_shuttle_id].ynEnabled == "Y") ? "checked" : "") + "/>";
			var showPointsHtml = "<button type='button' class='btn btn-info showPoints'>" + shuttle_list[_shuttle_id].pointCount + "</button>";
			
			list_table.row($('#' + _shuttle_id)).data([
				shuttle_list[_shuttle_id].name,
				shuttle_list[_shuttle_id].desc,
				shuttle_list[_shuttle_id].category_name + "(" + shuttle_list[_shuttle_id].category + ")",
				shuttle_list[_shuttle_id].routeType == "shuttle" ? "대리셔틀" : "심야버스",
				shuttle_list[_shuttle_id].phone,
				shuttle_list[_shuttle_id].firstTime,
				shuttle_list[_shuttle_id].lastTime,
				shuttle_list[_shuttle_id].term,
				shuttle_list[_shuttle_id].totalTime,
				showPointsHtml,
				shuttle_list[_shuttle_id].ynEnabled == "Y" ? "사용중" : "사용안함",
				ynEnabledHtml,
				deleteShuttleHtml
			]).draw(false);
			
			alert("수정 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = LOGIN_URL;
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function changeShuttleStatus(_shuttle_id, _yn_enabled) {
	var params = {};
	params.type = "change_shuttle_status";
	params.shuttle_id = _shuttle_id;
	params.ynEnabled = _yn_enabled;
		
	$.post(AJAX_URL, params, function(_data) {
		var ret_data = JSON.parse(_data);
		if(ret_data.result == "OK") {
			shuttle_list[_shuttle_id].ynEnabled = _yn_enabled;
			
			var ynEnabledHtml = "<input type='checkbox' class='btnYnEnabledToggle' " + ((shuttle_list[_shuttle_id].ynEnabled == "Y") ? "checked" : "") + "/>";
			var showPointsHtml = "<button type='button' class='btn btn-info showPoints'>" + shuttle_list[_shuttle_id].pointCount + "</button>";
			
			list_table.row($('#' + _shuttle_id)).data([
				shuttle_list[_shuttle_id].name,
				shuttle_list[_shuttle_id].desc,
				shuttle_list[_shuttle_id].category_name + "(" + shuttle_list[_shuttle_id].category + ")",
				shuttle_list[_shuttle_id].routeType == "shuttle" ? "대리셔틀" : "심야버스",
				shuttle_list[_shuttle_id].phone,
				shuttle_list[_shuttle_id].firstTime,
				shuttle_list[_shuttle_id].lastTime,
				shuttle_list[_shuttle_id].term,
				shuttle_list[_shuttle_id].totalTime,
				showPointsHtml,
				shuttle_list[_shuttle_id].ynEnabled == "Y" ? "사용중" : "사용안함",
				ynEnabledHtml,
				deleteShuttleHtml
			]).draw(false);
			
			alert("변경 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = LOGIN_URL;
		} else {
			list_table.row($('#' + _shuttle_id)).invalidate().draw(false);
			
			alert(ret_data.msg);
		}
	}, "text");
}

function deleteShuttle(_shuttle_id) {
	var params = {};
	params.type = "delete_shuttle";
	params.shuttle_id = _shuttle_id;
		
	$.post(AJAX_URL, params, function(_data) {
		var ret_data = JSON.parse(_data);
		if(ret_data.result == "OK") {
			list_table.row($('#' + _shuttle_id)).remove().draw(false);
			alert("삭제 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}
