var map;
var center = { lat : 37.566221, lng : 126.977921 };
var shuttle_route_path;
var shuttle_points_markers = [];
var last_index = 0;
var total_point_count = 0;
var __point_infowindow;
var shuttleLineList = [];
var lastCenter;
var shuttleLineColorList;

$(document).ready(function() {
	$("#showStationOnly").off("click").on("click", function() {
		setShuttlePoinsList();
	});
	
	getShuttleColorList();
});

function initMap() {
	map = new google.maps.Map(document.getElementById('map'), {
		center: center,
		zoom: 16
	});
	
	__point_infowindow = new google.maps.InfoWindow({
		content: ""
	});
	
	google.maps.event.addListener(map, 'click', function(_event) {		
		var newPoint = {
			busId : shuttle_id,
			index : last_index + 1,
			text : "__point",
			exceptOutback : 0,
			locations : {
				type : "Point",
				coordinates : [_event.latLng.lng(), _event.latLng.lat()]
			}
		};
		
		showShuttlePointAddPopup("add", newPoint);
	});
	
	google.maps.event.addListener(map, 'idle', function() {
		console.log("zoom : " + map.getZoom());
		
		if(map.getZoom() < 18) {
			removeAllObjectOnMap();
			return;
		}
		
		if(lastCenter != null) {
			var distance = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(lastCenter.lat(), lastCenter.lng()), new google.maps.LatLng(map.getCenter().lat(), map.getCenter().lng()));
			
			if(distance <= 500) {
				return;
			}
		}
		
		getNearStationList(map.getCenter());
		
		lastCenter = map.getCenter();
	});
	
	var max_height = $("body").height() - 130;
	$("#shuttle_point_list_wrap").css({"margin-top": 10});
	$(".table-responsive").css({"max-height": max_height, "overflow": "auto"});
	
	drawShuttleLineAndList(true);
}

function getShuttleColorList() {
	var params = {};
	params.type = "shuttle_line_color_list";
	
	shuttleLineColorList = {};
	
	$.post("/ajax/ajax.php", params, function(_ret_data) {
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			shuttleLineColorList = ret_data.data;
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function getNearStationList(_center_position) {
	var params = {};
	params.type = "get_near_shuttle_line";
	params.lat = _center_position.lat();
	params.lng = _center_position.lng();
	params.range = 3000;
		
	$.post("/ajax/ajax.php", params, function(_ret_data) {
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			drawShuttleLine(ret_data.data);
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function drawShuttleLine(_data) {
	$.each(_data, function(k, v) {
		v.shuttle_point.sort(function(a, b) {
			return a.index - b.index;
		});
		
		var isDrawLine = false;
		var prevPointIndex = null;
		var shuttleLinePoints = [];
		
		var shuttleLineColor = shuttleLineColorList[v._id.busId].lineColor;
		$.each(v.shuttle_point, function(k, shuttle_point_info) {
			if(prevPointIndex != null && (prevPointIndex + 1) != shuttle_point_info.index) {
				drawShuttlePolyLine(shuttleLineColor, shuttleLinePoints);
				shuttleLinePoints = [];
			} else {
				shuttleLinePoints.push({lat : shuttle_point_info.locations.coordinates[1], lng : shuttle_point_info.locations.coordinates[0]});
			}
		});
		
		drawShuttlePolyLine(shuttleLineColor, shuttleLinePoints);
	});
}

function drawShuttlePolyLine(_color, _points) {
	var shuttlePath = new google.maps.Polyline({
		path: _points,
		geodesic: true,
		strokeColor: _color,
		strokeOpacity: 1.0,
		strokeWeight: 5,
		icons: [{
			icon: {
				path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
			},
			strokeColor: _color,
            fillColor: _color,
            fillOpacity: 1,
			repeat: '200px',
			offset: '100%'
		}],
		map: map
	});
	
	shuttleLineList.push(shuttlePath);
}

function removeAllObjectOnMap() {
	lastCenter = null;
	
	$.each(shuttleLineList, function(k, v) {
		v.setMap(null);
	});
	
	shuttleLineList = [];
}

function drawShuttleLineAndList(_is_fit_bounds) {
	drawShuttleRoute(_is_fit_bounds);
	setShuttlePoinsList();
}

function setShuttlePoinsList() {
	var showStationOnly = $("#showStationOnly").is(":checked");
	var contentHtml = "";
	
	$.each(shuttle_points, function(k, v) {
		if(showStationOnly) {
			if(v.text != "__point") {
				contentHtml += "<tr id='" + k + "'>";
				contentHtml += "	<td class='font-small'>" + v.index + "</td>";
				contentHtml += "	<td class='font-small'>" + v.text + "</td>";
				contentHtml += "	<td class='font-small'>" + v.locations.coordinates[1] + "</br>" + v.locations.coordinates[0] + "</td>";
				contentHtml += "	<td><input type='checkbox' " + (v.isTransfer == 1 ? "checked" : "") + " class='check_transfer'/></td>";
				contentHtml += "	<td class='font-small'>" + v.transferName + "</td>";
				contentHtml += "	<td><input type='checkbox' " + (v.exceptOutback == 1 ? "checked" : "") + " class='check_outback'/></td>";
				contentHtml += "	<td><button class='btn btn-warning btn-circle btnEditPoints'><i class='fa fa-edit'></i></button><button class='btn btn-warning btn-circle btnDeletePoints' style='margin-left: 3px;'><i class='fa fa-times'></i></button></td>";
				contentHtml += "</tr>";				
			}
		} else {
			contentHtml += "<tr id='" + k + "'>";
			contentHtml += "	<td class='font-small'>" + v.index + "</td>";
			contentHtml += "	<td class='font-small'>" + v.text + "</td>";
			contentHtml += "	<td class='font-small'>" + v.locations.coordinates[1] + "</br>" + v.locations.coordinates[0] + "</td>";
			contentHtml += "	<td><input type='checkbox' " + (v.isTransfer == 1 ? "checked" : "") + " class='check_transfer'/></td>";
			contentHtml += "	<td class='font-small'>" + v.transferName + "</td>";
			contentHtml += "	<td><input type='checkbox' " + (v.exceptOutback == 1 ? "checked" : "") + " class='check_outback'/></td>";
			contentHtml += "	<td><button class='btn btn-warning btn-circle btnEditPoints'><i class='fa fa-edit'></i></button><button class='btn btn-warning btn-circle btnDeletePoints' style='margin-left: 3px;'><i class='fa fa-times'></i></button></td>";
			contentHtml += "</tr>";
		}
	});
	
	$("#shuttle_points_tbody").html(contentHtml);
	
	$("#shuttle_points_count").text("(" + total_point_count + ")");
	
	$('#shuttle_points_tbody').off("click").on('click', 'tr', function (event) {
		var index = $(this).index("#shuttle_points_tbody > tr");
		if($(event.target).is('#shuttle_points_tbody > tr:eq(' + index + ') > td:eq(0),#shuttle_points_tbody > tr:eq(' + index + ') > td:eq(1),#shuttle_points_tbody > tr:eq(' + index + ') > td:eq(2),#shuttle_points_tbody > tr:eq(' + index + ') > td:eq(3)')) {
			var shuttle_point_index = $(this).attr("id");
			var shuttle_point_info = shuttle_points[shuttle_point_index];
			
			if(shuttle_point_info) {
				var bounds = new google.maps.LatLngBounds();
				bounds.extend(new google.maps.LatLng(shuttle_point_info.locations.coordinates[1], shuttle_point_info.locations.coordinates[0]));
				map.fitBounds(bounds);
			
				var selectedMarker = shuttle_points_markers[shuttle_point_index].marker;
				var selectedInfowindow = shuttle_points_markers[shuttle_point_index].infowindow;
				
				if(selectedInfowindow) {
					selectedInfowindow.open(map, selectedMarker);
				} else {
					__point_infowindow.setContent(shuttle_point_info.text + "(" + shuttle_point_info.index + ")");
					__point_infowindow.open(map, selectedMarker);
				}
			}
		}
	});
	
	$(".check_outback").off("click").on('click', function (event) {
		var tr_id = $(this).parent("td").parent("tr").attr("id");
		var is_check = $(this).is(":checked") ? 1 : 0;
		
		if(shuttle_points[tr_id]) {
			shuttle_points[tr_id].exceptOutback = is_check;
		}
	});
	
	$(".check_transfer").off("click").on('click', function (event) {
		var tr_id = $(this).parent("td").parent("tr").attr("id");
		var is_check = $(this).is(":checked") ? 1 : 0;
		
		if(shuttle_points[tr_id]) {
			shuttle_points[tr_id].isTransfer = is_check;
		}
	});
	
	$(".btnEditPoints").off("click").on('click', function (event) {
		var shuttle_point_index = $(this).parent("td").parent("tr").attr("id");
		var shuttle_point_info = shuttle_points[shuttle_point_index];
		
		showShuttlePointAddPopup("edit", shuttle_point_info);
	});
	
	$('.btnDeletePoints').off("click").on('click', function (event) {
		var shuttle_point_index = $(this).parent("td").parent("tr").attr("id");
		var shuttle_point_info = shuttle_points[shuttle_point_index];
		
		var result = confirm("[ " + shuttle_point_info.text + "(" + shuttle_point_info.index + ") ]정류장을 삭제하시겠습니까?");
		if(result) {
			deleteShuttlePoint(shuttle_point_index);
			drawShuttleLineAndList(false);
		}
	});
	
	$('#btnApply').off("click").on('click', function (event) {
		var result = confirm("적용 하시겠습니까?");
		if(result) {
			applyShuttlePoints();
		}
	});
	
	$('#btnCancel').off("click").on('click', function (event) {
		window.close();
	});
}

function showShuttlePointAddPopup(_mode, _shuttle_point_data) {
	$("#modal_popup_content").html(getShuttlePointAddContentsHtml());
	
	$("#shuttle_point_text").val(_shuttle_point_data.text);
	$("#shuttle_point_index").val(_shuttle_point_data.index);
	$("#shuttle_point_lat").val(_shuttle_point_data.locations.coordinates[1]);
	$("#shuttle_point_lng").val(_shuttle_point_data.locations.coordinates[0]);
	
	if(_shuttle_point_data.exceptOutback == 1) {
		$("input:checkbox[id='except_outback']").attr("checked", true);
	}
	
	if(_shuttle_point_data.isTransfer == 1) {
		$("input:checkbox[id='is_transfer']").attr("checked", true);
	}
	
	$("#transfer_name").val(_shuttle_point_data.transferName);
	
	var footerHtml = "";
	
	if(_mode == "edit") {
		$("#modal_popup_label").text("셔틀 노선 정류장 상세보기");

		footerHtml += "<button type='submit' id='btnSaveChanges' class='btn btn-primary'>수정</button>";
	} else {
		$("#modal_popup_label").text("셔틀 노선 정류장 등록");
		footerHtml += "<button type='submit' id='btnAddShuttlePoint' class='btn btn-primary'>등록</button>";
	}
	
	footerHtml += "<button type='button' class='btn btn-default' data-dismiss='modal'>닫기</button>";
	$("#modal_popup_footer").html(footerHtml);
	
	$("#modal_popup").modal('show');
	
	if(_mode == "edit") {
		$('#btnSaveChanges').off("click").on("click", function() {
			editShuttlePoint(_shuttle_point_data.index - 1);
			drawShuttleLineAndList(false);
			
			$("#modal_popup").modal('hide');
		});
		
		$("#shuttle_point_form").submit(function(event) {
			event.preventDefault ? event.preventDefault() : (event.returnValue = false);
			
			editShuttlePoint(_shuttle_point_data.index - 1);
			drawShuttleLineAndList(false);
			
			$("#modal_popup").modal('hide');			
		});
	} else {
		$('#btnAddShuttlePoint').off("click").on("click", function() {
			addShuttlePoint();
			drawShuttleLineAndList(false);
			
			$("#modal_popup").modal('hide');
		});
		
		$("#shuttle_point_form").submit(function(event) {
			event.preventDefault ? event.preventDefault() : (event.returnValue = false);
			
			addShuttlePoint();
			drawShuttleLineAndList(false);
			
			$("#modal_popup").modal('hide');
		});
	}
	
	setTimeout(function() {
		$("#shuttle_point_text").focus();
	}, 500);
}

function getShuttlePointAddContentsHtml() {
	var html = "";
	
	html += "<div class='row'>";
	html += "	<form id='shuttle_point_form' class='form-horizontal' role='form'>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-lg-3 control-label' for='shuttle_point_text'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>정류장 명</label>";
	html += "			<div class='col-lg-8'><input type='text' class='form-control' id='shuttle_point_text'/></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-lg-3 control-label' for='shuttle_point_index'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>정류장 순번</label>";
	html += "			<div class='col-lg-8'><input type='text' class='form-control' id='shuttle_point_index'/></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-lg-3 control-label' for='shuttle_point_lat'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>위도</label>";
	html += "			<div class='col-lg-8'><input type='text' class='form-control' id='shuttle_point_lat' /></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-lg-3 control-label' for='shuttle_point_lng'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>경도</label>";
	html += "			<div class='col-lg-8'><input type='text' class='form-control' id='shuttle_point_lng' /></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-lg-3 control-label' for='except_outback'>오지기준 제외</label>";
	html += "			<div class='col-lg-8'>";
	html += "				<div class='checkbox'>";
	html += "					<label>";
	html += "						<input type='checkbox' name='except_outback' id='except_outback' value='1'/>제외";
	html += "					</label>";
	html += "				</div>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-lg-3 control-label' for='is_transfer'>환승역 여부</label>";
	html += "			<div class='col-lg-8'>";
	html += "				<div class='checkbox'>";
	html += "					<label>";
	html += "						<input type='checkbox' name='is_transfer' id='is_transfer' value='1'/>환승역";
	html += "					</label>";
	html += "				</div>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-lg-3 control-label' for='transfer_name'>환승역 명</label>";
	html += "			<div class='col-lg-8'><input type='text' class='form-control' id='transfer_name' /></div>";
	html += "		</div>";
	html += "		<button type='submit' class='hidden'></button>";
	html += "	</form>";
	html += "</div>";
	
	return html;
}

function applyShuttlePoints() {
	if(view_mode == "edit") {
		opener.addNewPointsList(shuttle_points);
	} else if(view_mode == "view") {
		opener.updatePointsList(shuttle_id, shuttle_points);
	}
	
	window.close();
}

function addShuttlePoint() {
	var shuttle_point_text = $.trim($("#shuttle_point_text").val());
	var shuttle_point_index = $.trim($("#shuttle_point_index").val());
	var shuttle_point_lat = $.trim($("#shuttle_point_lat").val());
	var shuttle_point_lng = $.trim($("#shuttle_point_lng").val());
	var except_outback = $("input:checkbox[id='except_outback']").is(":checked") == true ? 1 : 0;
	var transfer_name = $.trim($("#transfer_name").val());
	var is_transfer = $("input:checkbox[id='is_transfer']").is(":checked") == true ? 1 : 0;
	
	if(shuttle_point_text.length <= 0) {
		alert("셔틀 정류장명을 입력해주세요.");
		return;
	}
	
	if(shuttle_point_index.length <= 0) {
		alert("셔틀 정류장 순번을 입력해주세요.");
		return;
	}
	
	if(shuttle_point_lat.length <= 0) {
		alert("셔틀 정류장 좌표(위도)를 입력해주세요.");
		return;
	}
	
	if(shuttle_point_lng.length <= 0) {
		alert("셔틀 정류장 좌표(경도)를 입력해주세요.");
		return;
	}
	
	var newPoint = {
		busId : shuttle_id,
		index : parseInt(shuttle_point_index),
		text : shuttle_point_text,
		exceptOutback : except_outback,
		transferName : transfer_name,
		isTransfer : is_transfer,
		locations : {
			type : "Point",
			coordinates : [parseFloat(shuttle_point_lng), parseFloat(shuttle_point_lat)]
		}
	};
	
	shuttle_points.splice((shuttle_point_index - 1), 0, newPoint);
}

function editShuttlePoint(_pre_position) {
	var shuttle_point_text = $.trim($("#shuttle_point_text").val());
	var shuttle_point_index = $.trim($("#shuttle_point_index").val());
	var shuttle_point_lat = $.trim($("#shuttle_point_lat").val());
	var shuttle_point_lng = $.trim($("#shuttle_point_lng").val());
	var except_outback = $("input:checkbox[id='except_outback']").is(":checked") == true ? 1 : 0;
	var transfer_name = $.trim($("#transfer_name").val());
	var is_transfer = $("input:checkbox[id='is_transfer']").is(":checked") == true ? 1 : 0;
	
	if(shuttle_point_text.length <= 0) {
		alert("셔틀 정류장명을 입력해주세요.");
		return;
	}
	
	if(shuttle_point_index.length <= 0) {
		alert("셔틀 정류장 순번을 입력해주세요.");
		return;
	}
	
	if(shuttle_point_lat.length <= 0) {
		alert("셔틀 정류장 좌표(위도)를 입력해주세요.");
		return;
	}
	
	if(shuttle_point_lng.length <= 0) {
		alert("셔틀 정류장 좌표(경도)를 입력해주세요.");
		return;
	}
	
	var updatePoint = {
		busId : shuttle_id,
		index : parseInt(shuttle_point_index),
		text : shuttle_point_text,
		exceptOutback : except_outback,
		transferName : transfer_name,
		isTransfer : is_transfer,
		locations : {
			type : "Point",
			coordinates : [parseFloat(shuttle_point_lng), parseFloat(shuttle_point_lat)]
		}
	};
	
	deleteShuttlePoint(_pre_position);
	
	shuttle_points.splice((shuttle_point_index - 1), 0, updatePoint);
}

function deleteShuttlePoint(_index) {
	if(_index >= 0 && _index < shuttle_points.length) {
		shuttle_points.splice(_index, 1);
	}
}

function drawShuttleRoute(_is_fit_bounds) {
	var bounds = new google.maps.LatLngBounds();
	var shuttle_route_points = [];

	removeAllMarkers();
	
	$.each(shuttle_points, function(k, _point) {
		_point.index = k + 1;
		
		var shuttle_point_location = new google.maps.LatLng(_point.locations.coordinates[1], _point.locations.coordinates[0]);
		
		bounds.extend(shuttle_point_location);
		
		shuttle_route_points.push(shuttle_point_location);
		
		var marker = addShuttlePointMarker(shuttle_point_location, _point.text, _point.index);
		shuttle_points_markers.push(marker);
		
		if(last_index < _point.index) {
			last_index = _point.index;
		}
	});
	
	total_point_count = shuttle_route_points.length;
	
	if(total_point_count > 0) {
		drawShuttleRouteLine(shuttle_route_points);
		
		if(_is_fit_bounds) {
			map.fitBounds(bounds);
		}
	}
}

function drawShuttleRouteLine(_shuttle_route_points) {
	if(shuttle_route_path) {
		shuttle_route_path.setMap(null);
	}
	
	shuttle_route_path = new google.maps.Polyline({
		path: _shuttle_route_points,
		geodesic: true,
		strokeColor: "#FF0000",
		strokeOpacity: 1.0,
		strokeWeight: 5,
		icons: [{
			icon: {
				path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
			},
			strokeColor: "#FF0000",
            fillColor: "#FF0000",
            fillOpacity: 1,
			repeat: '200px',
			offset: '100%'
		}],
		map: map
	});
}

function addShuttlePointMarker(_position, _point_name, _point_index) {
	var marker = new google.maps.Marker({
		position: _position,
		map: map
	});
	
	var infowindow = null;
	if(_point_name != "__point") {
		infowindow = new google.maps.InfoWindow({
			content: _point_name + "(" + _point_index + ")",
			disableAutoPan: true
		});
	}
	
	marker.addListener('click', function() {
		if(infowindow) {
			infowindow.open(map, marker);
		} else {
			__point_infowindow.setContent(_point_name + "(" + _point_index + ")");
			__point_infowindow.open(map, marker);
		}
		
		$('.table-responsive').animate({
			scrollTop: $("#shuttle_points_tbody > tr").height() * _point_index
		}, 500);
	});

	if(infowindow) {
		infowindow.open(map, marker);
	}
	
	return {marker : marker, infowindow: infowindow};
}

function removeAllMarkers() {
	if(shuttle_points_markers.length > 0) {
		$.each(shuttle_points_markers, function(k, v) {
			if(v.marker) {
				v.marker.setMap(null);
			}
			
			if(v.infowindow) {
				v.infowindow.setMap(null);
			}
			
			v.marker = null;
			v.infowindow = null;
		});
	}
	
	shuttle_points_markers = [];
}
