var map;
var mapW, mapH;
var cLonLat, zoom;
var appKey = "013703a7-d144-36c8-a049-9dafe4e4417b" // 달빛기사 appKey
var type;

function setVariables(lat,lon){
    cLonLat = new Tmap.LonLat(lat, lon);
    zoom = 16;
    mapW = '560px';
    mapH = '400px';
}

function mapEvent(){
	map.events.register("click", map, onClickMap)
	function onClickMap(evt){
		var address = latlonsplit(map.getLonLatFromViewPortPx(evt.xy));
		var lat = address.lat;
        var lon = address.lon;

        var markers = new Tmap.Layer.Markers( "MarkerLayer" );
		map.addLayer(markers);

		var size = new Tmap.Size(22,30);
		var offset = new Tmap.Pixel(-(size.w/2), -size.h);
		var icon = new Tmap.Icon('http://www.pinnacleland.net/wp-content/themes/leisureq/img/ico_spot.png', size, offset);

		var marker = new Tmap.Marker(new Tmap.LonLat(lon, lat), icon);
		markers.addMarker(marker);

        var changed_latlon = changecoord(lat,lon,"EPSG3857","BESSELGEO");
		lat = changed_latlon.lat;
		lon = changed_latlon.lon;
        getaddress(lat,lon);
        $("#coord_lat").val(lat);
        $("#coord_lon").val(lon);
	}
}

function init(lat,lon) {
    setVariables(lat,lon);
    map = new Tmap.Map({div:'map_div', width:mapW, height:mapH, animation:true});
    map.setCenter(cLonLat,zoom);
}

function latlonsplit(lonlat) {
	var temp = String(lonlat).split(",");
    var lat = temp[1].replace("lat=", "");
    var lon = temp[0].replace("lon=", "");
	var latlon = {
		lat : lat,
		lon : lon
	};
	
	return latlon;
}

function getaddress(lat,lon) {
	$.ajax({
        type:"GET",
        url : "https://apis.skplanetx.com/tmap/geo/reversegeocoding",
        data : "version=1&appKey=" + appKey + "&lat=" + lat + "&lon=" + lon + "&coordType=" + "BESSELGEO" + "&addressType=A00",
        async: false,
        success : function(response) {
	        $("#address_lb").text(response.addressInfo.fullAddress);
        },
        error: function() {
            alert('통신 상태를 확인해주세요.');
        }
    });
}

function changecoord(lat,lon,from,to){
    var data;
    $.ajax({
        type:"GET",
        url : "https://apis.skplanetx.com/tmap/geo/coordconvert",
        data : "version=1&appKey=" + appKey + "&lat=" + lat + "&lon=" + lon + "&fromCoord=" + from + "&toCoord=" + to,
        async: false,
        success : function(response) {
	       	data = response;
        },
        error: function() {
            alert('통신 상태를 확인해주세요.');
        }
    });
	return data.coordinate;
}

function modal_show(){
    $("#map_div").empty();
    $("#find_addr").val("");
    $("#coord").val("");
    $("#address_lb").empty();
    setTimeout(function() {
	    init(14141034.224049, 4508667.1551121);
	    mapEvent();
    }, 400);
    
    $('#map_modal').modal('show');
}

function getInfo(){
	$.ajax({
        type:"GET",
        url : "https://apis.skplanetx.com/tmap/routes",
        data : "version=1&appKey=" + appKey + "&startX=" + $("#start_lon").val() + "&startY=" + $("#start_lat").val() + "&endX=" + $("#end_lon").val() + "&endY=" + $("#end_lat").val() + "&reqCoordType=" + "WGS84GEO",
        async: false,
        success : function(response) {
	       	data = response;
        },
        error: function() {
            alert('통신 상태를 확인해주세요.');
        }
    });
	
    return data.features[0].properties;
}

function geo_search(keyword,page){
	var count = 10;
	$.ajax({
        type:"GET",
        url : "https://apis.skplanetx.com/tmap/pois",
        data : "version=1&appKey=" + appKey + "&page=" + page + "&count=" + count + "&searchKeyword=" + encodeURI(keyword) + "&searchType=all",
        async: false,
        success : function(response) {
	       	data = response;
        },
        error: function() {
            alert('통신 상태를 확인해주세요.');
        }
    });
	
    return data;
}

$("#search_btn").click(function(){
	name_search($("#find_addr").val(), 1);
});

$("#find_addr").keypress(function(event){
	if ( event.which == 13 ) {
		name_search($("#find_addr").val(), 1);
	}
});

function name_search(keyword, page) {
	$("#addr_list > tbody").empty();
	$("#search_modal_label").text("검색어 : " + keyword);
	var result = geo_search(keyword, page);
	
	for(var i=1;i<=result.searchPoiInfo.count;i++){
		$("#addr_list > tbody").append(
									   "<tr>" +
									   "<td>" + i + "</td>" +
									   "<td>" + result.searchPoiInfo.pois.poi[i-1].upperAddrName + "</td>" +
									   "<td>" + result.searchPoiInfo.pois.poi[i-1].middleAddrName + "</td>" +
									   "<td>" + result.searchPoiInfo.pois.poi[i-1].lowerAddrName + "</td>" +
									   "<td>" + result.searchPoiInfo.pois.poi[i-1].name + "</td>" +
									   "<input id='latlon' type='hidden' value='" + result.searchPoiInfo.pois.poi[i-1].noorLat + "," + result.searchPoiInfo.pois.poi[i-1].noorLon + "'>" +
									   "</tr>"
									);
	}
	
	var total = result.searchPoiInfo.totalCount / 10;
	if((result.searchPoiInfo.totalCount % 10) != 0) {
		total++;
	}
	
	searchPagination(total, page, 10);
	
	$("#addr_list > tbody > tr").click(function(){
		$("#map_div").empty();
		var lonlat = latlonsplit($(this).children().filter("#latlon").val());
		init(lonlat.lat,lonlat.lon);
		mapEvent();
		$("#search_modal").modal('hide');
	});
	
	$("#search_modal").modal('show');
}

function searchPagination(totalPages, nowPage, limit) {
	$('.pagination').empty();
	$('.pagination').html('<li class="active"><a href="#">PREV</a>');
	var currentPage = lowerLimit = upperLimit = Math.min(nowPage, totalPages);

	for (var b = 1; b < limit && b < totalPages;) {
		if (lowerLimit > 1 ) { 
			lowerLimit--; 
			b++; 
		}
		
		if (b < limit && upperLimit < totalPages) {
			upperLimit++; 
			b++; 
		}
	}

	for (var i = lowerLimit; i <= upperLimit; i++) {
		if (i == currentPage) {
			$('.pagination').append('<li class="active"><span>' + i + '</span></li>');
		} else {
			$('.pagination').append('<li><span>' + i + '</span></li>');
		}
	}
	
	$('.pagination').append('<li class="active"><span>NEXT</span></li>');

	$(".pagination > li > span").click(function(){
		var page = $(this).text();

		if(page == "PREV") {
			if(lowerLimit-1 < 1) {
				page = 1;
			}
			
			page = lowerLimit-1;
		} else if(page == "NEXT") {
			page = upperLimit+1;
		}
		
		name_search($("#find_addr").val(), page);
	});
}