$(document).ready(function() {
	setTimeout(function() {
		$('.widget .widget-body.no-padding').css('height', $(window).height() - 210);
		$("tbody > tr").click(function(){
			$.ajax({
				type:"GET",
				url : "http://115.68.104.30:8888/call/" + $(this).attr("data-id"),
				async: false,
				success : function(response) {
					if(response.result.code == 100) {
						$("#u_phone").text(response.call.user.phone);
						$("#start").text(response.call.start.text);
						$("#through").text(response.call.through.text);
						$("#end").text(response.call.end.text);
						$("#check").text("?");
						$("#money").text(response.call.money);
						
						if(response.call.isDone) {
							status = "종료";
						} else if(response.call.isStart) {
							status = "매칭";
						} else if(response.call.isValid) {
							status = "가능";
						} else {
							status = "오류";
						}
						
						$("#status").text(status);
						
						var createdDate = getDateTimeString(response.call.createdAt);
						
						$("#createdAt").text(createdDate);
			    		$("#etc").val(response.call.etc);
			    	} else {
			    		alert(response.result.detail);
			    	}
				},
				error: function() {
					alert('통신 상태를 확인해주세요.');
				}
			});
			
			$("#del").click(function(){
				
			});
			
			$("#call_modal").modal('show');
		});
	}, 1);
});

function getDateTimeString(_date) {
	var dateTimeString = "";
	var date = new Date(_date);
	
	var fullYear = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	var hours = date.getHours();
	hours = hours < 10 ? '0' + hours : hours;
	var minutes = date.getMinutes();
	minutes = minutes < 10 ? '0' + minutes : minutes;
	var seconds = date.getSeconds();
	seconds = seconds < 10 ? '0' + seconds : seconds;
	
	dateTimeString = fullYear + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds;
	
	return dateTimeString;
}