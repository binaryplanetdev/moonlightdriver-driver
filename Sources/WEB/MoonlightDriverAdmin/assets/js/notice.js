function showNoticeDetailPopup(_data) {
	var mode = _data ? "edit" : "create";
	
	if(mode == "edit") {
		$("#modal_popup_label").text("공지사항 상세보기");
	} else {
		$("#modal_popup_label").text("공지사항 등록");
	}
	
	$("#modal_popup_content").html(getNoticeContentsHtml());
	
	var footerHtml = "";
	
	if(mode == "edit") {
		$("#title").text(_data.title);
		$("#contents").text(_data.contents);
		$("#image").val(_data.image);
		$("#link").val(_data.link);
		$("#start_date").val(_data.startDate);
		$("#end_date").val(_data.endDate);
		
		footerHtml += "<button type='button' id='btnEdit' class='btn btn-primary'>수정</button>";
	} else {
		footerHtml += "<button type='button' id='btnAdd' class='btn btn-primary'>등록</button>";
	}
	
	footerHtml += "<button type='button' class='btn btn-default' data-dismiss='modal'>닫기</button>";
	$("#modal_popup_footer").html(footerHtml);
	
	$("#modal_popup").modal('show');
	
	if(mode == "edit") {
		$("#btnEdit").off("click").on("click", function() {
			editNotice(_data);
		});
	} else {
		$("#btnAdd").off("click").on("click", function() {
			addNotice();
		});
	}
}

function getNoticeContentsHtml() {
	var html = "";
	
	html += "<div class='row'>";
	html += "<form class='form-horizontal'>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-2 control-label' for='title'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>제목</label>";
	html += "<div class='col-sm-9'><textarea class='form-control' id='title' rows='2'/></div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-2 control-label' for='contents'>내용</label>";
	html += "<div class='col-sm-9'><textarea class='form-control' id='contents' rows='15'/></div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-2 control-label' for='image'>이미지</label>";
	html += "<div class='col-sm-9'><input type='text' class='form-control' id='image'></div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-2 control-label' for='link'>링크</label>";
	html += "<div class='col-sm-9'><input type='text' class='form-control' id='link'></div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-2 control-label' for='start_date'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>시작일자</label>";
	html += "<div class='col-sm-9'>";
	html += "<input type='text' class='form-control' id='start_date'>";
	html += "<p class='help-block'>e.g. 2015-12-31 12:50:30</p>";
	html += "</div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-2 control-label' for='end_date'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>종료일자</label>";
	html += "<div class='col-sm-9'>";
	html += "<input type='text' class='form-control' id='end_date'>";
	html += "<p class='help-block'>e.g. 2016-01-01 09:00:00</p>";
	html += "</div>";
	html += "</div>";
	html += "</form>";
	html += "</div>";
	
	return html;
}

function editNotice(_notice_info) {
	var params = {};
	params.type = "edit_notice";
	params.notice_id = _notice_info.notice_id;
	params.title = $.trim($("#title").val());
	params.contents = $.trim($("#contents").val());
	params.image = $.trim($("#image").val());
	params.link = $.trim($("#link").val());
	params.start_date = $.trim($("#start_date").val());
	params.end_date = $.trim($("#end_date").val());
	
	if(params.title.length <= 0) {
		alert("공지사항 제목을 입력해주세요.");
		return;
	}
	
	if(params.contents.length <= 0 && params.image.length <= 0) {
		alert("공지사항 내용 또는 이미지 중 1가지는 입력해주세요.");
		return;
	}
	
	if(params.start_date.length <= 0) {
		alert("공지사항 출력 시작일자를 입력해주세요.");
		return;
	}
	
	if(params.end_date.length <= 0) {
		alert("공지사항 출력 종료일자를 입력해주세요.");
		return;
	}
	
	$.post("/ajax/ajax.php", params, function(_ret_data) {
		$("#modal_popup").modal('hide');
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			notice_list[_notice_info.notice_id].title = ret_data.data.title;
			notice_list[_notice_info.notice_id].contents = ret_data.data.contents;
			notice_list[_notice_info.notice_id].link = ret_data.data.link;
			notice_list[_notice_info.notice_id].startDate = ret_data.data.startDate;
			notice_list[_notice_info.notice_id].endDate = ret_data.data.endDate;
			
			list_table.row($('#' + _notice_info.notice_id)).data([
				notice_list[_notice_info.notice_id].seq,
				notice_list[_notice_info.notice_id].title,
				notice_list[_notice_info.notice_id].contents,
				"<a href='" + notice_list[_notice_info.notice_id].link + "' target='_blank'>" + notice_list[_notice_info.notice_id].link + "</a>",
				notice_list[_notice_info.notice_id].startDate,
				notice_list[_notice_info.notice_id].endDate,
				notice_list[_notice_info.notice_id].createdDate,
				deleteHtml
			]).draw(false);
			
			alert("수정 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function addNotice() {
	var params = {};
	params.type = "add_notice";
	params.title = $.trim($("#title").val());
	params.contents = $.trim($("#contents").val());
	params.image = $.trim($("#image").val());
	params.link = $.trim($("#link").val());
	params.start_date = $.trim($("#start_date").val());
	params.end_date = $.trim($("#end_date").val());
	
	if(params.title.length <= 0) {
		alert("공지사항 제목을 입력해주세요.");
		return;
	}
	
	if(params.contents.length <= 0 && params.image.length <= 0) {
		alert("공지사항 내용 또는 이미지 중 1가지는 입력해주세요.");
		return;
	}
	
	if(params.start_date.length <= 0) {
		alert("공지사항 출력 시작일자를 입력해주세요.");
		return;
	}
	
	if(params.end_date.length <= 0) {
		alert("공지사항 출력 종료일자를 입력해주세요.");
		return;
	}
	
	$.post("/ajax/ajax.php", params, function(_ret_data) {
		$("#modal_popup").modal('hide');
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			var notice_id = ret_data.data.notice_id;
			notice_list[notice_id] = ret_data.data;

			var new_node = list_table.row.add([
				notice_list[notice_id].seq,
				notice_list[notice_id].title,
				notice_list[notice_id].contents,
				"<a href='" + notice_list[notice_id].link + "' target='_blank'>" + notice_list[notice_id].link + "</a>",
				notice_list[notice_id].startDate,
				notice_list[notice_id].endDate,
				notice_list[notice_id].createdDate,
				deleteHtml
			]).draw(false).node();
			
			$(new_node).attr("id", notice_id);
			
			alert("등록 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function deleteNotice(_notice_id) {
	var params = {};
	params.type = "delete_notice";
	params.notice_id = _notice_id;
	
	$.post("/ajax/ajax.php", params, function(_ret_data) {
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			list_table.row($('#' + _notice_id)).remove().draw(false);
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}
