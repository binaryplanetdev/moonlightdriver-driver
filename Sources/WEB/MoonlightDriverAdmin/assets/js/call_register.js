$("#start").click(function(){
	type = "start";
	modal_show();
	$("#map_modal_label").text("시작지");
	$('#map_modal').addClass("start");
});

$("#through").click(function(){
	type = "through";
	modal_show();
	$("#map_modal_label").text("경유지");
	$('#map_modal').addClass("through");
});

$("#end").click(function(){
	type = "end";
	modal_show();
	$("#map_modal_label").text("도착지");
	$('#map_modal').addClass("end");
});

$("#submit_coord").click(function(){
	if(type == "start"){
		$("#start").val($("#address_lb").text());
		$("#start_lat").val($("#coord_lat").val());
		$("#start_lon").val($("#coord_lon").val());
	}
	else if(type == "through"){
		$("#through").val($("#address_lb").text());
		$("#through_lat").val($("#coord_lat").val());
		$("#through_lon").val($("#coord_lon").val());
	}
	else if(type == "end"){
		$("#end").val($("#address_lb").text());
		$("#end_lat").val($("#coord_lat").val());
		$("#end_lon").val($("#coord_lon").val());
	}
	$('#map_modal').modal('hide');
});

$("#submit_call").click(function(){
	var card = $("#card").is(":checked") ? 1 : 0;
	
	$.ajax({
		type:"POST",
		url : "http://115.68.104.30:8888/call",
		data : "phone=" + $("#phone").val() + "&start_lat=" + $("#start_lat").val() + "&start_lng=" + $("#start_lon").val() + "&start_text=" + $("#start").val() + "&through_lat=" + $("#through_lat").val() + "&through_lng=" + $("#through_lon").val() + "&through_text=" + $("#through").val() + "&end_lat=" + $("#end_lat").val()+ "&end_lng=" + $("#end_lon").val() + "&end_text=" + $("#end").val() + "&money=" + $("#money").val() + "&byCard=" + card  + "&etc=" + $("#etc").val(),
		async: false,
		success : function(response) {
			alert("정상적으로 콜 입력 완료 했습니다");
			location.reload();
		},
		error: function() {
			alert('통신 상태를 확인해주세요.');
		}
	});
});

$("#calc_money").click(function(){
	if($("#start").val() != "" && $("#end").val() != "") {
		var info = getInfo();
	} else {
		alert("출발지와 도착지를 설정해주세요.");
		return 0;
	}
	
	var date = new Date(),hour = date.getHours(),min = date.getMinutes();
	var price = 8000;
	var peak = { min : { hour :  21 , min : 0 }, max : { hour: 24 , min : 59 } , price : 10000 };
	var highpeak = { min : { hour : 23 , min : 0 }, max : { hour: 24 , min : 30 } , price : 12000 };

	// 1단계 피크 타임대 검사
	if(hour>=peak.min.hour && hour<=peak.max.hour) {
		if(!((hour == peak.min.hour && min < peak.min.min) || (hour == peak.max.hour && min > peak.max.min)))
			price = peak.price;
	}
	
	if(hour>=highpeak.min.hour && hour<=highpeak.max.hour) {
		if(!((hour == highpeak.min.hour && min < highpeak.min.min) || (hour == highpeak.max.hour && min > highpeak.max.min)))
			price = high.price;
	}

	// 2단계 주행거리 검사
	distance = Math.floor(info.totalDistance/1000);
	if(distance > 3)
		price+=(distance-3)/2*1000; //2키로당 1000

	$("#money").val(price);
});
