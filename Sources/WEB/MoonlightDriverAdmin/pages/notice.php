<?php
	header("Content-Type:text/html; charset=utf-8");

	try {
		require_once ("../inc/config.php");
		require_once_classes(array("CSession", "CDatabaseManager", "CNoticeManager"));
	
		$session = new CSession();

		if(!$session->isLogin()) {
			moveToSpecificPage(CONF_URL_LOGIN);
			exit;
		}
		
		$permissions = array("notice" => array("inquiry"));
		if(!$session->checkPermission($permissions)) {
			echo "<script>";
			echo "alert('접근권한이 없습니다.');";
			echo "location.href = '" . CONF_URL_ROOT . "';";
			echo "</script>";
			exit;
		}
		
		$notice_manager = new CNoticeManager();
		$database_manager = new CDatabaseManager();

		$database = $database_manager->getDb();
		
		$notice_list = $notice_manager->getNoticeList($database->notices);
		$notice_list_json = json_encode($notice_list, JSON_UNESCAPED_UNICODE);
	} catch (Exception $e) {
		echo $e->getMessage();
		mld_error($e->getMessage());
		moveToSpecificPage(CONF_URL_ERROR);
		exit;
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo CONF_SITE_TITLE . " > 공지사항 관리"; ?></title>
		
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/bootstrap.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/metisMenu.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/sb-admin-2.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/font-awesome.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/dataTables.bootstrap.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/dataTables.responsive.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/bootstrap-toggle.min.css">
		
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/jquery-2.1.3.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/metisMenu.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/sb-admin-2.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/dataTables.bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap-toggle.min.js"></script>
		
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css/main.css?<?php echo time();?>">
		
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js/notice.js?<?php echo time();?>"></script>
		
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		<script type="text/javascript">
			var LOGIN_URL = "<?php echo CONF_URL_LOGIN; ?>";
			var AJAX_URL = "<?php echo CONF_URL_AJAX; ?>";

			var list_table;
			var notice_list = <?php echo $notice_list_json; ?>;

			var deleteHtml = "<button type='button' class='btn btn-danger btn-sm btn_delete_notice'><i class='fa fa-times'></i></button>";

			$(function() {
				list_table = $('#notice_list').DataTable({
					columns: [
						null,
						{ className: "concat" },
						{ className: "concat" },
						{ className: "concat" },
						null,
						null,
						null,
						{ "orderable": false }
					],
					initComplete: function () {
						var html = "";
						html += "<label>";
						html += "<button type='button' class='btn btn-primary' id='btnCreateNotice'>공지사항 등록</button>";
						html += "</label>";
						$('#notice_list_length').html(html);
						
						$('#btnCreateNotice').off("click").on('click', function() {
							showNoticeDetailPopup(null);
						});
					},
					drawCallback: function() {
						$('#notice_list > tbody').off("click").on('click', 'tr', function (event) {
							var index = $(this).index("#notice_list > tbody > tr");
							
							if($(event.target).is('#notice_list > tbody > tr:eq(' + index + ') > td:eq(0),#notice_list > tbody > tr:eq(' + index + ') > td:eq(1),#notice_list > tbody > tr:eq(' + index + ') > td:eq(2),#notice_list > tbody > tr:eq(' + index + ') > td:eq(3),#notice_list > tbody > tr:eq(' + index + ') > td:eq(4),#notice_list > tbody > tr:eq(' + index + ') > td:eq(5),#notice_list > tbody > tr:eq(' + index + ') > td:eq(6)')) {
								var notice_id = $(this).attr("id");
								showNoticeDetailPopup(notice_list[notice_id]);
							}
						});

						$(".btn_delete_notice").off("click").on("click", function() {
							var noticeId = $(this).parent("td").parent("tr").attr("id");

							var ret = confirm("공지사항을 삭제 하시겠습니까?");
							if(ret) {
								deleteNotice(noticeId);
							}
						});
					}
				});

				$.each(notice_list, function(key, val) {
					var new_node = list_table.row.add([
						val.seq,
						val.title,
						val.contents,
						"<a href='" + val.link + "' target='_blank'>" + val.link + "</a>",
						val.startDate,
						val.endDate,
						val.createdDate,
						deleteHtml
					]).node();

					$(new_node).attr("id", val.notice_id);
				});

				list_table.draw();
			});
		</script>
	</head>
	<body>
		<div id="wrapper">
			<?php include_once CONF_URL_MENU; ?>
			<div id="page-wrapper">
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">공지사항 관리</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">공지사항 리스트</div>
							<div class="panel-body">
								<div class="dataTable_wrapper">
									<table class="table table-bordered table-hover" id="notice_list">
										<thead>
											<tr>
												<th>#</th>
												<th>제목</th>
												<th>내용</th>
												<th>링크</th>
												<th>시작일자</th>
												<th>종료일자</th>
												<th>등록일자</th>
												<th><i class="fa fa-times"></i></th>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade" id="modal_popup" tabindex="-1" role="dialog" aria-labelledby="modal_popup_label" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="modal_popup_label"></h4>
						</div>
						<div class="modal-body" id="modal_popup_content"></div>
						<div class="modal-footer" id="modal_popup_footer"></div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>