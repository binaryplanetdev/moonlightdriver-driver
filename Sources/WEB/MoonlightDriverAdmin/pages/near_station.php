<?php
	header("Content-Type:text/html; charset=utf-8");

	try {
		require_once ("../inc/config.php");
		require_once_classes(array("CSession"));
	
		$session = new CSession();

		if(!$session->isLogin()) {
			moveToSpecificPage(CONF_URL_LOGIN);
			exit;
		}
		
		$permissions = array("driver" => array("inquiry"));
		if(!$session->checkPermission($permissions)) {
			echo "<script>";
			echo "alert('접근권한이 없습니다.');";
			echo "location.href = '" . CONF_URL_ROOT . "';";
			echo "</script>";
			exit;
		}
	} catch (Exception $e) {
		echo $e->getMessage();
		mld_error($e->getMessage());
		moveToSpecificPage(CONF_URL_ERROR);
		exit;
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo CONF_SITE_TITLE . " > 기사위치 및 주변정류장 조회"; ?></title>
		
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/bootstrap.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/metisMenu.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/sb-admin-2.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/font-awesome.min.css">
		
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/jquery-2.1.3.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/metisMenu.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/sb-admin-2.js"></script>
		
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/underscore-min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/jquery.stickysectionheaders.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/jsts/javascript.util.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/jsts/jsts.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/wicket/wicket.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/wicket/wicket-gmap3.js"></script>
		
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css/layout.css?<?php echo time(); ?>"/>
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css/popup_layer.css?<?php echo time(); ?>"/>
		
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		<script>
			var map_center;
			$(function(){
				$("#btnSearchDriver").on("click", function() {
					searchDriver();
				});

				$("#driver_search").on("keypress", function(_event){
					if(_event.which == 13) {
						searchDriver();
					}
				});
			});
		</script>
		
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>/js/near_station.js?<?php echo time(); ?>"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>/js/near_station_map.js?<?php echo time(); ?>"></script>
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo GOOGLE_API_KEY; ?>&callback=initMap&libraries=geometry" async defer></script>
	</head>
	<body>
		<div id="wrapper">
			<?php include_once CONF_URL_MENU; ?>
			<div id="page-wrapper">
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">기사위치 및 주변정류장 조회</h1>
					</div>
				</div>
				<div>
					<div style="width: 100%; height: 60px;">
						<div class="col-lg-4">
							<div class="col-sm-8"><input type="text" class="form-control" id="driver_search" placeholder="전화번호 / 기사이름 입력(3자이상)"/></div>
							<div class="col-sm-3"><button type="button" id="btnSearchDriver" class="btn btn-warning">기사 검색</button></div>
						</div>
					</div>
				</div>
				<div style="position: relative;">
					<div id="map" style="min-height: 760px;"></div>
					<div style="position: absolute; left: 10px; top: 60px; width: 115px; height: 122px;">
						<button id="btnStationList" class="btn btn-outline btn-warning">정류장 리스트</button>
						<button id="btnShuttleLineColor" class="btn btn-outline btn-warning" style="margin-top: 10px;">셔틀 노선색</button>
						<button id="btnShowOutback" class="btn btn-outline btn-warning" style="margin-top: 10px;">오지 OFF</button>
					</div>
				</div>
			</div>
			<div class="popup_layer right" id="popup_layer">
				<div class="arrow"></div>
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" id="modal_popup_close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h5 class="modal-title" id="modal_popup_label"></h5>
					</div>
					<div class="modal-body" id="modal_popup_content"></div>
					<div class="modal-footer" id="modal_popup_footer"></div>
				</div>
			</div>
			<div class="popup_layer right" id="second_popup_layer">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" id="second_modal_popup_close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h5 class="modal-title" id="second_modal_popup_label"></h5>
					</div>
					<div class="modal-body" id="second_modal_popup_content"></div>
					<div class="modal-footer" id="second_modal_popup_footer"></div>
				</div>
			</div>
		</div>
	</body>
</html>