<?php
	header("Content-Type:text/html; charset=utf-8");

	try {
		require_once ("../inc/config.php");
		require_once_classes(array("CSession", "CDatabaseManager", "CShuttleManager"));
	
		$session = new CSession();

		if(!$session->isLogin()) {
			moveToSpecificPage(CONF_URL_LOGIN);
			exit;
		}
		
		$permissions = array("shuttle" => array("inquiry"));
		if(!$session->checkPermission($permissions)) {
			echo "<script>";
			echo "alert('접근권한이 없습니다.');";
			echo "location.href = '" . CONF_URL_ROOT . "';";
			echo "</script>";
			exit;
		}
		
		$shuttle_manager = new CShuttleManager();
		$database_manager = new CDatabaseManager();

		$database = $database_manager->getDb();
		
		$shuttle_list = $shuttle_manager->getShuttleInfoList($database->shuttleinfos);
		$shuttle_list_json = json_encode($shuttle_list, JSON_UNESCAPED_UNICODE);
		
		global $SHUTTLE_CATEGORY;
		$category_list = json_encode($SHUTTLE_CATEGORY, JSON_UNESCAPED_UNICODE);
		
		$permissions = array("shuttle" => array("delete"));
		$has_delete_permission = $session->checkPermission($permissions);
	} catch (Exception $e) {
		echo $e->getMessage();
		mld_error($e->getMessage());
		moveToSpecificPage(CONF_URL_ERROR);
		exit;
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo CONF_SITE_TITLE . " > 셔틀 노선 관리"; ?></title>
		
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/bootstrap.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/metisMenu.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/sb-admin-2.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/font-awesome.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/dataTables.bootstrap.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/dataTables.responsive.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/bootstrap-toggle.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/bootstrap-colorpicker.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/bootstrap-datepicker3.min.css">
		
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/jquery-2.1.3.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/metisMenu.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/sb-admin-2.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/dataTables.bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap-toggle.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap-colorpicker.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap-datepicker.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap-datepicker.kr.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/underscore-min.js"></script>
		
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css/main.css?<?php echo time();?>">
		
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js/shuttle.js?<?php echo time();?>"></script>
		
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		<script type="text/javascript">
			var LOGIN_URL = "<?php echo CONF_URL_LOGIN; ?>";
			var AJAX_URL = "<?php echo CONF_URL_AJAX; ?>";

			var list_table;
			var shuttle_list = <?php echo $shuttle_list_json; ?>;
			var category_list = <?php echo $category_list; ?>;
			var has_delete_permission = <?php echo $has_delete_permission ?>;

			var deleteShuttleHtml = "<button type='button' class='btn btn-danger deleteShuttle'>삭제</button>";

			if(!has_delete_permission) {
				deleteShuttleHtml = "<button type='button' class='btn btn-danger deleteShuttle' disabled>삭제</button>";
			}
			
			$(function() {
				list_table = $('#shuttle_list').DataTable({
					columns: [
						null,
						{ className: "concat" },
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						{ "visible": false },
						{ "orderData": 9},
						null
					],
					initComplete: function () {
						var html = "";
						html += "<label>";
						html += "<button type='button' class='btn btn-primary' id='btnCreateNewShuttle'>셔틀 노선 등록</button>";
						html += "</label>";
						$('#shuttle_list_length').html(html);
						
						$('#btnCreateNewShuttle').off("click").on('click', function() {
							showShuttleDetailPopup("add", null);
						});
					},
					drawCallback: function() {						
						$('#shuttle_list > tbody').off("click").on('click', 'tr', function (event) {
							var index = $(this).index("#shuttle_list > tbody > tr");
							var shuttle_id = $(this).attr("id");
							
							if($(event.target).is('#shuttle_list > tbody > tr:eq(' + index + ') > td:eq(0),#shuttle_list > tbody > tr:eq(' + index + ') > td:eq(1),#shuttle_list > tbody > tr:eq(' + index + ') > td:eq(2),#shuttle_list > tbody > tr:eq(' + index + ') > td:eq(3),#shuttle_list > tbody > tr:eq(' + index + ') > td:eq(4),#shuttle_list > tbody > tr:eq(' + index + ') > td:eq(5),#shuttle_list > tbody > tr:eq(' + index + ') > td:eq(6),#shuttle_list > tbody > tr:eq(' + index + ') > td:eq(7),#shuttle_list > tbody > tr:eq(' + index + ') > td:eq(8)')) {
								var shuttle_info = shuttle_list[shuttle_id];
								showShuttleDetailPopup("edit", shuttle_info);
							}
						});
						
						$('.btnYnEnabledToggle').bootstrapToggle({
							on: '사용중',
							off: '사용안함'
						}).off("change").on("change", function() {
							var yn_enabled = $(this).is(':checked') ? "Y" : "N";
							var shuttle_id = $(this).parent("div").parent("td").parent("tr").attr("id");

							changeShuttleStatus(shuttle_id, yn_enabled);
						});

						$(".showPoints").off("click").on("click", function() {
							var shuttle_id = $(this).parent("td").parent("tr").attr("id");

							showShuttlePointsMap("/pages/shuttle_points_map.php?m=v&shuttle_id=" + shuttle_id);
						});

						$(".deleteShuttle").off("click").on("click", function() {
							var shuttle_id = $(this).parent("td").parent("tr").attr("id");
							var shuttle_info = shuttle_list[shuttle_id];
							
							var ret = confirm("[" + shuttle_info.name + "] 셔틀 노선을 삭제하시겠습니까?");
							if(ret) {
								deleteShuttle(shuttle_id);
							}
						});
					}
				});

				$.each(shuttle_list, function(key, val) {
					var ynEnabledHtml = "<input type='checkbox' class='btnYnEnabledToggle' " + (val.ynEnabled == "Y" ? "checked" : "") + " />";
					var showPointsHtml = "<button type='button' class='btn btn-info showPoints'>" + val.pointCount + "</button>";
					
					var new_node = list_table.row.add([
						val.name,
						val.desc,
						val.category_name + "(" + val.category + ")",
						val.routeType == "shuttle" ? "대리셔틀" : "심야버스",
						val.phone.replace(",", "<br/>"),
						val.firstTime,
						val.lastTime,
						val.term,
						val.totalTime,
						showPointsHtml,
						val.ynEnabled == "Y" ? "사용중" : "사용안함",
						ynEnabledHtml,
						deleteShuttleHtml
					]).node();

					$(new_node).attr("id", val.shuttle_id);
				});

				list_table.draw();
			});
		</script>
	</head>
	<body>
		<div id="wrapper">
			<?php include_once CONF_URL_MENU; ?>
			<div id="page-wrapper">
				<div class="row">
					<div class="col-md-12">
						<h1 class="page-header">셔틀 노선 관리</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">셔틀 노선 리스트</div>
							<div class="panel-body">
								<div class="dataTable_wrapper">
									<table class="table table-striped table-bordered table-hover" id="shuttle_list">
										<thead>
											<tr>
												<th>이름</th>
												<th>구간</th>
												<th>카테고리</th>
												<th>셔틀/심야 구분</th>
												<th>휴대폰</th>
												<th>첫차시간</th>
												<th>막차시간</th>
												<th>배차시간<br/>(분)</th>
												<th>총소요시간<br/>(분)</th>
												<th>지도보기</th>
												<th>상태</th>
												<th>상태</th>
												<th>삭제</th>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade" id="modal_popup" tabindex="-1" role="dialog" aria-labelledby="modal_popup_label" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="modal_popup_label"></h4>
						</div>
						<div class="modal-body" id="modal_popup_content"></div>
						<div class="modal-footer" id="modal_popup_footer"></div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>