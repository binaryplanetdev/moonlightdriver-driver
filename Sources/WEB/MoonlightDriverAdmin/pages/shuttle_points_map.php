<?php
header("Content-Type:text/html; charset=utf-8");

	try {
		require_once ("../inc/config.php");
		require_once_classes(array("CSession", "CDatabaseManager", "CShuttleManager"));
	
		$session = new CSession();

		if(!$session->isLogin()) {
			moveToSpecificPage(CONF_URL_LOGIN);
			exit;
		}
		
		$permissions = array("shuttle" => array("inquiry"));
		if(!$session->checkPermission($permissions)) {
			echo "<script>";
			echo "alert('접근권한이 없습니다.');";
			echo "window.open('about:blank', '_self').close();";
			echo "</script>";
			exit;
		}
		
		$view_mode = isset($_GET["m"]) ? ($_GET["m"] == "v" ? "view" : "edit") : "view";
		$shuttle_id = isset($_GET["shuttle_id"]) ? $_GET["shuttle_id"] : "";
		$shuttle_points_list = array();
		
		if(!empty($shuttle_id)) {
			$database_manager = new CDatabaseManager();
			$shuttle_manager = new CShuttleManager();
			
			$database = $database_manager->getDb();
			
			$shuttle_points_list = $shuttle_manager->getShuttlePointsList($database->shuttlepoints, $shuttle_id);	
		}
		
		$shuttle_points_list_json = json_encode($shuttle_points_list, JSON_UNESCAPED_UNICODE);
	} catch (Exception $e) {
		echo "<script>alert('잘못된 접근입니다.'); window.open('about:blank', '_self').close();</script>";
		exit;
	}
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo CONF_SITE_TITLE . " > 셔틀 노선"; ?></title>
		
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/bootstrap.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/metisMenu.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/sb-admin-2.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/font-awesome.min.css">
		
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/jquery-2.1.3.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/metisMenu.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/sb-admin-2.js"></script>
		
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/underscore-min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/jquery.stickysectionheaders.js"></script>
		
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css/main.css?<?php echo time(); ?>"/>
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css/layout.css?<?php echo time(); ?>"/>
	</head>
	<body>
		<div id="shuttle_point_list_wrap">
			<div class="row">
				<div class="col-md-5" style="padding-right: 0px; padding-left: 5px;">
					<div class='panel panel-success'>
						<div class='panel-heading'>
							<div style="float: left;">셔틀 노선 정류장 <span id='shuttle_points_count'></span></div>
							<div style="float: right;">
								<input type="checkbox" id="showStationOnly" style="vertical-align: middle;"/><label for="showStationOnly" style="vertical-align: middle; margin: 0px 0px 0px 5px; ">정류장만 보기</label>
							</div>
							<div style="clear: both;"></div>
						</div>
						<div class='table-responsive'>
							<table class='table table-striped'>
								<thead>
									<tr>
										<th class="font-small">#</th>
										<th class="font-small">이름</th>
										<th class="font-small">좌표</th>
										<th class="font-small">환승역<br/>여부</th>
										<th class="font-small">환승역<br/>이름</th>
										<th class="font-small">오지기준<br/>제외여부</th>
										<th class='col-lg-2 font-small'>수정<br/>삭제</th>
									</tr>
								</thead>
								<tbody id='shuttle_points_tbody'></tbody>
							</table>
						</div>
						<div class="panel-footer" style="text-align: right;">
							<button type='button' id='btnApply' class='btn btn-primary' data-dismiss='modal'>적용</button>
							<button type='button' id='btnCancel' class='btn btn-default' data-dismiss='modal'>취소</button>
						</div>
					</div>
				</div>
				<div class="col-md-7" style="padding-right: 0px;">
					<div id="map"></div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="modal_popup" tabindex="-1" role="dialog" aria-labelledby="modal_popup_label" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="modal_popup_label"></h4>
					</div>
					<div class="modal-body" id="modal_popup_content"></div>
					<div class="modal-footer" id="modal_popup_footer"></div>
				</div>
			</div>
		</div>
		
		<script>
			var shuttle_points = <?php echo $shuttle_points_list_json; ?>;
			var shuttle_id = '<?php echo $shuttle_id; ?>';
			var view_mode = '<?php echo $view_mode; ?>';
		</script>
		
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>/js/shuttle_points_map.js?<?php echo time(); ?>"></script>
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo GOOGLE_API_KEY; ?>&callback=initMap" async defer></script>
	</body>
</html>

