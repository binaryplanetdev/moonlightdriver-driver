<?php
	header("Content-Type:text/html; charset=utf-8");

	try {
		require_once ("../inc/config.php");
		require_once_classes(array("CSession", "CDatabaseManager", "CQuestionManager"));
	
		$session = new CSession();

		if(!$session->isLogin()) {
			moveToSpecificPage(CONF_URL_LOGIN);
			exit;
		}
		
		$permissions = array("question" => array("inquiry"));
		if(!$session->checkPermission($permissions)) {
			echo "<script>";
			echo "alert('접근권한이 없습니다.');";
			echo "location.href = '" . CONF_URL_ROOT . "';";
			echo "</script>";
			exit;
		}
		
		$question_manager = new CQuestionManager();
		$database_manager = new CDatabaseManager();

		$database = $database_manager->getDb();
		
		$question_list = $question_manager->getQuestionList($database->questions);
		
		$driver_ids = array();
		$question_ids = array();
		foreach ($question_list as $row) {
			$driver_id = new MongoId($row["driverId"]);
			if(!in_array($driver_id, $driver_ids)) {
				$driver_ids[] = $driver_id;
			}
			
			$questionIds[] = $row["question_id"];
		}
		
		$question_answer_list = $question_manager->getQuestionAnswerList($database->questionanswers, $questionIds);
		$driver_list = $question_manager->getDriverList($database->drivers, $driver_ids);
		
		$question_list_json = json_encode($question_list, JSON_UNESCAPED_UNICODE);
		$question_answer_list_json = json_encode($question_answer_list, JSON_UNESCAPED_UNICODE);
		$driver_list_json = json_encode($driver_list, JSON_UNESCAPED_UNICODE);
	} catch (Exception $e) {
		echo $e->getMessage();
		mld_error($e->getMessage());
		moveToSpecificPage(CONF_URL_ERROR);
		exit;
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo CONF_SITE_TITLE . " > 문의사항 관리"; ?></title>
		
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/bootstrap.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/metisMenu.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/sb-admin-2.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/font-awesome.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/dataTables.bootstrap.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/dataTables.responsive.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/bootstrap-toggle.min.css">
		
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/jquery-2.1.3.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/metisMenu.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/sb-admin-2.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/dataTables.bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap-toggle.min.js"></script>
		
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css/main.css?<?php echo time();?>">
		
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js/question.js?<?php echo time();?>"></script>
		
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		<script type="text/javascript">
			var LOGIN_URL = "<?php echo CONF_URL_LOGIN; ?>";
			var AJAX_URL = "<?php echo CONF_URL_AJAX; ?>";

			var list_table;
			var question_list = <?php echo $question_list_json; ?>;
			var question_answer_list = <?php echo $question_answer_list_json; ?>;
			var driver_list = <?php echo $driver_list_json; ?>;
			var isShowQuestionPopup = false;

			var mapPopUpHtml = "<button type='button' class='btn btn-primary btn-sm show_map'><i class='fa fa-map-marker'></i></button>";
			var deleteHtml = "<button type='button' class='btn btn-danger btn-sm btn_delete_question'><i class='fa fa-times'></i></button>";
			
			$(function() {
				list_table = $('#question_list').DataTable({
					columns: [
						null,
						{ className: "concat" },
						{ className: "concat" },
						null,
						null,
						null,
						null,
						{ "orderable": false },
						{ "orderable": false }
					],
					order: [[ 0, "desc" ]],
					initComplete: function () {
						$('#question_list_length').html("");
					},
					drawCallback: function() {
						$('#question_list > tbody').off("click").on('click', 'tr', function (event) {
							var index = $(this).index("#question_list > tbody > tr");
							
							var isShowPopup = false;
							for(var i = 0; i < 7; i++) {
								if($(event.target).is("#question_list > tbody > tr:eq(" + index + ") > td:eq(" + i + ")")) {
									isShowPopup = true;
									break;
								}
							}
							
							if(isShowPopup) {
								var questionId = $(this).attr("id");
								showQuestionDetailPopup(question_list[questionId]);
							}
						});

						$(".show_map").off("click").on("click", function() {
							var questionId = $(this).parent("td").parent("tr").attr("id");
							showMapPopup(questionId);
						});

						$(".show_answer").off("click").on("click", function() {
							var questionId = $(this).parent("td").parent("tr").attr("id");
							if(question_list[questionId].answerCount > 0) {
								var tr = $(this).closest('tr');
								var row = list_table.row(tr);

								if (row.child.isShown()) {
									row.child.hide();
									tr.removeClass('shown');
								} else {
									var html = getAnswerRowHtml(questionId);
									row.child(html).show();
									tr.addClass('shown');

									$('.question_answer_list > tbody').off("click").on('click', 'tr', function (event) {
										var dataId = $(this).attr("id").split("_");
										var questionId = dataId[0];
										var questionAnswerId = dataId[1];
										showQuestionAnswerPopup(false, question_answer_list[questionId][questionAnswerId]);
									});
								}
							}
						});

						$(".btn_delete_question").off("click").on("click", function() {
							var questionId = $(this).parent("td").parent("tr").attr("id");

							var ret = confirm("문의사항을 삭제 하시겠습니까?");
							if(ret) {
								deleteQuestion(questionId);
							}
						});
					}
				});

				$.each(question_list, function(key, val) {
					var showAnswerHtml = "<button type='button' class='btn btn-primary btn-sm show_answer'>답변보기(" + val.answerCount + ")</button>";
					
					var new_node = list_table.row.add([
						val.seq,
						val.title,
						val.contents,
						driver_list[val.driverId].name,
						val.phone,
						showAnswerHtml,
						val.createdDate,
						mapPopUpHtml,
						deleteHtml
					]).node();

					$(new_node).attr("id", val.question_id);
				});

				list_table.draw();
			});
		</script>
	</head>
	<body>
		<div id="wrapper">
			<?php include_once CONF_URL_MENU; ?>
			<div id="page-wrapper">
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">문의사항 관리</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">문의사항 리스트</div>
							<div class="panel-body">
								<div class="dataTable_wrapper">
									<table class="table table-striped table-bordered table-hover" id="question_list">
										<thead>
											<tr>
												<th>#</th>
												<th>제목</th>
												<th>내용</th>
												<th>등록자 이름</th>
												<th>등록자 전화번호</th>
												<th>답변수</th>
												<th>등록 일자</th>
												<th>위치확인</th>
												<th><i class="fa fa-times"></i></th>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade" id="modal_popup" tabindex="-1" role="dialog" aria-labelledby="modal_popup_label" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="modal_popup_label"></h4>
						</div>
						<div class="modal-body" id="modal_popup_content"></div>
						<div class="modal-footer" id="modal_popup_footer"></div>
					</div>
				</div>
			</div>
			<div class="modal fade" id="second_modal_popup" tabindex="-1" role="dialog" aria-labelledby="second_modal_popup_label" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="second_modal_popup_label"></h4>
						</div>
						<div class="modal-body" id="second_modal_popup_content"></div>
						<div class="modal-footer" id="second_modal_popup_footer"></div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>