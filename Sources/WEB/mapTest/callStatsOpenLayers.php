<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="chrome=1">
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no, width=device-width">
		<script type="text/javascript" src="./js/jquery-1.11.2.min.js"></script>
		<script type="text/javascript" src="./js/proj4.js"></script>
		<script type="text/javascript" src="./js/5179.js"></script>
		<script type="text/javascript" src="./js/common.js"></script>
		<script type="text/javascript" src="./js/fastclick.min.js"></script>
		<script src="http://maps.google.com/maps/api/js?v=3&amp;sensor=false"></script>
		<style>
            html, body, .map, .fill { width: 100%; height: 100%; margin: 0px; padding: 0px; }
			ul, li { margin: 0px; padding: 0px; list-style: none; }
			.footInfo { position: absolute; right: 0px; bottom: 0px; width: 100%; height: 30px; background-color: #000; text-align: center; font-size: 14px; line-height: 30px; color: #fff; font-weight: bold; }
			.colorInfo { position: absolute; left: 0px; top: 0px; width: 100px; height: 20px; background-color: #fff; text-align: center; font-size: 14px; line-height: 30px; color: #000; border: 1px solid #000; padding: 5px; }
			.selectWrap { position: absolute; top: 0px; right: 0px; width: 50%; height: 30px; background-color: #fff; text-align: center; font-size: 12px; line-height: 30px; color: #000; }
			#selectType { margin: 0px; padding: 0px; width: 100%; height: 30px; }
        </style>
	</head>
	<body>
		<div id="map" class="map"></div>
		<div class="selectWrap">
            <select id="selectType">
                <option value='0' selected="selected">지난달 콜 수</option>
                <option value='1'>최근 3개월간 콜 수(평균)</option>
                <option value='2'>최근 1년간  콜 수(평균)</option>
            </select>
        </div>
		<div class="colorInfo">
			<ul style="width: 100px; height: 10px;">
				<li style="width: 20px; height: 10px; background-color: #00B0FF; float: left;"></li>
				<li style="width: 20px; height: 10px; background-color: #14E715; float: left;"></li>
				<li style="width: 20px; height: 10px; background-color: #FFEB3B; float: left;"></li>
				<li style="width: 20px; height: 10px; background-color: #FF8F00; float: left;"></li>
				<li style="width: 20px; height: 10px; background-color: #E51C23; float: left;"></li>
			</ul>
			<ul style="width: 100px; height: 10px;">
				<li style="width: 20px; height: 10px; float: left; text-align: left; font-size: 11px; line-height: 10px; color: #000;"><span id="txtMin"></span></li>
				<li style="width: 20px; height: 10px; float: left;"></li>
				<li style="width: 20px; height: 10px; float: left;"></li>
				<li style="width: 20px; height: 10px; float: left;"></li>
				<li style="width: 20px; height: 10px; float: left; text-align: right; font-size: 11px; line-height: 10px; color: #000;"><span id="txtMax"></span></li>
			</ul>
		</div>
		<div class="footInfo">확대하시면 동별 콜 수가 보입니다.</div>
		<script type="text/javascript" src="./js/loader.js?id=mobile-full-screen"></script>
	</body>
</html>