<?php
	header("Content-Type:text/html; charset=utf-8");
	require_once("./SGISManager.php");
	
	$SGISManager = new SGISManager();
	$access_token = $SGISManager->getAccessToken();

// 구별 좌표 조회
	$stage = $SGISManager->getArea(11, 0);
	$stage_json = json_decode($stage);
	$ret_names = array();

	foreach($stage_json->features as $feature) {
		$gucode = $feature->properties->adm_cd;
		
// 동별 좌표 조회
		$ret = $SGISManager->getArea($gucode, 0);
		$ret_json = json_decode($ret);
		
		foreach($ret_json->features as $feature) {
			$dongName = split(' ', $feature->properties->adm_nm);
			$replaced_name = "";
			
			if($dongName[2] == "중계2·3동") {
				$replaced_name = $dongName[0] . " " . $dongName[1] . " 중계동";
			} else if($dongName[2] == "상도1동") {
				$replaced_name = $dongName[0] . " " . $dongName[1] . " " . $dongName[2];
			} else if($dongName[2] == "금호1가동") {
				$replaced_name = $dongName[0] . " " . $dongName[1] . " 금호동1가";
			} else if($dongName[2] == "금호2·3가동") {
				$replaced_name = $dongName[0] . " " . $dongName[1] . " 금호동2가";
			} else if($dongName[2] == "금호4가동") {
				$replaced_name = $dongName[0] . " " . $dongName[1] . " 금호동4가";
			} else if($dongName[2] == "성수1가1동" || $dongName[2] == "성수1가2동") {
				$replaced_name = $dongName[0] . " " . $dongName[1] . " 성수동1가";
			} else if($dongName[2] == "성수2가1동" || $dongName[2] == "성수2가3동") {
				$replaced_name = $dongName[0] . " " . $dongName[1] . " 성수동2가";
			} else if($dongName[2] == "여의동") {
				$replaced_name = $dongName[0] . " " . $dongName[1] . " 여의도동";
			} else if($dongName[2] == "용산2가동") {
				$replaced_name = $dongName[0] . " " . $dongName[1] . " 용산동2가";
			} else if($dongName[2] == "종로1·2·3·4가동") {
				$replaced_name = $dongName[0] . " " . $dongName[1] . " 종로동1·2·3·4가";
			} else if($dongName[2] == "종로5·6가동") {
				$replaced_name = $dongName[0] . " " . $dongName[1] . " 종로동5·6가";
			} else {
				$replaced_name = $dongName[0] . " " . $dongName[1] . " ";
				$replaced_name .= preg_replace("/[0-9]동/s", "동", $dongName[2]);
			}
			
			$ret_names[] = $replaced_name . "(" . $feature->properties->adm_nm . ")";
		}
	}
	
	sort($ret_names);
	foreach($ret_names as $name) {
		echo $name . "</br>";
	}

?>
