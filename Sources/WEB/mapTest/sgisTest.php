<?php
	header("Content-Type:text/html; charset=utf-8");
	require_once("./SGISManager.php");
	
	$SGISManager = new SGISManager();
/*	$access_token = $SGISManager->getAccessToken();
	
	$stage = $SGISManager->getStage(11, 0);
	$stage_json = json_decode($stage);

	$retArray = array();
	foreach($stage_json->result as $row) {
		$ret = $SGISManager->getArea($row->cd, 0);
		$retArray[] = $ret;
	}

//	$retJsonString = json_encode($retArray);
//	echo $retJsonString;
	echo "<pre>";
	print_r($retArray);
	exit;

//	$coordinates = $SGISManager->getArea(11, 0);
*/
?>


<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<script type="text/javascript" src="./jsonData/DongGeoData.json"></script>
		<script type="text/javascript" src="./jsonData/SidoGeoData.json"></script>
		<script type="text/javascript" src="https://sgisapi.kostat.go.kr/OpenAPI3/auth/javascriptAuth?consumer_key=<?php echo $SGISManager->consumer_key ?>"></script>
		<script type="text/javascript">
			var geoLayer = null;
			//GeoJSON data geoData 생성
            var geoDataSido = JSON.parse(sidoGeoData);
            var geoDataDong = JSON.parse(dongGeoData);

			var polygonStyle = {
                weight: 2,
                opacity: 1,
                color: "#666666",
                dashArray: "1",
                fillColor : "#00B0FF",
                fillOpacity: 1.0
            };

			function getFillColor(_amount) {
				if(_amount <= 400) {
					return "#00B0FF";
				} else if(_amount > 400 && _amount <= 800) {
					return "#14E715";
				} else if(_amount > 800 && _amount <= 1200) {
					return "#FFEB3B";
				} else if(_amount > 1200 && _amount <= 1600) {
					return "#FF8F00";
				} else if(_amount > 1600) {
					return "#E51C23";
				}
			}
		</script>
	</head>
	<body>
		<div style="width:700px; height: 500px;">
			<div id="map" style="width:700px;height:500px;"></div>
			<div id="text"></div>
		</div>
		<script type="text/javascript">
			function drawPolygons(_type) {
				if(geoLayer) {
					map.removeLayer(geoLayer);
				}

				//geoJson 객체 생성 및 옵션 설정
				var geoData = geoDataSido;
				if(_type == 1)
					geoData = geoDataDong;

				geoLayer = sop.geoJson(geoData, {
					style: function (_data) {
						var amount = Math.floor((Math.random() * 2000) + 0);
						polygonStyle.fillColor = getFillColor(amount);
						return polygonStyle;
					},
					onEachFeature: function (feature, layer) {
					}
				}).addTo(map);
			}

			var map = sop.map("map", {scale : false, panControl: false, zoomSliderControl: false, measureControl: false, attributionControl: false}); //Map 생성
			map.setView([953427, 1950827], 5); // 지도 중심좌표로 뷰 설정

			map.on('zoomend', function(_e) {
				var drawPolygonsType = 0;
				var zoomlevel = map.getZoom();
				console.log(zoomlevel);
				
				if(zoomlevel > 5) {
					drawPolygonsType = 1;
                }

				drawPolygons(drawPolygonsType);
				
			});

			drawPolygons(0);
		</script>
	</body>
</html>

