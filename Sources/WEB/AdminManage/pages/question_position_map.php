<?php
	header("Content-Type: text/html; charset=UTF-8");
 	session_start();
	
	$GOOGLE_API_KEY = "AIzaSyDgKpZDI_xkyqEQtE-F6hWpdnkLOPXofRA";
	
	include_once($_SERVER["CONTEXT_DOCUMENT_ROOT"] . "/db.php");
	include_once($_SERVER["CONTEXT_DOCUMENT_ROOT"] . "/functions.php");
	
	if(!checkLogin()) {
		header("Location: /admin/pages/login.php");
		exit;
	}
	
	$questionId = $_GET["question_id"];

	$question = $db->questions;
	$taxifinds = $db->taxifinds;
	
	$question_info = $question->findOne(array('_id' => new MongoId($questionId)));
	
	if($question_info == null) {
		echo "<script>alert('잘못된 접근입니다.'); window.open('about:blank', '_self').close();</script>";
	}
	
	$taxifinds_info = $taxifinds->findOne(array('driverId' => $question_info["driverId"]));
	if($taxifinds_info == null) {
		echo "<script>alert('잘못된 접근입니다.'); window.open('about:blank', '_self').close();</script>";
	}
	
	$questionDriverInfo = array();
	$questionDriverInfo["questionId"] = $question_info["_id"] . "";
	$questionDriverInfo["driverId"] = $question_info["driverId"];
	$questionDriverInfo["phone"] = $question_info["phone"];
	$questionDriverInfo["register_location"] = array("lat" => $question_info["locations"]["coordinates"][1], "lng" => $question_info["locations"]["coordinates"][0]);
	$questionDriverInfo["current_location"] = array("lat" => $taxifinds_info["location"]["coordinates"][1], "lng" => $taxifinds_info["location"]["coordinates"][0]);
	
	$driverInfo = json_encode($questionDriverInfo, JSON_UNESCAPED_UNICODE);
?>

<!DOCTYPE html>
<html>
	<head>
		<title>문의 사항 등록자 위치 확인</title>
		<meta name="viewport" content="initial-scale=1.0">
		<meta charset="utf-8">
		
		<link rel="stylesheet" href="/admin/assets/css_ext/bootstrap.min.css">
		<link rel="stylesheet" href="/admin/assets/css_ext/sb-admin-2.css">
		<link rel="stylesheet" href="/admin/assets/css_ext/font-awesome.min.css">
		<script type="text/javascript" src="/admin/assets/js_ext/jquery-2.1.3.min.js"></script>
		<script type="text/javascript" src="/admin/assets/js_ext/bootstrap.min.js"></script>
		<script type="text/javascript" src="/admin/assets/js_ext/underscore-min.js"></script>
		<script type="text/javascript" src="/admin/assets/js_ext/jquery.stickysectionheaders.js"></script>
		<script type="text/javascript" src="/admin/assets/js_ext/jsts/javascript.util.js"></script>
		<script type="text/javascript" src="/admin/assets/js_ext/jsts/jsts.js"></script>
		<script type="text/javascript" src="/admin/assets/js_ext/wicket/wicket.js"></script>
		<script type="text/javascript" src="/admin/assets/js_ext/wicket/wicket-gmap3.js"></script>
		
		<link rel="stylesheet" href="/admin/assets/css/layout.css?<?php echo time(); ?>"/>
		<link rel="stylesheet" href="/admin/assets/css/popup_layer.css?<?php echo time(); ?>"/>
		
		<script>
			var driver_info = <?php echo $driverInfo; ?>;
		</script>
		
		<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $GOOGLE_API_KEY; ?>&callback=initMap" async defer></script>
		<script src="../assets/js/main.js?<?php echo time(); ?>"></script>
		<script src="../assets/js/shuttle_line_color.js?<?php echo time(); ?>"></script>
		<script src="../assets/js/near_station_map.js?<?php echo time(); ?>"></script>
		<script src="../assets/js/question_map.js?<?php echo time(); ?>"></script>
	</head>
	<body>
		<div id="map"></div>
		<div style="position: absolute; left: 10px; top: 80px; width: 115px; height: 122px;">
			<button id="btnStationList" class="btn btn-outline btn-warning">정류장 리스트</button>
			<button id="btnShuttleLineColor" class="btn btn-outline btn-warning" style="margin-top: 10px;">셔틀 노선색</button>
			<button id="btnShowOutback" class="btn btn-warning" style="margin-top: 10px;">오지 OFF</button>
		</div>
		<div class="popup_layer right" id="popup_layer">
			<div class="arrow"></div>
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" id="modal_popup_close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h5 class="modal-title" id="modal_popup_label"></h5>
				</div>
				<div class="modal-body" id="modal_popup_content"></div>
				<div class="modal-footer" id="modal_popup_footer"></div>
			</div>
		</div>
		<div class="popup_layer right" id="second_popup_layer">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" id="second_modal_popup_close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h5 class="modal-title" id="second_modal_popup_label"></h5>
				</div>
				<div class="modal-body" id="second_modal_popup_content"></div>
				<div class="modal-footer" id="second_modal_popup_footer"></div>
			</div>
		</div>
	</body>
</html>

