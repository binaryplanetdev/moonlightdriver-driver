<?php
	header("Content-Type: text/html; charset=UTF-8");
?>	
	
<!doctype html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>달빛기사 :: 관리자 페이지</title>
		
		<link rel="stylesheet" href="/admin/assets/css_ext/bootstrap.min.css">
		<link rel="stylesheet" href="/admin/assets/css_ext/bootstrap-theme.min.css">
		<link rel="stylesheet" href="/admin/assets/css/index.css">
		
		<script src="/admin/assets/js_ext/jquery-2.1.3.min.js"></script>
		<script src="/admin/assets/js_ext/bootstrap.min.js"></script>
	</head>
	<body>
		<div class="container">
			<div class="row" id="pwd-container">
				<div class="col-md-4"></div>
				<div class="col-md-4">
					<section class="login-form">
						<form method="post" action="/admin/pages/login_action.php" role="login">
							<img src="/admin/assets/images/company.png" class="img-responsive" alt="" />
							<input type="id" name="id" placeholder="아이디" required class="form-control input-lg" />
							<input type="password" name="pw" class="form-control input-lg" id="password" placeholder="비밀번호" required="" />
							<button type="submit" class="btn btn-lg btn-primary btn-block">로그인</button>
						</form>
					</section>
				</div>
			</div>
		</div>
	</body>
</html>
