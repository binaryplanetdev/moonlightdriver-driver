<?php
	header("Content-Type: text/html; charset=UTF-8");
	session_start();

	include_once($_SERVER["CONTEXT_DOCUMENT_ROOT"] . "/db.php");
	include_once($_SERVER["CONTEXT_DOCUMENT_ROOT"] . "/functions.php");
	
	if(!checkLogin()) {
		header("Location: /admin/pages/login.php");
		exit;
	}
	
	$permissions = array("shuttle" => array("inquiry"));
	if(!checkPermission($permissions)) {
		echo "<script>";
		echo "alert('접근권한이 없습니다.');";
		echo "location.href = '/admin';";
		echo "</script>";
		exit;
	}
		
	$page = $_POST["page"];
	if(empty($page)) {
		$page = 1;
	}

	$shuttleinfos = $db->shuttleinfos;
	$total = $shuttleinfos->count();
	$pageitem = 10;
	$endpage = (int)($total / $pageitem);

	if(($total % $pageitem) != 0) {
		$endpage++;
	}

	$start = ($page * $pageitem) - $pageitem;
	$end = $start + $pageitem;

	if($start == (int)($total / $pageitem)) {
		$end = ($start + $total) % $pageitem;
	}

	$shuttle_list = $shuttleinfos->find()->skip($start)->limit($pageitem)->sort(array('_id' => -1,'index' => -1));

	$key = array("name" , "desc" , "category" , "phone" , "firstTime", "lastTime", "term", "totalTime", "delete");
	$keyCount = count($key);
?>

<script src="./assets/js/main.js"></script>

<div class="row">
	<div class="col-lg-12">
		<rd-widget>
			<rd-widget-header icon="fa-tasks" title="셔틀 목록"></rd-widget-header>
			<rd-widget-body classes="no-padding">
				<div class="table-responsive">
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>이름</th>
								<th>구간</th>
								<th>카테고리</th>
								<th>휴대폰</th>
								<th>첫차시간</th>
								<th>막차시간</th>
								<th>배차시간</th>
								<th>총소요시간</th>
								<th>삭제</th>
							</tr>
						</thead>
						<tbody>
							<?php
								foreach ($shuttle_list as $shuttle) {
									$shuttle_id = $shuttle["_id"] . "";
									echo "<tr data-id=". "'" . $shuttle_id . "'>";
									
									for($i = 0; $i < $keyCount; $i++) {
										if($key[$i] == "delete") {
											echo "<td><button type='button' id='" . $shuttle_id . "_btnDelete' class='btn btn-danger btnDelete'>삭제</button></td>";
										} else {
											echo "<td>" .  $shuttle[$key[$i]]. "</td>";
										}
										
									}
									
									echo "</tr>";
								}
							?>
						</tbody>
					</table>
        		</div>
			</rd-widget-body>
		<rd-widget>
		<ul class="pagination"></ul>
		<script>
			Pagination("", <? echo $endpage; ?>, <? echo $page; ?>, 10);
		</script>
	</div>
</div>

<script src="./assets/js/shuttle.js"></script>
<link rel="stylesheet" href="./assets/css/call_list.css">
