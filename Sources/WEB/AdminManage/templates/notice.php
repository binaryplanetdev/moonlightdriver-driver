<?php
	header("Content-Type: text/html; charset=UTF-8");
	session_start();
	
	include_once($_SERVER["CONTEXT_DOCUMENT_ROOT"] . "/db.php");
	include_once($_SERVER["CONTEXT_DOCUMENT_ROOT"] . "/functions.php");
	
	if(!checkLogin()) {
		header("Location: /admin/pages/login.php");
		exit;
	}
	
	$page = $_POST["page"];
	if(empty($page)) {
		$page = 1;
	}

	$notice = $db->notices;
	
	$total = $notice->count();
	$pageitem = 20;
	$endpage = (int)($total / $pageitem);

	if(($total % $pageitem) != 0) {
		$endpage++;
	}

	$start = ($page * $pageitem) - $pageitem;
	$end = $start + $pageitem;

	if($start == (int)($total / $pageitem)) {
		$end = ($start + $total) % $pageitem;
	}

	$notice_list = $notice->find()->skip($start)->limit($pageitem)->sort(array('seq' => -1));
	
	$key = array("seq", "title", "contents", "link", "startDate", "endDate", "createdDate");
	$keyCount = count($key);
?>

<script src="./assets/js/main.js"></script>

<div class="row">
	<div class="col-lg-12">
		<rd-widget>
			<rd-widget-header icon="fa-tasks" title="공지사항 목록"></rd-widget-header>
			<rd-widget-body classes="medium no-padding">
				<div class="table-responsive">
					<table class="table table-bordered table-hover table-striped">
						<thead>
							<tr>
								<th>#</th>
								<th>제목</th>
								<th>내용</th>
								<th>링크</th>
								<th>시작일자</th>
								<th>종료일자</th>
								<th>등록일자</th>
							</tr>
						</thead>
						<tbody>
							<?php
								foreach ($notice_list as $document) {
									$notice_id = $document["_id"] . "";
									echo "<tr data-id='" . $notice_id . "'>";
									
									for($i = 0; $i < $keyCount; $i++) {
										if($key[$i] == "startDate" || $key[$i] == "endDate" || $key[$i] == "createdDate" || $key[$i] == "updatedDate") {
											$document[$key[$i]] = date("Y-m-d H:i:s", ($document[$key[$i]] / 1000));
										}

                                        echo "<td>" .  $document[$key[$i]]. "</td>";
									}
									
									echo "</tr>";
								}
							?>
						</tbody>
					</table>
        		</div>
			</rd-widget-body>
		<rd-widget>
		<ul class="pagination"></ul>
		<script>
			Pagination("notice", <? echo $endpage; ?>, <? echo $page; ?>, 10);
		</script>
	</div>
</div>

<?
    include_once("first_modal.html");
?>

<script src="./assets/js/notice.js"></script>
<link rel="stylesheet" href="./assets/css/call_list.css">
