<?php
	header("Content-Type: text/html; charset=UTF-8");
	session_start();

	include_once($_SERVER["CONTEXT_DOCUMENT_ROOT"] . "/db.php");
	include_once($_SERVER["CONTEXT_DOCUMENT_ROOT"] . "/functions.php");
	
	if(!checkLogin()) {
		header("Location: /admin/pages/login.php");
		exit;
	}
		
	$page = $_POST["page"];
	if(empty($page)) {
		$page = 1;
	}

	$calls = $db->calls;
	$total = $calls->count();
	$pageitem = 20;
	$endpage = (int)($total / $pageitem);

	if(($total % $pageitem) != 0) {
		$endpage++;
	}

	$start = ($page * $pageitem) - $pageitem;
	$end = $start + $pageitem;

	if($start == (int)($total / $pageitem)) {
		$end = ($start + $total) % $pageitem;
	}

	$call_list = $calls->find()->skip($start)->limit($pageitem)->sort(array('_id' => -1,'index' => -1));

	$key = array("start" , "end" , "money" , "createdAt" , "status");
	$keyCount = count($key);
?>

<script src="./assets/js/main.js"></script>

<div class="row">
	<div class="col-lg-12">
		<rd-widget>
			<rd-widget-header icon="fa-tasks" title="콜 목록">
			</rd-widget-header>
			<rd-widget-body classes="medium no-padding">
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
								<th>출발지</th>
								<th>도착지</th>
								<th>요금</th>
								<th>등록 시간</th>
								<th>상태</th>
							</tr>
						</thead>
						<tbody>
							<?php
								foreach ($call_list as $document) {
									echo "<tr data-id=". "'" . $document["_id"] . "'>";
									
									for($i = 0; $i < $keyCount; $i++) {
										if($key[$i] == "start" || $key[$i] =="end") {
											$document[$key[$i]] = $document[$key[$i]][text];
										} else if($key[$i] == "status") {
											if($document["isDone"] == 1) {
												$document[$key[$i]] = "종료";
											} else if($document["isStart"] == 1) {
												$document[$key[$i]] = "매칭";
											} else if($document["isValid"] == 1) {
												$document[$key[$i]] = "가능";
											} else {
												$document[$key[$i]] = "오류";
											}
										} else if($key[$i] == "createdAt") {
											$document[$key[$i]] = date("Y-m-d H:i:s", $document[$key[$i]]->sec);
										}
										
										echo "<td>" .  $document[$key[$i]]. "</td>";
									}
									
									echo "</tr>";
								}
							?>
						</tbody>
					</table>
        		</div>
			</rd-widget-body>
		<rd-widget>
		<ul class="pagination"></ul>
		<script>
			Pagination("", <? echo $endpage; ?>, <? echo $page; ?>, 10);
		</script>
	</div>
</div>

<?
	include_once("call_modal.html");
?>

<script src="./assets/js/call_list.js"></script>
<link rel="stylesheet" href="./assets/css/call_list.css">
