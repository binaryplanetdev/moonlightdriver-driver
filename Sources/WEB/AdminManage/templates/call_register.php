<link rel="stylesheet" href="./assets/css/call_register.css">
<div class="row">
	<div class="col-lg-12">
		<form>
			<div class="form-group">
				<label for="InputEmail">이용자 핸드폰 번호</label>
				<input type="tel" class="form-control" id="phone" placeholder="핸드폰 번호">
			</div>
			<div class="form-group">
				<label for="InputPassword">출발</label>
				<input type="text" class="form-control" id="start" placeholder="출발">
				<input type="hidden" id="start_lat">
				<input type="hidden" id="start_lon">
			</div>
			<div class="form-group">
				<label for="InputStart">도착</label>
				<input type="text" class="form-control" id="end" placeholder="도착">
				<input type="hidden" id="end_lat">
				<input type="hidden" id="end_lon">
			</div>
			<div class="form-group">
				<label for="InputPassword">경유지</label>
				<input type="text" class="form-control" id="through" placeholder="경유지">
				<input type="hidden" id="through_lat">
				<input type="hidden" id="through_lon">
			</div>
			<div class="form-group">
				<label for="InputStart">대리 요금</label>
				<div class="input-group">
					<div class="input-group-addon">₩</div>
					<input type="number" value="0" min="0" class="form-control" id="money" placeholder="요금">
				</div>
				<button type="button" id="calc_money" class="btn btn-primary btn-sm btn-block">가격 자동 계산</button>
			</div>
			<div class="form-group">
				<label for="InputStart">기타 사항</label>
				<textarea class="form-control" rows="3" id="etc" placeholder="기타사항"></textarea>
			</div>
			<div class="checkbox">
				<label><input type="checkbox" id="card"> 카드 사용 여부</label>
			</div>
			<button type="submit" id="submit_call" class="btn btn-default btn-lg btn-block">입력</button>
		</form>
	</div>
	<?
		include_once("map_modal.html");
		include_once("search_modal.html");
	?>
	<script src="./assets/js/call_register.js"></script>
	<script src="./assets/js/map.js"></script>
</div>
