<?php
	header("Content-Type: text/html; charset=UTF-8");
	session_start();
	
	include_once($_SERVER["CONTEXT_DOCUMENT_ROOT"] . "/functions.php");
	
	if(!checkLogin()) {
		header("Location: /admin/pages/login.php");
		exit;
	}
?>

<!doctype html>
<html ng-app="RDash">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>달빛기사 :: 관리자 페이지</title>
		
		<link type="text/css" rel="stylesheet" href="/admin/assets/css_ext/bootstrap.min.css">
		<link type="text/css" rel="stylesheet" href="/admin/assets/css/main.css"/>
		<link type="text/css" rel="stylesheet" href="/admin/assets/css_ext/metisMenu.min.css">
		<link type="text/css" rel="stylesheet" href="/admin/assets/css_ext/sb-admin-2.css">
		<link type="text/css" rel="stylesheet" href="/admin/assets/css_ext/font-awesome.min.css">
		<script type="text/javascript" src="/admin/assets/js_ext/jquery-2.1.3.min.js"></script>
		<script type="text/javascript" src="/admin/assets/js_ext/bootstrap.min.js"></script>
		<script type="text/javascript" src="/admin/assets/js_ext/metisMenu.js"></script>
		<script type="text/javascript" src="/admin/assets/js_ext/sb-admin-2.js"></script>
		<script type="text/javascript" src="/admin/assets/js_ext/dashboard.main.js"></script>
		<script type="text/javascript" src="/admin/assets/js/dashboard.js"></script>
		
		<script type="text/javascript" src="https://apis.skplanetx.com/tmap/js?version=1&format=javascript&appKey=013703a7-d144-36c8-a049-9dafe4e4417b"></script>
	</head>
	<body ng-controller="MasterCtrl">
		<div id="page-wrapper" ng-class="{'open': toggle}" ng-cloak>
			<div id="sidebar-wrapper">
				<ul class="sidebar">
					<li class="sidebar-main"><a ng-click="toggleSidebar()">달빛기사<span class="menu-icon"></span></a></li>
					<li class="sidebar-title"><span>NAVIGATION</span></li>
					<li class="sidebar-list"><a href="#">HOME<span class="menu-icon fa fa-tachometer"></span></a></li>
					<li class="sidebar-list"><a href="#/shuttle">셔틀 관리<span class="menu-icon fa fa-tachometer"></span></a></li>
					<li class="sidebar-list"><a href="#/call_list">콜 관리<span class="menu-icon fa fa-tachometer"></span></a></li>
					<li class="sidebar-list"><a href="#/call_register">콜 입력<span class="menu-icon fa fa-table"></span></a></li>
					<li class="sidebar-list"><a href="#/question">문의사항 관리<span class="menu-icon fa fa-table"></span></a></li>
					<li class="sidebar-list"><a href="#/notice">공지사항 관리<span class="menu-icon fa fa-table"></span></a></li>
				</ul>
			</div>
			<div id="content-wrapper">
				<div class="page-content">
					<div class="row header">
						<div class="col-xs-12">
							<div class="user pull-right">
								<div class="item dropdown">
									<a href="#" class="dropdown-toggle"><img src="/admin/assets/images/avatar.jpg"></a>
									<ul class="dropdown-menu dropdown-menu-right">
										<li class="dropdown-header">관리자 계정</li>
										<li class="divider"></li>
										<li class="link"><a href="/admin/pages/logout.php">로그아웃</a></li>
									</ul>
								</div>
							</div>
							<div class="meta">
								<div class="page">달빛기사 :: 관리자 페이지</div>
							</div>
						</div>
					</div>
					<div ui-view></div>
				</div>
			</div>
		</div>
	</body>
</html>
