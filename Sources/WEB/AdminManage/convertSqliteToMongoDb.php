<?php
	header("Content-Type:text/html; charset=utf-8");
	ini_set('max_execution_time', 300000);
	
	$db = new SQLite3("bus_data.db");
	
	$pageCount = 0;
	$limit = 30000;
	$offset = $pageCount * $limit;
	$results = $db->query("SELECT * From BusStation ORDER BY stationIndex LIMIT " . $limit . " OFFSET " . $offset . ";");
	$busStationList = array();
	
	while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
		$row["gpsX"] = floatval($row["gpsX"]);
		$row["gpsY"] = floatval($row["gpsY"]);
		
		$row["location"] = array(
			"type" => "Point",
			"coordinates" => array(floatval($row["gpsX"]), floatval($row["gpsY"]))
		);
		
		echo "<pre>";
// 		print_r($row);
		print_r(json_encode($row, JSON_UNESCAPED_UNICODE));
	}
?>
