function initializeMap() {
	var driver_current_position_marker = new google.maps.Marker({
		position: driver_info.current_location,
		map: map
	});

	var driver_register_position_marker = new google.maps.Marker({
		position: driver_info.register_location,
		map: map
	});

	var driver_current_position_infowindow = new google.maps.InfoWindow({
		content: '등록자 현위치 [' + driver_info.phone.substring(driver_info.phone.length - 4, driver_info.phone.length) + "]"
	});

	driver_current_position_marker.addListener('click', function() {
		driver_current_position_infowindow.open(map, driver_current_position_marker);
	});

	driver_current_position_infowindow.open(map, driver_current_position_marker);

	var driver_register_position_infowindow = new google.maps.InfoWindow({
		content: '문의사항 등록 위치 [' + driver_info.phone.substring(driver_info.phone.length - 4, driver_info.phone.length) + "]"
	});

	driver_register_position_marker.addListener('click', function() {
		driver_register_position_infowindow.open(map, driver_register_position_marker);
	});

	driver_register_position_infowindow.open(map, driver_register_position_marker);
}
