var map;
var LIMIT_BOUNDS_NE = { lat : 83.91177715928563, lng : -175.8688546344638 };
var LIMIT_BOUNDS_SW = { lat : -43.27909581852835, lng : 57.56865844130516 };
var isHidePopupLayer = true;
var isHideSecondPopupLayer = true;
var distance_select = [1000, 2000, 3000, 4000, 5000];
var selected_distance_index = 0;
var running_station_list;
var shuttle_line_color_list;
var isShowOutback = false;
var station_info_window;
var station_info_window_element;
var stationMarkerList = [];
var shuttleLineList = [];
var outbackPolygon;
var lastCenter;

function initMap() {
	initializeDocument();
	
	map = new google.maps.Map(document.getElementById('map'), {
		center: driver_info.current_location,
		zoom: 16
	});
	
	station_info_window = new google.maps.InfoWindow({
		content: ""
	});
	
	google.maps.event.addListener(map, 'idle', function() {		
		if(map.getZoom() < 15) {
			removeAllObjectOnMap();			
			return;
		}
		
		if(lastCenter != null) {
			var distance = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(lastCenter.lat(), lastCenter.lng()), new google.maps.LatLng(map.getCenter().lat(), map.getCenter().lng()));
			
			if(distance <= 500) {
				return;
			}
		}
		
		getNearStationList(map.getCenter());
		
		lastCenter = map.getCenter();
	});
	
	initializeMap();
}

function initializeDocument() {
	$("body").on("click", function() {
		if(isHidePopupLayer) {
			hidePopupLayer();
		}
		
		if(isHideSecondPopupLayer) {
			hideSecondPopupLayer();
		}
		
		isHidePopupLayer = true;
		isHideSecondPopupLayer = true;
	});
	
	$("#modal_popup_close").on("click", function() {
		hidePopupLayer();
		hideSecondPopupLayer();
	});
	
	$("#second_modal_popup_close").on("click", function() {
		hideSecondPopupLayer();
	});
	
	$('#btnStationList').on("click", function() {
		isHidePopupLayer = false;
		
		adjustPopupPosition($(this));
		
		showStationList();
	});
	
	$('#btnShuttleLineColor').on("click", function() {
		isHidePopupLayer = false;
		
		adjustPopupPosition($(this));
		
		showShuttleLineColor();
	});
	
	$("#btnShowOutback").on("click", function() {
		isShowOutback = !isShowOutback;
		
		changeBtnOutbackShowText();
		
		removeBackgroundPolygon();
		
		if(isShowOutback) {
			showOutbackPolygon();
		}
	});
	
	$('#station_list').stickySectionHeaders({
		stickyClass     : 'sticky',
		headlineSelector: 'strong'
	});
	
	$("#popup_layer").on("click", function() {
		isHidePopupLayer = false;
		isHideSecondPopupLayer = false;
	});
	
	$("#second_popup_layer").on("click", function() {
		isHideSecondPopupLayer = false;
		isHidePopupLayer = false;
	});
		
	changeBtnOutbackShowText();
}

function showOutbackPolygon() {
	removeBackgroundPolygon();
	
	var backgroundCoordinates = [
		{ lat : LIMIT_BOUNDS_SW.lat, lng : LIMIT_BOUNDS_NE.lng },
		LIMIT_BOUNDS_NE,
		{ lat : LIMIT_BOUNDS_NE.lat, lng : LIMIT_BOUNDS_SW.lng },
		LIMIT_BOUNDS_SW
	];
		
	var stationMarkerCircleList = [];
	
	var unionPoly = null;
	if(running_station_list) {
		unionPoly = getUnionStationPolygon(running_station_list);
	}
	
	if(unionPoly != null) {
		if(Array.isArray(unionPoly)) {
			$.each(unionPoly, function(_union_poly_key, _union_poly) {
				var paths = _union_poly.getPaths();
				
				for(var i = 0; i < paths.getLength(); i++) {
					var unionPaths = [];
					var path = paths.getAt(i);

					for(var j = 0; j < path.getLength(); j++) {
						unionPaths.push(path.getAt(j));
					}
					
					stationMarkerCircleList.push(unionPaths);
				}
			});
		} else {
			var paths = unionPoly.getPaths();
			
			for(var i = 0; i < paths.getLength(); i++) {
				var unionPaths = [];
				var path = paths.getAt(i);
				
				for(var j = 0; j < path.getLength(); j++) {
					unionPaths.push(path.getAt(j));
				}
				
				stationMarkerCircleList.push(unionPaths);
			}
			
		}
	}
	
	stationMarkerCircleList.unshift(backgroundCoordinates);
	
	outbackPolygon = new google.maps.Polygon({
		paths: stationMarkerCircleList,
		strokeWeight: 0,
		fillColor: '#FF0000',
		fillOpacity: 0.35,
		map: map
	});
}

function getUnionStationPolygon(_station_list) {
	var geometries = [];
	$.each(_station_list, function(_station_type, _list_data) {
		if(_list_data.length > 0) {
			$.each(_list_data, function(k, v) {
				geometries.push(getGeometry(v.location));
			});
		}
	});
	
	return cascadeUnion(geometries);
}

function getGeometry(_location) {
	var path = getCirclePoints(_location, 1000, 1);
	var wtk = getPointsToWTK(path);
	
	var wktReader = new jsts.io.WKTReader();
	
    return wktReader.read(wtk);
}

function cascadeUnion(_geometries) {
	var wicket = new Wkt.Wkt();
	var dissolvedGeometry = jsts.operation.union.CascadedPolygonUnion.union(_geometries);
	
	var wktWriter = new jsts.io.WKTWriter();
	var wkt = wktWriter.write(dissolvedGeometry);
	wicket.read(wkt);
	
	var polyOptions = {
		strokeWeight: 0,
		fillColor: '#FF0000',
		fillOpacity: 0.35
	};
	
	return wicket.toObject(polyOptions);
}

function getPointsToWTK(_point) {
	var poly = this;
	var wkt = "POLYGON((";
	
	for(var i = 0; i < _point.length; i++) {
		wkt += _point[i].lng().toString() + " " + _point[i].lat().toString() + ",";
	}
	
	wkt += _point[0].lng().toString() + " " + _point[0].lat().toString();
	
	wkt += "))";
	
	return wkt;
};

function changeBtnOutbackShowText() {	
	if(isShowOutback) {
		$("#btnShowOutback").css({
			"background-color" : "#f0ad4e",
			"color" : "#fff",
			"font-weight" : "bold"
		});
		
		$("#btnShowOutback").text("오지 ON");
	} else {
		$("#btnShowOutback").css({
			"background-color" : "#fff",
			"color" : "#f0ad4e",
			"font-weight" : "bold"
		});
		
		$("#btnShowOutback").text("오지 OFF");
	}
}

function showRouteListByStation(_station_type, _ars_id, _bus_route_area, _station_name) {
	initSecondPopupLayer();
	
	var contentHtml = "";
	
	contentHtml += "<div class='row'>";
	contentHtml += "<div id='bus_route_list' class='station_list'></div>";
	contentHtml += "</div>";
	
	var arsId = _ars_id;
	if(_station_type == "bus") {
		arsId += " (" + _bus_route_area + ")";
	}
	
	$("#second_modal_popup_label").html(_station_name + " [" + arsId+ "]");
	$("#second_modal_popup_content").html(contentHtml);
	$("#second_modal_popup_footer").hide();
		
	$('#second_popup_layer').show();
	
	if(_station_type == "bus") {
		getBusRouteListByStation(_ars_id, _bus_route_area);
	} else if(_station_type == "shuttle") {
		getShuttleRouteListByStation(_ars_id);
	}
	
	adjustListViewSize($('#second_popup_layer'), $("#bus_route_list > ul"), 85);
}

function getBusRouteListByStationHtml(_route_data) {
	_route_data.sort(function(a, b) {
		return a.busRouteTypeName - b.busRouteTypeName;
	});
	
	var route_list = _.groupBy(_route_data, function(route_data){ 
		return route_data.busRouteTypeName;
	});
	
	var contentHtml = "";
	contentHtml += "<ul>";
	
	$.each(route_list, function(key, data) {
		contentHtml += "<li>";
		contentHtml += "<strong>" + key + "</strong>";
		contentHtml += "<ul>";
		
		$.each(data, function(key2, data2) {
			contentHtml += "<li data='' class='bus_route_info'>";
			contentHtml += "<div class='bus_route_name_box'>";
			contentHtml += "<div class='bus_route_name'>" + data2.busRouteName + "</div>";
			contentHtml += "<div class='bus_route_next_info_box'>";
			contentHtml += "<div class='pull-left'>" + data2.direction + " 방면</div>";
			contentHtml += "<div class='pull-right'></div>";
			contentHtml += "</div>";
			contentHtml += "<div class='bus_route_next_info_box'>";
			contentHtml += "<div class='pull-left'>다음 : " + data2.nextStation + "</div>";
			contentHtml += "<div class='pull-right'></div>";
			contentHtml += "</div>";
			contentHtml += "</div>";
			contentHtml += "<div class='bus_route_more'>";
			contentHtml += "<img src='/admin/assets/images/list_more.png'/>";
			contentHtml += "</div>";
			contentHtml += "<div class='clear_both'></div>";
			contentHtml += "</li>";
		});
		
		contentHtml += "</ul>";
		contentHtml += "</li>";
	});
	
	contentHtml += "</ul>";
	
	$("#bus_route_list").html(contentHtml);
	
	adjustListViewSize($('#second_popup_layer'), $("#bus_route_list > ul"), 85);
	
	$('#bus_route_list').stickySectionHeaders({
		stickyClass     : 'sticky',
		headlineSelector: 'strong'
	});
	
	$('#second_popup_layer').css({"min-width" : 550});
}

function getShuttleRouteListByStationHtml(_route_data) {
	var contentHtml = "";
	contentHtml += "<ul>";
	contentHtml += "<li>";
	contentHtml += "<strong>대리셔틀</strong>";
	contentHtml += "<ul>";
	
	$.each(_route_data, function(key, data) {
		contentHtml += "<li data='' class='bus_route_info'>";
		contentHtml += "<div class='bus_route_name_box'>";
		contentHtml += "<div class='bus_route_name'>" + data.name + "</div>";
		contentHtml += "<div class='shuttle_route_desc_box'>" + data.desc + "</div>";
		contentHtml += "</div>";
		contentHtml += "<div class='bus_route_more'>";
		contentHtml += "<img src='/admin/assets/images/list_more.png'/>";
		contentHtml += "</div>";
		contentHtml += "<div class='clear_both'></div>";
		contentHtml += "</li>";
	});
	
	contentHtml += "</ul>";
	contentHtml += "</li>";
	contentHtml += "</ul>";
	
	$("#bus_route_list").html(contentHtml);
	
	adjustListViewSize($('#second_popup_layer'), $("#bus_route_list > ul"), 85);
	
	$('#bus_route_list').stickySectionHeaders({
		stickyClass     : 'sticky',
		headlineSelector: 'strong'
	});
	
	$('#second_popup_layer').css({"min-width" : 550});
}

function getBusRouteListByStation(_ars_id, _bus_route_area) {
	var params = {};
	params.type = "get_bus_route_list_by_station";
	params.ars_id = _ars_id;
	params.bus_route_area = _bus_route_area;
	
	sendAjaxPost(params, function(_ret_data) {
		getBusRouteListByStationHtml(_ret_data);
	});
	
//	$.post("/admin/a/ajax.php", params, function(_ret_data) {
//		var ret_data = JSON.parse(_ret_data);
//		if(ret_data.result == "OK") {
//			getBusRouteListByStationHtml(ret_data.data);
//		} else if(ret_data.result == "NOT_LOGIN") {
//			location.href = "/admin";
//		} else if(ret_data.result == "PERMISSION_DENIED") {
//			
//		} else {
//			alert(ret_data.message);
//		}
//	}, "text");
}

function getShuttleRouteListByStation(_ars_id) {
	var params = {};
	params.type = "get_shuttle_route_list_by_station";
	params.ars_id = _ars_id;
	
	sendAjaxPost(params, function(_ret_data) {
		getShuttleRouteListByStationHtml(ret_data);
	});
	
//	$.post("/admin/a/ajax.php", params, function(_ret_data) {
//		var ret_data = JSON.parse(_ret_data);
//		if(ret_data.result == "OK") {
//			getShuttleRouteListByStationHtml(ret_data.data);
//		} else if(ret_data.result == "NOT_LOGIN") {
//			location.href = "/admin";
//		} else {
//			alert(ret_data.message);
//		}
//	}, "text");
}

function showShuttleLineColor() {
	initPopupLayer();
	
	var contentHtml = "";
	
	contentHtml += "<div class='row'>";
	contentHtml += "<div id='shuttle_line_color_list'>";
	contentHtml += "</div>";
	contentHtml += "</div>";
	
	$("#modal_popup_label").text("셔틀 노선색");
	$("#modal_popup_content").html(contentHtml);
	$("#modal_popup_footer").hide();
	
	getShuttleLineColorListHtml();
}

function getShuttleLineColorListHtml() {
	var contentHtml = "";
	contentHtml += "<div class='list-group'>";
	
	$.each(shuttle_line_color_list, function(k, v) {
		contentHtml += "<a href='#' class='list-group-item'>";
		contentHtml += "<span class='shuttle_color' style='background-color: " + shuttleLineColorList[v.busId] + "'></span>";
		contentHtml += "<span class='shuttle_name'>" + v.name + "</span>";
		contentHtml += "</a>";
	});
		
	contentHtml += "</div>";
	
	$("#shuttle_line_color_list").html(contentHtml);
	
	$('#popup_layer').show();
	
	adjustListViewSize($('#popup_layer'), $("#shuttle_line_color_list"), 30);
	
	$('#popup_layer').css({"min-width" : 250});
}

function showStationList() {
	initPopupLayer();
	
	var contentHtml = "";
	
	contentHtml += "<div class='row'>";
	contentHtml += "<form class='form-horizontal'>";
	contentHtml += "<div class='form-group form-group-md'>";
	contentHtml += "<label class='col-sm-2 control-label' for='select_distance'>검색범위</label>";
	contentHtml += "<div class='col-sm-5'>";
	contentHtml += "<select id='select_distance' class='form-control'></select>";
	contentHtml += "</div>";
	contentHtml += "</div>";
	contentHtml += "</form>";
	contentHtml += "<div id='station_list' class='station_list'></div>";
	contentHtml += "</div>";
	
	$("#modal_popup_label").text("가까운 정류장");
	$("#modal_popup_content").html(contentHtml);
	$("#modal_popup_footer").hide();
	
	setDistanceSelect();
	
	getStationListHtml();
	
	$("#select_distance").off("change").on("change", function() {
		selected_distance_index = $(this).val();
		getStationListHtml();
	});
	
	$(".station_info").off("click").on("click", function() {
		var data = $(this).attr("data");
		var data_array = data.split("_");
		
		adjustSecondPopupPosition($('#popup_layer'));
		
		if(data_array[0] == "bus" || data_array[0] == "shuttle") {
			showRouteListByStation(data_array[0], data_array[1], data_array[2], data_array[3]);
		} else if(data_array[0] == "subway") {
			window.open("http://map.daum.net/subway/subwayStationInfo?type=4&stationId=SES" + data_array[1], "_blank", "width=680,height=690");
		}
	});
	
	$('#popup_layer').show();
	
	adjustListViewSize($('#popup_layer'), $("#station_list > ul"), 85);
}

function setDistanceSelect() {
	var html = "";
	$.each(distance_select, function(k, v) {
		html += "<option value='" + k + "' " + (k == selected_distance_index ? "selected" : "") + ">" + (v / 1000) + "km</option>";		
	});
	
	$("#select_distance").html(html);
}

function adjustPopupPosition(_btn_clicked) {
	var offset = _btn_clicked.offset();
	var width = _btn_clicked.outerWidth(true);
	var height = _btn_clicked.outerHeight(true);
	
	var top = (offset.top + (height / 2)) - 30;
	var left = offset.left + width + 20;
		
	$('#popup_layer').css({top: top, left: left});
}

function adjustSecondPopupPosition(_btn_clicked) {
	var offset = _btn_clicked.offset();
	var width = _btn_clicked.outerWidth(true);
	var height = _btn_clicked.outerHeight(true);
	
	var top = offset.top;
	var left = offset.left + width + 20;
	
	$('#second_popup_layer').css({top: top, left: left});
}

function adjustListViewSize(_popup_layer, _list_view, _paddings) {
	var body_height = $("body").height();
	var header_height = $('.modal-header').outerHeight(true);
	var content_padding = parseInt($("#modal_popup_content").css("padding").replace("px", ""));
	var offset = _popup_layer.offset();
	
	var height = body_height - offset.top - header_height - (content_padding * 2) - _paddings;
	
	_list_view.css({"max-height": height});
}

function hidePopupLayer() {
	$('#popup_layer').hide();
	initPopupLayer();
}

function initPopupLayer() {
	$("#modal_popup_label").text("");
	$("#modal_popup_content").html("");
	$("#modal_popup_footer").html("");
}

function hideSecondPopupLayer() {
	$('#second_popup_layer').hide();
	initSecondPopupLayer();
}

function initSecondPopupLayer() {
	$("#second_modal_popup_label").text("");
	$("#second_modal_popup_content").html("");
	$("#second_modal_popup_footer").html("");
}

function getIndexByStationType(_station_type) {
	switch(_station_type) {
		case "shuttle" : 
			return 0;
		case "subway" :
			return 1;
		case "carpool" : 
			return 2;
		case "bus" :
			return 3;
		default :
			return 4;
	}
}

function getHeaderTitleByStationType(_station_type) {
	switch(_station_type) {
		case "shuttle" : 
			return "대리셔틀";
		case "subway" :
			return "지하철역";
		case "carpool" : 
			return "택시카풀 등록자";
		case "bus" :
			return "시내버스 / 마을버스 / 심야 정거장";
		default :
			return "기타";
	}
}

function getStationListHtml() {
	var showing_shuttle_station_list = getStationListInSelectedDistance();
	
	var contentHtml = "";
	contentHtml += "<ul>";
	
	if(showing_shuttle_station_list) {
		$.each(showing_shuttle_station_list, function(_station_type, _list_data) {
			if(_list_data.length > 0) {
				contentHtml += "<li>";
				contentHtml += "<strong>" + getHeaderTitleByStationType(_station_type) + "</strong>";
				contentHtml += "<ul>";
				
				$.each(_list_data, function(k, v) {
					var distance;
					if(v.distance < 1000) {
						distance = v.distance.toFixed(2) + "m";
					} else {
						distance = ((v.distance / 1000).toFixed(2));
						distance += "km";
					}
					
					var arsId = v.ars_id;
					if(v.station_type == "bus") {
						arsId += " (" + v.bus_route_area + ")";
					}
					
					var data = _station_type + "_" + v.ars_id + "_" + v.bus_route_area + "_" + v.station_name;
					
					contentHtml += "<li data='" + data + "' class='station_info'>";
					contentHtml += "<div class='station_name_box'>";
					contentHtml += "<div class='station_name'>" + v.station_name + "</div>";
					contentHtml += "<div class='station_arsid_box'><span style='margin-right: 10px;'>[" + arsId + "]</span> | <span class='station_distance'>" + distance + "</span></div>";
					contentHtml += "</div>";
					contentHtml += "<div class='station_info_more'>";
					contentHtml += "<img src='/admin/assets/images/list_more.png'/>";
					contentHtml += "</div>";
					contentHtml += "<div class='clear_both'></div>";
					contentHtml += "</li>";
				});
				
				contentHtml += "</ul>";
				contentHtml += "</li>";
			}
		});
	}
	
	contentHtml += "</ul>";
	
	$("#station_list").html(contentHtml);
	
	adjustListViewSize($('#popup_layer'), $("#station_list > ul"), 85);
	
	$('#station_list').stickySectionHeaders({
		stickyClass     : 'sticky',
		headlineSelector: 'strong'
	});
	
	$('#popup_layer').css({"min-width" : 550});	
}

function getStationListInSelectedDistance() {
	var showing_shuttle_station_list = null;
	
	if(running_station_list) {
		showing_shuttle_station_list = {};
		$.each(running_station_list, function(_station_type, _list_data) {
			showing_shuttle_station_list[_station_type] = [];
			
			if(_list_data.length > 0) {
				$.each(_list_data, function(k, v) {
					if(v.distance <= distance_select[selected_distance_index]) {
						showing_shuttle_station_list[_station_type].push(v);
					}
				});
			}
			
			showing_shuttle_station_list[_station_type].sort(function(a, b) {
				return a.distance - b.distance;
			});
		});
	}
		
	return showing_shuttle_station_list;
}

function getNearStationList(_center_position) {
	var params = {};
	params.type = "get_near_station";
	params.lat = _center_position.lat();
	params.lng = _center_position.lng();
	params.range = 5000;
	
	sendAjaxPost(params, function(_ret_data) {
		showStationMarker(_ret_data);
		
		drawShuttleLine(_ret_data.shuttleLineList);
		
		shuttle_line_color_list = _ret_data.shuttleInfoList;
	});
	
//	$.post("/admin/a/ajax.php", params, function(_ret_data) {
//		var ret_data = JSON.parse(_ret_data);
//		if(ret_data.result == "OK") {
//			showStationMarker(ret_data.data);
//			drawShuttleLine(ret_data.data.shuttleLineList);
//			
//			shuttle_line_color_list = [];
//			$.each(ret_data.data.shuttleInfoList, function(k, v) {
//				shuttle_line_color_list = ret_data.data.shuttleInfoList;
//			});
//		} else if(ret_data.result == "NOT_LOGIN") {
//			location.href = "/admin";
//		} else {
//			alert(ret_data.message);
//		}
//	}, "text");
}

function removeBackgroundPolygon() {
	if(outbackPolygon) {
		outbackPolygon.setMap(null);
		outbackPolygon = null;
	}
}

function removeAllMarkers() {
	$.each(stationMarkerList, function(k, v) {
		v.setMap(null);
	});
	
	stationMarkerList = [];
	running_station_list = {};
}

function removeAllShuttlLines() {
	$.each(shuttleLineList, function(k, v) {
		v.setMap(null);
	});
	
	shuttleLineList = [];
}

function removeAllObjectOnMap() {
	removeBackgroundPolygon();
	removeAllMarkers();
	removeAllShuttlLines();
	
	isShowOutback = false;
	changeBtnOutbackShowText();
	
	lastCenter = null;
	
	$('#btnStationList').attr("disabled", true);
	$('#btnShuttleLineColor').attr("disabled", true);
}

var polygons = null;
function showStationMarker(_data) {
	removeAllMarkers();
	
	removeAllShuttlLines();
	
	running_station_list = {};
	
	running_station_list.shuttle = [];
	$.each(_data.shuttleMarkerList, function(k, v) {
		var m = new google.maps.Marker({
			position: v.location,
			map: map,
			icon: "/admin/assets/images/pin_shuttle_40.png",
			info_data : v
		});
		
		running_station_list.shuttle.push({
			location: v.location,
			station_type : "shuttle",
			station_name : v.text,
			ars_id : v.arsId,
			distance : v.distance,
			bus_route_area : "서울"
		});
		
		m.addListener('click', function() {
			var data = "shuttle_" + v.arsId + "_" + "서울" + "_" + v.text;
			var info_window_content = "<div id='info_window_content' data='" + data + "' style='cursor: pointer;'>" + "[대리셔틀]" + v.text + "</div>";
			
			setInfowindow(m, info_window_content);
		});
		
		stationMarkerList.push(m);
	});
	
	running_station_list.carpool = [];
	$.each(_data.carpoolRegistrantList, function(k, v) {
		var m = new google.maps.Marker({
			position: v.location,
			map: map,
			icon: "/admin/assets/images/pin_taxi_registrant_40.png",
			info_data : v
		});
		
		running_station_list.carpool.push({
			location: v.location,
			station_type : "carpool",
			station_name : "등록자 " + v.registrant + " 기사님",
			ars_id : v.taxiId,
			distance : v.distance,
			bus_route_area : "서울"
		});
		
		m.addListener('click', function() {
			var station_name = "등록자" + v.registrant + "기사님";
			var data = "carpool_" + v.taxiId + "_" + "서울" + "_" + station_name;
			var info_window_content = "<div id='info_window_content' data='" + data + "' style='cursor: pointer;'>" + "[택시카풀]" + station_name + "</div>";
			
			setInfowindow(m, info_window_content);
		});
		
		stationMarkerList.push(m);
	});
	
	running_station_list.subway = [];
	$.each(_data.subwayStationList, function(k, v) {
		var m = new google.maps.Marker({
			position: v.location,
			map: map,
			icon: "/admin/assets/images/pin_subway_40.png"
		});
		
		running_station_list.subway.push({
			location: v.location,
			station_type : "subway",
			station_name : v.stationName,
			ars_id : (v.stationCodeDaum != null && v.stationCodeDaum.length > 0) ? v.stationCodeDaum : v.stationCode,
			distance : v.distance,
			bus_route_area : "서울"
		});
		
		m.addListener('click', function() {
			var ars_id = (v.stationCodeDaum != null && v.stationCodeDaum.length > 0) ? v.stationCodeDaum : v.stationCode;
			var data = "subway_" + ars_id + "_" + "서울" + "_" + v.stationName;
			var info_window_content = "<div id='info_window_content' data='" + data + "' style='cursor: pointer;'>" + "[지하철]" + v.stationName + "</div>";
			
			setInfowindow(m, info_window_content);
		});
		
		stationMarkerList.push(m);
	});
	
	running_station_list.bus = [];
	$.each(_data.busStationList, function(k, v) {
		var m = new google.maps.Marker({
			position: v.location,
			map: map,
			icon: "/admin/assets/images/pin_bus_40.png"
		});
		
		running_station_list.bus.push({
			location: v.location,
			station_type : "bus",
			station_name : v.stationName,
			bus_route_area : v.busRouteArea,
			ars_id : v.arsId,
			distance : v.distance
		});
		
		m.addListener('click', function() {
			var data = "bus_" + v.arsId + "_" + v.busRouteArea + "_" + v.stationName;
			var info_window_content = "<div id='info_window_content' data='" + data + "'>" + "[버스]" + v.stationName + "(" + v.busRouteArea + ")" + "</div>";
			
			setInfowindow(m, info_window_content);
		});
		
		stationMarkerList.push(m);
	});
	
	if(stationMarkerList.length <= 0) {
		$('#btnStationList').attr("disabled", true);
	} else {
		$('#btnStationList').attr("disabled", false);
	}
	
//	console.log("station count : " + stationMarkerList.length);
	
	if(isShowOutback) {
		showOutbackPolygon();
	}
}

function setInfowindow(_marker, _content) {
	station_info_window.setContent(_content);
	station_info_window.open(map, _marker);
	
	var info_root = $("#info_window_content").parent("div").parent("div").parent("div").parent("div");
	info_root.css({cursor: "pointer"});
	info_root.off("click").on("click", function() {
		isHideSecondPopupLayer = false;
		var data = $("#info_window_content").attr("data");
		if(data) {
			var data_array = data.split("_");
			
			if(data_array[0] == "bus" || data_array[0] == "shuttle") {
				adjustSecondPopupPosition($('#btnStationList'));
				showRouteListByStation(data_array[0], data_array[1], data_array[2], data_array[3]);
			}
		}
	});
}

function drawShuttleLine(_data) {
	var list_count = 0;
	
	$.each(_data, function(k, v) {
		v.shuttle_point.sort(function(a, b) {
			return a.index - b.index;
		});
		
		var isDrawLine = false;
		var prevPointIndex = null;
		var shuttleLinePoints = [];
		var shuttleLineColor = shuttleLineColorList[v._id.busId];
		$.each(v.shuttle_point, function(k, shuttle_point_info) {
			if(prevPointIndex != null && (prevPointIndex + 1) != shuttle_point_info.index) {
				drawShuttlePolyLine(shuttleLineColor, shuttleLinePoints);
				shuttleLinePoints = [];
			} else {
				shuttleLinePoints.push({lat : shuttle_point_info.locations.coordinates[1], lng : shuttle_point_info.locations.coordinates[0]});
			}
		});
		
		drawShuttlePolyLine(shuttleLineColor, shuttleLinePoints);
		list_count++;
	});
	
	if(list_count == 0) {
		$('#btnShuttleLineColor').attr("disabled", true);
	} else {
		$('#btnShuttleLineColor').attr("disabled", false);
	}
}

function drawShuttlePolyLine(_color, _points) {
	var shuttlePath = new google.maps.Polyline({
		path: _points,
		geodesic: true,
		strokeColor: _color,
		strokeOpacity: 1.0,
		strokeWeight: 5,
		icons: [{
			icon: {
				path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
			},
			strokeColor: _color,
            fillColor: _color,
            fillOpacity: 1,
			repeat: '200px',
			offset: '100%'
		}],
		map: map
	});
	
	shuttleLineList.push(shuttlePath);
}

function getCirclePoints(_point, _radius, _direction) {
	var d2r = Math.PI / 180;   // degrees to radians
	var r2d = 180 / Math.PI;   // radians to degrees
	var earthsradius = 3963; // 3963 is the radius of the earth in miles
	var points = 50;
	var start = 0;
	var end = 0;
	
	var radius = (_radius / 1000) / 1.61;
	
	var rlat = (radius / earthsradius) * r2d;
	var rlng = rlat / Math.cos(_point.lat * d2r);
	
	var retCirclePoints = [];
	
	if (_direction == 1) {
		start = 0;
		end = points + 1;
	} else {
		start = points + 1;
		end = 0;
	}
	
	for (var i = start; (_direction == 1 ? i < end : i > end); i = i + _direction) {
		var theta = Math.PI * (i / (points / 2));
		ey = _point.lng + (rlng * Math.cos(theta));
		ex = _point.lat + (rlat * Math.sin(theta));
		
		retCirclePoints.push(new google.maps.LatLng(ex, ey));
	}
	
	return retCirclePoints;
}
