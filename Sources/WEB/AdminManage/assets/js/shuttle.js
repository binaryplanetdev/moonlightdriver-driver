$(document).ready(function() {
	setTimeout(function() {
		$("tbody > tr").click(function(){
			var data_id = $(this).attr("data-id").split("_");
			var index = $(this).index(".table > tbody > tr");

			if((data_id[0] == "answer") || (data_id[0] == "question" && $(event.target).is('.table > tbody > tr:eq(' + index + ') > td:eq(0), .table > tbody > tr:eq(' + index + ') > td:eq(1), .table > tbody > tr:eq(' + index + ') > td:eq(2), .table > tbody > tr:eq(' + index + ') > td:eq(3), .table > tbody > tr:eq(' + index + ') > td:eq(4), .table > tbody > tr:eq(' + index + ') > td:eq(5), .table > tbody > tr:eq(' + index + ') > td:eq(6)'))) {
				var params = {};
				params.type = "get_question_info";
				params.data_id = data_id[1];
				params.question_type = data_id[0];
				
				sendAjaxPost(params, function(_ret_data) {
					if(params.question_type == "question") {
						showQuestionDetailPopup(_ret_data);
						
						$('#btnShowAnswerPopup').on('click', function() {
							$("#first_modal").modal('hide');
							showQuestionAnswerPopup(true, _ret_data);
						});
					} else if(params.question_type == "answer") {
						showQuestionAnswerPopup(false, _ret_data);
					}
				});
				
//				$.post("/admin/a/ajax.php", params, function(_data) {
//					var ret_data = JSON.parse(_data);
//					console.log(ret_data);
//					if(ret_data.result == "OK") {
//						if(params.question_type == "question") {
//							showQuestionDetailPopup(ret_data.data);
//							
//							$('#btnShowAnswerPopup').on('click', function() {
//								$("#first_modal").modal('hide');
//								showQuestionAnswerPopup(true, ret_data.data);
//							});
//						} else if(params.question_type == "answer") {
//							showQuestionAnswerPopup(false, ret_data.data);
//						}
//					} else if(ret_data.result == "NOT_LOGIN") {
//						location.href = "/admin";
//					} else {
//						alert(ret_data.message);
//					}
//				}, "text");
			} else if($(event.target).is('.table > tbody > tr:eq(' + index + ') > td:eq(7)')) {
				showMapPopup(data_id[1]);
			}
		});
		
		$(".show_map").on("click", function() {
			var data_id = $(this).parent("td").parent("tr").attr("data-id").split("_");
			showMapPopup(data_id[1]);
		});
	}, 1);
});

function showMapPopup(_question_id) {
	window.open("/admin/pages/question_position_map.php?question_id=" + _question_id);
}

function showQuestionAnswerPopup(_isEdit, _data) {
	$("#second_modal_title").text("문의사항 답변보내기");
	$("#second_modal_content").html(getQuestionAnswerContentsHtml(_isEdit, _data));
	$("#second_modal_footer").html(getQuestionAnswerFooterHtml(_isEdit));
	
	$("#second_modal").modal('show');
	
	if(_isEdit) {
		$("#btnSaveAnswer").on('click', function() {
			var params = {};
			params.type = "save_question_answer";
			params.question_id = _data.questionId;
			params.answer = $("#answer").val();
			
			sendAjaxPost(params, function(_ret_data) {
				sendPush(params.question_id);
			});
			
//			$.post("/admin/a/ajax.php", params, function(_ret_data) {
//				var ret_data = JSON.parse(_ret_data);
//				if(ret_data.result == "OK") {
//					sendPush(params.question_id);
//				} else if(ret_data.result == "NOT_LOGIN") {
//					location.href = "/admin";
//				} else {
//					alert(ret_data.message);
//				}
//			}, "text");
		});
	}
}

function sendPush(_questionId) {
	var message = "문의사항에 대한 답변이 등록되었습니다.";
	$.get("http://115.68.104.30:8888/question/push?questionId=" + _questionId + "&message=" + message, function(_data) {
		var ret_data = JSON.parse(_data);
		if(ret_data.result.code == 100) {
			alert("답변이 등록 되었습니다.");
			location.reload();
		} else {
			alert(ret_data.result.detail);
		}
	}, "text");
}

function getQuestionAnswerContentsHtml(_isEdit, _data) {
	var html = "";
	
	html += "<div class='row'>";
	html += "<form class='form-horizontal'>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-2 control-label' for='answer'>답변</label>";
	html += "<div class='col-sm-9'><textarea class='form-control' id='answer' rows='25' " + (_isEdit ? "" : "readonly") + ">" + (_isEdit ? "" : _data.answer) + "</textarea></div>";
	html += "</div>";
	html += "</form>";
	html += "</div>";
	
	return html;
}

function getQuestionAnswerFooterHtml(_isEdit) {
	var html = "";
	
	if(_isEdit) {
		html += "<button type='button' id='btnSaveAnswer' class='btn btn-danger' data-dismiss='modal'>등록</button>";
	}
	
	html += "<button type='button' class='btn btn-default' data-dismiss='modal'>닫기</button>";
	
	return html;
}

function showQuestionDetailPopup(_data) {
	$("#first_modal_title").text("문의사항 상세보기");
	$("#first_modal_content").html(getQuestionDetailContentsHtml());
	$("#first_modal_footer").html(getQuestionDetailFooterHtml());
	
	$("#title").text(_data.title);
	$("#contents").text(_data.contents);
	$("#register_name").val(_data.name);
	$("#register_phone").val(_data.phone);
	$("#register_date").val(_data.createdDate);
	
	$("#first_modal").modal('show');
}

function getQuestionDetailContentsHtml() {
	var html = "";
	
	html += "<div class='row'>";
	html += "<form class='form-horizontal'>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-3 control-label' for='register_name'>등록자 이름</label>";
	html += "<div class='col-sm-8'><input type='text' class='form-control' id='register_name' readonly/></div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-3 control-label' for='register_phone'>등록자 전화번호</label>";
	html += "<div class='col-sm-8'><input type='text' class='form-control' id='register_phone' readonly/></div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-3 control-label' for='title'>제목</label>";
	html += "<div class='col-sm-8'><textarea class='form-control' id='title' rows='3' readonly></textarea></div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-3 control-label' for='contents'>내용</label>";
	html += "<div class='col-sm-8'><textarea class='form-control' id='contents' rows='20' readonly></textarea></div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-3 control-label' for='register_date'>등록일자</label>";
	html += "<div class='col-sm-8'><input type='text' class='form-control' id='register_date' readonly/></div>";
	html += "</div>";
	html += "</form>";
	html += "</div>";
	
	return html;
}

function getQuestionDetailFooterHtml() {
	var html = "";
	
	html += "<button type='button' id='btnShowAnswerPopup' class='btn btn-danger' data-dismiss='modal'>답변보내기</button>";
	html += "<button type='button' class='btn btn-default' data-dismiss='modal'>닫기</button>";
	
	return html;
}
