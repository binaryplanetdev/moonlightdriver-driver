<?php
	session_start();
	include_once "login.php";
	include_once 'db.php';
	
	if(!login($_SESSION["id"],$_SESSION["pw"]))
		exit;

	$type = $_POST["type"];
	$dbname = $_POST["dbname"];
	$id = $_POST["id"];
	$value = $_POST["value"];

	$collection = $db->$dbname;

	function CheckLevel($level) {
		if($_SESSION["level"] > $level) {
			echo "권한 없음";
			exit;
		}
	}

	if($type == "delete") {
		CheckLevel(1);
		
		$collection->remove( array('_id' => new MongoId($id)) );
		echo "삭제 완료.";
	} else if($type == "sub_delete") {
		CheckLevel(2);

		$id = split(",",$id);
		$collection->update(
			array('_id' => new MongoId($id[0])),
			array('$pull' => array('lists'=> array('_id' => new MongoId($id[1]))), '$inc' => array('__v' => -1))
		);
		
		echo "삭제 완료.";
	} else if($type == "sub_modify") {
		CheckLevel(2);

		$id = split(",", $id);
		$name = $_POST["name"];
		$lat = floatval($_POST["lat"]);
		$lng = floatval($_POST["lng"]);

		$collection->update(
			array('_id' => new MongoId($id[1]), 'lists._id' => new MongoId($id[2])),
			array('$set' => array("lists.$.text" => $name, "lists.$.lat" => $lat, "lists.$.lng" => $lng))
		);

		echo "수정 완료";
	} else if($type == "category_change") {
		CheckLevel(2);

    	$collection->update(
		array('_id' => new MongoId($id)),
		array('$set' => array("category" => $value)));
		echo "변경 완료.";
	} else if($type == "modify_desc") {
		CheckLevel(2);

		$collection->update(
		array('_id' => new MongoId($id)),
		array('$set' => array("desc" => $value)));
	} else if($type == "sub_modify_desc") {
		CheckLevel(2);

		$id = split(",",$id);
		$collection->update(array('_id' => new MongoId($id[0]), 'lists._id' => new MongoId($id[1])), array('$set' => array("lists.$.text" => $value)));
	} else if($type == "get_near_gps") {
		CheckLevel(2);

		$result = $collection->command(array(
                'geoNear' => 'user',
                'near' => $lnglat,
                'spherical' => true,
                'num' => 10
        ));

		var_dump($result);
	} else if($type == "sub_add") {
		CheckLevel(2);

		$position = intval($_POST["position"]);
		$name = $_POST["name"];
        $lat = floatval($_POST["lat"]);
        $lng = floatval($_POST["lng"]);
        $exceptOutback = $_POST["except_outback"];
		$index = intval($position + 1);
		
		$new_list = array('_id' => new MongoId(), 'index' => $index, 'text' => $name, 'lat' => $lat, 'lng' => $lng, 'exceptOutback' => $exceptOutback, 'createdAt' => new MongoDate());

		$ret = $collection->find(array('_id' => new MongoId($id)));
		$lists = null;
		$list_count = 1;
		foreach($ret as $doc) {
			$lists = $doc["lists"];

			foreach($lists as &$row) {
				if($row["index"] >= $index) {
					$row["index"] = $row["index"] + 1;
				}

				$list_count++;
			}

			array_splice($lists, $position, 0, array($new_list));
		}

		$ret = $collection->update(array('_id' => new MongoId($id)), array('$set' => array('lists' => $lists, '__v' => $list_count)));

		$added_list = array(
			'doc_id' => $id,
			'_id' => $new_list['_id']->{'$id'},
			'text' => $new_list['text'],
			'index' => $new_list['index'],
			'lat' => $new_list['lat'],
			'lng' => $new_list['lng'],
			'exceptOutback' => $new_list['exceptOutback'],
			'createdAt' => date( "Y-m-d H:i:s", $new_list['createdAt']->sec)
		);

		echo json_encode($added_list);
		exit;
	} else if($type == "sub_except") {
		$check_list = split(",", $_POST["check_list"]);
		
		$ret = $collection->find(array('_id' => new MongoId($id)));
		
		foreach($ret as $doc) {
			$lists = $doc["lists"];
		
			foreach($lists as &$row) {
				if(in_array($row['_id']->{'$id'}, $check_list)) {
					$row["exceptOutback"] = 1;
				} else {
					$row["exceptOutback"] = 0;
				}
			}
		}
		
		$ret = $collection->update(array('_id' => new MongoId($id)), array('$set' => array('lists' => $lists)));
		
		echo "적용 완료";
	}
?>
