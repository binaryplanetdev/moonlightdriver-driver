<?
	if(!login($_SESSION["id"],$_SESSION["pw"]))
		exit;
?>
<link href="./css/manage.css" rel="stylesheet">
<ul class="nav nav-tabs nav-justified" role="tablist">
  <li role="presentation"><a href="#" id="drivers" role="tab" data-toggle="tab">기사 관리</a></li>
  <li role="presentation"><a href="#" id="calls" role="tab" data-toggle="tab">대리 기사 요청 관리</a></li>
  <li role="presentation"><a href="#" id="buses" role="tab" data-toggle="tab">버스노선도 수집 관리</a></li>
  <li role="presentation"><a href="#" id="busnews" role="tab" data-toggle="tab">버스노선도 수집 관리 NEW</a></li>
  <li role="presentation"><a href="#" id="logout" role="tab" data-toggle="tab">로그아웃</a></li>
</ul>
<div class="Contents"></div>
<script src="./js/manage.js"></script>