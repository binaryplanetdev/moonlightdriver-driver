$("#chk_look").click(function() {
	$(this).removeClass("unchecked");
 	$(this).addClass("checked");
 	$("#chk_volunteer").removeClass("checked");
 	$("#chk_volunteer").addClass("unchecked");
});
$("#chk_volunteer").click(function() {
	$(this).removeClass("unchecked");
 	$(this).addClass("checked");
 	$("#chk_look").removeClass("checked");
 	$("#chk_look").addClass("unchecked");
});
$("#submit_button").click(function() {
	var title = $("input[name$='title']").val();
	var type;
	if($("#chk_look").hasClass("checked"))
		type = '0';
	else if($("#chk_volunteer").hasClass("checked"))
		type = '1';

	var data = "&title=" + title + "&type=" + type;
	 $('.fill').each(function(){
		data = data + "&" + $(this).attr('name') + "=" + $(this).val();
    });
	$.ajax(
	{
        type:"POST",
        url : "./ajax.php",
        data : "query=content_insert" + data,
        async: false,
        success : function(response) {
            data = response;
            alert("글쓰기 완료");
            window.location.href = "./content.php?n=" + response;
        },
        error: function() {
            alert('통신 상태를 확인해주세요.');
		}
	});
});
$(".left").click(function() {
	window.location.href = "./list.php";
});