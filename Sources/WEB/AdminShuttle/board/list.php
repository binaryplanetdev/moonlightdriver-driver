<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, user-scalable=no">
		<meta name="viewport" content="initial-scale=1, maximum-scale=1">
		<link href="./assets/css/style.css" rel="stylesheet">
		<script src="./assets/js/jquery-1.11.1.min.js"></script>
	</head>
	<body>
		<div id="container">
			<div class="header">
				<div class="left" style="visibility: hidden;"></div>
				<span class="menu">2인1조 게시판</span>
				<div class="right">
					<a href="./write.php" class="submit_button">글쓰기</a>
				</div>
			</div>
		</div>
		<script src="./assets/js/list.js"></script>
		<script type="text/javascript">
		  WebFontConfig = {
		    custom: {
		        families: ['Nanum Gothic'],
		        urls: ['http://fonts.googleapis.com/earlyaccess/nanumgothic.css']
		    }
		  };
		  (function() {
		    var wf = document.createElement('script');
		    wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
		      '://ajax.googleapis.com/ajax/libs/webfont/1.4.10/webfont.js';
		    wf.type = 'text/javascript';
		    wf.async = 'true';
		    var s = document.getElementsByTagName('script')[0];
		    s.parentNode.insertBefore(wf, s);
		  })();
 		</script>
	</body>
</html>