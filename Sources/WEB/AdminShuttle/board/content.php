<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, user-scalable=no">
		<meta name="viewport" content="initial-scale=1, maximum-scale=1">
		<link href="./assets/css/style.css" rel="stylesheet">
		<script src="./assets/js/jquery-1.11.1.min.js"></script>
		<?
			$n = $_GET["n"];
		?>
	</head>
	<body>
		<div id="container">
			<input type="hidden" class="n" id="<?=$n?>">
			<div class="header">
				<div class="left"></div>
				<span class="menu">상세보기</span>
				<div class="right">
					<!--<a href="#" class="submit_button">답글쓰기</a>-->
				</div>
			</div>
			<div id="top_menu">
				<div id="category" class=""></div>
				<div id="top_title">
					<p id="content_title"></p>
					<div id="bottom_title">
						<span id="nick"></span>
						<span id="date"></span>
					</div>
				</div>
				<div id="comment_count"><span id="count"></span></div>
			</div>
			<div id="content2">
				<div id="writes2">

				</div>
			</div>
			<div id="bottom_comment_count">
				<span>댓글 </span><span id="counts"></span>
			</div>
			<div id="comments">
			</div>
			<div id="submit_comment">
				<input id="text_nick" name="nick" placeholder="닉네임" type="text">
				<input id="text_comment" name="comment" placeholder="댓글을 입력해주세요." type="text">
				<a id="btn_submit" class="submit_button">등록</a>
			</div>
		</div>
		<script src="./assets/js/content.js"></script>
		<script type="text/javascript">
		  WebFontConfig = {
		    custom: {
		        families: ['Nanum Gothic'],
		        urls: ['http://fonts.googleapis.com/earlyaccess/nanumgothic.css']
		    }
		  };
		  (function() {
		    var wf = document.createElement('script');
		    wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
		      '://ajax.googleapis.com/ajax/libs/webfont/1.4.10/webfont.js';
		    wf.type = 'text/javascript';
		    wf.async = 'true';
		    var s = document.getElementsByTagName('script')[0];
		    s.parentNode.insertBefore(wf, s);
		  })();
 		</script>
	</body>
</html>