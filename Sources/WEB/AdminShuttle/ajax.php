<?
	session_start();
	header('Access-Control-Allow-Origin: *');
	include_once "login.php";
	if(!login($_SESSION["id"],$_SESSION["pw"]))
		exit;

	$n = 0;
	$type = $_POST["type"];
	$page = $_POST["page"];

	if($type == "logout")
	{
		$level = 1;
		session_destroy();
		session_unset();
		echo "<script>window.location.href = './'</script>";
	}
	if($type == "drivers")
	{
		$level = 1;
		$keyname = array("토큰" , "폰 번호" , "아이디" , "비밀번호" , "이름" , "유효여부");
		$key = array("token" , "phone" , "id" , "password" , "name" , "isValid");
	}
	else if($type == "calls")
	{
		$level = 1;
		$keyname = array("카드결제","타입","완료여부","시작여부","유효여부","금액");
		$key = array("byCard","callType","isDone","isStart","isValid","money");
	}
	else if($type == "buses")
	{
		$level = 2;
		$keyname = array("이름","구간","리스트(수)","삭제","지도 보기","핸드폰번호","카테고리");
		$key = array("name","desc","__v","delete", "map","phone","category");
	}
	else if($type == "busnews")
	{
		$level = 2;
		$keyname = array("이름","구간","리스트(수)","삭제","지도 보기","핸드폰번호","카테고리");
		$key = array("name","desc","__v","delete", "map","phone","category");
	}
	if($_SESSION["level"] > $level )
	{
		echo "<script>alert('권한 없음');</script>";
		exit;
	}
	if(empty($page))
		$page = 1;

	include 'db.php';
	$collection = $db->$type;
	$total = $collection->count();
	$pageitem = 20;

	$endpage = (int)($total/$pageitem);

	if($total%$pageitem!=0)
		$endpage++;

	$start = $page*$pageitem-$pageitem;
	$end = $start+$pageitem;

	if($page*$pageitem-$pageitem==(int)($total/$pageitem))
		$end = $start+$total%$pageitem;

	$cursor = $collection->find()->skip($start)->limit($pageitem)->sort(array('_id' => -1,'index' => -1));
	$ff = array("1 김포/사우동","2 고양/파주","3 의정부/양주","4 구리/남양주","5 인천/부천","6 안양/안산","7 수원/성남","8 강동/하남","9 합정","10 연신내/수유","11 노원/태릉","12 구로/화곡","13 교보");
	if($type == "busnews")
		include_once 'createBus.php';
?>
<input class="type" value="<?=$type?>" type="hidden">
<? if($type == "buses" || $type=="busnews"){ ?>
<div class="btn-group">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
    카테고리 선택 <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
  <? for($k=1;$k<=13;$k++) { ?>
    	<li id="select-category"><a href="#" name="<?=$k?>"><?=$ff[$k-1]?></a></li>
  <? } ?>
  </ul>
  <? } ?>
</div>
<table id="info" class="table table-condensed">
	<thead>
		<tr>
			<?
				for($i=0;$i<count($keyname);$i++)
					echo "<td>" . $keyname[$i] . "</td>";
			?>
		</tr>
	</thead>
<tbody>
	<?
		foreach ($cursor as $document)
		{
			echo "<tr id='$n'>";
			for($i=0;$i<count($key);$i++)
			{
				echo "<td>";
				if($key[$i] == "__v")
				{
					?>
						<button id="<?=$document["_id"]?>" type="button" class="btn btn-primary btn-default showlist" data-toggle="modal" data-target="#sub_<?=$document["_id"]?>>"><?=$document[$key[$i]]?></button>
						<button id="<?=$document["_id"]?>" type="button" class="btn btn-primary btn-default showlist hell" data-toggle="modal" data-target="#sub_<?=$document["_id"]?>>">*</button>
					<?
				}
				else if($key[$i] == "delete")
				{
					echo '<button type="button" id=' . "'" . $document["_id"] . "'" . 'class="btn btn-default delete sub_delete">삭제</button>';
				}
				else if($key[$i] == "map")
				{
					echo "<a href=./map.php?type=" . $type  . "&id=". $document["_id"] . " target='_blank'>" .  '<button type="button" class="btn btn-default">보기</button>' . '</a>';
				}
				else if($key[$i] == "category")
				{
					if(empty($document["category"]))
						$document["category"] = 0;
					?>
						<div class="dropdown" id="<?=$document["_id"]?>">
						  <button class="btn btn-default dropdown-toggle" type="button" id="category_button" data-toggle="dropdown" aria-expanded="true">
							<?=$ff[$document["category"]-1]?>
							<span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
						<? for($k=1;$k<=13;$k++) { ?>
							<li id="category" role="presentation"><a name="<?=$k?>" role="menuitem" tabindex="-1" href="#"><?=$ff[$k-1]?></a></li>
						<? } ?>
						  </ul>
						</div>
					<?
				}
				else
				{
					if($document[$key[$i]] == 1)
						$document[$key[$i]] = "O";
					else if($document[$key[$i]] == "")
						$document[$key[$i]] = "X";
					echo $document[$key[$i]];
				}
				echo "</td>";
			}
			$n++;
			echo "</tr>";
		}
	?>
</tbody>
</table>
<div id="listTable"></div>
<div class="page">
<?
	if($page>1)
	{
?>
	<i id="<?=$type?>-<?=$page-1?>" class="glyphicon glyphicon-chevron-left"></i>
<?	} echo $page . "/" . $endpage ?>
	<?
	if($page!=$endpage)
	{
	?>
	<i id="<?=$type?>-<?=$page+1?>" class="glyphicon glyphicon-chevron-right"></i>
<?	} ?>
</div>
<script src="./js/ajax.js"></script>