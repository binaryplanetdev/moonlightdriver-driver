$(".lists > tbody > tr").click(function(){
	var index = $(this).index(".lists > tbody > tr"); 
	if($(event.target).is(".lists > tbody > tr:eq(" + index + ") > td:eq(0),.lists > tbody > tr:eq(" + index + ") > td:eq(1),.lists > tbody > tr:eq(" + index + ") > td:eq(2),.lists > tbody > tr:eq(" + index + ") > td:eq(3),.lists > tbody > tr:eq(" + index + ") > td:eq(4)"))
		window.open("./map.php?type=" + $(".type").val() + "&id=" + $(event.target).closest("table").attr('id') + "&hl=" + $(this).find('td:eq(2)').text() + "," +  $(this).find('td:eq(3)').text() + "&subid=" + $(this).find('td:eq(0)').text() );

/*	
	var index = $(this).attr('id');
	if($(event.target).is(".lists > tbody > tr:eq(" + index + ") > td:eq(1)"))
	{
		if($(event.target).text() != "")
			$(".lists > tbody > tr:eq(" + index + ") > td:eq(1)").html("<input type='text' value='" + $(event.target).text() + "'>");
		$(".lists > tbody > tr:eq(" + index + ") > td:eq(1) > input[type=text]").keypress(function( event ) {
			if ( event.which == 13 && $(this).val() != "") {
				$(".lists > tbody > tr:eq(" + index + ") > td:eq(1)").html($(this).val());
					$.ajax({
				        type:"POST",
				        url : "./function.php",
				        data : "type=" + "sub_modify_desc" + "&dbname=" + $(".type").val() + "&id=" + $(".lists > tbody > tr:eq(" + index + ") > td:eq(5) > button").attr("id") + "&value=" + $(this).val(),
				        async: false,
				        success : function(response) {
				        },
				        error: function() {
				            alert('통신 상태를 확인해주세요.');
				        }
				    });
			    }
			});
	}
*/
});

function deletePoint(_btnThis) {
	var result = confirm("삭제 하시겠습니까?");
	if(result) {
		$.ajax({
        	type:"POST",
	        url : "./function.php",
    	    data : "type=" + "sub_delete" + "&dbname=" + $(".type").val() + "&id=" + $(_btnThis).attr("id"),
        	async: false,
	        success : function(response) {
    	        if(response!="권한 없음")
        	        $(_btnThis).closest("tr").remove();
				alert(response);
	        },
    	    error: function() {
        	    alert('통신 상태를 확인해주세요.');
	        }
    	});
	}
}

$(".sub_delete").click(function(){
	deletePoint(this);
});

function modifyPointData(_btnThis) {
	var parentTr = $(_btnThis).parent("td").parent("tr");
    var type = $(_btnThis).text() == "수정" ? "modify" : "save";

	if(type == "modify") {
		$(_btnThis).text("저장");
        parentTr.children("td:eq(1)").html("<input type='text' style='width: 90px;' value='" + parentTr.children("td:eq(1)").text() + "'>");
        parentTr.children("td:eq(2)").html("<input type='text' style='width: 90px;' value='" + parentTr.children("td:eq(2)").text() + "'>");
        parentTr.children("td:eq(3)").html("<input type='text' style='width: 90px;' value='" + parentTr.children("td:eq(3)").text() + "'>");
    } else if(type == "save") {
        var name = parentTr.children("td:eq(1)").children("input").val();
        var lat = parentTr.children("td:eq(2)").children("input").val();
        var lng = parentTr.children("td:eq(3)").children("input").val();

        if(name.length <= 0) {
            alert("이름을 입력해 주세요.");
            return;
        }

        if(lat.length <= 0) {
            alert("위도를 입력해 주세요.");
            return;
        }

        if(lng.length <= 0) {
            alert("경도를 입력해 주세요.");
            return;
        }

        parentTr.children("td:eq(1)").html(name);
        parentTr.children("td:eq(2)").html(lat);
        parentTr.children("td:eq(3)").html(lng);
		$(_btnThis).text("수정");

        $.ajax({
            type:"POST",
            url : "./function.php",
            data : "type=sub_modify&dbname=" + $(".type").val() + "&id=" + $(_btnThis).attr("id") + "&name=" + name + "&lat=" + lat + "&lng=" + lng,
            async: false,
            success : function(response) {
                alert(response);
            },
            error: function() {
                alert('통신 상태를 확인해주세요.');
            }
        });
    }
}

$(".sub_modify").click(function(){
	modifyPointData(this);
});

function addNewPoint(_id, _type, _parentTr) {
	if(_type == "add" ) {
		var html = "";
		html += "<tr>";
		html += "<td></td>";
		html += "<td><input type='text' style='width: 90px;' /></td>";
		html += "<td><input type='text' style='width: 90px;' /></td>";
		html += "<td><input type='text' style='width: 90px;' /></td>";
		html += "<td></td>";
		html += "<td><input type='checkbox' /></td>";
		html += "<td><button type='button' class='btn btn-default sub_add' id='add," + _id + "'>저장</button></td>";
		html += "<td><button type='button' class='btn btn-default sub_delete' disabled>수정</button></td>";
		html += "<td><button type='button' class='btn btn-default sub_delete'>삭제</button></td>";

		_parentTr.before(html);
		_parentTr.prev().children("td:eq(6)").children(".sub_add").off("click").on("click", function() {
			var parentTr = _parentTr.prev();
			var type = $(this).text() == "추가" ? "add" : "save";
			var id = $(this).attr("id").split(",");
			
			addNewPoint(id[1], type, parentTr)
		});

		_parentTr.prev().children("td:eq(8)").children(".sub_delete").off("click").on("click", function() {
            var parentTr = _parentTr.prev();
			parentTr.remove();
        });
	} else if(_type == "save" ) {
		var position = _parentTr.index(".lists > tbody > tr");
		var name = _parentTr.children("td:eq(1)").children("input").val();
		var lat = _parentTr.children("td:eq(2)").children("input").val();
		var lng = _parentTr.children("td:eq(3)").children("input").val();
		var except_outback = _parentTr.children("td:eq(5)").children("input").is(":checked") ? 1 : 0;

		if(name.length <= 0) {
			alert("이름을 입력해 주세요.");
			return;
		}

		if(lat.length <= 0) {
			alert("위도를 입력해 주세요.");
			return;
		}

		if(lng.length <= 0) {
			alert("경도를 입력해 주세요.");
			return;
		}

		$.ajax({
			type:"POST",
			url : "./function.php",
			data : "type=sub_add&dbname=" + $(".type").val() + "&id=" + _id + "&name=" + name + "&lat=" + lat + "&lng=" + lng + "&position=" + position + "&except_outback=" + except_outback,
			async: false,
			success : function(response) {
				console.log(response);
				var ret = JSON.parse(response);
				_parentTr.children("td:eq(0)").text(ret._id);
				_parentTr.children("td:eq(1)").text(ret.text);
				_parentTr.children("td:eq(2)").text(ret.lat);
				_parentTr.children("td:eq(3)").text(ret.lng);
				_parentTr.children("td:eq(4)").text(ret.createdAt);
				_parentTr.children("td:eq(5)").html("<input type='checkbox' id='check_" + ret._id + "' " + (ret.exceptOutback == 1 ? "checked" : "") + "/>");
				_parentTr.children("td:eq(6)").children("button").text("추가");
				_parentTr.children("td:eq(7)").html("<button type='button' id='modify," + ret.doc_id + "," + ret._id + "' class='btn btn-default sub_modify'>수정</button>");
				_parentTr.children("td:eq(8)").html("<button type='button' id='" + ret.doc_id + "," + ret._id + "' class='btn btn-default sub_delete'>삭제</button>");
			
				_parentTr.children("td:eq(7)").children(".sub_modify").off("click").on("click", function() {
					modifyPointData(this);
				});

				_parentTr.children("td:eq(8)").children(".sub_delete").off("click").on("click", function() {
					deletePoint(this);
				});
			},
			error: function() {
				alert('통신 상태를 확인해주세요.');
			}
		});
	}
}

$(".sub_add").click(function(){
	var id = $(this).attr("id").split(",");
    var parentTr = $(this).parent("td").parent("tr");
	var type = $(this).text() == "추가" ? "add" : "save";
    addNewPoint(id[1], type, parentTr);
});

$(".sub_check").on("click", function() {
	var checkList = [];
	var doc_id = null;
	$(".lists > tbody > tr").each(function(_index, _element) {
		var id = $(this).children("td:eq(5)").children("input").attr("id").split(",");
		if(doc_id == null) {
			doc_id = id[1];
		}
		
		if($(this).children("td:eq(5)").children("input").is(":checked")) {
			checkList.push(id[2]);
		}
	});
	
	$.ajax({
        type:"POST",
        url : "./function.php",
        data : "type=sub_except&dbname=" + $(".type").val() + "&id=" + doc_id + "&check_list=" + checkList,
        async: false,
        success : function(response) {
            alert(response);
        },
        error: function() {
            alert('통신 상태를 확인해주세요.');
        }
    });
});
