$(".showlist").click(function(){
	var id = $(this).attr("id");
	var func = "normal";
	if($(this).hasClass("hell"))
		func = "hell";
	 $.ajax({
        type:"POST",
        url : "./lists.php",
        data : "type=" + $(".type").val() + "&id=" + id + "&func=" + func,
        async: false,
        success : function(response) {
            $("#listTable").html(response);
            $("#sub_" + id).modal();
        },
        error: function() {
            alert('통신 상태를 확인해주세요.');
        }
    });
});
$("i").click(function(){
	var temp = $(this).attr('id').split('-');

	 $.ajax({
        type:"POST",
        url : "./ajax.php",
        data : "type=" + temp[0] + "&page=" + temp[1],
        async: false,
        success : function(response) {
            $(".Contents").html(response);
        },
        error: function() {
            alert('통신 상태를 확인해주세요.');
        }
    });
});
$(".delete").click(function(){
	 $.ajax({
        type:"POST",
        url : "./function.php",
        data : "type=" + "delete" + "&dbname=" + $(".type").val() + "&id=" + $(this).attr("id"),
        async: false,
        success : function(response) {
			if(response!="권한 없음")
				$(event.target).closest("tr").remove();
			alert(response);
        },
        error: function() {
            alert('통신 상태를 확인해주세요.');
        }
    });
});
$(".submitBus").click(function(){
	$.ajax({
        type:"POST",
        url : "http://115.68.104.30:8888/busnew",
        data : "name=" + $(".nameText").val() + "&desc=" + $(".descText").val(),
        async: false,
        success : function(response) {
	       alert("등록되었습니다.");
	       location.reload();
        },
        error: function() {
            alert('통신 상태를 확인해주세요.');
        }
    });
});
$("#category a").click(function(){
	$(this).parents('.dropdown').find("#category_button").html($(this).text() + ' <span class="caret"></span>');
	$.ajax({
        type:"POST",
        url : "./function.php",
        data : "type=" + "category_change" + "&dbname=" + $(".type").val() + "&id=" + $(this).parents('.dropdown').attr("id") + "&value=" + $(this).attr("name"),
        async: false,
        success : function(response) {
	       alert(response);
        },
        error: function() {
            alert('통신 상태를 확인해주세요.');
        }
	});
});
$("#select-category a").click(function(){
	window.location.href = "./map.php?category=" + $(this).attr("name") + "&type=" + $(".type").val();
});
$("#info > tbody > tr").click(function(){
	var tab = $('.type').val();
	if( tab == "buses" || tab == "busnews")
	{
		var index = $(this).attr('id');
		if($(event.target).is("#info > tbody > tr:eq(" + index + ") > td:eq(1)"))
		{
			if($(event.target).text() != "")
				$("#info > tbody > tr:eq(" + index + ") > td:eq(1)").html("<input type='text' value='" + $(event.target).text() + "'>");
			$("#info > tbody > tr:eq(" + index + ") > td:eq(1) > input[type=text]").keypress(function( event ) {
				if ( event.which == 13 && $(this).val() != "") {
						$("#info > tbody > tr:eq(" + index + ") > td:eq(1)").html($(this).val());
						 $.ajax({
					        type:"POST",
					        url : "./function.php",
					        data : "type=" + "modify_desc" + "&dbname=" + $(".type").val() + "&id=" + $("#info > tbody > tr:eq(" + index + ") > td:eq(3) > button").attr("id") + "&value=" + $(this).val(),
					        async: false,
					        success : function(response) {
					        },
					        error: function() {
					            alert('통신 상태를 확인해주세요.');
					        }
					    });
				}
			});
		}
		else if($(event.target).is("#info > tbody > tr:eq(" + index + ") > td:eq(5)"))
		{
			if($(event.target).text() != "")
				$("#info > tbody > tr:eq(" + index + ") > td:eq(5)").html("<input type='text' value='" + $(event.target).text() + "'>");
			$("#info > tbody > tr:eq(" + index + ") > td:eq(5) > input[type=text]").keypress(function( event ) {
				if ( event.which == 13 && $(this).val() != "") {
						if(tab == "buses")
							tab = "bus";
						else if(tab =="busnews")
							tab = "busnew";
						$("#info > tbody > tr:eq(" + index + ") > td:eq(5)").html($(this).val());
						 $.ajax({
					        type:"POST",
					        url : "http://115.68.104.30:8888/" + tab + "/" + $("#info > tbody > tr:eq(" + index + ") > td:eq(3) > button").attr("id") + "/phone",
					        data : "phone=" + $(this).val(),
					        async: false,
					        success : function(response) {
						       alert("등록되었습니다.");
					        },
					        error: function() {
					            alert('통신 상태를 확인해주세요.');
					        }
					    });
				}
			});
		}
	}
});