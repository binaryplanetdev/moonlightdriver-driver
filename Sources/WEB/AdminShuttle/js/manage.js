$("li").click(function(){
    if(!$(this).hasClass("active"))
    {
	    $("li").removeClass("active");
	    $(this).addClass("active");
		 $.ajax({
	        type:"POST",
	        url : "./ajax.php",
	        data : "type=" + $(this).find("a").attr('id'),
	        async: false,
	        success : function(response) {
	            $(".Contents").html(response);
	        },
	        error: function() {
	            alert('통신 상태를 확인해주세요.');
	        }
	    });
    }
});