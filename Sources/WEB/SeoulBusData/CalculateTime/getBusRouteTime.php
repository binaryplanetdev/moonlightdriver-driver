<?php
	header("Content-Type:text/html; charset=utf-8");
	ini_set('max_execution_time', 300000);
	
	$db = new SQLite3("D:/Project/02.MoonlightDriver/Source/ANDROIDSTUDIO/moon_android_driver/Driver/src/main/assets/bus_data.db"); 
// 	SELECT * FROM BusRoute WHERE busRouteIndex >= 1035 AND busRouteArea = "경기" AND firstTime is not null;
// 	"SELECT * FROM BusRoute WHERE busRouteIndex IN (SELECT busRouteIndex FROM BusStation WHERE startTime IS NULL AND busRouteArea = '서울' GROUP BY busRouteId);"
	$results = $db->query("select * From BusRoute where busRouteId in (select busRouteId from BusStation where endTime is null and busRouteArea = '서울' group by busRouteId);");
	$busRouteList = array();
	while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
		$busRouteList[] = $row;
	}
	
	echo count($busRouteList) . " Routes Start!<br/>";
	
// 	echo "<pre>";
// 	print_r($busRouteList);
// 	exit;
	
	foreach($busRouteList as $row) {
// 		echo "firstTime : " . $row["firstTime"] . ", lastTime : " . $row["lastTime"] . "<br/>";
		$results = $db->query("SELECT * FROM BusStation WHERE busRouteIndex = " . $row["busRouteIndex"] . " ORDER BY stationSequence;");
		$prev_lat = null;
		$prev_lng = null;
		$prev_first_time = $row["firstTime"];
		$prev_last_time = $row["lastTime"];
		while ($station_rows = $results->fetchArray(SQLITE3_ASSOC)) {
			$increase_minutes = 0;
			$dist = 0;
			if($prev_lat != null) {
				$dist = get_distance($prev_lat, $prev_lng, $station_rows["gpsY"], $station_rows["gpsX"]);
				$increase_minutes = $dist + 1;
			}
			
			$startTime = calculateTime($prev_first_time, $increase_minutes);
			$endTime = calculateTime($prev_last_time, $increase_minutes);
			
// 			$db->query("UPDATE BusStation SET startTime = '" . $startTime . "', endTime = '" . $endTime . "' WHERE stationIndex = " . $station_rows["stationIndex"] . ";");

// 			echo "[busRouteIndex : " . $station_rows["busRouteIndex"] . ", stationIndex : " . $station_rows["stationIndex"] . ", dist : " . $dist . ", minutes : " . $increase_minutes . "] : ";
			echo "UPDATE BusStation SET startTime = '" . $startTime . "', endTime = '" . $endTime . "' WHERE stationIndex = " . $station_rows["stationIndex"] . ";<br/>";
			
			$prev_lat = $station_rows["gpsY"];
			$prev_lng = $station_rows["gpsX"];
			
			$prev_first_time = $startTime;
			$prev_last_time = $endTime;
		}
	}
	
	function calculateTime($_orgTime, $_incrTime) {
		$orgTimeArray = split(":", $_orgTime);
		$org_hour = intval($orgTimeArray[0]);
		$org_minute = intval($orgTimeArray[1]) + $_incrTime;
		
		if($org_minute >= 60) {
			$org_minute = $org_minute % 60;
			$org_hour = $org_hour + 1;
		}
		
		if($org_hour >= 24) {
			$org_hour = $org_hour % 24;
		}
		
		if($org_minute < 10) {
			$org_minute = '0' . $org_minute;
		}
		
		if($org_hour < 10) {
			$org_hour = '0' . $org_hour;
		}
		
		return $org_hour . ":" . $org_minute;
	}
	
	function get_distance($lat1, $lon1, $lat2, $lon2) {
		/* WGS84 stuff */
		$a = 6378137;
		$b = 6356752.3142;
		$f = 1/298.257223563;
		/* end of WGS84 stuff */
	
		$L = deg2rad($lon2-$lon1);
		$U1 = atan((1-$f) * tan(deg2rad($lat1)));
		$U2 = atan((1-$f) * tan(deg2rad($lat2)));
		$sinU1 = sin($U1);
		$cosU1 = cos($U1);
		$sinU2 = sin($U2);
		$cosU2 = cos($U2);
	
		$lambda = $L;
		$lambdaP = 2*pi();
		$iterLimit = 20;
		while ((abs($lambda-$lambdaP) > pow(10, -12)) && ($iterLimit-- > 0)) {
			$sinLambda = sin($lambda);
			$cosLambda = cos($lambda);
			$sinSigma = sqrt(($cosU2*$sinLambda) * ($cosU2*$sinLambda) + ($cosU1*$sinU2-$sinU1*$cosU2*$cosLambda) * ($cosU1*$sinU2-$sinU1*$cosU2*$cosLambda));
	
			if ($sinSigma == 0) {
				return 0;
			}
	
			$cosSigma   = $sinU1*$sinU2 + $cosU1*$cosU2*$cosLambda;
			$sigma      = atan2($sinSigma, $cosSigma);
			$sinAlpha   = $cosU1 * $cosU2 * $sinLambda / $sinSigma;
			$cosSqAlpha = 1 - $sinAlpha*$sinAlpha;
			$cos2SigmaM = $cosSigma - 2*$sinU1*$sinU2/$cosSqAlpha;
	
			if (is_nan($cos2SigmaM)) {
				$cos2SigmaM = 0;
			}
	
			$C = $f/16*$cosSqAlpha*(4+$f*(4-3*$cosSqAlpha));
			$lambdaP = $lambda;
			$lambda = $L + (1-$C) * $f * $sinAlpha *($sigma + $C*$sinSigma*($cos2SigmaM+$C*$cosSigma*(-1+2*$cos2SigmaM*$cos2SigmaM)));
		}
	
		if ($iterLimit == 0) {
			// formula failed to converge
			return NaN;
		}
	
		$uSq = $cosSqAlpha * ($a*$a - $b*$b) / ($b*$b);
		$A = 1 + $uSq/16384*(4096+$uSq*(-768+$uSq*(320-175*$uSq)));
		$B = $uSq/1024 * (256+$uSq*(-128+$uSq*(74-47*$uSq)));
		$deltaSigma = $B*$sinSigma*($cos2SigmaM+$B/4*($cosSigma*(-1+2*$cos2SigmaM*$cos2SigmaM)- $B/6*$cos2SigmaM*(-3+4*$sinSigma*$sinSigma)*(-3+4*$cos2SigmaM*$cos2SigmaM)));
	
		return round($b*$A*($sigma-$deltaSigma) / 1000);
	
	
		/* sphere way */
		$distance = rad2deg(acos(sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($lon1 - $lon2))));
	
		$distance *= 111.18957696; // Convert to km
	
		return $distance;
	}
?>