<?php
	$fileName = "../data/daum_subway.json";
	if(!file_exists($fileName)) {
		echo "file not exist!!";
		exit;
	}
	
	$fp = fopen($fileName, "r");
	$jsonStr = fread($fp, filesize($fileName));
	fclose($fp);
	
	$retJson = json_decode($jsonStr, true);

	$i = 0;
	$retArr = array();
	foreach($retJson as $item) {
		$id_pre = substr($item["id"], 0, 3);
		if($id_pre == "DJS") {
			continue;
		}
		
		$find_index = strpos($item["name"], "역");
		if($find_index > 0 && $find_index <= strlen($item["name"])) {
			$name = substr($item["name"], 0, $find_index);
		} else {
			$name = $item["name"];
		}
		
		$id = substr($item["id"], -4);
		
		if(!isset($retArr[$name])) {
			$retArr[$name] = array();
		}
		
		$retArr[$name][] = $id;
	}
	
// 	echo "<pre>";
// 	print_r($retArr);
// 	exit;
	
	foreach ($retArr as $k => $v) {
		$name = $k;
		
		if(count($v) == 1) {
			$id = $v[0];
			echo "UPDATE SubwayStation SET stationCodeDaum = '" . $id . "' WHERE stationName = '" . $name . "' AND stationCode != '" . $id . "';</br>";
		} else {
			$id = "";
			$min_id = 0;
			foreach ($v as $r) {
				if($min_id == 0 || $min_id > intval($r)) {
					$min_id = intval($r);
				}
				
				$id .= "'" . $r . "',";
			}
			
			if($min_id < 1000) {
				$min_id = '0' . $min_id;
			}
			
			$id = substr($id, 0, -1);
			
			echo "UPDATE SubwayStation SET stationCodeDaum = '" . $min_id . "' WHERE stationName = '" . $name . "' AND stationCode NOT IN (" . $id . ");</br>";
		}		
	}
?>