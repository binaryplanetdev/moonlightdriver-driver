<?php
	$routeURL = "http://api.gjcity.net/json/lineInfo?serviceKey=SeXIq%2F47tYT4doM%2Fnu5I8dhNBjD%2BGySm0ayiAsDEWHd4ZCOFl5qQDHiT6UIp7PunXRsJ%2FJtRKAJQb1i%2F0yIsZg%3D%3D";
	$stationByRouteURL = "http://api.gjcity.net/json/lineStationInfo?serviceKey=SeXIq%2F47tYT4doM%2Fnu5I8dhNBjD%2BGySm0ayiAsDEWHd4ZCOFl5qQDHiT6UIp7PunXRsJ%2FJtRKAJQb1i%2F0yIsZg%3D%3D&LINE_ID=";

	$routeFileName = "../data/gwangjuRoute.json";
	$routeParseFileName = "../data/gwangjuRouteParse.json";
	$stationFileName = "../data/gwangjuStation.json";
	$stationParseFileName = "../data/gwangjuStationParse.json";
	
	$retRoute = file_get_contents($routeURL);
	echo "get route complete\n";
	file_put_contents($routeFileName, $retRoute, FILE_APPEND | LOCK_EX);
		
	$retJson = json_decode($retRoute, true);
// 	echo "<pre>";
// 	print_r($retJson["LINE_LIST"]);
// 	exit;
	
	$busRouteType = Array("1" => "급명간선", "2" => "간선", "3" => "지선", "4" => "마을버스", "5" => "공항버스");
	$busRouteId = Array();
	
	$headerStr = "busRouteIndex|busRouteId|busRouteName|busRouteType|busRouteTypeName|busRouteArea|busRouteCompany|startStationName|endStationName|firstTime|lastTime\n";
	file_put_contents($routeParseFileName, $headerStr, FILE_APPEND | LOCK_EX);
	$i = 0;
	foreach($retJson["LINE_LIST"] as $row) {
		echo "line ID : " . $row["LINE_ID"] . "\n";
		$busRouteId[] = $row["LINE_ID"];
		$rowStr = (++$i) . "|" . $row["LINE_ID"] . "|" . $row["LINE_NAME"] . "|" . $row["LINE_KIND"] . "|" . $busRouteType[$row["LINE_KIND"]] . "|" . "광주|" . "|" . $row["DIR_UP_NAME"] . "|" . $row["DIR_DOWN_NAME"] . "|" . $row["FIRST_RUN_TIME"] . "|" . $row["LAST_RUN_TIME"] . "\n";
		file_put_contents($routeParseFileName, $rowStr, FILE_APPEND | LOCK_EX);
	}
	
	echo "parse route complete[" . $i . "]\n";
	
	file_put_contents($stationFileName, "[", FILE_APPEND | LOCK_EX);
	$i = 0;
	foreach ($busRouteId as $routeId) {
		$retStation = file_get_contents($stationByRouteURL . $routeId);
		if($i > 0) {
			file_put_contents($stationFileName, ",", FILE_APPEND | LOCK_EX);			
		}
		
		file_put_contents($stationFileName, $retStation, FILE_APPEND | LOCK_EX);
		$i++;
		
		echo "get station by route[" . $routeId . "]" . $i ."\n";
	}
	file_put_contents($stationFileName, "]", FILE_APPEND | LOCK_EX);
	echo "get station complete\n";
	
	if(!file_exists($stationFileName)) {
		echo "file not exist!!";
		exit;
	}
	
	$fp = fopen($stationFileName, "r");
	$jsonStr = fread($fp, filesize($stationFileName));
	fclose($fp);
	
	$retJson = json_decode($jsonStr, true);
	
	$headerStr = "stationIndex|busRouteId|busRouteName|stationSequence|arsId|stationName|gpsX|gpsY|startTime|endTime\n";
	file_put_contents($stationParseFileName, $headerStr, FILE_APPEND | LOCK_EX);
	$i = 0;
	foreach($retJson as $stationRow) {
		foreach($stationRow["BUSSTOP_LIST"] as $row) {
			echo "line ID : " . $row["LINE_ID"] . "(" . $row["BUSSTOP_NUM"] . ")\n";
			$rowStr = (++$i) . "|" . $row["LINE_ID"] . "|" . $row["LINE_NAME"] . "|" . $row["BUSSTOP_NUM"] . "|" . $row["ARS_ID"] . "|" . $row["BUSSTOP_NAME"] . "|" . $row["LONGITUDE"] . "|" . $row["LONGITUDE"] . "|" . $row["LATITUDE"] . "|" . "|\n";
			file_put_contents($stationParseFileName, $rowStr, FILE_APPEND | LOCK_EX);
		}
	}
	
	echo "parse station complete\n";
?>