<?php
	header("Content-Type:text/html; charset=utf-8");
	ini_set('max_execution_time', 300000);
	
	include_once './pointLocation.php';
	$pointLocation = new pointLocation();
	
	$retRoute = file_get_contents("./data/seoul_coordinates.json");
	$retJson = json_decode($retRoute);
	
	$polygon = array();
	
	foreach ($retJson as $c) {
		$polygon[] = $c[1] . "," . $c[0];
	}
	
	$db = new SQLite3("D:/Project/02.MoonlightDriver/Source/ANDROIDSTUDIO/moon_android_driver/Driver/src/main/assets/bus_data.db");
	
	$hasNext = true;
	$offset = 0;
	$limit = 10000;
	
	$outsideCount = 0;
	$insideCount = 0;
	
	while($hasNext) {
		$start = $offset * $limit;
		$results = $db->query("SELECT * FROM BusStation WHERE busRouteArea = '경기' ORDER BY stationIndex LIMIT " . $limit . " OFFSET " . $start . ";");
		$busRouteList = array();
		while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
			$busRouteList[] = $row;
		}
		
		$totCount = count($busRouteList);
		
		if($totCount <= 0) {
			$hasNext = false;
			break;
		}
		
		echo "</br></br>" . $totCount . " Station Start! OFFSET " . $offset . "</br></br>";
		
		foreach ($busRouteList as $key => $r) {
			$point = $r["gpsY"] . "," . $r["gpsX"];
			$inPolygon = $pointLocation->pointInPolygon($point, $polygon);
		
// 			echo "point " . ($key + 1) . " ($point): " . $inPolygon . "</br>";
			
			if($inPolygon != "outside") {
				$insideCount++;
// 				echo "UPDATE BusStation SET inSeoul = 1 WHERE stationIndex = " . $r["stationIndex"] . ";</br>";
			} else {
				$outsideCount++;
			}
		}
		
		$offset = $offset + 1;
	}
	
	echo "insideCount : " . $insideCount . ", outsideCount : " . $outsideCount . "</br>";
?>
