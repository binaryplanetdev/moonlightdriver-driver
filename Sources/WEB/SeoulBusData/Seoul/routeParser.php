<?php
	header("Content-Type:text/html; charset=utf-8");

	$fileName = "../data/getBusRouteList.json";
	if(!file_exists($fileName)) {
		echo "file not exist!!";
		exit;
	}
	
	$fp = fopen($fileName, "r");
	$jsonStr = fread($fp, filesize($fileName));
	fclose($fp);
	
	$retJson = json_decode($jsonStr, true);
	// 	echo "<pre>";
	// 	print_r($retJson["resultList"]);
	// 	exit;
	
	$busRouteType = Array("1" => "공항", "2" => "마을", "3" => "간선", "4" => "지선", "5" => "순환", "6" => "광역", "7" => "인천", "8" => "경기", "9" => "폐지", "0" => "공용");
	
// 	echo "busRouteIndex|busRouteId|busRouteName|busRouteType|busRouteTypeName|busRouteArea|busRouteCompany|startStationName|endStationName|firstTime|lastTime<br/>";
	$i = 0;
	$busRouteId = array('115000001','124000013','124000012','124000011','114900004','233000140','241000910','204000083','241001320','241000810','241000830','241004280','241004220','241004230','241004250','241003600','241004710','241005910','241005940','233000136','241006510','241005960','241000590','241000740','241002610','241003770','241006600','241003340','241003320','241000460','241000800','241001340','241000820','241000840','241003400','233000139','241004890','241004370','233000135','241005980','241003970','241005870','241005880','241005890','241005840','221000025');
	foreach($retJson["resultList"] as $row) {
		/**
		 * ========================
		 */
// 		$company = split(" ", $row["corpNm"]);
// 		echo (++$i) . "|";
// 		echo $row["busRouteId"] . "|";
// 		echo $row["busRouteNm"] . "|";
// 		echo $row["routeType"] . "|";
// 		echo $busRouteType[$row["routeType"]] . "|";
// 		echo $row["routeType"] == 8 ? "경기|" : $row["routeType"] == 7 ? "인천|" : "서울|";
// 		echo $company[0] . "|";
// 		echo $row["stStationNm"] . "|";
// 		echo $row["edStationNm"] . "|";
// 		echo substr($row["firstBusTm"], 8, 2) . ":" . substr($row["firstBusTm"], 10, 2) . "|";
// 		echo substr($row["lastBusTm"], 8, 2) . ":" . substr($row["lastBusTm"], 10, 2);
// 		echo "<br/>";
		
		/**
		 * ========================
		 */
// 		$busRouteArea = $row["routeType"] == 8 ? "경기" : $row["routeType"] == 7 ? "인천" : "서울";
		
// 		echo "UPDATE BusRoute SET busRouteIndex = '" . $row["busRouteId"] . "', startStationName = '" . $row["stStationNm"] . "', endStationName = '" . $row["edStationNm"] . "', firstTime = '" . substr($row["firstBusTm"], 8, 2) . ":" . substr($row["firstBusTm"], 10, 2) . "', lastTime = '" . substr($row["lastBusTm"], 8, 2) . ":" . substr($row["lastBusTm"], 10, 2) . "'";
// 		echo " WHERE busRouteName = '" . $row["busRouteNm"] . "' AND busRouteType = " . $row["routeType"] . " AND busRouteArea = '" . $busRouteArea . "';";
// 		echo "<br/>";

		/**
		 * ========================
		 */
		if(in_array($row["busRouteId"], $busRouteId)) {
			$company = split(" ", $row["corpNm"]);
			$busRouteArea = $row["routeType"] == 8 ? "경기" : $row["routeType"] == 7 ? "인천" : "서울";
			
			echo "INSERT INTO BusRoute(busRouteId, busRouteName, busRouteName_s, busRouteType, busRouteTypeName, busRouteArea, busRouteCompany, startStationName, endStationName, firstTime, lastTime)";
			echo "VALUES('" . $row["busRouteId"] . "', '" . $row["busRouteNm"] . "', '" . $row["busRouteNm"] . "', " . $row["routeType"] . ", '" . $busRouteType[$row["routeType"]] . "', '" . $busRouteArea . "', '" . $company[0] . "', '" . $row["stStationNm"] . "', '" . $row["edStationNm"] . "', '" . substr($row["firstBusTm"], 8, 2) . ":" . substr($row["firstBusTm"], 10, 2) . "', '" . substr($row["lastBusTm"], 8, 2) . ":" . substr($row["lastBusTm"], 10, 2) . "');";
			echo "<br/>";
		}
	}
?>