<?php
	$fileName = "../data/getBusStation.json";
	if(!file_exists($fileName)) {
		echo "file not exist!!";
		exit;
	}
	
	$fp = fopen($fileName, "r");
	$jsonStr = fread($fp, filesize($fileName));
	fclose($fp);
	
	$retJson = json_decode($jsonStr, true);

	echo "busRouteId,stationName,stationSequence,direction\n";
	$i = 0;
	foreach($retJson as $list) {
		foreach($list["resultList"] as $row) {
			echo $row["busRouteId"] . "|";
			echo $row["stationNm"] . "|";
			echo $row["seq"] . "|";
			echo $row["direction"] . "\n";
		}
	}
?>