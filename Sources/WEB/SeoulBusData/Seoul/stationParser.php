<?php
	header("Content-Type:text/html; charset=utf-8");

	$fileName = "../data/getBusStation.json";
	if(!file_exists($fileName)) {
		echo "file not exist!!";
		exit;
	}
	
	$fp = fopen($fileName, "r");
	$jsonStr = fread($fp, filesize($fileName));
	fclose($fp);
	
	$retJson = json_decode($jsonStr, true);

// 	echo "stationIndex,busRouteId,busRouteName,stationSequence,arsId,stationName,gpsX,gpsY,startTime,endTime\n";
	$i = 0;
	foreach($retJson as $list) {
// 		foreach($list["resultList"] as $row) {
// 			echo (++$i) . ",";
// 			echo $row["busRouteId"] . ",";
// 			echo $row["busRouteNm"] . ",";
// 			echo $row["seq"] . ",";
// 			echo $row["arsId"] . ",";
// 			echo $row["stationNm"] . ",";
// 			echo $row["gpsX"] . ",";
// 			echo $row["gpsY"] . ",";
// 			echo $row["beginTm"] . ",";
// 			echo $row["lastTm"] . "\n";
// 		}

		if(isset($list["resultList"])) {
			echo "DELETE FROM BusStation WHERE busRouteId = '" . $list["resultList"][0]["busRouteId"] . "' AND busRouteArea = '서울';\n";
			
			foreach($list["resultList"] as $row) {
				$stationName_s = GetUtf8String($row["stationNm"]);
				$startTime = strlen($row["beginTm"]) > 3 ? $row["beginTm"] : "";
				$lastTime = strlen($row["lastTm"]) > 3 ? $row["lastTm"] : "";
				echo "INSERT INTO BusStation(busRouteId, busRouteName, busRouteArea, stationSequence, arsId, stationName, stationName_s, gpsX, gpsY, startTime, endTime, direction) ";
				echo "VALUES('" . $row["busRouteId"] . "', '" . $row["busRouteNm"] . "', '서울', " . $row["seq"] . ", '" . $row["arsId"] . "', '" . $row["stationNm"] . "', '" . $stationName_s . "', " . $row["gpsX"] . ", " . $row["gpsY"] . ", '" . $startTime . "', '" . $lastTime . "', '" . $row["direction"] . "');";
				echo "\n";
			}
		}
	}
	
	function GetUtf8String($str) {
		$arr_cho = array("ㄱ", "ㄲ", "ㄴ", "ㄷ", "ㄸ", "ㄹ", "ㅁ","ㅂ", "ㅃ", "ㅅ", "ㅆ", "ㅇ", "ㅈ", "ㅉ","ㅊ", "ㅋ", "ㅌ", "ㅍ", "ㅎ");
		$arr_jung = array("ㅏ", "ㅐ", "ㅑ", "ㅒ", "ㅓ", "ㅔ", "ㅕ","ㅖ", "ㅗ", "ㅘ", "ㅙ", "ㅚ", "ㅛ", "ㅜ","ㅝ", "ㅞ", "ㅟ", "ㅠ", "ㅡ", "ㅢ", "ㅣ");
		$arr_jong = array("", "ㄱ", "ㄲ", "ㄳ", "ㄴ", "ㄵ", "ㄶ","ㄷ", "ㄹ", "ㄺ", "ㄻ", "ㄼ", "ㄽ", "ㄾ","ㄿ", "ㅀ", "ㅁ", "ㅂ", "ㅄ", "ㅅ", "ㅆ","ㅇ", "ㅈ", "ㅊ", "ㅋ", "ㅌ", "ㅍ", "ㅎ");
	
		$unicode = array();
		$values = array();
		$lookingFor = 1;
	
		for ($i=0, $loop=strlen($str);$i<$loop;$i++) {
			$thisValue = ord($str[$i]);
				
			if ($thisValue < 128) {
				$unicode[] = $thisValue;
			} else {
				if (count($values) == 0)
					$lookingFor = $thisValue < 224 ? 2 : 3;
	
					$values[] = $thisValue;
	
					if (count($values) == $lookingFor) {
						$number = $lookingFor == 3 ? (($values[0]%16)*4096)+(($values[1]%64)*64)+($values[2]%64) : (($values[0]%32)*64)+($values[1]%64);
						$unicode[] = $number;
						$values = array();
						$lookingFor = 1;
					}
			}
		}
	
		$splitStr = '';
		while (list($key,$code) = each($unicode)) {
			if ($code >= 44032 && $code <= 55203) {
				$temp = $code-44032;
				$cho = (int)($temp/21/28);
				//$jung = (int)(($temp%(21*28)/28));
				//$jong = (int)($temp%28);
	
				//$splitStr.= $arr_cho[$cho].$arr_jung[$jung].$arr_jong[$jong];
				$splitStr.= $arr_cho[$cho];
			} else {
				$temp = array($unicode[$key]);
	
				foreach ($temp as $ununicode) {
					if ($ununicode < 128) {
						$splitStr.= chr($ununicode);
					} else if ($ununicode < 2048) {
						$splitStr.= chr(192+(($ununicode-($ununicode%64))/64));
						$splitStr.= chr(128+($ununicode%64));
					} else {
						$splitStr.= chr(224+(($ununicode-($ununicode%4096))/4096));
						$splitStr.= chr(128+((($ununicode%4096)-($ununicode%64))/64));
						$splitStr.= chr(128+($ununicode%64));
					}
				}
			}
		}
	
		$splitStr = str_replace(' ','',$splitStr);
	
		return $splitStr;
	}
?>