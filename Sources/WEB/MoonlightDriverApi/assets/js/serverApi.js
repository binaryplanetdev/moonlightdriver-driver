var host = "http://115.68.104.30:8888";
var clickedItem = null;

$(function() {
	drawMenu();
	
	$("#menu").menu({
		items: "> :not(.ui-widget-header)"
	});
	
	$("#menu > li:not(.ui-widget-header)").each(function() {
		$(this).on( "click", function() {
			$('#resultWrap').html("");
			onClickApi($(this).attr("id"));
		});
	});
	
	$('#btnExecute').on("click", function() {
		$('#resultWrap').html("");
		
		var parameters = {};
		$('.parameters > .parametersInput > input').each(function(){
			parameters[$(this).attr('id')] = $(this).val();
		});
		
		var url = getReplacedURL(parameters);
		
		if(clickedItem.params.to && clickedItem.params.to == "external") {
			sendCrossDomainRequest(url, clickedItem.method, parameters);
		} else {
			sendRequest(url, clickedItem.method, parameters);
		}
	});
	
	onClickApi("createTaxi");
});

function getReplacedURL(_parameters) {
	var url = clickedItem.url;
	var pattern = /:\w*/gi;
	var matchArray;

	while((matchArray = pattern.exec(url)) != null){
		var foundStr = matchArray[0];
		var replaceStr = matchArray[0].replace(":", "");
		url = url.replace(foundStr, _parameters[replaceStr]);
		delete(_parameters[replaceStr]);
	}
	
	if(clickedItem.params.to && clickedItem.params.to == "external") {
		return clickedItem.url;
	} else {
		return host + url;
	}
}

function isNumber(n) {
	return /^-?[\d.]+(?:e-?\d+)?$/.test(n);
} 

function onClickApi(_divId) {
	getClickedItem(_divId);
	showApiInfo();
}

function showApiInfo() {
	if(clickedItem) {
		$('#txtTitle').text(clickedItem.name);
		if(clickedItem.params.to && clickedItem.params.to == "external") {
			$('#txtUrl').text(clickedItem.url);
		} else {
			$('#txtUrl').text(host + clickedItem.url);
		}
		
		var html = getParametersHtml();
		$('.parameters').html(html);	
	}
}

function getParametersHtml() {
	var html = "";
	if(clickedItem) {
		$.each(clickedItem.params, function(_key, _data) {
			html += "<div class='parametersInput'>";
			html += _key + " : <input type='text' id='" + _key + "' value='" + _data + "'/>";
			html += "</div>";		
		});
	}
	
	return html;
}

function getClickedItem(_divId) {
	clickedItem = null;

	$.each(apiList, function(_key, _data) {
		$.each(_data.items, function(_index, _items) {
			if(_divId == _items.divId) {
				clickedItem = _items;
			}

			if(clickedItem != null)
				return false;
		});
		
		if(clickedItem != null)
			return false;
	});
	
	return true;
}

function drawMenu() {
	var html = "";
	
	$.each(apiList, function(_key, _data) {
		html += "<li class='ui-widget-header'>" + _data.category_name + "</li>";
		$.each(_data.items, function(_index, _items) {
			html += "<li id='" + _items.divId + "'>" + _items.name + "</li>";
		});
	});
	
	$('#menu').html(html);
}

function showResult(_url, _method, _params, _data) {
	var html = "";
	html += "<p>===== [요청] =========================</p>";
	html += "<div><pre>URL : " + _url + "</pre></div>";
	html += "<div><pre>METHOD : " + _method + "</pre></div>";
	html += "<div><pre>PARAMS : " + JSON.stringify(_params, undefined, 4) + "</pre></div>";
	html += "<p>===== [응답] =========================</p>";
//	html += "<pre>";
	html += "<div><pre>" + JSON.stringify(_data, undefined, 4) + "</pre></div>";
	$('#resultWrap').html(html);
}

function sendRequest(_url, _method, _params) {
	if(_method == "post") {
		$.post(_url, _params, function(_data) {
			showResult(_url, _method, _params, _data);
		}, "json");		
	} else if(_method == "get") {
		$.get(_url, _params, function(_data) {
			showResult(_url, _method, _params, _data);
		}, "json");
	}
}

function sendCrossDomainRequest(_url, _method, _params) {
	$.ajax({
		url: _url,
		type: "get",
		data: _params,
		dataType: 'jsonp',
		success: function(data) {
			console.log('성공 - ', data);
		},
		error: function(xhr) {
			console.log('실패 - ', xhr);
		}
	});
}
