<?php
	header("Content-Type:text/html; charset=utf-8");
	require_once("./db.php");
	
	$shuttleinfos = $db->shuttleinfos;
	
	$shuttleCursor = $shuttleinfos->find();
	foreach($shuttleCursor as $row) {
		$firstTime = $row["firstTime"];
		$lastTime = $row["lastTime"];
		
		$firstTimeArray = split(":", $firstTime);
		$firstTimeHours = intval($firstTimeArray[0]);
		if($firstTimeHours < 23) {
			$firstTimeHours = $firstTimeHours + 24;
			if($firstTimeHours < 10) {
				$firstTimeHours = "0" . $firstTimeHours;
			}
		}
		
		$firstTime = $firstTimeHours . ":" . $firstTimeArray[1];
		
		$lastTimeArray = split(":", $lastTime);
		$lastTimeHours = intval($lastTimeArray[0]);
		if($lastTimeHours < 23) {
			$lastTimeHours = $lastTimeHours + 24;
			if($lastTimeHours < 10) {
				$lastTimeHours = "0" . $lastTimeHours;
			}
		}
		
		$lastTime = $lastTimeHours . ":" . $lastTimeArray[1];
		
		$row["firstTime"] = $firstTime;
		$row["lastTime"] = $lastTime;
		
		$shuttleinfos->save($row);
		
// 		echo "<pre>";
// 		print_r($row);
// 		exit;
	}
?>