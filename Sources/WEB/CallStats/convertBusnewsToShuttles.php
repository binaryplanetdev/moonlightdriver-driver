<?php
	header("Content-Type:text/html; charset=utf-8");
	require_once("../db.php");

	$shuttles = $db->shuttles;
	$busnews = $db->busnews;
	
	$cur = $busnews->find();
	foreach($cur as $row) {
		$result = array(
			"name" => $row['name'],
			"category" => $row['category'],
			"desc" => $row['desc'],
			"lists" => array()
		);
		
		if(!empty($row["phone"])) {
			$result["phone"] = $row["phone"];
		}

		if(!empty($row["comment"])) {
// 			$result["comment"] = $row["comment"];
			$result["comment"] = array();
			foreach ($row["comment"] as $comment) {
				$commentsArray = array();
				$commentsArray['_id'] = $comment['_id'];
				$commentsArray['message'] = $comment['message'];
				$commentsArray['nickName'] = $comment['nickname'];
				$commentsArray['createdAt'] = $comment['createdAt'];
			
				$result["comment"][] = $commentsArray;
			}
		}
		
		foreach($row['lists'] as $lists) {
			$location = array(
				"index" => $lists['index'],
				"text" => $lists['text'],
				"locations" => array(
						"type"=>"Point",
						"coordinates" => array($lists['lng'], $lists['lat'])
				),
				"createdAt" => $lists['createdAt']
			);
			
			$result["lists"][] = $location;
		}
		
		$shuttles->save($result);
// 		echo "<pre>";
// 		print_r($result);
// 		print_r($row);
// 		exit;
	}
/*	
	$shuttles->ensureIndex(array('lists.location' => '2dsphere'));
	$query = array(
		"geoNear" => "shuttles",
		"distanceField" => "dist.calculated",
		"includeLocs" => "dist.location",
		"near" => array("type" => "Point", "coordinates" => [126.7754522, 37.7709692]),
		"spherical" => true,
		"maxDistance" => 5000
	);
	$result = $db->command($query);
	echo "<pre>";
	print_r(count($result["results"]));
	print_r($result);
*/
?>
