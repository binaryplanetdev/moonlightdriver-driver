<?php
	header("Content-Type:text/html; charset=utf-8");
	require_once("../db.php");

	$shuttles = $db->shuttles;
	$shuttlesInfo = $db->shuttlesinfos;
	$shuttlePoints = $db->shuttlepoints;

	$cur = $shuttles->find();
	foreach($cur as $row) {
		$shuttle_document = array(
			"name" => $row['name'],
			"category" => $row['category'],
			"desc" => $row['desc']
		);
		
		if(!empty($row["phone"])) {
			$shuttle_document["phone"] = $row["phone"];
		}

		if(!empty($row["comment"])) {
			$shuttle_document["comment"] = array();
			$shuttle_document["comment"] = $row["comment"];
		}

		$shuttlesInfo->save($shuttle_document);
		$shuttle_id = $shuttle_document['_id'];
		
		
		foreach($row['lists'] as $lists) {
			$shuttle_points_doc = array(
				"shuttleId" => $shuttle_id . "",
				"index" => $lists['index'],
				"text" => $lists['text'],
				"locations" => array("type" => $lists['locations']['type'], "coordinates" => $lists['locations']['coordinates']),
				"createdAt" => $lists['createdAt']
			);
			
			$shuttlePoints->save($shuttle_points_doc);
		}
		
// 		echo "<pre>";
// 		print_r($result);
// 		print_r($row);
//  	exit;
	}
/*
	$shuttlePoints->ensureIndex(array('locations.coordinates' => '2dsphere'));
	$query = array(
		"geoNear" => "shuttlepoints",
		"distanceField" => "dist.calculated",
		"includeLocs" => "dist.location",
		"near" => array("type" => "Point", "coordinates" => [126.7754522, 37.7709692]),
		"spherical" => true,
		"maxDistance" => 5000
	);
	$result = $db->command($query);
	echo "<pre>";
	print_r(count($result["results"]));
	print_r($result);
*/
?>
