<?php
	header("Content-Type:text/html; charset=utf-8");
	require_once("../db.php");
	
	$coordinates = $db->coordinates;
	$call_history = $db->callhistories;
	
	$type = $_POST["type"];
	
	if($type == "t") {
		// 구별 좌표 데이터 조회
		$gu_data = array();
		
		$gu_coord = $coordinates->find(array("areaType" => "gu"))->sort(array('si_name' => 1, 'gu_name' => 1, 'dong_name' => 1));
		foreach($gu_coord as $row) {
			$gu_data[] = array(
				"si_name" => $row["si_name"],
				"gu_name" => $row["gu_name"],
				"dong_name" => $row["dong_name"],
				"coordinates" => $row["geometry"]["coordinates"][0]
			);
		}
		
		// 동별 좌표 데이터 조회
		$dong_data = array();
		
		$dong_coord = $coordinates->find(array("areaType" => "dong"))->sort(array('si_name' => 1, 'gu_name' => 1, 'dong_name' => 1));
		foreach($dong_coord as $row) {
			$dong_data[] = array(
				"si_name" => $row["si_name"],
				"gu_name" => $row["gu_name"],
				"dong_name" => $row["dong_name"],
				"coordinates" => $row["geometry"]["coordinates"][0]
			);
		}
		
		echo json_encode(array('gu_data' => $gu_data, 'dong_data' => $dong_data));
		exit;
	} else if($type == "coordinates_data") {
		// 구별 좌표 데이터 조회
		$gu_data = array(
			"type" => "FeatureCollection",
			"features" => array()
		);
		
		$gu_coord = $coordinates->find(array("areaType" => "gu"))->sort(array('si_name' => 1, 'gu_name' => 1, 'dong_name' => 1));
		foreach($gu_coord as $row) {
			$gu_data["features"][] = $row;
		}
		
		// 동별 좌표 데이터 조회
		$dong_data = array(
			"type" => "FeatureCollection",
			"features" => array()
		);
		
		$dong_coord = $coordinates->find(array("areaType" => "dong"))->sort(array('si_name' => 1, 'gu_name' => 1, 'dong_name' => 1));
		foreach($dong_coord as $row) {
			$dong_data["features"][] = $row;
		}
		
		echo json_encode(array('gu_data' => $gu_data, 'dong_data' => $dong_data));
		exit;
	} else if($type == "call_history") {
		$select_type = $_POST["select_type"];
		$ret = array();
		$gu_ret = array();
		
		switch($select_type) {
			case 0 :
				// 지난달 콜 수 조회
				$this_month = date("Ym");
				$last_month = date('Ym', strtotime($this_month . '-1 month'));
				
				$call_stats = $call_history->find(array("date" => $last_month))->sort(array('si_name' => 1, 'gu_name' => 1, 'dong_name' => 1));
				foreach($call_stats as $stat) {
					$gu_name = $stat["gu_name"];
					$name = $stat["dong_name"];
					$ret[$name] = $stat["call_count"];
					
					if(empty($gu_ret[$gu_name])) {
						$gu_ret[$gu_name] = 0;
					}
					
					$gu_ret[$gu_name] = $gu_ret[$gu_name] + $stat["call_count"];
				}
			break;
			case 1 :
				// 최근 3개월 평균 콜 수 조회
				$this_month = date("Ym");
				$last_month = date('Ym', strtotime($this_month . '-3 month'));
					
				$call_stats = $call_history->aggregate(
					array(
						array(
							'$match' => array(
								'date' => array('$gte' => $last_month)
							)
						),
						array(
							'$group' => array(
								'_id' => array(
									'si_name' => '$si_name',
									'gu_name' => '$gu_name',
									'dong_name' => '$dong_name'
								),
								'call_count' => array('$avg' => '$call_count'),
								'count' => array('$sum' => 1)
							)
						)
					)
				);
				
				foreach($call_stats['result'] as $stat) {
					$gu_name = $stat['_id']["gu_name"];
					$name = $stat['_id']["dong_name"];
					$ret[$name] = $stat["call_count"];
					
					if(empty($gu_ret[$gu_name])) {
						$gu_ret[$gu_name] = 0;
					}
					
					$gu_ret[$gu_name] = $gu_ret[$gu_name] + $stat["call_count"];
				}
			break;
			case 2 :
				// 최근 1년 평균 콜 수 조회
				$this_month = date("Ym");
				$last_month = date('Ym', strtotime($this_month . '-1 year'));
					
				$call_stats = $call_history->aggregate(
					array(
						array(
							'$match' => array(
								'date' => array('$gte' => $last_month)
							)
						),
						array(
							'$group' => array(
								'_id' => array(
									'si_name' => '$si_name',
									'gu_name' => '$gu_name',
									'dong_name' => '$dong_name'
								),
								'call_count' => array('$avg' => '$call_count'),
								'count' => array('$sum' => 1)
							)
						)
					)
				);
				
				foreach($call_stats['result'] as $stat) {
					$gu_name = $stat['_id']["gu_name"];
					$name = $stat['_id']["dong_name"];
					$ret[$name] = $stat["call_count"];
					
					if(empty($gu_ret[$gu_name])) {
						$gu_ret[$gu_name] = 0;
					}
					
					$gu_ret[$gu_name] = $gu_ret[$gu_name] + $stat["call_count"];
				}
			break;
		}
		
		echo json_encode(array("dong_data" => $ret, "gu_data" => $gu_ret, "total_count" => count($ret)));
		exit;
	}
?>
