<?php
	header("Content-Type:text/html; charset=utf-8");
	require_once("./db.php");

	$busroutes = $db->busroutes;
	$busstations = $db->busstations;
	
	$busRouteCursor = $busroutes->find();
	
	foreach ($busRouteCursor as $route_row) {
		echo "UPDATE BusRoute SET firstTime = '" . $route_row["firstTime"] . "', lastTime = '" . $route_row["lastTime"] . "' WHERE busRouteIndex = " . $route_row["busRouteIndex"] . ";\n";
	}
	
	$busStationCursor = $busstations->find();
	
	foreach ($busStationCursor as $station_row) {
		echo "UPDATE BusStation SET startTime = '" . $station_row["startTime"] . "', endTime = '" . $station_row["endTime"] . "' WHERE stationIndex = " . $station_row["stationIndex"] . ";\n";
	}
	
	exit;
?>