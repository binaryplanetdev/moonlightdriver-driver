<?php
	header("Content-Type:text/html; charset=utf-8");
	require_once("../db.php");
	
	$SERVICE_ID = "0aab2ced55dc3c47f901b51414464099";
	
	$busroutes_odsay_backup_20160306 = $db->busroutes_odsay_backup_20160306;
	$busstations_odsay_backup_20160306 = $db->busstations_odsay_backup_20160306;
	
	$busroutes = $db->busroutes;
	$busroutestations = $db->busroutestations;
	$busstations = $db->busstations;
	
	$busRouteStationIndex = 0;
	
// 	$busRouteCursor = $busroutes_odsay_backup_20160306->find()->sort(array("busId" => 1));
	
// 	echo "bus route start!!\n";
// 	foreach ($busRouteCursor as $route_row) {
// 		$newRouteData = array(
// 			"busRouteId" => $route_row["busId"],
// 			"localBusRouteId" => $route_row["busRouteId"],
// 			"busRouteName" => $route_row["busRouteName"],
// 			"busRouteType" => $route_row["busRouteType"],
// 			"busRouteTypeName" => $route_row["busRouteTypeName"],
// 			"busRouteArea" => $route_row["busRouteArea"],
// 			"startStationName" => $route_row["startStationName"],
// 			"endStationName" => $route_row["endStationName"],
// 			"firstTime" => $route_row["firstTime"],
// 			"lastTime" => $route_row["lastTime"],
// 			"term" => $route_row["term"]
// 		);
		
// 		$busroutes->insert($newRouteData);
		
// 		$stationCount = 0;
// 		$busRouteStationCursor = $busstations_odsay_backup_20160306->find(array("busId" => $route_row["busId"]))->sort(array("stationSequence" => 1));
// 		foreach ($busRouteStationCursor as $station_row) {
// 			$stationCount++;
// 			$busRouteStationIndex++;
			
// 			$startTimeHour = intval(substr($station_row["startTime"], 0, 2));
// 			$endTimeHour = intval(substr($station_row["endTime"], 0, 2));
			
// 			if($endTimeHour < $startTimeHour) {
// 				$endTimeHour = $endTimeHour + 24;
// 			}
			
// 			$endTime = $endTimeHour . substr($station_row["endTime"], 2, 3);
			
// 			$newRouteStationData = array(
// 				"busRouteStationIndex" => $busRouteStationIndex,
// 				"busRouteId" => $station_row["busId"],
// 				"arsId" => $station_row["arsId"],
// 				"stationSequence" => $station_row["stationSequence"],
// 				"startTime" => $station_row["startTime"],
// 				"endTime" => $endTime,
// 				"direction" => ""
// 			);
			
// 			$busroutestations->insert($newRouteStationData);
// 		}
		
// 		echo $route_row["busRouteName"] . "[" . $route_row["busId"] . "] has " . $stationCount . " stations;\n";
// 	}
	
	$busStationCursor = $busstations_odsay_backup_20160306->aggregate(
		array(
			'$group' => array(
				'_id' => array(
					'arsId' => '$arsId',
					'stationName' => '$stationName',
					'gpsX' => '$gpsX',
					'gpsY' => '$gpsY',
				)
			)
		)
	);
	
	
	$stationIndex = 0;
	
	echo "bus station start!!\n";
	foreach ($busStationCursor["result"] as $station_row) {
		$stationIndex++;
		
		$newStationData = array(
			"stationIndex" => $stationIndex,
			"arsId" => $station_row["_id"]["arsId"],
			"stationName" => $station_row["_id"]["stationName"],
			"gpsX" => $station_row["_id"]["gpsX"],
			"gpsY" => $station_row["_id"]["gpsY"]
		);
		
		$busstations->insert($newStationData);
		
		echo $stationIndex . " : " . $station_row["_id"]["stationName"] . "\n";
	}
	
	echo "end!!!\n";
	
	exit;
?>
