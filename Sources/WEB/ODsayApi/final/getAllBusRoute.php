<?php
	header("Content-Type:text/html; charset=utf-8");
	require_once("../db.php");
	require_once("chosung.php");
	require_once("pointLocation.php");
	
	$SERVICE_ID = "0aab2ced55dc3c47f901b51414464099";
	
	$busRouteType = array(
		1 => "일반",
		2 => "좌석",
		3 => "마을버스",
		4 => "직행좌석",
		5 => "공항버스",
		6 => "간선급행",
		10 => "외곽",
		11 => "간선",
		12 => "지선",
		13 => "순환",
		14 => "광역",
		15 => "급행",
		20 => "농어촌버스",
		21 => "제주도 시외형버스",
		22 => "경기도 시외형버스",
		26 => "급행간선"
	);
	
	$pointLocation = new pointLocation();
	
	$retRoute = file_get_contents("./seoul_coordinates.json");
	$retJson = json_decode($retRoute);
	
	$polygon = array();
	
	foreach ($retJson as $c) {
		$polygon[] = $c[1] . "," . $c[0];
	}
	
	$odsaybuscompanies = $db->odsaybuscompanies;
	$busroutes = $db->busroutes;
	$busstations = $db->busstations;
	
	$busCompanyCursor = $odsaybuscompanies->find()->sort(array("busID" => 1));
	
	$busRouteIndex = 1;
	$stationIndex = 1;
	foreach ($busCompanyCursor as $route_row) {
		$route_data = array(
			"busRouteIndex" => $busRouteIndex++,
			"busId" => $route_row["busID"],
			"busRouteId" => $route_row["localBusID"],
			"busRouteName" => $route_row["busNo"],
			"busRouteName_s" => GetUtf8String($route_row["busNo"]),
			"busRouteType" => $route_row["type"],
			"busRouteTypeName" => $busRouteType[$route_row["type"]],
			"busRouteArea" => $route_row["busCityName"],
			"startStationName" => $route_row["busStartPoint"],
			"endStationName" => $route_row["busEndPoint"],
			"firstTime" => $route_row["busFirstTime"],
			"lastTime" => $route_row["busLastTime"],
			"term" => $route_row["busInterval"],
			"companyId" => $route_row["companyID"]
		);
	
		$busroutes->insert($route_data);
		
		echo $route_row["busNo"] . "[" . $route_row["busID"] . "] route ";
		
		$ret = file_get_contents("http://dev.odsay.com/denny_test/appletree/v1/0/Bus/Lane/result.asp?busID=" . $route_row["busID"] . "&svcid=" . $SERVICE_ID . "&output=json");
		
		$retList = json_decode($ret, true);
		
		$stationCount = 0;
		if(isset($retList)) {
			$stationSequence = 1;
			$stationCount = count($retList["result"]["station"]);
			foreach ($retList["result"]["station"] as $station_row) {
				$point = $station_row["y"] . "," . $station_row["x"];
				$inPolygon = $pointLocation->pointInPolygon($point, $polygon);
				
				$inSeoul = 0;
				if($inPolygon != "outside") {
					$inSeoul = 1;
				}
				
				$station_data = array(
					"stationIndex" => $stationIndex++,
					"busRouteIndex" => $route_data["busRouteIndex"],
					"busId" => $route_data["busId"],
					"busRouteId" => $route_data["busRouteId"],
					"busRouteName" => $route_data["busRouteName"],
					"busRouteArea" => $route_data["busRouteArea"],
					"stationSequence" => $stationSequence++,
					"arsId" => $station_row["stationID"],
					"stationName" => $station_row["stationName"],
					"stationName_s" => GetUtf8String($station_row["stationName"]),
					"gpsX" => $station_row["x"],
					"gpsY" => $station_row["y"],
					"stationDistance" => $station_row["stationDistance"],
					"firstTime" => "",
					"lastTime" => "",
					"direction" => "",
					"inSeoul" => $inSeoul
				);
				
				$busstations->insert($station_data);
			}
		}
		
		echo "has " . $stationCount . " stations;\n";
	}
	
	exit;
?>