<?php
	header("Content-Type:text/html; charset=utf-8");
	require_once("../db.php");

	$busroutes = $db->busroutes;
	$busstations = $db->busstations;
	
	$busRouteCursor = $busroutes->find()->sort(array("busRouteIndex" => 1));
	
	$i = 0;
	foreach ($busRouteCursor as $route_row) {
		updateBusStation($busstations, $route_row["busId"], $route_row["firstTime"], $route_row["lastTime"]);
		
		$route_row["startTime"] = $route_row["firstTime"];
		$route_row["endTime"] = $route_row["lastTime"];
		
		$busroutes->save($route_row);
		
		echo "(" . (++$i) . ") " . $route_row["busRouteName"] . "[" . $route_row["busId"] . "]\n";
	}
	
	function updateBusStation($_busstations, $_busId, $_first_time, $_last_time) {
		$busStationCursor = $_busstations->find(array("busId" => $_busId))->sort(array("stationSequence" => 1));
		
		$isFirstTimeConverted = false;
		$firstTimeArray = split(":", $_first_time);
		$firstTimeHours = intval($firstTimeArray[0]);
		if($firstTimeHours > 23) {
			$isFirstTimeConverted = true;
			$firstTimeHours = $firstTimeHours -24;
			if($firstTimeHours < 10) {
				$firstTimeHours = "0" + $firstTimeHours;
			}
			
			$_first_time = $firstTimeHours + ":" + $firstTimeArray[1];
		}
		
		$isLastTimeConverted = false;
		$lastTimeArray = split(":", $_last_time);
		$lastTimeHours = intval($lastTimeArray[0]);
		if($lastTimeHours > 23) {
			$isLastTimeConverted = true;
			$lastTimeHours = $lastTimeHours -24;
			if($lastTimeHours < 10) {
				$lastTimeHours = "0" + $lastTimeHours;
			}
				
			$_last_time = $lastTimeHours + ":" + $lastTimeArray[1];
		}
		
		$prev_lat = null;
		$prev_lng = null;
		$prev_first_time = $_first_time . ":00";
		$prev_last_time = $_last_time . ":00";
		
		$time_per_meter = floatval(120 / 1000);
		
		foreach ($busStationCursor as $row) {
			$increase_time = 0;
			$dist = 0;
			
			if($prev_lat != null) {
				$dist = get_distance($prev_lat, $prev_lng, $row["gpsY"], $row["gpsX"]);
				$increase_time = ($dist * $time_per_meter);
			}
			
			$first_time = date("H:i:s", strtotime(date("Ymd") . " " . $prev_first_time) + $increase_time);
			$last_time = date("H:i:s", strtotime(date("Ymd") . " " . $prev_last_time) + $increase_time);
			
			if($isFirstTimeConverted) {
				$firstTimeArray = split(":", $first_time);
				$firstTimeHours = intval($firstTimeArray[0]);
				
				$firstTimeHours = $firstTimeHours + 24;
				
				$first_time = $firstTimeHours + $firstTimeArray[1];
			}
			
			if($isLastTimeConverted) {
				$lastTimeArray = split(":", $last_time);
				$lastTimeHours = intval($lastTimeArray[0]);
				
				$lastTimeHours = $lastTimeHours + 24;
				
				$last_time = $lastTimeHours + $lastTimeArray[1];
			}
			
			$row["startTime"] = substr($first_time, 0, 5);
			$row["endTime"] = substr($last_time, 0, 5);
			$row["location"] = array(
				"type" => "Point",
				"coordinates" => array($row["gpsX"], $row["gpsY"])
			);
			
			$_busstations->save($row);
			
			$prev_lat = $row["gpsY"];
			$prev_lng = $row["gpsX"];
				
			$prev_first_time = $first_time;
			$prev_last_time = $last_time;
		}
	}
	
	function get_distance($lat1, $lon1, $lat2, $lon2) {
		/* WGS84 stuff */
		$a = 6378137;
		$b = 6356752.3142;
		$f = 1/298.257223563;
		/* end of WGS84 stuff */
	
		$L = deg2rad($lon2-$lon1);
		$U1 = atan((1-$f) * tan(deg2rad($lat1)));
		$U2 = atan((1-$f) * tan(deg2rad($lat2)));
		$sinU1 = sin($U1);
		$cosU1 = cos($U1);
		$sinU2 = sin($U2);
		$cosU2 = cos($U2);
	
		$lambda = $L;
		$lambdaP = 2*pi();
		$iterLimit = 20;
		while ((abs($lambda-$lambdaP) > pow(10, -12)) && ($iterLimit-- > 0)) {
			$sinLambda = sin($lambda);
			$cosLambda = cos($lambda);
			$sinSigma = sqrt(($cosU2*$sinLambda) * ($cosU2*$sinLambda) + ($cosU1*$sinU2-$sinU1*$cosU2*$cosLambda) * ($cosU1*$sinU2-$sinU1*$cosU2*$cosLambda));
	
			if ($sinSigma == 0) {
				return 0;
			}
	
			$cosSigma   = $sinU1*$sinU2 + $cosU1*$cosU2*$cosLambda;
			$sigma      = atan2($sinSigma, $cosSigma);
			$sinAlpha   = $cosU1 * $cosU2 * $sinLambda / $sinSigma;
			$cosSqAlpha = 1 - $sinAlpha*$sinAlpha;
			$cos2SigmaM = $cosSigma - 2*$sinU1*$sinU2/$cosSqAlpha;
	
			if (is_nan($cos2SigmaM)) {
				$cos2SigmaM = 0;
			}
	
			$C = $f/16*$cosSqAlpha*(4+$f*(4-3*$cosSqAlpha));
			$lambdaP = $lambda;
			$lambda = $L + (1-$C) * $f * $sinAlpha *($sigma + $C*$sinSigma*($cos2SigmaM+$C*$cosSigma*(-1+2*$cos2SigmaM*$cos2SigmaM)));
		}
	
		if ($iterLimit == 0) {
			// formula failed to converge
			return NaN;
		}
	
		$uSq = $cosSqAlpha * ($a*$a - $b*$b) / ($b*$b);
		$A = 1 + $uSq/16384*(4096+$uSq*(-768+$uSq*(320-175*$uSq)));
		$B = $uSq/1024 * (256+$uSq*(-128+$uSq*(74-47*$uSq)));
		$deltaSigma = $B*$sinSigma*($cos2SigmaM+$B/4*($cosSigma*(-1+2*$cos2SigmaM*$cos2SigmaM)- $B/6*$cos2SigmaM*(-3+4*$sinSigma*$sinSigma)*(-3+4*$cos2SigmaM*$cos2SigmaM)));
	
		// 		return round($b*$A*($sigma-$deltaSigma) / 1000);
		return round($b*$A*($sigma-$deltaSigma));
	
	
		/* sphere way */
		$distance = rad2deg(acos(sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($lon1 - $lon2))));
	
		$distance *= 111.18957696; // Convert to km
	
		return $distance;
	}
	
	exit;
	
	
?>