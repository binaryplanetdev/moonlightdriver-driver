<?php
	header("Content-Type:text/html; charset=utf-8");
	require_once("../db.php");

	$busroutestations = $db->busroutestations;
	$busstations = $db->busstations;
	
	$busRouteStationCursor = $busroutestations->find();
	
	$i = 0;
	foreach ($busRouteStationCursor as $row) {
		unset($row["location"]);
		$busroutestations->save($row);
		echo $row["busRouteStationIndex"] . "\n";
	}
	
	$busStationCursor = $busstations->find()->sort(array("staitonIndex" => 1));
	
	$i = 0;
	foreach ($busStationCursor as $row) {
		$row["location"] = array(
			"type" => "Point",
			"coordinates" => array($row["gpsX"], $row["gpsY"])
		);
		
		$busstations->save($row);
		echo $row["stationName"] . "[" . $row["staitonIndex"] . "]" . "\n";
	}
?>