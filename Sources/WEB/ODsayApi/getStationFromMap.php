<?php
	$GOOGLE_API_KEY = "AIzaSyDgKpZDI_xkyqEQtE-F6hWpdnkLOPXofRA";
?>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<script type="text/javascript">
			var MAX_DISTANCE = 50;
			var map;
			var map_center;
			var rectangles = [];
			var marker;
			function initMap() {
				map_center = new google.maps.LatLng(37.566657, 126.978307);
				map = new google.maps.Map(document.getElementById('map'), {
					center: map_center,
					zoom: 8
				});

				google.maps.event.addListener(map, 'idle', function() {
					console.log("zoom : " + map.getZoom());
				});

				google.maps.event.addListener(map, 'click', function(_event) {
					drawMarker(_event);
				});
			}

			function drawMarker(_position) {
				if(marker) {
					marker.setMap(null);
				}
				
				marker = new google.maps.Marker({
					position: _position.latLng,
					map: map,
					draggable: true
				});

				marker.addListener('dragend', drawRectangle);

				drawRectangle(_position);
			}

			function drawRectangle(_position) {
				if(rectangles.length > 0) {
					for(var i = 0; i < rectangles.length; i++) {
						rectangles[i].setMap(null);
					}

					rectangles = [];
				}

				console.log("lat : " + _position.latLng.lat() + ", lng : " + _position.latLng.lng());

				var position;
				var distance;
				var centerY = _position.latLng;
				var centerX = _position.latLng;
				for(var y = 0; y < 7; y++) {
					distance = (MAX_DISTANCE * 2) * y;
					if(distance > 0) {
						var distanceCoordinates = getCoordinates(_position.latLng, distance, 1, 4);
						centerY = distanceCoordinates[3];
						centerX = centerY;
					}
					
					for(var x = 0; x < 6; x++) {
						distance = (MAX_DISTANCE * 2) * x;
						if(distance > 0) {
							var distanceCoordinates = getCoordinates(centerY, distance, 1, 4);
							centerX = distanceCoordinates[0];	
						}
						
						var bounds = getCoordinates(centerX, MAX_DISTANCE, 1, 4);
						
						var rectangle = new google.maps.Rectangle({
							strokeColor: '#FF0000',
						    strokeOpacity: 0.8,
						    strokeWeight: 2,
						    fillColor: '#FF0000',
						    fillOpacity: 0.35,
						    map: map,
						    bounds: {
							    north: bounds[1].lat(),
							    south: bounds[3].lat(),
							    east: bounds[0].lng(),
							    west: bounds[2].lng()
						    }
						});

						console.log("param=" + bounds[2].lng() + ":" + bounds[1].lat() + ":" + bounds[0].lng() + ":" + bounds[3].lat());

						rectangles.push(rectangle);
					}	
				}				
			}

			function getCoordinates(_center, _radius, _dir, _points) {
				var d2r = Math.PI / 180;   // degrees to radians
				var r2d = 180 / Math.PI;   // radians to degrees
				var earthsradius = 3963; // 3963 is the radius of the earth in miles

				_radius = _radius / 1.61;
				
				// find the raidus in lat/lon
				var rlat = (_radius / earthsradius) * r2d;
				var rlng = rlat / Math.cos(_center.lat() * d2r);

				var extp = new Array();
				if (_dir == 1)	{
					var start = 0;
					var end = _points + 1;
				} else {
					var start = _points + 1;
					var end = 0;
				}

				for (var i = start; (_dir == 1 ? i < end : i > end); i = i + _dir) {
					var theta = Math.PI * (i / (_points / 2));
					ey = _center.lng() + (rlng * Math.cos(theta)); // center a + radius x * cos(theta)
					ex = _center.lat() + (rlat * Math.sin(theta)); // center b + radius y * sin(theta)

					extp.push(new google.maps.LatLng(ex, ey));
				}

				return extp;
			}
		</script>
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo $GOOGLE_API_KEY; ?>&callback=initMap" async defer></script>
	</head>
	<body style="width: 100%; height: 100%;">
		<div id="map" style="width: 100%; height: 900px;"></div>
	</body>
</html>