<?php
	header("Content-Type:text/html; charset=utf-8");
	require_once("./db.php");

	$busroutes = $db->busroutes;
	$busstations = $db->busstations;
	
	$i = 0;
	$busStationCursor = $busstations->find();
	foreach ($busStationCursor as $station_row) {
		if($station_row["startTime"] <= $station_row["endTime"]) {
			continue;
		}
		
		$i++;
		
		$endTimeArray = split(":", $station_row["endTime"]);
		$endTimeHour = intval(substr($endTimeArray[0], 0, 2));
		$endTimeHour = $endTimeHour + 24;
			
		$station_row["endTime"] = $endTimeHour . ":" . $endTimeArray[1];
			
		$busstations->save($station_row);
		
		echo "(" . $i . ")" . $station_row["stationName"] . "[" . $station_row["stationIndex"] . "] is updated;\n";
	}

	exit;
?>