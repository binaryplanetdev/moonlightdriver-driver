<?php
	header("Content-Type:text/html; charset=utf-8");
	require_once("./db.php");

	$busstations = $db->busstations;
	
	$retBusRouteCursor = $busstations->find()->sort(array("busRouteIndex" => 1));
	
	$busStationListCursor = $busstations->aggregate(
		array(
			'$group' => array(
				'_id' => array(
					'arsId' => '$arsId',
					'stationName' => '$stationName'
				),
				'shuttle_times' => array(
					'$push' => array(
						'startTime' => '$startTime',
						'endTime' => '$endTime'
					)
				)
			)
		)
	);
	
	foreach ($busStationListCursor as $row) {
		echo "<pre>";
		print_r($row);
		exit;
	}

	exit;
?>