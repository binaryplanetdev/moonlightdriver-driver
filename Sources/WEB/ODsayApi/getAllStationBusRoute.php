<?php
	header("Content-Type:text/html; charset=utf-8");
	require_once("./db.php");
	
	$odsaybusroutes = $db->odsaybusroutes;
	$odsaybusstations = $db->odsaybusstations;
	
	$busStationCursor = $odsaybusstations->find()->sort(array("stationID" => 1));
	
	$i = 0;
	foreach ($busStationCursor as $row) {
		$i++;
		
		$ret = file_get_contents("http://test.odsay.com/Bus/LaneMap/XML_BusStopResult.asp?BID=" . $row["stationID"]);
		
		$parsedXml = simplexml_load_string($ret, "SimpleXMLElement", LIBXML_NOCDATA);
		
		echo "(" . $i . ") " . $row["stationName"] . "[" . $row["stationID"] . "] has " . count($parsedXml->lane) . " routes;\n";
		foreach ($parsedXml->lane as $busRoute) {
			echo $busRoute->busNo . "[" . $busRoute->busID . "], ";
			
			$data = array(
				"busID" => strval($busRoute->busID),
				"busNo" => strval($busRoute->busNo),
				"type" => intval($busRoute->type)
			);
			
			$odsaybusroutes->insert($data);
		}
		
		echo "\n";
	}
	
	exit;
?>