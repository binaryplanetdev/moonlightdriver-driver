<?php
	header("Content-Type:text/html; charset=utf-8");
// 	ini_set('max_execution_time', 300000);
	
// 	$db = new SQLite3("D:/Project/01.MoonlightDriver/Source/ANDROIDSTUDIO/moon_android_driver/Driver/src/main/assets/bus_data.db"); 

// 	$page = 2;
// 	$limit = 100000;
// 	$offset = $page * $limit;
	
// 	$stationCursor = $db->query("select stationIndex, minStartTime, maxEndTime from BusStation limit " . $limit . " offset " . $offset . ";");
// 	$stationList = array();
// 	while ($row = $stationCursor->fetchArray(SQLITE3_ASSOC)) {
// 		$stationList[] = $row;
// 	}
	
// 	echo count($stationList) . " Station Start!<br/>";
	
// 	$retStationData = array();
// 	foreach($stationList as $row) {
// 		$retStationData[$row["stationIndex"]] = $row;
// 	}
	
// 	echo "<pre>";
// 	print_r(json_encode($retStationData, JSON_UNESCAPED_UNICODE));
// 	exit;

	require_once("./db.php");
	
	$busstations = $db->busstations;

	$result = file_get_contents("busStationRunningTime.json");
	$stationList = json_decode($result, true);
	
	$i = 0;
	foreach ($stationList as $key => $row) {
		$i++;
		
		$busstations->update(array("stationIndex" => $key), array('$set' => array("minStartTime" => $row["minStartTime"], "maxEndTime" => $row["maxEndTime"])));
		
		echo "(" . $i . ")" . $key . "\n";
	}
?>