package kr.moonlightdriver.driver.Network.NetData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by youngmin on 2015-12-31.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShuttleTransferLocationData {
	@JsonProperty("shuttleName")
	private String mShuttleName;

	@JsonProperty("routeType")
	private String mRouteType;

	@JsonProperty("text")
	private String mStationName;

	@JsonProperty("transferName")
	private String mTransferName;

	@JsonProperty("lat")
	private double mLat;

	@JsonProperty("lng")
	private double mLng;

	public ShuttleTransferLocationData() {
	}

	public ShuttleTransferLocationData(String mShuttleName, String mRouteType, String mStationName, String mTransferName, double mLat, double mLng) {
		this.mShuttleName = mShuttleName;
		this.mRouteType = mRouteType;
		this.mStationName = mStationName;
		this.mTransferName = mTransferName;
		this.mLat = mLat;
		this.mLng = mLng;
	}

	public String getmTransferName() {
		return mTransferName;
	}

	public void setmTransferName(String mTransferName) {
		this.mTransferName = mTransferName;
	}

	public String getmShuttleName() {
		return mShuttleName;
	}

	public void setmShuttleName(String mShuttleName) {
		this.mShuttleName = mShuttleName;
	}

	public String getmRouteType() {
		return mRouteType;
	}

	public void setmRouteType(String mRouteType) {
		this.mRouteType = mRouteType;
	}

	public String getmStationName() {
		return mStationName;
	}

	public void setmStationName(String mStationName) {
		this.mStationName = mStationName;
	}

	public double getmLat() {
		return mLat;
	}

	public void setmLat(double mLat) {
		this.mLat = mLat;
	}

	public double getmLng() {
		return mLng;
	}

	public void setmLng(double mLng) {
		this.mLng = mLng;
	}
}
