package kr.moonlightdriver.driver.fragment;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.loopj.android.http.RequestParams;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import kr.moonlightdriver.driver.Network.NetApi.CarpoolListAck;
import kr.moonlightdriver.driver.Network.NetApi.NetAPI;
import kr.moonlightdriver.driver.Network.NetApi.NetResponse;
import kr.moonlightdriver.driver.Network.NetApi.NetResponseCallback;
import kr.moonlightdriver.driver.Network.NetClient.NetClient;
import kr.moonlightdriver.driver.Network.NetData.CarpoolData;
import kr.moonlightdriver.driver.Network.NetData.DriverPointData;
import kr.moonlightdriver.driver.Network.NetData.ResultData;
import kr.moonlightdriver.driver.R;
import kr.moonlightdriver.driver.activity.MainActivity;
import kr.moonlightdriver.driver.utils.BalloonAdapter;
import kr.moonlightdriver.driver.utils.Global;
import kr.moonlightdriver.driver.utils.SVUtil;

/**
 * Created by redjjol on 3/08/15.
 */
public class CarpoolFragment extends Fragment implements View.OnClickListener {
	private static final String TAG = "CarpoolFragment";
	private static final int LIMIT_COUNT = 20;
	private static final int MAX_SEARCH_RANGE = 5000;
	private GoogleMap mGoogleMap;
	private LinearLayout mCarpoolListLayout;

	private ImageButton mBtnClose;
	private ImageButton mBtnMakeCarpool;
	private ImageButton mBtnCall;
	private ImageButton mBtnCurrentPosition;
	private Button mBtnShowCarpoolList;
	private Button mBtnMakeCarpool2;
	private Button mBtnPushAlarm;
	private Button mBtnPushAlarm2;

	private Marker mCurrentPositionMarker;

	private ArrayList<CarpoolData> mCarpoolList;
	private CarpoolListAdapter mCarpoolListAdapter;

	private TextView mCarpoolListNoItemTextView;

	private ArrayList<Marker> mCarpoolRegistrantMarkers;
	private ArrayList<Marker> mNearDriverMarkers;

	private MainActivity mMainActivity;

	private View mView;

	private int mPageNum;
	private int mPreLast;

	public static boolean mIsFinishFragment;

	private Marker mShowInfoWindowMarker;

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		try {
			mView = inflater.inflate(R.layout.frag_carpool, container, false);

			mMainActivity = (MainActivity) getActivity();

			mIsFinishFragment = false;

			mCarpoolRegistrantMarkers = new ArrayList<>();
			mNearDriverMarkers = new ArrayList<>();

			mBtnPushAlarm = (Button) mView.findViewById(R.id.frag_sb_carpool_pushBtn);
			mBtnPushAlarm2 = (Button) mView.findViewById(R.id.frag_sb_carpool_pushBtn2);
			mBtnMakeCarpool2 = (Button) mView.findViewById(R.id.frag_sb_carpool_createBtn);
			mBtnCurrentPosition = (ImageButton) mView.findViewById(R.id.frag_sb_carpool_currentBtn);
			mBtnShowCarpoolList = (Button) mView.findViewById(R.id.frag_sb_carpool_listBtn);
			mBtnClose = (ImageButton) mView.findViewById(R.id.frag_sb_carpool_closeBtn);
			mBtnMakeCarpool = (ImageButton) mView.findViewById(R.id.frag_sb_carpool_makeBtn);
			mBtnCall = (ImageButton) mView.findViewById(R.id.frag_sb_carpool_callBtn);
			mCarpoolListNoItemTextView = (TextView) mView.findViewById(R.id.frag_sb_carpool_no_item_text_view);
			ImageButton btnZoomIn = (ImageButton) mView.findViewById(R.id.frag_sb_carpool_zoomInBtn);
			btnZoomIn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mGoogleMap.animateCamera(CameraUpdateFactory.zoomIn());
				}
			});

			ImageButton btnZoomOut = (ImageButton) mView.findViewById(R.id.frag_sb_carpool_zoomOutBtn);
			btnZoomOut.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mGoogleMap.animateCamera(CameraUpdateFactory.zoomOut());
				}
			});

			mBtnMakeCarpool.setOnClickListener(this);
			mBtnClose.setOnClickListener(this);
			mBtnCurrentPosition.setOnClickListener(this);
			mBtnShowCarpoolList.setOnClickListener(this);
			mBtnCall.setOnClickListener(this);
			mBtnPushAlarm.setOnClickListener(this);
			mBtnPushAlarm2.setOnClickListener(this);
			mBtnMakeCarpool2.setOnClickListener(this);

			changePushAlarm();

			mCarpoolListLayout = (LinearLayout) mView.findViewById(R.id.frag_sb_carpool_listLL);
			ListView carpoolListView = (ListView) mView.findViewById(R.id.frag_sb_carpool_listView);
			carpoolListView.setOnItemClickListener(new OnItemClick_taxi());

			mCarpoolList = new ArrayList<>();
			mCarpoolListAdapter = new CarpoolListAdapter(mMainActivity, R.layout.row_carpool_list, mCarpoolList);
			carpoolListView.setAdapter(mCarpoolListAdapter);

			carpoolListView.setOnScrollListener(new AbsListView.OnScrollListener() {
				@Override
				public void onScrollStateChanged(AbsListView view, int scrollState) {
				}

				@Override
				public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
					try {
						if (firstVisibleItem == 0 && visibleItemCount <= totalItemCount) {
							return;
						}

						int lastItem = firstVisibleItem + visibleItemCount;
						if (lastItem == totalItemCount && mPreLast != lastItem) {
							mPreLast = lastItem;
							mPageNum = mPageNum + 1;
							getCarpoolList(mPageNum);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			setUpMapIfNeeded();
		} catch(Exception e) {
			e.printStackTrace();
		}

		return mView;
	}

	private void setEventListener() {
		try {
			mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
				@Override
				public boolean onMarkerClick(Marker marker) {
					try {
						if (marker.getSnippet() == null) {
							return true;
						}

						if(mShowInfoWindowMarker != null && mShowInfoWindowMarker.getSnippet().equals(marker.getSnippet())) {
							String[] snippetData = marker.getSnippet().split("_");
							showCarpoolDetailFragment(snippetData[0], snippetData[1]);
						} else {
							marker.showInfoWindow();
							mShowInfoWindowMarker = marker;
						}
					} catch (Exception e) {
						e.printStackTrace();
					}

					return false;
				}
			});

			mGoogleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
				@Override
				public void onInfoWindowClick(Marker marker) {
					try {
						if (marker.getSnippet() == null) {
							return;
						}

						String[] snippetData = marker.getSnippet().split("_");

						showCarpoolDetailFragment(snippetData[0], snippetData[1]);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
				@Override
				public void onMapClick(LatLng latLng) {
					try {
						if (mShowInfoWindowMarker != null) {
							mShowInfoWindowMarker.hideInfoWindow();
							mShowInfoWindowMarker = null;
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mMainActivity.mCurrentLocation, 15));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void changeMyLocation() {
		try {
			SVUtil.log("error", TAG, "===== changeMyLocation() =====");

			if(mIsFinishFragment) {
				return;
			}

			if (mGoogleMap != null) {
				mCurrentPositionMarker.setPosition(mMainActivity.mCurrentLocation);
				mGoogleMap.animateCamera(CameraUpdateFactory.newLatLng(mMainActivity.mCurrentLocation));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void showCarpoolDetailFragment(String _taxiId, String _phone) {
		try {
			Bundle args = new Bundle();
			args.putString("taxiId", _taxiId);
			args.putString("registrant_phone", _phone);

			CarpoolDetailFragment frag = new CarpoolDetailFragment();
			frag.setArguments(args);

			mMainActivity.replaceFragment(frag, R.id.mainFrameLayout, Global.FRAG_CARPOOL_DETAIL_TAG, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void getCarpoolList(int _page) {
		SVUtil.showProgressDialog(mMainActivity, "로딩중...");

		RequestParams params = new RequestParams();
		params.add("lat", mMainActivity.mCurrentLocation.latitude + "");
		params.add("lng", mMainActivity.mCurrentLocation.longitude + "");
		params.add("dist", MAX_SEARCH_RANGE + "");
		params.add("pageNum", _page + "");
		params.add("limit", LIMIT_COUNT + "");

		NetClient.send(mMainActivity, Global.URL_CARPOOL_LIST, "POST", params, new CarpoolListAck(), new NetResponseCallback(new NetResponse() {
			@Override
			public void onResponse(NetAPI _netAPI) {
				SVUtil.hideProgressDialog();

				try {
					if (_netAPI != null && !mIsFinishFragment) {
						CarpoolListAck retNetApi = (CarpoolListAck) _netAPI;
						ResultData result = retNetApi.getmResultData();

						if (result.getmCode() != 100) {
							SVUtil.showSimpleDialog(mMainActivity, result.getmDetail());
						} else {
							ArrayList<CarpoolData> retData = retNetApi.getmCarpoolList();
							ArrayList<DriverPointData> driverPointData = retNetApi.getmDriverPointData();
							ArrayList<DriverPointData> nearDriverList = retNetApi.getmNearDriverList();
							int pointsSize = driverPointData.size();
							String markerText;
							String snippet;
							LatLng markerPosition;

							int listSize = retData.size();
							for (int i = 0; i < listSize; i++) {
								if (mGoogleMap == null) {
									return;
								}

								mCarpoolList.add(retData.get(i));

								for (int j = 0; j < pointsSize; j++) {
									DriverPointData item = driverPointData.get(j);
									if (retData.get(i).getmRegistrantPhone().equals(item.getmPhoneNumber())) {
										markerText = item.getmPhoneNumber().substring(item.getmPhoneNumber().length() - 4, item.getmPhoneNumber().length()) + " 기사님";
										markerPosition = new LatLng(item.getmLocation().getmCoordinates().get(1), item.getmLocation().getmCoordinates().get(0));
										snippet = retData.get(i).getmTaxiId() + "_" + item.getmPhoneNumber();

										addTextMarker(markerText, markerPosition, snippet);

										break;
									}
								}
							}

							for(DriverPointData data : nearDriverList) {
								if(data.getmPhoneNumber().equals(mMainActivity.mUserPhoneNumber)) {
									continue;
								}

								markerText = data.getmPhoneNumber().substring(data.getmPhoneNumber().length() - 4, data.getmPhoneNumber().length()) + " 기사님";
								markerPosition = new LatLng(data.getmLocation().getmCoordinates().get(1), data.getmLocation().getmCoordinates().get(0));
								snippet = data.getmPhoneNumber();

								addNearDriverMarker(markerText, markerPosition, snippet);
							}

							if (mCarpoolList.size() > 0) {
								mCarpoolListNoItemTextView.setVisibility(View.GONE);
								mCarpoolListLayout.setVisibility(View.VISIBLE);
							} else {
								mCarpoolListNoItemTextView.setVisibility(View.VISIBLE);
								mCarpoolListLayout.setVisibility(View.GONE);
							}
						}

						mCarpoolListAdapter.notifyDataSetChanged();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}));
	}

	private void addNearDriverMarker(String _text, LatLng _position, String _snippet) {
		try {
			Marker m = mGoogleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_driver_40)).position(_position).title(_text).snippet(_snippet).anchor(0.5f, 1.0f));

			mNearDriverMarkers.add(m);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void addTextMarker(String _text, LatLng _position, String _snippet) {
		try {
			Marker m = mGoogleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_taxi_registrant_40)).position(_position).title(_text).snippet(_snippet).anchor(0.5f, 1.0f));

			mCarpoolRegistrantMarkers.add(m);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void removeRegistrantMarkers() {
		try {
			for(int i = 0; i < mCarpoolRegistrantMarkers.size(); i++) {
				mCarpoolRegistrantMarkers.get(i).remove();
			}

			mCarpoolRegistrantMarkers.clear();
			mCarpoolRegistrantMarkers = new ArrayList<>();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void destroyFragment() {
		try {
			if(mGoogleMap != null) {
				mGoogleMap.clear();
				mGoogleMap = null;
			}

			SupportMapFragment f = (SupportMapFragment) MainActivity.mSupportFragmentManager.findFragmentById(R.id.frag_sb_carpool_FL);
			if(f != null) {
				MainActivity.mSupportFragmentManager.beginTransaction().remove(f).commitAllowingStateLoss();
			}

			if(mView != null){
				ViewGroup parent = (ViewGroup) mView.getParent();
				if(parent!=null){
					parent.removeView(mView);
				}
			}

			mMainActivity.mFragmentCarpool = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setUpMapIfNeeded() {
		try {
			if (mGoogleMap == null) {
				mGoogleMap = ((SupportMapFragment) MainActivity.mSupportFragmentManager.findFragmentById(R.id.frag_sb_carpool_FL)).getMap();
				if (mGoogleMap != null) {
					setUpMap();
				}
			}

//			if (mGoogleMap == null) {
//				SupportMapFragment mapFragment = (SupportMapFragment) MainActivity.mSupportFragmentManager.findFragmentById(R.id.frag_sb_carpool_FL);
//				mapFragment.getMapAsync(this);
//			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

//	@Override
//	public void onMapReady(GoogleMap googleMap) {
//		try {
//			mGoogleMap = googleMap;
//			setUpMap();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	private void setUpMap() {
		try {
			mGoogleMap.getUiSettings().setRotateGesturesEnabled(false);
			mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
			mGoogleMap.getUiSettings().setCompassEnabled(false);

			mCurrentPositionMarker = mGoogleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_carpool_current_position)).position(mMainActivity.mCurrentLocation).anchor(0.5f, 1.0f));

			mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mMainActivity.mCurrentLocation, 15)); //15

			mGoogleMap.setInfoWindowAdapter(new BalloonAdapter(mMainActivity.getLayoutInflater()));

			setEventListener();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void changePushAlarm() {
		try {
			if(mMainActivity.mYnCarpoolPush.equals("Y")) {
				mBtnPushAlarm.setText("알림 ON");
				mBtnPushAlarm.setBackgroundResource(R.drawable.shape_button_bg_on);
				mBtnPushAlarm2.setText("알림 ON");
				mBtnPushAlarm2.setBackgroundResource(R.drawable.shape_button_bg_on);
			} else {
				mBtnPushAlarm.setText("알림 OFF");
				mBtnPushAlarm.setBackgroundResource(R.drawable.shape_button_bg_off);
				mBtnPushAlarm2.setText("알림 OFF");
				mBtnPushAlarm2.setBackgroundResource(R.drawable.shape_button_bg_off);
			}

			mMainActivity.saveYnCarpoolPush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private class OnItemClick_taxi implements AdapterView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			try {
				showCarpoolDetailFragment(mCarpoolList.get(position).getmTaxiId(), mCarpoolList.get(position).getmRegistrantPhone());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private class CarpoolListViewHolder {
		// 도착지, 인원, carpoolId, isCatch
		private TextView mId;
		private TextView mIsCatch;
		private TextView mStartLocation;
		private TextView mEndLocation;
		private TextView mDepartTime;
		private TextView mPrice;
	}

	private class CarpoolListAdapter extends ArrayAdapter<CarpoolData> {
		ArrayList<CarpoolData> items;

		private int cellID;
		CarpoolListViewHolder holder;

		public CarpoolListAdapter(Context cxt, int cellID, ArrayList<CarpoolData> data) {
			super(cxt, cellID, data);
			this.items = data;
			this.cellID = cellID;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;

			try {
				if (v == null) {
					LayoutInflater li = (LayoutInflater) mMainActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					v = li.inflate(cellID, null);

					holder = new CarpoolListViewHolder();

					holder.mId = (TextView) v.findViewById(R.id.row_carpool_list_id);
					holder.mIsCatch = (TextView) v.findViewById(R.id.row_carpool_list_TV1);
					holder.mStartLocation = (TextView) v.findViewById(R.id.row_carpool_list_TV2);
					holder.mEndLocation = (TextView) v.findViewById(R.id.row_carpool_list_TV3);
					holder.mDepartTime = (TextView) v.findViewById(R.id.row_carpool_list_TV4);
					holder.mPrice = (TextView) v.findViewById(R.id.row_carpool_list_TV5);

					v.setTag(holder);
				} else {
					holder = (CarpoolListViewHolder) v.getTag();
				}

				CarpoolData box = items.get(position);

				if (box.ismIsCatch()) {
					holder.mIsCatch.setText("가능");
				} else {
					holder.mIsCatch.setText("요청");
				}

				String id = box.getmRegistrant() + "\n기사님";
				String start = "출발 : " + box.getmStartLocation().getmLocationName();
				String end = "도착 : " + box.getmEndLocation().getmLocationName();
				String price = box.getmPrice() + "";
				String departTime = new SimpleDateFormat("HH:mm").format(new Date(box.getmDepartureTime()));
				departTime += "\n\n";
				departTime += box.getmDriverListData().getCurrentDriverCount() + "/" + box.getmDriverListData().getmMax() + "명";

				holder.mId.setText(id);
				holder.mStartLocation.setText(start);
				holder.mEndLocation.setText(end);
				holder.mDepartTime.setText(departTime);
				holder.mPrice.setText(price);
			} catch (Exception e) {
				e.printStackTrace();
			}

			return v;
		}
	}

	@Override
	public void onClick(View view) {
		try {
			if (view == mBtnCurrentPosition) {
				mGoogleMap.animateCamera(CameraUpdateFactory.newLatLng(mMainActivity.mCurrentLocation));
			} else if (view == mBtnShowCarpoolList) {
				mCarpoolListLayout.setVisibility(View.VISIBLE);
			} else if (view == mBtnClose) {
				mCarpoolListLayout.setVisibility(View.GONE);
			} else if (view == mBtnMakeCarpool || view == mBtnMakeCarpool2) {
				CarpoolMakeFragment frag = new CarpoolMakeFragment();

				Bundle args = new Bundle();
				args.putString("taxiId", null);

				frag.setArguments(args);

				FragmentTransaction transaction = getFragmentManager().beginTransaction();
				transaction.replace(R.id.mainFrameLayout, frag, Global.FRAG_CARPOOL_MAKE_TAG);
				transaction.addToBackStack(null);
				transaction.commit();
			} else if(view == mBtnCall) {
				SVUtil.showDialogWithListener(mMainActivity, "알림", "상황실 전화 연결은 유료로 추가 요금이 발생할수 있습니다.\n연결 하시겠습니까?", "연결", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();

						mMainActivity.makePhoneCall();
					}
				}, "취소", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
			} else if(view == mBtnPushAlarm || view == mBtnPushAlarm2) {
				if(mMainActivity.mYnCarpoolPush.equals("Y")) {
					mMainActivity.mYnCarpoolPush = "N";
				} else {
					mMainActivity.mYnCarpoolPush = "Y";
				}

				changePushAlarm();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onStart() {
		super.onStart();

		mIsFinishFragment = false;
	}

	@Override
	public void onResume() {
		super.onResume();

		try {
			mPageNum = 0;

			mCarpoolList.clear();
			removeRegistrantMarkers();

			getCarpoolList(mPageNum);

			mMainActivity.changeButtonImage(mMainActivity.mImgBtnCarpool);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onPause() {
		super.onPause();

		SVUtil.hideProgressDialog();

		mIsFinishFragment = true;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();

		destroyFragment();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		setTargetFragment(null, -1);
	}
}
