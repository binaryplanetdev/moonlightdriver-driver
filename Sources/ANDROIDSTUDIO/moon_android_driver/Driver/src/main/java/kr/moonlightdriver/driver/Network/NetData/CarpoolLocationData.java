package kr.moonlightdriver.driver.Network.NetData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by youngmin on 2015-10-28.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CarpoolLocationData {
	@JsonProperty("text")
	private String mLocationName;

	@JsonProperty("location")
	private ShuttleLocationData mLocations;

	public CarpoolLocationData() {}

	public CarpoolLocationData(String mLocationName, ShuttleLocationData mLocations) {
		this.mLocationName = mLocationName;
		this.mLocations = mLocations;
	}

	public String getmLocationName() {
		return mLocationName;
	}

	public void setmLocationName(String mLocationName) {
		this.mLocationName = mLocationName;
	}

	public ShuttleLocationData getmLocations() {
		return mLocations;
	}

	public void setmLocations(ShuttleLocationData mLocations) {
		this.mLocations = mLocations;
	}
}
