package kr.moonlightdriver.driver.Network.NetData;

/**
 * Created by youngmin on 2015-12-31.
 */
public class BusPositionInfoData {
	private String mPlainNo;
	private String mSectionId;
	private int mSectionOrder;
	private int mBusType;
	private boolean mIsLast;
	private double mGpsY;
	private double mGpsX;

	public BusPositionInfoData() {}

	public BusPositionInfoData(int _sectOrd, String _plainNo, int _busType, String _sectionId, boolean _isLast, double _gpsY, double _gpsX) {
		this.mSectionOrder = _sectOrd;
		this.mPlainNo = _plainNo;
		this.mBusType = _busType;
		this.mSectionId = _sectionId;
		this.mIsLast = _isLast;
		this.mGpsX = _gpsX;
		this.mGpsY = _gpsY;
	}

	public int getmBusType() {
		return mBusType;
	}

	public void setmBusType(int mBusType) {
		this.mBusType = mBusType;
	}

	public double getmGpsX() {
		return mGpsX;
	}

	public void setmGpsX(double mGpsX) {
		this.mGpsX = mGpsX;
	}

	public double getmGpsY() {
		return mGpsY;
	}

	public void setmGpsY(double mGpsY) {
		this.mGpsY = mGpsY;
	}

	public boolean ismIsLast() {
		return mIsLast;
	}

	public void setmIsLast(boolean mIsLast) {
		this.mIsLast = mIsLast;
	}

	public String getmPlainNo() {
		return mPlainNo;
	}

	public void setmPlainNo(String mPlainNo) {
		this.mPlainNo = mPlainNo;
	}

	public String getmSectionId() {
		return mSectionId;
	}

	public void setmSectionId(String mSectionId) {
		this.mSectionId = mSectionId;
	}

	public int getmSectionOrder() {
		return mSectionOrder;
	}

	public void setmSectionOrder(int mSectionOrder) {
		this.mSectionOrder = mSectionOrder;
	}
}
