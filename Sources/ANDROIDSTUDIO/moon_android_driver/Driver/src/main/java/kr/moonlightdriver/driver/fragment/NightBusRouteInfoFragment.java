package kr.moonlightdriver.driver.fragment;

import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.kevinsawicki.http.HttpRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import kr.moonlightdriver.driver.Network.NetApi.BusRouteDetailAck;
import kr.moonlightdriver.driver.Network.NetApi.BusStationListByBusRouteIdAck;
import kr.moonlightdriver.driver.Network.NetClient.NetClient;
import kr.moonlightdriver.driver.Network.NetData.BusPositionInfoData;
import kr.moonlightdriver.driver.Network.NetData.BusRouteInfoData;
import kr.moonlightdriver.driver.Network.NetData.BusRouteStationInfoData;
import kr.moonlightdriver.driver.Network.NetData.BusStationListData;
import kr.moonlightdriver.driver.Network.NetData.ResultData;
import kr.moonlightdriver.driver.R;
import kr.moonlightdriver.driver.activity.MainActivity;
import kr.moonlightdriver.driver.utils.Global;
import kr.moonlightdriver.driver.utils.SVUtil;

/**
 * Created by redjjol on 3/08/15.
 */
public class NightBusRouteInfoFragment extends Fragment {
	private static final String TAG = "NightBusRouteInfoFragment";
	private MainActivity mMainActivity;
	private GoogleMap mGoogleMap;
	private Polyline mBusPolyLine;
	private ArrayList<Marker> mBusStationMarkers;

	private ArrayList<BusRouteStationInfoData> mRouteStationInfoList;
	private ArrayList<BusPositionInfoData> mBusPositionList;

	private ImageButton mImgBtnClose;
	private Button mBtnBusStationList;
	private LinearLayout mNightBusStationListLayout;
	private ImageButton mImgBtnBack;
	private ImageButton mImgBtnReload;
	private ImageButton mImgBtnZoomIn;
	private ImageButton mImgBtnZoomOut;
	private TextView mTextViewNightBusStationListTitle;
	private ImageButton mImgBtnNightBusInfo;
	private ListView mListViewNightBusStationList;
	private BusRouteStationInfoAdapter mRouteStationInfoAdapter;

	private View mView;
	private String mBusRouteId;
	private String mBusRouteArea;
	private BusRouteInfoData mNightBusRouteInfo;

	private boolean mIsFinishFragment;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		try {
			mView = inflater.inflate(R.layout.frag_night_bus_info, container, false);

			mMainActivity = (MainActivity) getActivity();

			mIsFinishFragment = false;

			Bundle bundle = NightBusRouteInfoFragment.this.getArguments();
			mBusRouteId = bundle.getString("busId");
			mBusRouteArea = bundle.getString("busRouteArea");

			mNightBusStationListLayout = (LinearLayout) mView.findViewById(R.id.frag_night_bus_station_list_layout);
			mNightBusStationListLayout.setVisibility(View.GONE);

			mImgBtnBack = (ImageButton) mView.findViewById(R.id.frag_night_bus_btn_back);
			mImgBtnReload = (ImageButton) mView.findViewById(R.id.frag_night_bus_btn_reload);
			mImgBtnZoomIn = (ImageButton) mView.findViewById(R.id.frag_night_bus_btn_zoom_in);
			mImgBtnZoomOut = (ImageButton) mView.findViewById(R.id.frag_night_bus_btn_zoom_out);
			mImgBtnNightBusInfo = (ImageButton) mView.findViewById(R.id.frag_night_bus_btn_info);
			mTextViewNightBusStationListTitle = (TextView) mView.findViewById(R.id.frag_night_bus_text_view_title);
			mListViewNightBusStationList = (ListView) mView.findViewById(R.id.frag_night_bus_station_list_view);
			mImgBtnClose = (ImageButton) mView.findViewById(R.id.frag_night_bus_btn_close);
			mBtnBusStationList = (Button) mView.findViewById(R.id.frag_night_bus_btn_list);

			mBusStationMarkers = new ArrayList<>();
			mBusPositionList = new ArrayList<>();
			mRouteStationInfoList = new ArrayList<>();

			mRouteStationInfoAdapter = new BusRouteStationInfoAdapter(mMainActivity, R.layout.row_bus_info, mRouteStationInfoList);
			mListViewNightBusStationList.setAdapter(mRouteStationInfoAdapter);

			setUpMapIfNeeded();

			new GetNightBusRouteInfoFromLocalDB().execute();
		} catch(InflateException e) {
			e.printStackTrace();
		}

		return mView;
	}

	private void showBusRouteInfoDialog() {
		try {
			LayoutInflater factory = LayoutInflater.from(mMainActivity);
			View layoutView = factory.inflate(R.layout.popup_bus_route_info, null);
			TextView startStationTV = (TextView) layoutView.findViewById(R.id.popup_bus_route_info_start_station);
			TextView endStationTV = (TextView) layoutView.findViewById(R.id.popup_bus_route_info_end_station);
			TextView termTV = (TextView) layoutView.findViewById(R.id.popup_bus_route_info_term);
			TextView firstTimeTV = (TextView) layoutView.findViewById(R.id.popup_bus_route_info_first_time);
			TextView lastTimeTV = (TextView) layoutView.findViewById(R.id.popup_bus_route_info_last_time);

			startStationTV.setText(mNightBusRouteInfo.getmStartStationName());
			endStationTV.setText(mNightBusRouteInfo.getmEndStationName());
			termTV.setText(mNightBusRouteInfo.getmTerm() != null ? mNightBusRouteInfo.getmTerm() : "-");
			firstTimeTV.setText(mNightBusRouteInfo.getmFirstTime() != null ? mNightBusRouteInfo.getmFirstTime() : "-");
			lastTimeTV.setText(mNightBusRouteInfo.getmLastTime() != null ? mNightBusRouteInfo.getmLastTime() : "-");

			SVUtil.showDialogWithListener(mMainActivity, "[" + mNightBusRouteInfo.getmBusRouteName() + "] 노선 정보", layoutView, "확인", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void getSeoulBusData() {
		AsyncHttpClient asyncHttpClient = NetClient.getAsyncHttpClient();

//		String url = "http://ws.bus.go.kr/api/rest/busRouteInfo/getStaionByRoute?ServiceKey=" + Global.TRANSPORT_SERVICE_KEY + "&busRouteId=" + mBusRouteId;
		final String url = Global.BUS_STATION_BY_ROUTE_ID + mNightBusRouteInfo.getmLocalBusRouteId();

		asyncHttpClient.get(url, new TextHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, String responseBody) {
				try {
					if(mIsFinishFragment) {
						return;
					}

					if(!responseBody.isEmpty()) {
						JSONObject resultHeader = XML.toJSONObject(responseBody).getJSONObject("ServiceResult").getJSONObject("msgHeader");
						if(resultHeader.getInt("headerCd") == 0) {
							JSONObject jsonObj = XML.toJSONObject(responseBody).getJSONObject("ServiceResult").getJSONObject("msgBody");
							JSONArray arrayObj = new JSONArray();

							if (jsonObj.toString().contains("[{")) {
								arrayObj = jsonObj.getJSONArray("itemList");
							} else {
								arrayObj.put(jsonObj.getJSONObject("itemList"));
							}

							for (int i = 0; i < arrayObj.length(); i++) {
								updateSeoulBusRouteStationInfo(arrayObj.getJSONObject(i));
							}
						}
					}
				} catch(Exception e) {
					e.printStackTrace();
				} finally {
					mRouteStationInfoAdapter.notifyDataSetChanged();
					getSeoulBusPositionData();
				}
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable error) {
				SVUtil.log("error", TAG, "ERROR URL : " + url);

				error.printStackTrace();

				if (error instanceof java.net.SocketTimeoutException) {
					SVUtil.showTimeoutDialog(mMainActivity);
				}

				getSeoulBusPositionData();
			}
		});
	}

	private void getSeoulBusPositionData() {
		AsyncHttpClient asyncHttpClient = NetClient.getAsyncHttpClient();

//		String url = "http://ws.bus.go.kr/api/rest/busRouteInfo/getStaionByRoute?ServiceKey=" + Global.TRANSPORT_SERVICE_KEY + "&busRouteId=" + mBusRouteId;
		final String url = Global.BUS_POSITION_BY_ROUTE_ID + mNightBusRouteInfo.getmLocalBusRouteId();

		asyncHttpClient.get(url, new TextHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, String responseBody) {
				try {
					if(mIsFinishFragment) {
						return;
					}

					if(!responseBody.isEmpty()) {
						JSONObject resultHeader = XML.toJSONObject(responseBody).getJSONObject("ServiceResult").getJSONObject("msgHeader");
						if (resultHeader.getInt("headerCd") == 0) {
							JSONObject jsonObj = XML.toJSONObject(responseBody).getJSONObject("ServiceResult").getJSONObject("msgBody");
							JSONArray arrayObj = new JSONArray();

							if (jsonObj.toString().contains("[{")) {
								arrayObj = jsonObj.getJSONArray("itemList");
							} else {
								arrayObj.put(jsonObj.getJSONObject("itemList"));
							}

							mBusPositionList.clear();

							for (int i = 0; i < arrayObj.length(); i++) {
								JSONObject inArrObj = arrayObj.getJSONObject(i);

								mBusPositionList.add(new BusPositionInfoData(inArrObj.getInt("sectOrd"), inArrObj.getString("plainNo"), inArrObj.getInt("busType"), inArrObj.getString("sectionId"), inArrObj.getString("islastyn").equals("1"),
										Double.parseDouble(inArrObj.getString("gpsY")), Double.parseDouble(inArrObj.getString("gpsX"))));
							}
						}
					}
				} catch (Exception _e) {
					_e.printStackTrace();
				} finally {
					mRouteStationInfoAdapter.notifyDataSetChanged();
				}
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable error) {
				SVUtil.log("error", TAG, "ERROR URL : " + url);

				error.printStackTrace();

				if (error instanceof java.net.SocketTimeoutException) {
					SVUtil.showTimeoutDialog(mMainActivity);
				}
			}
		});
	}

	private class GetGBusPositionData extends AsyncTask<Void, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			mBusPositionList.clear();
		}

		@Override
		protected String doInBackground(Void... voids) {
			try {
				String url = "http://openapi.gbis.go.kr/ws/rest/buslocationservice?serviceKey=" + Global.TRANSPORT_SERVICE_KEY + "&routeId=" + mNightBusRouteInfo.getmLocalBusRouteId();
				String result = HttpRequest.get(url).body();

				if(result != null && !result.isEmpty()) {
					JSONObject resultHeader = XML.toJSONObject(result).getJSONObject("response").getJSONObject("msgHeader");
					if(resultHeader.getInt("resultCode") == 0) {
						JSONObject jsonObj = XML.toJSONObject(result).getJSONObject("response").getJSONObject("msgBody");
						JSONArray arrayObj = new JSONArray();

						if (jsonObj.toString().contains("[{")) {
							arrayObj = jsonObj.getJSONArray("busLocationList");
						} else {
							arrayObj.put(jsonObj.getJSONObject("busLocationList"));
						}

						for (int i = 0; i < arrayObj.length(); i++) {
							JSONObject inArrObj = arrayObj.getJSONObject(i);
							mBusPositionList.add(new BusPositionInfoData(inArrObj.getInt("stationSeq"), inArrObj.getString("plateNo"), inArrObj.getInt("lowPlate"), null, inArrObj.getString("endBus").equals("1"), 0, 0));
						}
					}
				}
			} catch(Exception e) {
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(String _result) {
			try {
				SVUtil.hideProgressDialog();

				if(mIsFinishFragment) {
					return;
				}

				mRouteStationInfoAdapter.notifyDataSetChanged();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void updateSeoulBusRouteStationInfo(JSONObject _newData) {
		try {
			for(int i = 0; i < mRouteStationInfoList.size(); i++) {
				BusRouteStationInfoData box = mRouteStationInfoList.get(i);

				if(box.getmSectSpd() == -2) {
					box.setmSectSpd(-1);
				}

				if(box.getmSeq() == _newData.optInt("seq", -1)) {
					String direction = _newData.optString("direction", "").trim();
					String beginTime = _newData.optString("beginTm", "").trim();
					String lastTime = _newData.optString("lastTm", "").trim();
					double gpsX = _newData.optDouble("gpsX", 0);
					double gpsY = _newData.optDouble("gpsY", 0);

					box.setmDirection(direction.isEmpty() ? box.getmDirection() : direction);
					box.setmBeginTime(beginTime.length() < 3 ? box.getmBeginTime() : beginTime);
					box.setmLastTime(lastTime.length() < 3 ? box.getmLastTime() : lastTime);
					box.setmTransYn(_newData.optString("transYn", "").trim().equals("Y"));
					box.setmSectSpd(Integer.parseInt(_newData.optString("sectSpd", "-1")));
					box.setmGpsX(gpsX == 0 ? box.getmGpsX() : gpsX);
					box.setmGpsY(gpsY == 0 ? box.getmGpsY() : gpsY);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private BusStationListByBusRouteIdAck getStationListByBusRouteId(String busRouteId) {
		BusStationListByBusRouteIdAck retBusStationListNetApi = null;
		Reader reader = null;
		JsonParser jp = null;

		try {
			String url = Global.URL_BUS_STATION_LIST_BY_ROUTE_ID + "?busRouteId=" + busRouteId;
//			SVUtil.log("error", TAG, "bus route detail url : " + url);
			HttpRequest request = HttpRequest.get(url);
			request.acceptGzipEncoding().uncompress(true);
			reader = request.reader();
			ObjectMapper mapper = new ObjectMapper();

			jp = mapper.getFactory().createParser(reader);
			retBusStationListNetApi = mapper.readValue(jp, BusStationListByBusRouteIdAck.class);

			reader.close();
			reader = null;
			jp.close();
			jp = null;
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				if(reader != null) {
					reader.close();
				}

				if(jp != null && !jp.isClosed()) {
					jp.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return retBusStationListNetApi;
	}

	private class GetStationByRouteIdFromLocalDB extends AsyncTask<Void, Void, Boolean> {
		ArrayList<BusRouteStationInfoData> mRetRouteStationInfoList;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			SVUtil.showProgressDialog(mMainActivity, "로딩중...");
			this.mRetRouteStationInfoList = new ArrayList<>();
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			try {
				BusStationListByBusRouteIdAck retBusStationListNetApi = getStationListByBusRouteId(mBusRouteId);
				if(retBusStationListNetApi == null) {
					return false;
				}

				ResultData result = retBusStationListNetApi.getmResultData();
				if(result.getmCode() != 100) {
					return false;
				}

				BusRouteInfoData retBusRouteInfoData = retBusStationListNetApi.getmBusRouteDetailData();
				ArrayList<BusStationListData> retBusStationListData = retBusStationListNetApi.getmBusStationDataList();
				for(BusStationListData busStationItem : retBusStationListData) {
					if(mIsFinishFragment) {
						return true;
					}

					String startTime = SVUtil.convertTime(busStationItem.getmStartTime());
					String endTime = SVUtil.convertTime(busStationItem.getmEndTime());

					this.mRetRouteStationInfoList.add(new BusRouteStationInfoData(retBusRouteInfoData.getmBusRouteArea(), busStationItem.getmStationSequence(), busStationItem.getmStationName(), busStationItem.getmDirection(),
							busStationItem.getmArsId(), startTime, endTime, false, -2, busStationItem.getmGpsY(), busStationItem.getmGpsX(), retBusRouteInfoData.getmLocalBusRouteId(), busStationItem.getmStationId() + ""));
				}
			} catch(Exception e) {
				e.printStackTrace();
				return false;
			}

			return true;
		}

		@Override
		protected void onPostExecute(Boolean _retStatus) {
			SVUtil.hideProgressDialog();

			if(mIsFinishFragment) {
				return;
			}

			try {
				if(_retStatus) {
					mRouteStationInfoList.clear();

					for(int i = 0; i < this.mRetRouteStationInfoList.size(); i++) {
						mRouteStationInfoList.add(this.mRetRouteStationInfoList.get(i));
					}

					mRouteStationInfoAdapter.notifyDataSetChanged();
					mNightBusStationListLayout.setVisibility(View.VISIBLE);

					if(mNightBusRouteInfo.getmLocalBusRouteId().isEmpty()) {
						return;
					}

					if(mNightBusRouteInfo.getmBusRouteArea().startsWith("서울")) {
						getSeoulBusData();
					} else if(mNightBusRouteInfo.getmBusRouteArea().startsWith("경기")) {
//						getGBusPositionData();
						new GetGBusPositionData().execute();
					}
				} else {
					SVUtil.showDialogWithListener(mMainActivity, "심야 버스 정류장 조회 중 오류가 발생하였습니다.\n잠시 후 다시 시도해주세요.", "확인", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
						returnToShuttleFragment();
						}
					});
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private BusRouteDetailAck getBusRouteDetailData(String busRouteId) {
		BusRouteDetailAck retBusRouteDetailNetApi = null;
		Reader reader = null;
		JsonParser jp = null;

		try {
			String url = Global.URL_BUS_ROUTE_DETAIL + "?busRouteId=" + busRouteId;
//			SVUtil.log("error", TAG, "bus route detail url : " + url);
			HttpRequest request = HttpRequest.get(url);
			request.acceptGzipEncoding().uncompress(true);
			reader = request.reader();
			ObjectMapper mapper = new ObjectMapper();

			jp = mapper.getFactory().createParser(reader);
			retBusRouteDetailNetApi = mapper.readValue(jp, BusRouteDetailAck.class);

			reader.close();
			reader = null;
			jp.close();
			jp = null;
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				if(reader != null) {
					reader.close();
				}

				if(jp != null && !jp.isClosed()) {
					jp.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return retBusRouteDetailNetApi;
	}

	private class GetNightBusRouteInfoFromLocalDB extends AsyncTask<Void, Void, Boolean> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			SVUtil.showProgressDialog(mMainActivity, "로딩중...");
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			try {
				BusRouteDetailAck retBusRouteDetailNetApi = getBusRouteDetailData(mBusRouteId);
				if(retBusRouteDetailNetApi == null) {
					return false;
				}

				ResultData result = retBusRouteDetailNetApi.getmResultData();
				if(result.getmCode() != 100) {
					return false;
				}

				BusRouteInfoData retBusRouteData = retBusRouteDetailNetApi.getmBusRouteDetailData();
				if(retBusRouteData.getmTerm().startsWith("#")) {
					retBusRouteData.setmTerm(retBusRouteData.getmTerm().replace("#", "") + "회");
				} else {
					retBusRouteData.setmTerm(retBusRouteData.getmTerm() + "분");
				}

				mNightBusRouteInfo = new BusRouteInfoData(retBusRouteData.getmBusRouteId(), retBusRouteData.getmBusRouteName(), retBusRouteData.getmBusRouteArea(), retBusRouteData.getmStartStationName(), retBusRouteData.getmEndStationName(),
						retBusRouteData.getmFirstTime(), retBusRouteData.getmLastTime(), retBusRouteData.getmTerm(), retBusRouteData.getmLocalBusRouteId());
			} catch(Exception e) {
				e.printStackTrace();
				return false;
			}

			return true;
		}

		@Override
		protected void onPostExecute(Boolean _retStatus) {
			SVUtil.hideProgressDialog();

			if(mIsFinishFragment) {
				return;
			}

			try {
				if(_retStatus) {
					if(!mNightBusRouteInfo.getmLocalBusRouteId().isEmpty()) {
						getBusRoutePath(mNightBusRouteInfo.getmLocalBusRouteId());
					}
				} else {
					SVUtil.showDialogWithListener(mMainActivity, "심야 버스 정보 조회 중 오류가 발생하였습니다.\n잠시 후 다시 시도해주세요.", "확인", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							returnToShuttleFragment();
						}
					});
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void showNightBusStationList() {
		try {
			mTextViewNightBusStationListTitle.setText(mNightBusRouteInfo.getmBusRouteName());

			new GetStationByRouteIdFromLocalDB().execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void returnToShuttleFragment() {
		try {
			mMainActivity.mFragmentShuttle = new ShuttleFragment();
			mMainActivity.replaceFragment(mMainActivity.mFragmentShuttle, R.id.mainFrameLayout, Global.FRAG_SHUTTLE_TAG, false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setUpMapIfNeeded() {
		try {
			if (mGoogleMap == null) {
				mGoogleMap = ((SupportMapFragment) MainActivity.mSupportFragmentManager.findFragmentById(R.id.frag_night_bus_map)).getMap();
				if (mGoogleMap != null) {
					setUpMap();
				}
			}

//			if (mGoogleMap == null) {
//				SupportMapFragment mapFragment = (SupportMapFragment) MainActivity.mSupportFragmentManager.findFragmentById(R.id.frag_night_bus_map);
//				mapFragment.getMapAsync(this);
//			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

//	@Override
//	public void onMapReady(GoogleMap googleMap) {
//		try {
//			mGoogleMap = googleMap;
//			setUpMap();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	private void setUpMap() {
		try {
			mGoogleMap.getUiSettings().setRotateGesturesEnabled(false);
			mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
			mGoogleMap.getUiSettings().setCompassEnabled(false);
			mGoogleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_carpool_current_position)).position(mMainActivity.mCurrentLocation).anchor(0.5f, 1.0f));
			mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mMainActivity.mCurrentLocation, 15)); //15

			setEventListener();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setEventListener() {
		try {
			mImgBtnBack.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mNightBusStationListLayout.setVisibility(View.GONE);
				}
			});

			mImgBtnReload.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					showNightBusStationList();
				}
			});

			mImgBtnZoomIn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mGoogleMap.animateCamera(CameraUpdateFactory.zoomIn());
				}
			});

			mImgBtnZoomOut.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mGoogleMap.animateCamera(CameraUpdateFactory.zoomOut());
				}
			});

			mImgBtnNightBusInfo.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					showBusRouteInfoDialog();
				}
			});

			mImgBtnClose.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					returnToShuttleFragment();
				}
			});

			mBtnBusStationList.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					showNightBusStationList();
				}
			});

			mListViewNightBusStationList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					try {
						mNightBusStationListLayout.setVisibility(View.GONE);

						BusRouteStationInfoData busStationData = mRouteStationInfoList.get(position);
						mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(busStationData.getmGpsY(), busStationData.getmGpsX()), 18));
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
				@Override
				public boolean onMarkerClick(Marker marker) {
					try {
						if (marker.getSnippet() == null) {
							return false;
						}

						if (marker.getSnippet().equals("night_bus")) {
							mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 18));
						}
					} catch (Exception e) {
						e.printStackTrace();
					}

					return true;
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void moveToBounds(Polyline p) {
		try {
			LatLngBounds.Builder builder = new LatLngBounds.Builder();
			List<LatLng> arr = p.getPoints();

			for(int i = 0; i < arr.size();i++){
				builder.include(arr.get(i));
			}

			LatLngBounds bounds = builder.build();
			int padding = 40; // offset from edges of the map in pixels
			mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, padding));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void getBusRoutePath(String _busRouteId) {
		AsyncHttpClient asyncHttpClient = NetClient.getAsyncHttpClient();
		final String url = Global.BUS_STATION_BY_ROUTE_ID + _busRouteId;

		asyncHttpClient.get(url, new TextHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, String responseBody) {

				try {
					if(mIsFinishFragment) {
						return;
					}

					PolylineOptions opt = new PolylineOptions();
					opt.color(Color.parseColor("#0000ff"));

					if (mBusPolyLine != null) {
						mBusPolyLine.remove();
					}

					if(mBusStationMarkers != null) {
						for (Marker m : mBusStationMarkers) {
							m.remove();
						}

						mBusStationMarkers.clear();
					}

					JSONObject jsonObj = XML.toJSONObject(responseBody).getJSONObject("ServiceResult").getJSONObject("msgBody");
					JSONArray arrayObj = new JSONArray();

					if (jsonObj.toString().contains("[{")) {
						arrayObj = jsonObj.getJSONArray("itemList");
					} else {
						arrayObj.put(jsonObj.getJSONObject("itemList"));
					}

					for (int i = 0; i < arrayObj.length(); i++) {
						JSONObject inArrObj = arrayObj.getJSONObject(i);
						opt.add(new LatLng(inArrObj.getDouble("gpsY"), inArrObj.getDouble("gpsX")));
						mBusStationMarkers.add(mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(inArrObj.getDouble("gpsY"), inArrObj.getDouble("gpsX"))).icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_nightbus_30)).snippet("night_bus").anchor(0.5f, 1.0f)));
					}

					mBusPolyLine = mGoogleMap.addPolyline(opt);
					moveToBounds(mBusPolyLine);
				} catch (JSONException e) {
					e.printStackTrace();
				} finally {
					SVUtil.hideProgressDialog();
				}
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable error) {
				SVUtil.log("error", TAG, "ERROR URL : " + url);

				error.printStackTrace();

				if (error instanceof java.net.SocketTimeoutException) {
					SVUtil.showTimeoutDialog(mMainActivity);
				} else {
					SVUtil.showDialogWithListener(mMainActivity, "심야 버스 정보를 조회 중 오류 발생", "확인", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							returnToShuttleFragment();
						}
					});
				}
			}
		});
	}

	private class ViewHolder {
		public TextView mStationNameTV;
		public TextView mDirectionTV;
		public TextView mTimeTV;
		public TextView mConditionTV;
		public TextView mPlainNoTV;
		public TextView mKmTV;
		public ImageView mBusIV;
		public ImageView mConditionImage;
		public LinearLayout mBusLayout;
	}

	private class BusRouteStationInfoAdapter extends ArrayAdapter<BusRouteStationInfoData> {
		ArrayList<BusRouteStationInfoData> mRouteStationInfoBoxList;
		private int cellID;
		ViewHolder holder;

		public BusRouteStationInfoAdapter(Context cxt, int cellID, ArrayList<BusRouteStationInfoData> _routeStationInfoBoxList) {
			super(cxt, cellID, _routeStationInfoBoxList);
			this.mRouteStationInfoBoxList = _routeStationInfoBoxList;
			this.cellID = cellID;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;

			try {
				if (v == null) {
					LayoutInflater li = (LayoutInflater) mMainActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					v = li.inflate(cellID, null);
					holder = new ViewHolder();

					holder.mBusLayout = (LinearLayout) v.findViewById(R.id.row_passing_info_bus_layout);
					holder.mConditionImage = (ImageView) v.findViewById(R.id.row_passing_info_condition_image);
					holder.mStationNameTV = (TextView) v.findViewById(R.id.row_passing_info_stationNmTV);
					holder.mDirectionTV = (TextView) v.findViewById(R.id.row_passing_info_directionTV);
					holder.mTimeTV = (TextView) v.findViewById(R.id.row_passing_info_timeTV);
					holder.mConditionTV = (TextView) v.findViewById(R.id.row_passing_info_conditionTV);
					holder.mKmTV = (TextView) v.findViewById(R.id.row_passing_info2_kmTV);
					holder.mBusIV = (ImageView) v.findViewById(R.id.row_passing_info_IV);
					holder.mPlainNoTV = (TextView) v.findViewById(R.id.row_passing_info_plainNoTV);

					v.setTag(holder);
				} else {
					holder = (ViewHolder) v.getTag();
				}

				BusRouteStationInfoData box = mRouteStationInfoBoxList.get(position);
				String direction = (box.getmDirection() != null && !box.getmDirection().isEmpty()) ? (box.getmDirection() + " 방면") : "";
				String runningTime = "[" + (box.getmArsId() != null ? box.getmArsId() : " - ") + "]  운행 " + box.getmBeginTime() + "~" + box.getmLastTime();

				holder.mStationNameTV.setText(box.getmStationName());
				holder.mDirectionTV.setText(direction);
				holder.mTimeTV.setText(runningTime);

				boolean busPassing = false;
				String plainNo="";
				int busType = 0;

				for (BusPositionInfoData b : mBusPositionList) {
					if (b.getmSectionOrder() == box.getmSeq()) {
						busPassing = true;
						plainNo = b.getmPlainNo();
						busType = b.getmBusType();
						break;
					}
				}

				if (busPassing) {
					if(busType == 1)  {
						holder.mBusIV.setImageResource(R.drawable.gp_bus_lower);
					} else {
						holder.mBusIV.setImageResource(R.drawable.gp_bus);
					}

					holder.mBusLayout.setVisibility(View.VISIBLE);
					holder.mPlainNoTV.setText(plainNo);
				} else {
					holder.mBusLayout.setVisibility(View.GONE);
				}

				String speed = box.getmSectSpd() + "km/h";
				holder.mKmTV.setText(speed);
				holder.mKmTV.setVisibility(View.VISIBLE);

				if (box.getmSectSpd() == -2) {
					holder.mConditionTV.setText("로딩중");
					holder.mKmTV.setVisibility(View.GONE);
					holder.mConditionImage.setImageResource(R.drawable.bg_bus_speed_gray);
				} else if (box.getmSectSpd() == -1) {
					holder.mConditionTV.setText("정보\n없음");
					holder.mKmTV.setVisibility(View.GONE);
					holder.mConditionImage.setImageResource(R.drawable.bg_bus_speed_gray);
				} else if (box.getmSectSpd() <= 10) {
					holder.mConditionTV.setText("정체");
					holder.mConditionImage.setImageResource(R.drawable.bg_bus_speed_red);
				} else if (box.getmSectSpd() < 25) {
					holder.mConditionTV.setText("지체");
					holder.mConditionImage.setImageResource(R.drawable.bg_bus_speed_blue);
				} else {
					holder.mConditionTV.setText("원활");
					holder.mConditionImage.setImageResource(R.drawable.bg_bus_speed_green);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return v;
		}
	}

	@Override
	public void onStart() {
		super.onStart();

		mIsFinishFragment = false;
	}

	@Override
	public void onResume() {
		super.onResume();

		try {
			mMainActivity.changeButtonImage(mMainActivity.mImgBtnShuttle);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		SVUtil.hideProgressDialog();

		mIsFinishFragment = true;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();

		try {
			if(mGoogleMap != null) {
				mGoogleMap.clear();
				mGoogleMap = null;
			}

			SupportMapFragment f = (SupportMapFragment) MainActivity.mSupportFragmentManager.findFragmentById(R.id.frag_night_bus_map);
			if(f != null) {
				MainActivity.mSupportFragmentManager.beginTransaction().remove(f).commitAllowingStateLoss();
			}

			if(mView != null){
				ViewGroup parent = (ViewGroup) mView.getParent();
				if(parent!=null){
					parent.removeView(mView);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		setTargetFragment(null, -1);
	}
}
