package kr.moonlightdriver.driver.Network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import kr.moonlightdriver.driver.Network.NetData.BusRouteInfoData;
import kr.moonlightdriver.driver.Network.NetData.ResultData;

/**
 * Created by youngmin on 2015-10-13.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BusRouteDetailAck extends NetAPI {
	@JsonProperty("result")
	private ResultData mResultData;

	@JsonProperty("busRouteInfo")
	private BusRouteInfoData mBusRouteDetailData;

	public BusRouteDetailAck() {}

	public BusRouteDetailAck(BusRouteInfoData mBusRouteDetailData, ResultData mResultData) {
		this.mBusRouteDetailData = mBusRouteDetailData;
		this.mResultData = mResultData;
	}

	public BusRouteInfoData getmBusRouteDetailData() {
		return mBusRouteDetailData;
	}

	public void setmBusRouteDetailData(BusRouteInfoData mBusRouteDetailData) {
		this.mBusRouteDetailData = mBusRouteDetailData;
	}

	public ResultData getmResultData() {
		return mResultData;
	}

	public void setmResultData(ResultData mResultData) {
		this.mResultData = mResultData;
	}
}
