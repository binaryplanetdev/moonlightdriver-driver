package kr.moonlightdriver.driver.Network.NetData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by youngmin on 2015-11-05.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShuttleListData {
	@JsonProperty("busId")
	private String mBusId;
	@JsonProperty("name")
	private String mShuttleName;
	@JsonProperty("routeType")
	private String mRouteType;
	@JsonProperty("desc")
	private String mDescription;
	@JsonProperty("category")
	private int mCategory;
	@JsonProperty("minDistance")
	private double mDistance;
	@JsonProperty("firstTime")
	private String mFirstTime;
	@JsonProperty("lastTime")
	private String mLastTime;
	@JsonProperty("term")
	private int mTerm;
	@JsonProperty("totalTime")
	private int mTotalTime;
	@JsonProperty("lineColor")
	private String mLineColor;

	private boolean mIsShuttle;

	public ShuttleListData() {
		this.mIsShuttle = true;
	}

	public ShuttleListData(boolean mIsShuttle, String mBusId, int mCategory, String mRouteType, String mDescription, double mDistance, String mFirstTime, String mLastTime, String mShuttleName, int mTerm, int mTotalTime) {
		this.mBusId = mBusId;
		this.mCategory = mCategory;
		this.mDescription = mDescription;
		this.mRouteType = mRouteType;
		this.mDistance = mDistance;
		this.mShuttleName = mShuttleName;
		this.mIsShuttle = mIsShuttle;
		this.mFirstTime = mFirstTime;
		this.mLastTime = mLastTime;
		this.mTerm = mTerm;
		this.mTotalTime = mTotalTime;
	}

	public ShuttleListData(String mBusId, int mCategory, String mRouteType, String mDescription, double mDistance, String mFirstTime, String mLastTime, String mShuttleName, int mTerm, int mTotalTime) {
		this.mBusId = mBusId;
		this.mCategory = mCategory;
		this.mRouteType = mRouteType;
		this.mDescription = mDescription;
		this.mDistance = mDistance;
		this.mFirstTime = mFirstTime;
		this.mLastTime = mLastTime;
		this.mShuttleName = mShuttleName;
		this.mTerm = mTerm;
		this.mTotalTime = mTotalTime;
	}

	public ShuttleListData(String mBusId, int mCategory, String mRouteType, String mDescription, double mDistance, String mFirstTime, boolean mIsShuttle, String mLastTime, String mLineColor, String mShuttleName, int mTerm, int mTotalTime) {
		this.mBusId = mBusId;
		this.mCategory = mCategory;
		this.mRouteType = mRouteType;
		this.mDescription = mDescription;
		this.mDistance = mDistance;
		this.mFirstTime = mFirstTime;
		this.mIsShuttle = mIsShuttle;
		this.mLastTime = mLastTime;
		this.mLineColor = mLineColor;
		this.mShuttleName = mShuttleName;
		this.mTerm = mTerm;
		this.mTotalTime = mTotalTime;
	}

	public ShuttleListData(String mBusId, int mCategory, String mDescription, double mDistance, String mFirstTime, boolean mIsShuttle, String mLastTime, String mLineColor, String mRouteType, String mShuttleName, int mTerm, int mTotalTime) {
		this.mBusId = mBusId;
		this.mCategory = mCategory;
		this.mDescription = mDescription;
		this.mDistance = mDistance;
		this.mFirstTime = mFirstTime;
		this.mIsShuttle = mIsShuttle;
		this.mLastTime = mLastTime;
		this.mLineColor = mLineColor;
		this.mRouteType = mRouteType;
		this.mShuttleName = mShuttleName;
		this.mTerm = mTerm;
		this.mTotalTime = mTotalTime;
	}

	public String getmRouteType() {
		return mRouteType;
	}

	public void setmRouteType(String mRouteType) {
		this.mRouteType = mRouteType;
	}

	public String getmLineColor() {
		return mLineColor;
	}

	public void setmLineColor(String mLineColor) {
		this.mLineColor = mLineColor;
	}

	public String getmBusId() {
		return mBusId;
	}

	public void setmBusId(String mBusId) {
		this.mBusId = mBusId;
	}

	public int getmCategory() {
		return mCategory;
	}

	public void setmCategory(int mCategory) {
		this.mCategory = mCategory;
	}

	public String getmDescription() {
		return mDescription;
	}

	public void setmDescription(String mDescription) {
		this.mDescription = mDescription;
	}

	public double getmDistance() {
		return mDistance;
	}

	public void setmDistance(double mDistance) {
		this.mDistance = mDistance;
	}

	public String getmFirstTime() {
		return mFirstTime;
	}

	public void setmFirstTime(String mFirstTime) {
		this.mFirstTime = mFirstTime;
	}

	public boolean ismIsShuttle() {
		return mIsShuttle;
	}

	public void setmIsShuttle(boolean mIsShuttle) {
		this.mIsShuttle = mIsShuttle;
	}

	public String getmLastTime() {
		return mLastTime;
	}

	public void setmLastTime(String mLastTime) {
		this.mLastTime = mLastTime;
	}

	public String getmShuttleName() {
		return mShuttleName;
	}

	public void setmShuttleName(String mShuttleName) {
		this.mShuttleName = mShuttleName;
	}

	public int getmTerm() {
		return mTerm;
	}

	public void setmTerm(int mTerm) {
		this.mTerm = mTerm;
	}

	public int getmTotalTime() {
		return mTotalTime;
	}

	public void setmTotalTime(int mTotalTime) {
		this.mTotalTime = mTotalTime;
	}
}
