package kr.moonlightdriver.driver.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;

import kr.moonlightdriver.driver.fragment.ShuttleChattingFragment;

/**
 * Created by youngmin on 2016-11-04.
 */

public class IncomingCallReceiver extends BroadcastReceiver {
	@Override
	public void onReceive(Context _context, Intent _intent){
		try{
			String state = _intent.getStringExtra(TelephonyManager.EXTRA_STATE);

//			if(state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
//				Toast.makeText(_context, "Phone Is Ringing", Toast.LENGTH_LONG).show();
//			} else if(state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
//				Toast.makeText(_context, "Call Received", Toast.LENGTH_LONG).show();
//			} else if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
//				Toast.makeText(_context, "Phone Is Idle", Toast.LENGTH_LONG).show();
//			}

			Intent pushMsgIntent = new Intent();
			pushMsgIntent.setAction(ShuttleChattingFragment.PUSH_MSG_6);
			pushMsgIntent.putExtra("call_state", state);

			_context.sendBroadcast(pushMsgIntent);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
