package kr.moonlightdriver.driver.Network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

import kr.moonlightdriver.driver.Network.NetData.QuestionData;
import kr.moonlightdriver.driver.Network.NetData.ResultData;

/**
 * Created by youngmin on 2015-10-13.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class QuestionListAck extends NetAPI {
	@JsonProperty("result")
	private ResultData mResultData;

	@JsonProperty("list")
	private ArrayList<QuestionData> mQuestionDataList;

	public QuestionListAck() {}

	public QuestionListAck(ArrayList<QuestionData> mQuestionDataList, ResultData mResultData) {
		this.mQuestionDataList = mQuestionDataList;
		this.mResultData = mResultData;
	}

	public ArrayList<QuestionData> getmQuestionDataList() {
		return mQuestionDataList;
	}

	public void setmQuestionDataList(ArrayList<QuestionData> mQuestionDataList) {
		this.mQuestionDataList = mQuestionDataList;
	}

	public ResultData getmResultData() {
		return mResultData;
	}

	public void setmResultData(ResultData mResultData) {
		this.mResultData = mResultData;
	}
}
