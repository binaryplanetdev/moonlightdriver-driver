package kr.moonlightdriver.driver.Network.NetData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by youngmin on 2015-11-05.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShuttleChatData {
	@JsonProperty("_id")
	private String mId;
	@JsonProperty("messageType")
	private String mMessageType;
	@JsonProperty("message")
	private String mMessage;
	@JsonProperty("driverId")
	private String mDriverId;
	@JsonProperty("nickName")
	private String mNickName;
	@JsonProperty("lat")
	private double mLat;
	@JsonProperty("lng")
	private double mLng;
	@JsonProperty("createdAt")
	private Long mCreateTime;

	public ShuttleChatData() {}

	public ShuttleChatData(Long mCreateTime, String mDriverId, String mId, double mLat, double mLng, String mMessage, String mMessageType, String mNickName) {
		this.mCreateTime = mCreateTime;
		this.mDriverId = mDriverId;
		this.mId = mId;
		this.mLat = mLat;
		this.mLng = mLng;
		this.mMessage = mMessage;
		this.mMessageType = mMessageType;
		this.mNickName = mNickName;
	}

	public Long getmCreateTime() {
		return mCreateTime;
	}

	public void setmCreateTime(Long mCreateTime) {
		this.mCreateTime = mCreateTime;
	}

	public String getmDriverId() {
		return mDriverId;
	}

	public void setmDriverId(String mDriverId) {
		this.mDriverId = mDriverId;
	}

	public String getmId() {
		return mId;
	}

	public void setmId(String mId) {
		this.mId = mId;
	}

	public double getmLat() {
		return mLat;
	}

	public void setmLat(double mLat) {
		this.mLat = mLat;
	}

	public double getmLng() {
		return mLng;
	}

	public void setmLng(double mLng) {
		this.mLng = mLng;
	}

	public String getmMessage() {
		return mMessage;
	}

	public void setmMessage(String mMessage) {
		this.mMessage = mMessage;
	}

	public String getmMessageType() {
		return mMessageType;
	}

	public void setmMessageType(String mMessageType) {
		this.mMessageType = mMessageType;
	}

	public String getmNickName() {
		return mNickName;
	}

	public void setmNickName(String mNickName) {
		this.mNickName = mNickName;
	}
}
