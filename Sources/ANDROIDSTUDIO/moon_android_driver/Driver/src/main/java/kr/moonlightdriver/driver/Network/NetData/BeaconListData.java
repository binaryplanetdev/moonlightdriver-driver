package kr.moonlightdriver.driver.Network.NetData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BeaconListData {

	@JsonProperty("beacon_list")
	private ArrayList<BeaconData> mBeaconList;

	public BeaconListData() {}

	public BeaconListData(ArrayList<BeaconData> mBeaconList) {
		this.mBeaconList = mBeaconList;
	}

	public ArrayList<BeaconData> getmBeaconList() {
		return mBeaconList;
	}

	public void setmBeaconList(ArrayList<BeaconData> mBeaconList) {
		this.mBeaconList = mBeaconList;
	}


	public int getCurrentDriverCount() {
		return this.mBeaconList.size();
	}
}
