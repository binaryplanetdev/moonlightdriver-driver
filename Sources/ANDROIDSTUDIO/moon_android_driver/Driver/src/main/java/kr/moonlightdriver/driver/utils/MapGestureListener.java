package kr.moonlightdriver.driver.utils;

import android.view.GestureDetector;
import android.view.MotionEvent;

/**
 * Created by youngmin on 2016-01-22.
 */
public class MapGestureListener extends GestureDetector.SimpleOnGestureListener {
	public static final String TAG = "MapGestureListener";

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
		try {
			float x1 = e1.getX();
			float y1 = e1.getY();

			float x2 = e2.getX();
			float y2 = e2.getY();

//			SVUtil.log("error", TAG, "onFling() (velocityX, velocityY) - (" + velocityX + ", " + velocityY + ")");

			double angle = getAngle(x1, y1, x2, y2);

			onFlingDetected(angle, velocityX, velocityY);

			return super.onFling(e1, e2, velocityX, velocityY);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
		onScrollingDetected();
		return super.onScroll(e1, e2, distanceX, distanceY);
	}

	public void onFlingDetected(double _angle, float _velocityX, float _velocityY) {}
	public void onScrollingDetected() {}

	public double getAngle(float _x1, float _y1, float _x2, float _y2) {
		double rad = Math.atan2((_y1 - _y2), (_x2 - _x1)) + Math.PI;

		return (((rad * 180) / Math.PI) + 180) % 360;
	}
}
