package kr.moonlightdriver.driver.activity;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import java.util.Iterator;
import java.util.List;

import kr.moonlightdriver.driver.R;

/**
 * Created by lk on 2016. 10. 17..
 */

public class ServiceExtenstionDialog extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.3f;
        getWindow().setAttributes(lpWindow);

        setContentView(R.layout.dialog_wokitoki_service_extenstion);

        stopWokitokiService();

        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(ServiceExtenstionDialog.this);
        builder.setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle("달빛기사")
                .setContentText("접속이 종료되었습니다. 다시 눌러 시작할 수 있습니다."); // 알림바에서 자동 삭제;
        builder.setAutoCancel(true);
        builder.setOngoing(false);
        Intent intent = new Intent(getApplicationContext(), ServiceExtenstionDialog.class);
        PendingIntent pIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pIntent);
        manager.notify(1, builder.build());

        SharedPreferences pref = getSharedPreferences("pref", MODE_PRIVATE);
        String mShuttleName = pref.getString("mShuttleName", "");

        TextView tvName = (TextView) findViewById(R.id.dialog_name);
        tvName.setText(mShuttleName);

        Button btnExtenstion = (Button) findViewById(R.id.btn_dialog_extenstion);
        btnExtenstion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ServiceExtenstionDialog.this, WokitokiService.class);
                intent.setPackage("kr.moonlightdriver.driver");
                startService(intent);
                finish();
            }
        });

        Button btnQuit = (Button) findViewById(R.id.btn_dialog_quit);
        btnQuit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void stopWokitokiService() {
        try {
            ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
            List<ActivityManager.RunningServiceInfo> info;
            info = am.getRunningServices(Integer.MAX_VALUE);
            //Log.i("aa", "Service 갯수 : " + info.size());
            for (Iterator it = info.iterator(); it.hasNext(); ) {
                ActivityManager.RunningServiceInfo runningServiceInfo = (ActivityManager.RunningServiceInfo) it.next();
                Log.i("aa", runningServiceInfo.service.getClassName() +"");
                if (runningServiceInfo.service.getClassName().contains("kr.moonlightdriver.driver.activity.WokitokiService")) {
                    Intent i = new Intent();
                    i.setComponent(runningServiceInfo.service);
                    stopService(i);
                    Log.i("aa", "Service 종료 시킴");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
