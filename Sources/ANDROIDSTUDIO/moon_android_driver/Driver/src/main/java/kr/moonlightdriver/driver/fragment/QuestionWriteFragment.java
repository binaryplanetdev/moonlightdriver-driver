package kr.moonlightdriver.driver.fragment;

import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import com.loopj.android.http.RequestParams;

import kr.moonlightdriver.driver.Network.NetApi.NetAPI;
import kr.moonlightdriver.driver.Network.NetApi.NetResponse;
import kr.moonlightdriver.driver.Network.NetApi.NetResponseCallback;
import kr.moonlightdriver.driver.Network.NetApi.NoResponseDataAck;
import kr.moonlightdriver.driver.Network.NetClient.NetClient;
import kr.moonlightdriver.driver.Network.NetData.ResultData;
import kr.moonlightdriver.driver.R;
import kr.moonlightdriver.driver.activity.MainActivity;
import kr.moonlightdriver.driver.utils.Global;
import kr.moonlightdriver.driver.utils.SVUtil;

public class QuestionWriteFragment extends Fragment {
	private EditText mTitle;
	private EditText mContents;

	private MainActivity mMainActivity;
	private View mView;

	private boolean mIsFinishFragment;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		try {
			mView = inflater.inflate(R.layout.frag_question_write, container, false);

			mMainActivity = (MainActivity) getActivity();

			mIsFinishFragment = false;

			ImageButton btnBack = (ImageButton) mView.findViewById(R.id.question_write_btn_back);
			mTitle = (EditText) mView.findViewById(R.id.question_write_title);
			mContents = (EditText) mView.findViewById(R.id.question_write_contents);
			ImageButton btnRegisterQuestion = (ImageButton) mView.findViewById(R.id.question_write_btn_register);

			btnBack.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					moveToQuestionFragment();
				}
			});

			btnRegisterQuestion.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						String checkRet = checkParams();
						if(checkRet.equals("OK")) {
							registerQuestion();
						} else {
							SVUtil.showSimpleDialog(mMainActivity, checkRet);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		} catch(InflateException e) {
			e.printStackTrace();
		}

		return mView;
	}

	private void registerQuestion() {
		try {
			SVUtil.showProgressDialog(mMainActivity, "등록중...");

			String title = mTitle.getText().toString();
			String contents = mContents.getText().toString();

			RequestParams params = new RequestParams();
			params.add("driverId", mMainActivity.mDriverInfo.getmDriverId());
			params.add("phone", mMainActivity.mDriverInfo.getmPhone());
			params.add("title", title);
			params.add("contents", contents);
			params.add("lat", mMainActivity.mCurrentLocation.latitude + "");
			params.add("lng", mMainActivity.mCurrentLocation.longitude + "");

			NetClient.send(mMainActivity, Global.URL_QUESTION_SEND, "POST", params, new NoResponseDataAck(), new NetResponseCallback(new NetResponse() {
				@Override
				public void onResponse(NetAPI _netAPI) {
					SVUtil.hideProgressDialog();

					try {
						if (_netAPI != null && !mIsFinishFragment) {
							NoResponseDataAck retNetApi = (NoResponseDataAck) _netAPI;
							ResultData result = retNetApi.getmResultData();

							if (result.getmCode() != 100) {
								SVUtil.showSimpleDialog(mMainActivity, result.getmDetail());
							} else {
								SVUtil.showDialogWithListener(mMainActivity, "등록되었습니다.", "확인", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										moveToQuestionFragment();
									}
								});
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String checkParams() {
		try {
			if(mMainActivity.mDriverInfo.getmDriverId() == null || mMainActivity.mDriverInfo.getmDriverId().isEmpty()) {
				return "기사로 등록한 후 입력 가능합니다.";
			} else if(mTitle.getText().toString().isEmpty()) {
				return "제목을 입력해주세요.";
			} else if(mContents.getText().toString().isEmpty()) {
				return "내용을 입력해주세요.";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "OK";
	}

	private void moveToQuestionFragment() {
		try {
			mMainActivity.mFragmentQuestion = new QuestionFragment();
			mMainActivity.replaceFragment(mMainActivity.mFragmentQuestion, R.id.mainFrameLayout, Global.FRAG_QUESTION_TAG, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onStart() {
		super.onStart();

		mIsFinishFragment = false;
	}

	@Override
	public void onResume() {
		super.onResume();

		try {
			mMainActivity.changeButtonImage(mMainActivity.mImgBtnQuestion);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onPause() {
		super.onPause();

		mIsFinishFragment = true;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();

		try {
			if(mView != null){
				ViewGroup parent = (ViewGroup) mView.getParent();
				if(parent!=null){
					parent.removeView(mView);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		setTargetFragment(null, -1);
	}
}
