package kr.moonlightdriver.driver.Network.NetData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by youngmin on 2015-11-05.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ChatJoinedDriverData {
	@JsonProperty("driverId")
	private String mDriverId;
	@JsonProperty("nickName")
	private String mNickName;
	@JsonProperty("joinDate")
	private String mJoinDate;

	public ChatJoinedDriverData() {}

	public ChatJoinedDriverData(String mDriverId, String mNickName, String joinDate) {
		this.mDriverId = mDriverId;
		this.mNickName = mNickName;
		this.mJoinDate = joinDate;
	}

	public String getmDriverId() {
		return mDriverId;
	}

	public void setmDriverId(String mDriverId) {
		this.mDriverId = mDriverId;
	}

	public String getmNickName() {
		return mNickName;
	}

	public void setmNickName(String mNickName) {
		this.mNickName = mNickName;
	}

	public String getmJoinDate(){
		return mJoinDate;
	}
}
