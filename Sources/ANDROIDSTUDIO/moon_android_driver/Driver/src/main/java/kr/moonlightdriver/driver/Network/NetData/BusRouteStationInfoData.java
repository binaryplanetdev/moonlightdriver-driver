package kr.moonlightdriver.driver.Network.NetData;

/**
 * Created by youngmin on 2015-12-31.
 */
public class BusRouteStationInfoData {
	private String mBusRouteArea;
	private String mStationName;
	private String mDirection;
	private String mArsId;
	private String mBeginTime;
	private String mLastTime;
	private String mPlateNo;
	private double mGpsY;
	private double mGpsX;
	private String mLocalBusRouteId;
	private boolean mTransYn;
	private int mSectSpd;
	private int mSeq;
	private boolean mIsLast;
	private boolean mLowPlate;
	private String mStationId;

	public BusRouteStationInfoData() {}

	public BusRouteStationInfoData(String _busRouteArea, int _seq, String _stationName, String _direction, String _arsId, String _beginTime, String _lastTime, boolean _transYn, int _sectSpd, double _gpsY, double _gpsX, String _localBusRouteId, String _stationId) {
		this.mBusRouteArea = _busRouteArea;
		this.mSeq = _seq;
		this.mStationName = _stationName;
		this.mDirection = _direction;
		this.mArsId = _arsId;
		this.mBeginTime = _beginTime;
		this.mLastTime = _lastTime;
		this.mTransYn = _transYn;
		this.mSectSpd = _sectSpd;
		this.mGpsY = _gpsY;
		this.mGpsX = _gpsX;
		this.mLocalBusRouteId = _localBusRouteId;
		this.mStationId = _stationId;
	}

	public String getmStationId() {
		return mStationId;
	}

	public void setmStationId(String mStationId) {
		this.mStationId = mStationId;
	}

	public String getmLocalBusRouteId() {
		return mLocalBusRouteId;
	}

	public void setmLocalBusRouteId(String mLocalBusRouteId) {
		this.mLocalBusRouteId = mLocalBusRouteId;
	}

	public String getmArsId() {
		return mArsId;
	}

	public void setmArsId(String mArsId) {
		this.mArsId = mArsId;
	}

	public String getmBeginTime() {
		return mBeginTime;
	}

	public void setmBeginTime(String mBeginTime) {
		this.mBeginTime = mBeginTime;
	}

	public String getmBusRouteArea() {
		return mBusRouteArea;
	}

	public void setmBusRouteArea(String mBusRouteArea) {
		this.mBusRouteArea = mBusRouteArea;
	}

	public String getmDirection() {
		return mDirection;
	}

	public void setmDirection(String mDirection) {
		this.mDirection = mDirection;
	}

	public double getmGpsX() {
		return mGpsX;
	}

	public void setmGpsX(double mGpsX) {
		this.mGpsX = mGpsX;
	}

	public double getmGpsY() {
		return mGpsY;
	}

	public void setmGpsY(double mGpsY) {
		this.mGpsY = mGpsY;
	}

	public boolean ismIsLast() {
		return mIsLast;
	}

	public void setmIsLast(boolean mIsLast) {
		this.mIsLast = mIsLast;
	}

	public String getmLastTime() {
		return mLastTime;
	}

	public void setmLastTime(String mLastTime) {
		this.mLastTime = mLastTime;
	}

	public boolean ismLowPlate() {
		return mLowPlate;
	}

	public void setmLowPlate(boolean mLowPlate) {
		this.mLowPlate = mLowPlate;
	}

	public String getmPlateNo() {
		return mPlateNo;
	}

	public void setmPlateNo(String mPlateNo) {
		this.mPlateNo = mPlateNo;
	}

	public int getmSectSpd() {
		return mSectSpd;
	}

	public void setmSectSpd(int mSectSpd) {
		this.mSectSpd = mSectSpd;
	}

	public int getmSeq() {
		return mSeq;
	}

	public void setmSeq(int mSeq) {
		this.mSeq = mSeq;
	}

	public String getmStationName() {
		return mStationName;
	}

	public void setmStationName(String mStationName) {
		this.mStationName = mStationName;
	}

	public boolean ismTransYn() {
		return mTransYn;
	}

	public void setmTransYn(boolean mTransYn) {
		this.mTransYn = mTransYn;
	}
}
