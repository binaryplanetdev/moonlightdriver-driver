package kr.moonlightdriver.driver.Network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

import kr.moonlightdriver.driver.Network.NetData.BusRouteInfoData;
import kr.moonlightdriver.driver.Network.NetData.ResultData;

/**
 * Created by youngmin on 2015-10-13.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class NightBusRouteListAck extends NetAPI {
	@JsonProperty("result")
	private ResultData mResultData;

	@JsonProperty("nightBusRouteList")
	private ArrayList<BusRouteInfoData> mNightBusRouteDataList;

	public NightBusRouteListAck() {
	}

	public NightBusRouteListAck(ArrayList<BusRouteInfoData> mNightBusRouteDataList, ResultData mResultData) {
		this.mNightBusRouteDataList = mNightBusRouteDataList;
		this.mResultData = mResultData;
	}

	public ArrayList<BusRouteInfoData> getmNightBusRouteDataList() {
		return mNightBusRouteDataList;
	}

	public void setmNightBusRouteDataList(ArrayList<BusRouteInfoData> mNightBusRouteDataList) {
		this.mNightBusRouteDataList = mNightBusRouteDataList;
	}

	public ResultData getmResultData() {
		return mResultData;
	}

	public void setmResultData(ResultData mResultData) {
		this.mResultData = mResultData;
	}
}
