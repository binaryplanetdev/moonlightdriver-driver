package kr.moonlightdriver.driver.utils;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import kr.moonlightdriver.driver.activity.WakingActivity;
import kr.moonlightdriver.driver.fragment.ShuttleChattingFragment;

/**
 * Created by redjjol on 24/12/14.
 */
public class GcmBroadcastReceiver extends WakefulBroadcastReceiver {
	private static final String TAG = "GcmBroadcastReceiver";

	private String mTaxiId;
	private String mMessage;
	private String mApplicantPhoneNumber;
	private String mPushType;

	@Override
	public void onReceive(Context context, Intent intent) {
		try {
			ComponentName comp = new ComponentName(context.getPackageName(), GcmIntentService.class.getName());
			startWakefulService(context, (intent.setComponent(comp)));

//		SVUtil.log("error", TAG, "receive : " + intent.getExtras().toString()); // 서버에서 어떻게 넘어올지 모르니 일단.. pass

			String nickName;
			String driverId;
			String messageType;
			String sendDate;
			String senderNickName;
			String senderDriverId;
			double lat = 0;
			double lng = 0;

			mTaxiId = intent.getExtras().getString("taxiId", "");
			mMessage = intent.getExtras().getString("message", "");
			mApplicantPhoneNumber = intent.getExtras().getString("phone", "");
			mPushType = intent.getExtras().getString("type", "");
			sendDate = intent.getExtras().getString("sendDate", "0");

			if (mPushType == null) {
				return;
			}

			switch (mPushType) {
				case "msg_shuttle_chat_notice" :
					String strDrivers = intent.getExtras().getString("drivers", "");

					Intent pushMsgIntent1 = new Intent();
					pushMsgIntent1.setAction(ShuttleChattingFragment.PUSH_MSG_1);
					pushMsgIntent1.putExtra("sendDate", Long.parseLong(sendDate));
					pushMsgIntent1.putExtra("message", mMessage);
					pushMsgIntent1.putExtra("strDrivers", strDrivers);

					context.sendBroadcast(pushMsgIntent1);
					break;
				case "msg_shuttle_chat" :
					nickName = intent.getExtras().getString("nickName", "");
					driverId = intent.getExtras().getString("driverId", "");
					messageType = intent.getExtras().getString("messageType", "");
					lat = Double.parseDouble(intent.getExtras().getString("lat", "0"));
					lng = Double.parseDouble(intent.getExtras().getString("lng", "0"));

					Intent pushMsgIntent2 = new Intent();
					pushMsgIntent2.setAction(ShuttleChattingFragment.PUSH_MSG_2);
					pushMsgIntent2.putExtra("sendDate", Long.parseLong(sendDate));
					pushMsgIntent2.putExtra("driverId", driverId);
					pushMsgIntent2.putExtra("nickName", nickName);
					pushMsgIntent2.putExtra("message", mMessage);
					pushMsgIntent2.putExtra("messageType", messageType);
					pushMsgIntent2.putExtra("lat", lat);
					pushMsgIntent2.putExtra("lng", lng);

					context.sendBroadcast(pushMsgIntent2);
					break;
				case "msg_req_location" :
					senderDriverId = intent.getExtras().getString("sendDriverId", "");
					senderNickName = intent.getExtras().getString("sendNickName", "");

					Intent pushMsgIntent3 = new Intent();
					pushMsgIntent3.setAction(ShuttleChattingFragment.PUSH_MSG_3);
					pushMsgIntent3.putExtra("senderDriverId", senderDriverId);
					pushMsgIntent3.putExtra("senderNickName", senderNickName);
					pushMsgIntent3.putExtra("message", mMessage);

					context.sendBroadcast(pushMsgIntent3);
					break;
				case "msg_res_location" :
					senderNickName = intent.getExtras().getString("sendNickName", "");
					String result = intent.getExtras().getString("result", "");

					if(result.equals("OK")) {
						lat = Double.parseDouble(intent.getExtras().getString("lat", "0"));
						lng = Double.parseDouble(intent.getExtras().getString("lng", "0"));
					}

					Intent pushMsgIntent4 = new Intent();
					pushMsgIntent4.setAction(ShuttleChattingFragment.PUSH_MSG_4);
					pushMsgIntent4.putExtra("senderNickName", senderNickName);
					pushMsgIntent4.putExtra("result", result);
					pushMsgIntent4.putExtra("lat", lat);
					pushMsgIntent4.putExtra("lng", lng);

					context.sendBroadcast(pushMsgIntent4);
					break;
				case "register_virtual_number" :
					String virtualNumber = intent.getExtras().getString("virtual_number", "");
					result = intent.getExtras().getString("result", "");

					Intent pushMsgIntent5 = new Intent();
					pushMsgIntent5.setAction(ShuttleChattingFragment.PUSH_MSG_5);
					pushMsgIntent5.putExtra("virtualNumber", virtualNumber);
					pushMsgIntent5.putExtra("result", result);

					context.sendBroadcast(pushMsgIntent5);
					break;
				case "request_taxi" :
				case "success_taxi" :
				case "decline_taxi" :
				case "create_taxi" :
					wakingUpBlackScreen(context);
					break;
				case "question_answer" :
				case "question_register" :
					String questionId = intent.getExtras().getString("questionId");
					showQuestionAnswerNotice(context, questionId, mMessage, mPushType);
					break;
				case "twoinone_answer" :
					String boardId = intent.getExtras().getString("boardId");
					showTwoInOneAnswerNotice(context, boardId, mMessage, mPushType);
					break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void showNormalNotice(Context context) {
		try {
			Intent i = new Intent();
			i.putExtra("message", mMessage);
			i.putExtra("type", mPushType);

			i.setClass(context, WakingActivity.class);
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
			context.startActivity(i);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void wakingUpBlackScreen(Context context) {
		try {
			Intent i = new Intent();
			i.putExtra("taxiId", mTaxiId);
			i.putExtra("message", mMessage);
			i.putExtra("phone_apply", mApplicantPhoneNumber);
			i.putExtra("type", mPushType);

			i.setClass(context, WakingActivity.class);
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
			context.startActivity(i);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void showQuestionAnswerNotice(Context context, String _questionId, String _message, String _pushType) {
		try {
			Intent i = new Intent();
			i.putExtra("questionId", _questionId);
			i.putExtra("message", _message);
			i.putExtra("type", _pushType);

			i.setClass(context, WakingActivity.class);
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
			context.startActivity(i);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void showTwoInOneAnswerNotice(Context context, String _boardId, String _message, String _pushType) {
		try {
			Intent i = new Intent();
			i.putExtra("boardId", _boardId);
			i.putExtra("message", _message);
			i.putExtra("type", _pushType);

			i.setClass(context, WakingActivity.class);
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
			context.startActivity(i);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}