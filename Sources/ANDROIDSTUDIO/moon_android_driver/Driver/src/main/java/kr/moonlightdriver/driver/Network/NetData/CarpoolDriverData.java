package kr.moonlightdriver.driver.Network.NetData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by youngmin on 2015-10-29.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CarpoolDriverData {
	@JsonProperty("_id")
	private String mId;

	@JsonProperty("phone")
	private String mPhoneNumber;

	@JsonProperty("isAccept")
	private boolean mIsAccept;

	@JsonProperty("status")
	private String mStatus;

	public CarpoolDriverData() {}

	public CarpoolDriverData(String mId, boolean mIsAccept, String mPhoneNumber, String mStatus) {
		this.mId = mId;
		this.mIsAccept = mIsAccept;
		this.mPhoneNumber = mPhoneNumber;
		this.mStatus = mStatus;
	}

	public String getmId() {
		return mId;
	}

	public void setmId(String mId) {
		this.mId = mId;
	}

	public boolean ismIsAccept() {
		return mIsAccept;
	}

	public void setmIsAccept(boolean mIsAccept) {
		this.mIsAccept = mIsAccept;
	}

	public String getmPhoneNumber() {
		return mPhoneNumber;
	}

	public void setmPhoneNumber(String mPhoneNumber) {
		this.mPhoneNumber = mPhoneNumber;
	}

	public String getmStatus() {
		return mStatus;
	}

	public void setmStatus(String mStatus) {
		this.mStatus = mStatus;
	}
}
