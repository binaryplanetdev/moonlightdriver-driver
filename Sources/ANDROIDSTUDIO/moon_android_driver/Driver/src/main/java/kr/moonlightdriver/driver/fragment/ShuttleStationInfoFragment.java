package kr.moonlightdriver.driver.fragment;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import kr.moonlightdriver.driver.Network.NetApi.NetAPI;
import kr.moonlightdriver.driver.Network.NetApi.NetResponse;
import kr.moonlightdriver.driver.Network.NetApi.NetResponseCallback;
import kr.moonlightdriver.driver.Network.NetApi.ShuttleListAck;
import kr.moonlightdriver.driver.Network.NetClient.NetClient;
import kr.moonlightdriver.driver.Network.NetData.ResultData;
import kr.moonlightdriver.driver.Network.NetData.ShuttleListData;
import kr.moonlightdriver.driver.R;
import kr.moonlightdriver.driver.activity.MainActivity;
import kr.moonlightdriver.driver.utils.SVUtil;
import kr.moonlightdriver.driver.utils.Global;

/**
 * Created by redjjol on 19/11/14.
 */
public class ShuttleStationInfoFragment extends Fragment {
	private static final String TAG = "ShuttleStationInfo";
	private View mView;

	//view
	private ShuttleStationInfoAdapter mShuttleListAdapter;
	private ArrayList<ShuttleListData> mShuttleListData;
	private MainActivity mMainActivity;

	private String mOutbackFrom;
	private double mCenterLat;
	private double mCenterLng;
	private double mPositionLat;
	private double mPositionLng;

	private boolean mIsFinishFragment;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		try {
			mView = inflater.inflate(R.layout.frag_shuttle_station_info, container, false);

			mMainActivity = (MainActivity) getActivity();

			mIsFinishFragment = false;

			TextView titleTv = (TextView) mView.findViewById(R.id.shuttle_info_title);
			TextView shuttleIdTv = (TextView) mView.findViewById(R.id.shuttle_info_shuttle_id);
			ImageButton btnBack = (ImageButton) mView.findViewById(R.id.shuttle_info_btn_back);
			btnBack.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						Bundle args = new Bundle();
						args.putString("from", mOutbackFrom);
						args.putDouble("lat", mPositionLat);
						args.putDouble("lng", mPositionLng);
						args.putDouble("centerLat", mCenterLat);
						args.putDouble("centerLng", mCenterLng);

						mMainActivity.mFragmentOutback = new OutbackFragment();
						mMainActivity.mFragmentOutback.setArguments(args);

						mMainActivity.removeGoogleMapFragment(Global.FRAG_OUTBACK_TAG, R.id.outback_map);

						mMainActivity.ChangeOutbackFragmentHandler.sendEmptyMessageDelayed(0, 100);
//					mMainActivity.replaceFragment(mMainActivity.mFragmentOutback, R.id.mainFrameLayout, Global.FRAG_OUTBACK_TAG);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			mShuttleListData = new ArrayList<>();

			ListView shuttleListView = (ListView) mView.findViewById(R.id.shuttle_info_listview);
			mShuttleListAdapter = new ShuttleStationInfoAdapter(mMainActivity, R.layout.row_shuttle_info, mShuttleListData);
			shuttleListView.setAdapter(mShuttleListAdapter);

			shuttleListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					mMainActivity.checkDriverConfirmData(mShuttleListData.get(position).getmBusId(), "main");
				}
			});

			Bundle bundle = ShuttleStationInfoFragment.this.getArguments();
			String arsId = bundle.getString("arsId", "");
			String stationName = bundle.getString("stationName", "");
			mOutbackFrom = bundle.getString("from", "");
			mCenterLat = bundle.getDouble("centerLat");
			mCenterLng = bundle.getDouble("centerLng");
			mPositionLat = bundle.getDouble("positionLat");
			mPositionLng = bundle.getDouble("positionLng");

			stationName += "(대리셔틀)";
			String stationId = "[" + arsId + "]";

			titleTv.setText(stationName);
			shuttleIdTv.setText(stationId);

			getShuttleListByArsId(arsId);
		} catch(InflateException e) {
			e.printStackTrace();
		}

		return mView;
	}

	private void getShuttleListByArsId(String arsId) {
		SVUtil.showProgressDialog(mMainActivity, "로딩중...");

		String url = Global.URL_SHUTTLE_LIST_BY_ID.replaceAll(":arsId", arsId);
		NetClient.send(mMainActivity, url, "GET", null, new ShuttleListAck(), new NetResponseCallback(new NetResponse() {
			@Override
			public void onResponse(NetAPI _netAPI) {
				SVUtil.hideProgressDialog();

				try {
					mShuttleListData.clear();

					if(_netAPI != null && !mIsFinishFragment) {
						ShuttleListAck retNetApi = (ShuttleListAck) _netAPI;
						ResultData result = retNetApi.getmResultData();
						if (result.getmCode() != 100) {
							SVUtil.showSimpleDialog(mMainActivity, result.getmDetail());
							return;
						}

						int listSize = retNetApi.getmShuttleListData().size();
						for (int i = 0; i < listSize; i++) {
							mShuttleListData.add(retNetApi.getmShuttleListData().get(i));
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					mShuttleListAdapter.notifyDataSetChanged();
				}
			}
		}));
	}

	private class ShuttleInfoHolder {
		public TextView mShuttleName;
		public TextView mShuttleDesc;
	}

	private class ShuttleStationInfoAdapter extends ArrayAdapter<ShuttleListData> {
		ArrayList<ShuttleListData> mItems;
		private int mCellId;
		private ShuttleInfoHolder mShuttleInfoHolder;


		public ShuttleStationInfoAdapter(Context cxt, int cellID, ArrayList<ShuttleListData> data) {
			super(cxt, cellID, data);

			this.mItems = data;
			this.mCellId = cellID;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;

			try {
				if (v == null) {
					LayoutInflater li = (LayoutInflater) mMainActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					v = li.inflate(mCellId, null);

					mShuttleInfoHolder = new ShuttleInfoHolder();
					mShuttleInfoHolder.mShuttleName = (TextView) v.findViewById(R.id.row_shuttle_info_shuttle_name);
					mShuttleInfoHolder.mShuttleDesc = (TextView) v.findViewById(R.id.row_shuttle_info_shuttle_desc);

					v.setTag(mShuttleInfoHolder);
				} else {
					mShuttleInfoHolder = (ShuttleInfoHolder) v.getTag();
				}

				ShuttleListData box = mItems.get(position);

				mShuttleInfoHolder.mShuttleName.setText(box.getmShuttleName());
				mShuttleInfoHolder.mShuttleDesc.setText(box.getmDescription());
			} catch (Exception e) {
				e.printStackTrace();
			}

			return v;
		}
	}

	@Override
	public void onStart() {
		super.onStart();

		mIsFinishFragment = false;
	}

	@Override
	public void onResume() {
		super.onResume();

		try {
			mMainActivity.changeButtonImage(mMainActivity.mImgBtnShuttle);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onPause() {
		super.onPause();

		mIsFinishFragment = true;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();

		try {
			if(mView != null){
				ViewGroup parent = (ViewGroup) mView.getParent();
				if(parent != null){
					parent.removeView(mView);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		setTargetFragment(null, -1);
	}
}
