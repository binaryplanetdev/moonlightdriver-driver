package kr.moonlightdriver.driver.Network.NetData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by youngmin on 2015-10-30.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BusStationListData {
	@JsonProperty("stationId")
	private int mStationId;

	@JsonProperty("stationName")
	private String mStationName;

	@JsonProperty("arsId")
	private String mArsId;

	@JsonProperty("gpsX")
	private double mGpsX;

	@JsonProperty("gpsY")
	private double mGpsY;

	@JsonProperty("distance")
	private double mDistance;

	@JsonProperty("startTime")
	private String mStartTime;

	@JsonProperty("endTime")
	private String mEndTime;

	@JsonProperty("busRouteArea")
	private String mBusRouteArea;

	@JsonProperty("localStationId")
	private String mLocalStationId;

	@JsonProperty("stationSequence")
	private int mStationSequence;

	@JsonProperty("direction")
	private String mDirection;

	@JsonProperty("location")
	private ShuttleLocationData mLocation;

	public BusStationListData() {}

	public BusStationListData(String mArsId, String mBusRouteArea, String mDirection, double mDistance, String mEndTime, double mGpsX, double mGpsY, String mLocalStationId, String mStartTime, int mStationId, String mStationName, int mStationSequence) {
		this.mArsId = mArsId;
		this.mBusRouteArea = mBusRouteArea;
		this.mDirection = mDirection;
		this.mDistance = mDistance;
		this.mEndTime = mEndTime;
		this.mGpsX = mGpsX;
		this.mGpsY = mGpsY;
		this.mLocalStationId = mLocalStationId;
		this.mStartTime = mStartTime;
		this.mStationId = mStationId;
		this.mStationName = mStationName;
		this.mStationSequence = mStationSequence;
	}

	public String getmLocalStationId() {
		return mLocalStationId;
	}

	public void setmLocalStationId(String mLocalStationId) {
		this.mLocalStationId = mLocalStationId;
	}

	public String getmArsId() {
		return mArsId;
	}

	public void setmArsId(String mArsId) {
		this.mArsId = mArsId;
	}

	public String getmBusRouteArea() {
		return mBusRouteArea;
	}

	public void setmBusRouteArea(String mBusRouteArea) {
		this.mBusRouteArea = mBusRouteArea;
	}

	public double getmDistance() {
		return mDistance;
	}

	public void setmDistance(double mDistance) {
		this.mDistance = mDistance;
	}

	public String getmEndTime() {
		return mEndTime;
	}

	public void setmEndTime(String mEndTime) {
		this.mEndTime = mEndTime;
	}

	public double getmGpsX() {
		return mGpsX;
	}

	public void setmGpsX(double mGpsX) {
		this.mGpsX = mGpsX;
	}

	public double getmGpsY() {
		return mGpsY;
	}

	public void setmGpsY(double mGpsY) {
		this.mGpsY = mGpsY;
	}

	public String getmStartTime() {
		return mStartTime;
	}

	public void setmStartTime(String mStartTime) {
		this.mStartTime = mStartTime;
	}

	public int getmStationId() {
		return mStationId;
	}

	public void setmStationId(int mStationId) {
		this.mStationId = mStationId;
	}

	public String getmStationName() {
		return mStationName;
	}

	public void setmStationName(String mStationName) {
		this.mStationName = mStationName;
	}

	public String getmDirection() {
		return mDirection;
	}

	public void setmDirection(String mDirection) {
		this.mDirection = mDirection;
	}

	public int getmStationSequence() {
		return mStationSequence;
	}

	public void setmStationSequence(int mStationSequence) {
		this.mStationSequence = mStationSequence;
	}
}
