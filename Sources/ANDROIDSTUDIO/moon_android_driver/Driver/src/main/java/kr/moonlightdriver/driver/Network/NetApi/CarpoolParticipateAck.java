package kr.moonlightdriver.driver.Network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import kr.moonlightdriver.driver.Network.NetData.DriverData;
import kr.moonlightdriver.driver.Network.NetData.ResultData;

/**
 * Created by youngmin on 2015-10-13.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CarpoolParticipateAck extends NetAPI {
	@JsonProperty("result")
	private ResultData mResultData;
	@JsonProperty("driver")
	private DriverData mDriverInfo;

	public CarpoolParticipateAck() {}

	public CarpoolParticipateAck(DriverData mDriverInfo, ResultData mResultData) {
		this.mDriverInfo = mDriverInfo;
		this.mResultData = mResultData;
	}

	public DriverData getmDriverInfo() {
		return mDriverInfo;
	}

	public void setmDriverInfo(DriverData mDriverInfo) {
		this.mDriverInfo = mDriverInfo;
	}

	public ResultData getmResultData() {
		return mResultData;
	}

	public void setmResultData(ResultData mResultData) {
		this.mResultData = mResultData;
	}
}
