package kr.moonlightdriver.driver.fragment;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import kr.moonlightdriver.driver.R;

class InviteAgreeDialog extends Dialog {
    private ImageButton mDisAgreeButton;
    private ImageButton mAgreeButton;
    private View.OnClickListener mAgreeListener;
    private View.OnClickListener mDisAgreeListener;
    private TextView mSenderNick;
    private String senderNick;

    public InviteAgreeDialog(Context context, String senderNick, View.OnClickListener mAgreeListener, View.OnClickListener mDisAgreeListener) {
        super(context);
        this.mAgreeListener = mAgreeListener;
        this.mDisAgreeListener = mDisAgreeListener;
        this.senderNick = senderNick;
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
    }

    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);

            WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
            lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
            lpWindow.dimAmount = 0.3f;
            getWindow().setAttributes(lpWindow);

            setContentView(R.layout.dialog_wokitoki_invite);

            mDisAgreeButton = (ImageButton) findViewById(R.id.btn_dialog_disagree);
            mAgreeButton = (ImageButton) findViewById(R.id.btn_dialog_agree);
            mSenderNick = (TextView) findViewById(R.id.dialog_sendernick);

            mDisAgreeButton.setOnClickListener(mDisAgreeListener);
            mAgreeButton.setOnClickListener(mAgreeListener);

            mSenderNick.setText(senderNick);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}