package kr.moonlightdriver.driver.Network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

import kr.moonlightdriver.driver.Network.NetData.BusStationListData;
import kr.moonlightdriver.driver.Network.NetData.ResultData;
import kr.moonlightdriver.driver.Network.NetData.SubwayStationListData;

/**
 * Created by youngmin on 2015-10-13.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PublicTransitStationListAck extends NetAPI {
	@JsonProperty("result")
	private ResultData mResultData;
	@JsonProperty("busStationList")
	private ArrayList<BusStationListData> mBusStationDataList;
	@JsonProperty("subwayStationList")
	private ArrayList<SubwayStationListData> mSubwayStationDataList;

	public PublicTransitStationListAck() {}

	public PublicTransitStationListAck(ArrayList<BusStationListData> mBusStationDataList, ResultData mResultData, ArrayList<SubwayStationListData> mSubwayStationDataList) {
		this.mBusStationDataList = mBusStationDataList;
		this.mResultData = mResultData;
		this.mSubwayStationDataList = mSubwayStationDataList;
	}

	public ArrayList<BusStationListData> getmBusStationDataList() {
		return mBusStationDataList;
	}

	public void setmBusStationDataList(ArrayList<BusStationListData> mBusStationDataList) {
		this.mBusStationDataList = mBusStationDataList;
	}

	public ResultData getmResultData() {
		return mResultData;
	}

	public void setmResultData(ResultData mResultData) {
		this.mResultData = mResultData;
	}

	public ArrayList<SubwayStationListData> getmSubwayStationDataList() {
		return mSubwayStationDataList;
	}

	public void setmSubwayStationDataList(ArrayList<SubwayStationListData> mSubwayStationDataList) {
		this.mSubwayStationDataList = mSubwayStationDataList;
	}
}
