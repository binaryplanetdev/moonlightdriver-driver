package kr.moonlightdriver.driver.Network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

import kr.moonlightdriver.driver.Network.NetData.NoticeData;
import kr.moonlightdriver.driver.Network.NetData.ResultData;

/**
 * Created by youngmin on 2015-10-13.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class NoticeListAck extends NetAPI {
	@JsonProperty("result")
	private ResultData mResultData;

	@JsonProperty("notices")
	private ArrayList<NoticeData> mNoticeDataList;

	public NoticeListAck() {}

	public NoticeListAck(ArrayList<NoticeData> mNoticeDataList, ResultData mResultData) {
		this.mNoticeDataList = mNoticeDataList;
		this.mResultData = mResultData;
	}

	public ArrayList<NoticeData> getmNoticeDataList() {
		return mNoticeDataList;
	}

	public void setmNoticeDataList(ArrayList<NoticeData> mNoticeDataList) {
		this.mNoticeDataList = mNoticeDataList;
	}

	public ResultData getmResultData() {
		return mResultData;
	}

	public void setmResultData(ResultData mResultData) {
		this.mResultData = mResultData;
	}
}
