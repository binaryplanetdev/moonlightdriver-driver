package kr.moonlightdriver.driver.Network.NetData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by lk on 2016. 10. 2..
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class WokitokiData {
    @JsonProperty("_id")
    private String mId;
    @JsonProperty("phone")
    private String mPhone;
    @JsonProperty("messageSrc")
    private String mMessageSrc;
    @JsonProperty("driverId")
    private String mDriverId;
    @JsonProperty("nickName")
    private String mNickName;

    public WokitokiData(String mId, String mPhone, String mMessageSrc, String mDriverId, String mNickName, double mLat, double mLng, String mDate) {
        this.mId = mId;
        this.mPhone = mPhone;
        this.mMessageSrc = mMessageSrc;
        this.mDriverId = mDriverId;
        this.mNickName = mNickName;
        this.mLat = mLat;
        this.mLng = mLng;
        this.mDate = mDate;
    }

    public WokitokiData(){}

    @JsonProperty("lat")
    private double mLat;
    @JsonProperty("lng")
    private double mLng;
    @JsonProperty("date")
    private String mDate;

    public String getmId() {
        return mId;
    }

    public String getmPhone() {
        return mPhone;
    }

    public String getmMessageSrc() {
        return mMessageSrc;
    }

    public String getmDriverId() {
        return mDriverId;
    }

    public String getmNickName() {
        return mNickName;
    }

    public double getmLat() {
        return mLat;
    }

    public double getmLng() {
        return mLng;
    }

    public String getmDate() {
        return mDate;
    }
}
