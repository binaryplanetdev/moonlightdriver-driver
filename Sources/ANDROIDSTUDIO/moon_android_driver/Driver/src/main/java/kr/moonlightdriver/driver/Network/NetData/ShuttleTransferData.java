package kr.moonlightdriver.driver.Network.NetData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

/**
 * Created by youngmin on 2015-12-31.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShuttleTransferData {
	@JsonProperty("depart")
	private ShuttleTransferLocationData mDepartStation;

	@JsonProperty("arrive")
	private ShuttleTransferLocationData mArriveStation;

	@JsonProperty("transferName")
	private String mTransferName;

	@JsonProperty("pathList")
	private ArrayList<ShuttleTransferPathData> mTransferPathList;

	private ArrayList<ArrayList<LatLng>> mPedestrianPathList;

	public ShuttleTransferData() {
		mPedestrianPathList = new ArrayList<>();
	}

	public ShuttleTransferData(ShuttleTransferLocationData mDepartStation, ShuttleTransferLocationData mArriveStation, String mTransferName, ArrayList<ShuttleTransferPathData> mTransferPathList, ArrayList<ArrayList<LatLng>> mPedestrianPathList) {
		this.mDepartStation = mDepartStation;
		this.mArriveStation = mArriveStation;
		this.mTransferName = mTransferName;
		this.mTransferPathList = mTransferPathList;
		this.mPedestrianPathList = mPedestrianPathList;
	}

	public ShuttleTransferLocationData getmDepartStation() {
		return mDepartStation;
	}

	public void setmDepartStation(ShuttleTransferLocationData mDepartStation) {
		this.mDepartStation = mDepartStation;
	}

	public ShuttleTransferLocationData getmArriveStation() {
		return mArriveStation;
	}

	public void setmArriveStation(ShuttleTransferLocationData mArriveStation) {
		this.mArriveStation = mArriveStation;
	}

	public String getmTransferName() {
		return mTransferName;
	}

	public void setmTransferName(String mTransferName) {
		this.mTransferName = mTransferName;
	}

	public ArrayList<ShuttleTransferPathData> getmTransferPathList() {
		return mTransferPathList;
	}

	public void setmTransferPathList(ArrayList<ShuttleTransferPathData> mTransferPathList) {
		this.mTransferPathList = mTransferPathList;
	}

	public ArrayList<ArrayList<LatLng>> getmPedestrianPathList() {
		return mPedestrianPathList;
	}

	public void setmPedestrianPathList(ArrayList<ArrayList<LatLng>> mPedestrianPathList) {
		this.mPedestrianPathList = mPedestrianPathList;
	}
}
