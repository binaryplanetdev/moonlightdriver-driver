package kr.moonlightdriver.driver.Network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

import kr.moonlightdriver.driver.Network.NetData.ResultData;
import kr.moonlightdriver.driver.Network.NetData.ShuttleRealTimeData;

/**
 * Created by youngmin on 2015-10-13.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShuttleRealTimeDataAck extends NetAPI {
	@JsonProperty("result")
	private ResultData mResultData;
	@JsonProperty("realTimePosition")
	private ArrayList<ShuttleRealTimeData> mShuttleRealTimeData;

	public ShuttleRealTimeDataAck() {}

	public ShuttleRealTimeDataAck(ResultData mResultData, ArrayList<ShuttleRealTimeData> mShuttleRealTimeData) {
		this.mResultData = mResultData;
		this.mShuttleRealTimeData = mShuttleRealTimeData;
	}

	public ResultData getmResultData() {
		return mResultData;
	}

	public void setmResultData(ResultData mResultData) {
		this.mResultData = mResultData;
	}

	public ArrayList<ShuttleRealTimeData> getmShuttleRealTimeData() {
		return mShuttleRealTimeData;
	}

	public void setmShuttleRealTimeData(ArrayList<ShuttleRealTimeData> mShuttleRealTimeData) {
		this.mShuttleRealTimeData = mShuttleRealTimeData;
	}
}
