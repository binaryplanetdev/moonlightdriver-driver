package kr.moonlightdriver.driver.Network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

import kr.moonlightdriver.driver.Network.NetData.InsuranceCompanyListData;
import kr.moonlightdriver.driver.Network.NetData.ResultData;

/**
 * Created by youngmin on 2015-10-13.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CheckDriverConfirmDataAck extends NetAPI {
	@JsonProperty("result")
	private ResultData mResultData;

	@JsonProperty("company_list")
	private ArrayList<InsuranceCompanyListData> mInsuranceCompanyDataList;

	public CheckDriverConfirmDataAck() {}

	public CheckDriverConfirmDataAck(ArrayList<InsuranceCompanyListData> mInsuranceCompanyDataList, ResultData mResultData) {
		this.mInsuranceCompanyDataList = mInsuranceCompanyDataList;
		this.mResultData = mResultData;
	}

	public ArrayList<InsuranceCompanyListData> getmInsuranceCompanyDataList() {
		return mInsuranceCompanyDataList;
	}

	public void setmInsuranceCompanyDataList(ArrayList<InsuranceCompanyListData> mInsuranceCompanyDataList) {
		this.mInsuranceCompanyDataList = mInsuranceCompanyDataList;
	}

	public ResultData getmResultData() {
		return mResultData;
	}

	public void setmResultData(ResultData mResultData) {
		this.mResultData = mResultData;
	}
}
