package kr.moonlightdriver.driver.Network.NetApi;

/**
 * Created by youngmin on 2015-10-13.
 */
public class NetResponseCallback {
	private NetResponse mNetResponse;

	public NetResponseCallback(NetResponse mNetResponse) {
		this.mNetResponse = mNetResponse;
	}

	public void onResponse(NetAPI _netAPI) {
		this.mNetResponse.onResponse(_netAPI);
	}
}
