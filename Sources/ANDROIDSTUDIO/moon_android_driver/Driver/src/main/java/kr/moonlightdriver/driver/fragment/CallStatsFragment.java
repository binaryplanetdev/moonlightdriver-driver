package kr.moonlightdriver.driver.fragment;

import android.app.Fragment;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequence;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import kr.moonlightdriver.driver.Network.NetClient.NetClient;
import kr.moonlightdriver.driver.Network.NetData.CallStatsCoordinatesData;
import kr.moonlightdriver.driver.Network.NetData.CallStatsInfoData;
import kr.moonlightdriver.driver.R;
import kr.moonlightdriver.driver.activity.MainActivity;
import kr.moonlightdriver.driver.utils.Global;
import kr.moonlightdriver.driver.utils.SVUtil;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * Created by youngmin on 2015-07-22.
 */
public class CallStatsFragment extends Fragment {
	private static final String TAG = "CallStats Fragment";
	private static final int ZOOM = 11;

	private GoogleMap mGoogleMap; // Might be null if Google Play services APK is not available.

	private LatLngBounds mCurrentScreen;
	private com.vividsolutions.jts.geom.Polygon mExtendedScreenPolygon;
	private JSONArray mPolygonList;
	private Spinner mSpinner;
	private int mSelectedType;

	private ArrayList<Marker> mMarkerList;
	private JSONObject mGuHistoryData;
	private JSONObject mDongHistoryData;

//	private boolean mIsDoneFetchData;
	private MainActivity mMainActivity;

	private LinearLayout mCallStatsListLayout;
	private Button mBtnCallStatsCallList;
	private Button mBtnCallStatsAroundMap;
	private ImageButton mBtnZoomIn;
	private ImageButton mBtnZoomOut;
	private CallStatsAdapter mCallStatsAdapter;
	private ArrayList<CallStatsInfoData> mCallStatsList;

	private GetCoordinatesData mGetCoordinatesDataTask;

	private boolean mIsShowNumberOnly;
	private boolean mIsUseAlpha;

	private View mView;

	private boolean mIsFinishFragment;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		try {
			mView = inflater.inflate(R.layout.frag_call_stats, container, false);

			mMainActivity = (MainActivity) getActivity();

			mIsFinishFragment = false;

			init();

			mCallStatsListLayout = (LinearLayout) mView.findViewById(R.id.frgCallStatsListLayout);
			StickyListHeadersListView callStatsListView = (StickyListHeadersListView) mView.findViewById(R.id.frgCallStatsListView);
			mBtnCallStatsCallList = (Button) mView.findViewById(R.id.btnCallStatsCallList);
			mBtnCallStatsAroundMap = (Button) mView.findViewById(R.id.btnCallStatsAroundMap);
			mBtnCallStatsAroundMap.setVisibility(View.GONE);

			mBtnZoomIn = (ImageButton) mView.findViewById(R.id.btnCallStatsZoomIn);
			mBtnZoomOut = (ImageButton) mView.findViewById(R.id.btnCallStatsZoomOut);

			mSpinner = (Spinner)mView.findViewById(R.id.spinner);
			ArrayAdapter<String> adapter = new ArrayAdapter<>(mMainActivity, android.R.layout.simple_spinner_dropdown_item, Global.CALL_HISTORY_TYPE);
			mSpinner.setAdapter(adapter);

			mCallStatsAdapter = new CallStatsAdapter(mMainActivity, R.layout.row_call_count, mCallStatsList);
			callStatsListView.setAdapter(mCallStatsAdapter);

			setUpMapIfNeeded();
		} catch(InflateException e) {
			e.printStackTrace();
		}

		return mView;
	}

	private void setUpMapIfNeeded() {
		try {
			if (mGoogleMap == null) {
				mGoogleMap = ((SupportMapFragment) MainActivity.mSupportFragmentManager.findFragmentById(R.id.call_stats_map)).getMap();
				if (mGoogleMap != null) {
					setUpMap();
				}
			}

//			if (mGoogleMap == null) {
//				SupportMapFragment mapFragment = (SupportMapFragment) MainActivity.mSupportFragmentManager.findFragmentById(R.id.call_stats_map);
//				mapFragment.getMapAsync(this);
//			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

//	@Override
//	public void onMapReady(GoogleMap googleMap) {
//		try {
//			mGoogleMap = googleMap;
//			setUpMap();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	private void setUpMap() {
		try {
			mGoogleMap.getUiSettings().setRotateGesturesEnabled(false);
			mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
			mGoogleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_carpool_current_position)).position(mMainActivity.mCurrentLocation).anchor(0.5f, 1.0f));
			mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mMainActivity.mCurrentLocation, ZOOM));
			mCurrentScreen = mGoogleMap.getProjection().getVisibleRegion().latLngBounds;

			setEventListener();

			getCallHistoryData();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private class GetCoordinatesData extends AsyncTask<Void, Void, ArrayList<CallStatsCoordinatesData>> {
		private boolean mIsCanceled = false;
		private String mGuName;
		private int mIsGu;
		private int mIsDong;

		public GetCoordinatesData(String _guName, int _isGu, int _isDong) {
			this.mGuName = _guName;
			this.mIsGu = _isGu;
			this.mIsDong = _isDong;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected ArrayList<CallStatsCoordinatesData> doInBackground(Void... params) {
			ArrayList<CallStatsCoordinatesData> retList = new ArrayList<>();

			try {
				Cursor cursor;

				if(!mIsCanceled) {
					String sql;
					if(this.mGuName.isEmpty()) {
						sql = "SELECT seoulAreaIndex, siName, guName, dongName, coordinates FROM SeoulArea WHERE isGu = " + this.mIsGu + " AND isDong = " + this.mIsDong + ";";
					} else {
						sql = "SELECT seoulAreaIndex, siName, guName, dongName, coordinates FROM SeoulArea WHERE guName = '" + this.mGuName + "' AND isGu = " + this.mIsGu + " AND isDong = " + this.mIsDong + ";";
					}

					cursor = mMainActivity.mDatabase.rawQuery(sql, null);
//				SVUtil.log("error", TAG, sql);

					if(!mIsCanceled && !mIsFinishFragment && cursor != null) {
						cursor.moveToFirst();

						int row_count = cursor.getCount();

						if(row_count > 0) {
							do {
								int seoulAreaIndex = cursor.getInt(0);
								String siName = cursor.getString(1);
								String guName = cursor.getString(2);
								String dongName = cursor.getString(3);
								String coordinates = cursor.getString(4);

								retList.add(new CallStatsCoordinatesData(seoulAreaIndex, siName, guName, dongName, this.mIsGu, this.mIsDong, coordinates));
							} while (cursor.moveToNext());
						}

						cursor.close();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return retList;
		}

		@Override
		protected void onCancelled() {
			mIsCanceled = true;
		}

		@Override
		protected void onPostExecute(ArrayList<CallStatsCoordinatesData> _retList) {
			super.onPostExecute(_retList);

			SVUtil.hideProgressDialog();

			try {
				if (!mIsCanceled && !mIsFinishFragment) {
					makePolygon(_retList);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void makePolygon(ArrayList<CallStatsCoordinatesData> _coordinatesDataList) {
		try {
			com.vividsolutions.jts.geom.Polygon jtsPolygon;
			Polygon googlePolygon;

			int listSize = _coordinatesDataList.size();
			for(int i = 0; i < listSize; i++) {
				if(mIsFinishFragment) {
					return;
				}

				CallStatsCoordinatesData callStatsCoordinatesData = _coordinatesDataList.get(i);

				jtsPolygon = makeJTSPolygons(callStatsCoordinatesData.getmCoordinatesJTS());
				googlePolygon = makeGooglePolygon(callStatsCoordinatesData.getmCoordinatesGoogle());

				JSONObject polygons = new JSONObject();
				polygons.put("name", callStatsCoordinatesData.getName());
				polygons.put("jtsPolygon", jtsPolygon);
				polygons.put("googlePolygon", googlePolygon);
				polygons.put("geoType", callStatsCoordinatesData.getGeoType());

				mPolygonList.put(polygons);
			}
		} catch(JSONException e) {
			e.printStackTrace();
		} finally {
			drawPolygonWithHistoryData();
		}
	}

	private com.vividsolutions.jts.geom.Polygon makeJTSPolygons(Coordinate[] _coordinates) {
		try {
			GeometryFactory fact = new GeometryFactory();
			LinearRing linear = new GeometryFactory().createLinearRing(_coordinates);

			return new com.vividsolutions.jts.geom.Polygon(linear, null, fact);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private Polygon makeGooglePolygon(ArrayList<LatLng> _coordinates) {
		try {
			PolygonOptions options = new PolygonOptions();
			options.addAll(_coordinates);
			options.strokeColor(Color.parseColor("#0D83B5"));	//Set the rectangle's stroke color to red
			options.strokeWidth(3);

			Polygon poly = mGoogleMap.addPolygon(options);	//Get back the mutable Polygon
			poly.setVisible(false);

			return poly;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private void getCallHistoryData() {
		SVUtil.showProgressDialog(mMainActivity, "로딩중...");

		AsyncHttpClient asyncHttpClient = NetClient.getAsyncHttpClient();
		final String url = Global.URL_CALL_HISTORY + "?selectedType=" + mSelectedType;

		asyncHttpClient.get(url, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject responseBody) {
				try {
					if (mIsFinishFragment) {
						return;
					}

					if (responseBody != null) {
						int totalCount = responseBody.getInt("total_count");
						mGuHistoryData = new JSONObject();
						mDongHistoryData = new JSONObject();

						if (totalCount > 0) {
							mGuHistoryData = responseBody.getJSONObject("gu_data");
							mDongHistoryData = responseBody.getJSONObject("dong_data");
						}
					}
				} catch (Exception e) {
					SVUtil.hideProgressDialog();
					e.printStackTrace();
				} finally {
					if (mGetCoordinatesDataTask != null) {
						mGetCoordinatesDataTask.cancel(false);
					}

					mGetCoordinatesDataTask = new GetCoordinatesData("", 1, 0);
					mGetCoordinatesDataTask.execute();

					makeCallCountList(mDongHistoryData);
				}
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, Throwable error, JSONObject responseBody) {
				SVUtil.log("error", TAG, "ERROR URL : " + url);

				SVUtil.hideProgressDialog();
				error.printStackTrace();

				if (error instanceof java.net.SocketTimeoutException) {
					SVUtil.showTimeoutDialog(mMainActivity);
				}
			}
		});
	}

	private void drawPolygonWithHistoryData() {
		try {
			setInvisiblePolygons();
			setVisiblePolygons();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setInvisiblePolygons() {
		try {
			removeMarkers();

			int length = mPolygonList.length();
			for(int i = 0; i < length; i++) {
				JSONObject polyObject = mPolygonList.getJSONObject(i);
				Polygon _p = (Polygon)polyObject.opt("polygons");
				if(_p != null) {
					_p.setVisible(false);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private void setVisiblePolygons() {
		try {
			JSONObject historyData;
			Polygon googlePolygon;
			JSONObject polyObject;
			com.vividsolutions.jts.geom.Polygon jtsPolygon;

			int length = mPolygonList.length();
			for(int i = 0; i < length; i++) {
				polyObject = mPolygonList.getJSONObject(i);
				jtsPolygon = (com.vividsolutions.jts.geom.Polygon)polyObject.get("jtsPolygon");
				googlePolygon = (Polygon)polyObject.get("googlePolygon");

				if(checkContains(jtsPolygon)) {
					String name = polyObject.getString("name");
					LatLng center = new LatLng(jtsPolygon.getCentroid().getY(), jtsPolygon.getCentroid().getX());
					int geoType = polyObject.getInt("geoType");
					if(geoType == 1) {
						historyData = mDongHistoryData.optJSONObject(name);
					} else {
						historyData =  mGuHistoryData.optJSONObject(name);
					}

					int amount = 0;
					if(historyData != null) {
						amount = historyData.optInt("call_count", 0);
					}

					String fillColor = getFillColor(amount, geoType, mIsUseAlpha);
					googlePolygon.setFillColor(Color.parseColor(fillColor));
					googlePolygon.setVisible(true);
					String markerName;
					if(mIsShowNumberOnly && geoType == 1) {
						markerName =  amount + "";
					} else {
						String[] tmpName = name.split("_");
						if(tmpName.length > 1) {
							name = tmpName[1];
						}

						markerName = name + "(" + amount + ")";
					}

					mMarkerList.add(addTextMarker(markerName, center));
				} else {
					googlePolygon.setVisible(false);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private Marker addTextMarker(String _markerName, LatLng _position) {
		try {
			Rect boundsText = new Rect();
			Paint paintText = new Paint();
			paintText.setColor(Color.WHITE);
			paintText.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.NORMAL));
			paintText.setTextAlign(Paint.Align.CENTER);
			paintText.setTextSize(30);
			paintText.setAntiAlias(true);

			paintText.getTextBounds(_markerName, 0, _markerName.length(), boundsText);
			Bitmap.Config conf = Bitmap.Config.ARGB_8888;
			Bitmap bmpText = Bitmap.createBitmap(boundsText.width(), boundsText.height() + 20, conf);

			Canvas canvasText = new Canvas(bmpText);
			canvasText.drawText(_markerName, canvasText.getWidth() / 2, canvasText.getHeight() - 5, paintText);

			return mGoogleMap.addMarker(new MarkerOptions().position(_position).icon(BitmapDescriptorFactory.fromBitmap(bmpText)));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private boolean checkContains(com.vividsolutions.jts.geom.Polygon _jtsPolygon) {
		try {
			return (mExtendedScreenPolygon.overlaps(_jtsPolygon) || mExtendedScreenPolygon.contains(_jtsPolygon) || _jtsPolygon.contains(mExtendedScreenPolygon));
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	private void removeMarkers() {
		try {
			if(mMarkerList != null) {
				for(int i = 0; i < mMarkerList.size(); i++) {
					mMarkerList.get(i).remove();
				}

				mMarkerList.clear();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String getFillColor(int _amount, int _geoType, boolean _isUseAlpha) {
		String colorValue = "000000";

		try {
			if(_geoType == 0) {
				return "#ABABAB";
			}

			if(_amount < 10) {
				colorValue = "000000";
			} else if(_amount >= 10 && _amount < 60) {
				colorValue = "662d91";
			} else if(_amount >= 60 && _amount < 110) {
				colorValue = "2e3192";
			} else if(_amount >= 110 && _amount < 200) {
				colorValue = "0054a6";
			} else if(_amount >= 200 && _amount < 300) {
				colorValue = "1d96d5";
			} else if(_amount >= 300 && _amount < 400) {
				colorValue = "00a651";
			} else if(_amount >= 400 && _amount < 500) {
				colorValue = "1dd545";
			} else if(_amount >= 500 && _amount < 600) {
				colorValue = "c5ea18";
			} else if(_amount >= 600 && _amount < 700) {
				colorValue = "f4e902";
			} else if(_amount >= 700 && _amount < 900) {
				colorValue = "ff9200";
			} else if(_amount >= 900) {
				colorValue = "ff331e";
			} else {
				colorValue = "000000";
			}

			if(_isUseAlpha) {
				colorValue = "#C0" + colorValue;
			} else {
				colorValue = "#" + colorValue;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return colorValue;
	}

	private String getClickedPolygonName(LatLng _clickedLatLng) {
		String clickedName = null;

		try {
			int length = mPolygonList.length();
			for(int i = 0; i < length; i++) {
				JSONObject polyObject = mPolygonList.getJSONObject(i);
				Polygon googlePolygon = (Polygon)polyObject.get("googlePolygon");
				int geoType = polyObject.getInt("geoType");
				com.vividsolutions.jts.geom.Polygon jtsPolygon = (com.vividsolutions.jts.geom.Polygon) polyObject.get("jtsPolygon");

				Coordinate[] coord = new Coordinate[] {
						new Coordinate(_clickedLatLng.longitude, _clickedLatLng.latitude)
				};

				Point compPoint = new Point(new CoordinateArraySequence(coord), new GeometryFactory());

				if(googlePolygon.isVisible() && geoType == 0 && jtsPolygon.contains(compPoint)) {
					clickedName = polyObject.getString("name");

					setZoomToFitBounds(googlePolygon);

					googlePolygon.remove();
					mPolygonList = removeJSONArray(mPolygonList, i);
					break;
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return clickedName;
	}

	private void setZoomToFitBounds(Polygon _polygon) {
		try {
			LatLngBounds.Builder builder = new LatLngBounds.Builder();
			List<LatLng> points = _polygon.getPoints();
			for(int i = 0; i < points.size(); i++) {
				builder.include(points.get(i));
			}

			LatLngBounds bounds = builder.build();

			mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 3));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private JSONArray removeJSONArray(JSONArray _orgArray, int _position) {
		JSONArray retArray = new JSONArray();

		try {
			for(int i = 0; i < _orgArray.length(); i++) {
				if(i != _position) {
					retArray.put(_orgArray.get(i));
				}
			}
		} catch(JSONException e) {
			e.printStackTrace();
		}

		return retArray;
	}

	private void changeClickedPolygon(LatLng _clickedLatLng) {
		try {
			String clickedName = getClickedPolygonName(_clickedLatLng);
			if(clickedName != null) {
				if (mGetCoordinatesDataTask != null) {
					mGetCoordinatesDataTask.cancel(false);
				}

				mGetCoordinatesDataTask = new GetCoordinatesData(clickedName, 0, 1);
				mGetCoordinatesDataTask.execute();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setEventListener() {
		mBtnZoomOut.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					mGoogleMap.animateCamera(CameraUpdateFactory.zoomOut());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		mBtnZoomIn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					mGoogleMap.animateCamera(CameraUpdateFactory.zoomIn());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		mBtnCallStatsCallList.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					mBtnCallStatsAroundMap.setVisibility(View.VISIBLE);
					mBtnCallStatsCallList.setVisibility(View.GONE);
					mCallStatsListLayout.setVisibility(View.VISIBLE);
					mSpinner.setVisibility(View.GONE);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		mBtnCallStatsAroundMap.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					mCallStatsAdapter.notifyDataSetChanged();
					mBtnCallStatsAroundMap.setVisibility(View.GONE);
					mBtnCallStatsCallList.setVisibility(View.VISIBLE);
					mCallStatsListLayout.setVisibility(View.GONE);
					mSpinner.setVisibility(View.VISIBLE);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				try {
					mSelectedType = position;
					getCallHistoryData();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {}
		});

		mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
			@Override
			public void onMapClick(LatLng latLng) {
				changeClickedPolygon(latLng);
			}
		});

		mGoogleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
			@Override
			public void onCameraChange(CameraPosition cameraPosition) {
				try {
					if (mGoogleMap == null) {
						return;
					}

					float zoom = mGoogleMap.getCameraPosition().zoom;

					mCurrentScreen = mGoogleMap.getProjection().getVisibleRegion().latLngBounds;

					createExtendedScreen();

//				SVUtil.log("error", TAG, "zoom : " + cameraPosition.zoom);

					mIsShowNumberOnly = false;
					mIsUseAlpha = false;
					if(zoom < 12) {
						// 숫자만 표시
						mIsShowNumberOnly = true;
					} else if(zoom > 13) {
						// 폴리건 반투명
						mIsUseAlpha = true;
					}

					drawPolygonWithHistoryData();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
			@Override
			public boolean onMarkerClick(Marker marker) {
				try {
					changeClickedPolygon(marker.getPosition());
				} catch (Exception e) {
					e.printStackTrace();
				}

				return false;
			}
		});
	}

	private void createExtendedScreen() {
		try {
			Coordinate[] jtsCoordinates = new Coordinate[5];
			LatLng center = mCurrentScreen.getCenter();

			LatLngBounds extendsScreen = new LatLngBounds(
					new LatLng(mCurrentScreen.southwest.latitude + (mCurrentScreen.southwest.latitude - center.latitude),
							mCurrentScreen.southwest.longitude + (mCurrentScreen.southwest.longitude - center.longitude)),
					new LatLng(mCurrentScreen.northeast.latitude + (mCurrentScreen.northeast.latitude - center.latitude),
							mCurrentScreen.northeast.longitude + (mCurrentScreen.northeast.longitude - center.longitude))
			);

			jtsCoordinates[0] = new Coordinate(extendsScreen.northeast.longitude, extendsScreen.northeast.latitude);
			jtsCoordinates[1] = new Coordinate(extendsScreen.southwest.longitude, extendsScreen.northeast.latitude);
			jtsCoordinates[2] = new Coordinate(extendsScreen.southwest.longitude, extendsScreen.southwest.latitude);
			jtsCoordinates[3] = new Coordinate(extendsScreen.northeast.longitude, extendsScreen.southwest.latitude);
			jtsCoordinates[4] = new Coordinate(extendsScreen.northeast.longitude, extendsScreen.northeast.latitude);

			mExtendedScreenPolygon = makeJTSPolygons(jtsCoordinates);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void init() {
		try {
			mMarkerList = new ArrayList<>();
			mPolygonList = new JSONArray();
			mSelectedType = 0;
			mCallStatsList = new ArrayList<>();
			mIsShowNumberOnly = false;
			mIsUseAlpha = false;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private class ViewHolder {
		public TextView mTextViewColor;
		public TextView mTextViewOrder;
		public TextView mTextViewLocation;
		public TextView mTextViewCount;
		public LinearLayout mLayoutContent;
	}

	private class ViewHolder_header {
		TextView mTextViewTitle;
	}

	private class CallStatsAdapter extends ArrayAdapter<CallStatsInfoData> implements StickyListHeadersAdapter {
		ArrayList<CallStatsInfoData> mCallStatsInfoList;
		private int mRowId;
		private ViewHolder mHolder;

		public CallStatsAdapter(Context _context, int _rowId, ArrayList<CallStatsInfoData> _callStatsInfoList) {
			super(_context, _rowId, _callStatsInfoList);
			this.mCallStatsInfoList = _callStatsInfoList;
			this.mRowId = _rowId;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;

			try {
				if (v == null) {
					LayoutInflater li = (LayoutInflater) mMainActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					v = li.inflate(mRowId, null);
					mHolder = new ViewHolder();
					mHolder.mLayoutContent = (LinearLayout) v.findViewById(R.id.row_call_count_contentLL);
					mHolder.mTextViewColor = (TextView) v.findViewById(R.id.row_call_count_colorTV);
					mHolder.mTextViewOrder = (TextView) v.findViewById(R.id.row_call_count_orderTV);
					mHolder.mTextViewLocation = (TextView) v.findViewById(R.id.row_call_count_locationTV);
					mHolder.mTextViewCount = (TextView) v.findViewById(R.id.row_call_count_countTV);

					v.setTag(mHolder);
				} else {
					mHolder = (ViewHolder) v.getTag();
				}

				CallStatsInfoData callStatsbox = mCallStatsInfoList.get(position);

				String rank = (position + 1) + "위";
				String location = callStatsbox.getmSiName() + " " + callStatsbox.getmGuName() + " " + callStatsbox.getmDongName();
				String callCount = callStatsbox.getmCallCount() + "";

				mHolder.mTextViewOrder.setText(rank);
				mHolder.mTextViewLocation.setText(location);
				mHolder.mTextViewCount.setText(callCount);

				String fillColor = getFillColor(callStatsbox.getmCallCount(), 1, false);
				mHolder.mTextViewColor.setBackgroundColor(Color.parseColor(fillColor));
			} catch (Exception e) {
				e.printStackTrace();
			}

			return v;
		}

		@Override
		public View getHeaderView(int position, View view, ViewGroup viewGroup) {
			try {
				ViewHolder_header holder;
				if (view == null) {
					holder = new ViewHolder_header();
					view = mMainActivity.getLayoutInflater().inflate(R.layout.row_header, viewGroup, false);
					holder.mTextViewTitle = (TextView) view.findViewById(R.id.row_header_titleTV);
					view.setTag(holder);
				} else {
					holder = (ViewHolder_header) view.getTag();
				}

				long headerId = getHeaderId(position);
				String title = ((headerId * 50) + 1) + "위 ~ " + ((headerId * 50) + 50) + "위";
				holder.mTextViewTitle.setText(title);
			} catch (Exception e) {
				e.printStackTrace();
			}

			return view;
		}

		@Override
		public long getHeaderId(int position) {
			return (long) (position / 50);
		}
	}

	private final Comparator<CallStatsInfoData> callCountSort = new Comparator<CallStatsInfoData>() {
		@Override
		public int compare(CallStatsInfoData box, CallStatsInfoData box2) {
			return (box2.getmCallCount() - box.getmCallCount());
		}
	};

	private void makeCallCountList(JSONObject _data) {
		try {
			mCallStatsList.clear();

			Iterator<String> iter = _data.keys();
			CallStatsInfoData callStatsInfoBox;
			while (iter.hasNext()) {
				String key = iter.next();

				JSONObject objData = _data.getJSONObject(key);
				String si_name = objData.optString("si_name");
				String gu_name = objData.optString("gu_name");
				String dong_name = objData.optString("dong_name");
				int call_count = objData.optInt("call_count");

				callStatsInfoBox = new CallStatsInfoData(si_name, gu_name, dong_name, call_count);
				mCallStatsList.add(callStatsInfoBox);
			}

			Collections.sort(mCallStatsList, callCountSort);
		} catch(JSONException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onStart() {
		super.onStart();

		mIsFinishFragment = false;
	}

	@Override
	public void onResume() {
		super.onResume();

		try {
			mMainActivity.changeButtonImage(mMainActivity.mImgBtnCallStats);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onPause() {
		super.onPause();

		try {
			mIsFinishFragment = true;

			if(mGetCoordinatesDataTask != null) {
				mGetCoordinatesDataTask.cancel(false);
			}

			SVUtil.hideProgressDialog();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onStop() {
		super.onStop();
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();

		try {
			if(mGoogleMap != null) {
				mGoogleMap.clear();
				mGoogleMap = null;
			}

			SupportMapFragment f = (SupportMapFragment) MainActivity.mSupportFragmentManager.findFragmentById(R.id.call_stats_map);
			if(f != null) {
				MainActivity.mSupportFragmentManager.beginTransaction().remove(f).commitAllowingStateLoss();
			}

			if(mView != null){
				ViewGroup parent = (ViewGroup) mView.getParent();
				if(parent!=null){
					parent.removeView(mView);
				}

				mView = null;
			}

			mMainActivity.mFragmentCallStats = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		setTargetFragment(null, -1);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onDetach() {
		super.onDetach();
	}
}
