package kr.moonlightdriver.driver.Network.NetData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by youngmin on 2015-10-28.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CarpoolData {
	@JsonProperty("taxiId")
	private String mTaxiId;

	@JsonProperty("driverId")
	private String mDriverId;

	@JsonProperty("isValid")
	private boolean mIsValid;

	@JsonProperty("isCatch")
	private boolean mIsCatch;

	@JsonProperty("isPush")
	private boolean mIsPush;

	@JsonProperty("money")
	private int mPrice;

	@JsonProperty("host")
	private String mRegistrantPhone;

	@JsonProperty("time")
	private Long mDepartureTime;

	@JsonProperty("divideN")
	private boolean mDivide;

	@JsonProperty("drivers")
	private DriverListData mDriverListData;

	@JsonProperty("end")
	private CarpoolLocationData mEndLocation;

	@JsonProperty("start")
	private CarpoolLocationData mStartLocation;

	@JsonProperty("distance")
	private double mDistance;

	@JsonProperty("registrant")
	private String mRegistrant;

	public CarpoolData() {}

	public CarpoolData(Long mDepartureTime, double mDistance, boolean mDivide, String mDriverId, DriverListData mDriverListData, CarpoolLocationData mEndLocation, boolean mIsCatch, boolean mIsPush, boolean mIsValid, int mPrice, String mRegistrant, String mRegistrantPhone, CarpoolLocationData mStartLocation, String mTaxiId) {
		this.mDepartureTime = mDepartureTime;
		this.mDistance = mDistance;
		this.mDivide = mDivide;
		this.mDriverId = mDriverId;
		this.mDriverListData = mDriverListData;
		this.mEndLocation = mEndLocation;
		this.mIsCatch = mIsCatch;
		this.mIsPush = mIsPush;
		this.mIsValid = mIsValid;
		this.mPrice = mPrice;
		this.mRegistrant = mRegistrant;
		this.mRegistrantPhone = mRegistrantPhone;
		this.mStartLocation = mStartLocation;
		this.mTaxiId = mTaxiId;
	}

	public Long getmDepartureTime() {
		return mDepartureTime;
	}

	public void setmDepartureTime(Long mDepartureTime) {
		this.mDepartureTime = mDepartureTime;
	}

	public double getmDistance() {
		return mDistance;
	}

	public void setmDistance(double mDistance) {
		this.mDistance = mDistance;
	}

	public boolean ismDivide() {
		return mDivide;
	}

	public void setmDivide(boolean mDivide) {
		this.mDivide = mDivide;
	}

	public String getmDriverId() {
		return mDriverId;
	}

	public void setmDriverId(String mDriverId) {
		this.mDriverId = mDriverId;
	}

	public DriverListData getmDriverListData() {
		return mDriverListData;
	}

	public void setmDriverListData(DriverListData mDriverListData) {
		this.mDriverListData = mDriverListData;
	}

	public CarpoolLocationData getmEndLocation() {
		return mEndLocation;
	}

	public void setmEndLocation(CarpoolLocationData mEndLocation) {
		this.mEndLocation = mEndLocation;
	}

	public boolean ismIsCatch() {
		return mIsCatch;
	}

	public void setmIsCatch(boolean mIsCatch) {
		this.mIsCatch = mIsCatch;
	}

	public boolean ismIsPush() {
		return mIsPush;
	}

	public void setmIsPush(boolean mIsPush) {
		this.mIsPush = mIsPush;
	}

	public boolean ismIsValid() {
		return mIsValid;
	}

	public void setmIsValid(boolean mIsValid) {
		this.mIsValid = mIsValid;
	}

	public int getmPrice() {
		return mPrice;
	}

	public void setmPrice(int mPrice) {
		this.mPrice = mPrice;
	}

	public String getmRegistrant() {
		return mRegistrant;
	}

	public void setmRegistrant(String mRegistrant) {
		this.mRegistrant = mRegistrant;
	}

	public String getmRegistrantPhone() {
		return mRegistrantPhone;
	}

	public void setmRegistrantPhone(String mRegistrantPhone) {
		this.mRegistrantPhone = mRegistrantPhone;
	}

	public CarpoolLocationData getmStartLocation() {
		return mStartLocation;
	}

	public void setmStartLocation(CarpoolLocationData mStartLocation) {
		this.mStartLocation = mStartLocation;
	}

	public String getmTaxiId() {
		return mTaxiId;
	}

	public void setmTaxiId(String mTaxiId) {
		this.mTaxiId = mTaxiId;
	}
}
