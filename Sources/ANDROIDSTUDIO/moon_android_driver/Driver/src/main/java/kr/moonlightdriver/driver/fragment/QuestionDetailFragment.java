package kr.moonlightdriver.driver.fragment;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import kr.moonlightdriver.driver.Network.NetApi.NetAPI;
import kr.moonlightdriver.driver.Network.NetApi.NetResponse;
import kr.moonlightdriver.driver.Network.NetApi.NetResponseCallback;
import kr.moonlightdriver.driver.Network.NetApi.QuestionDetailAck;
import kr.moonlightdriver.driver.Network.NetClient.NetClient;
import kr.moonlightdriver.driver.Network.NetData.QuestionAnswerData;
import kr.moonlightdriver.driver.Network.NetData.QuestionData;
import kr.moonlightdriver.driver.Network.NetData.ResultData;
import kr.moonlightdriver.driver.R;
import kr.moonlightdriver.driver.activity.MainActivity;
import kr.moonlightdriver.driver.utils.Global;
import kr.moonlightdriver.driver.utils.SVUtil;

public class QuestionDetailFragment extends Fragment {
	private static final String TAG = "QuestionDetailFragment";
	private static final int LIST_COUNT_PER_PAGE = 1;

	private TextView mTitle;
	private TextView mContents;
	private TextView mAnswerCount;
	private QuestionAnswerAdapter mQuestionAnswerAdapter;
	private ArrayList<QuestionAnswerData> mQuestionAnswerList;
	private ArrayList<QuestionAnswerData> mQuestionShowAnswerList;
	private ImageButton mBtnPrev;
	private ImageButton mBtnNext;
	private TextView mPageText;

	private int mTotalPageCount;
	private int mCurrentPage;
	private boolean mIsFinishFragment;
	public String mQuestionId;

	private View mView;
	private MainActivity mMainActivity;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		try {
			mView = inflater.inflate(R.layout.frag_question_detail, container, false);

			mMainActivity = (MainActivity) getActivity();

			Bundle bundle = QuestionDetailFragment.this.getArguments();
			mQuestionId = bundle.getString("questionId", "");

			mIsFinishFragment = false;
			mTotalPageCount = 0;
			mCurrentPage = 0;
			mQuestionAnswerList = new ArrayList<>();
			mQuestionShowAnswerList = new ArrayList<>();

			ListView questionAnswerListView = (ListView) mView.findViewById(R.id.question_detail_answer_list);
			ImageButton btnBack = (ImageButton) mView.findViewById(R.id.question_detail_btn_back);

			View header = inflater.inflate(R.layout.frag_question_detail_header, null, false);
			View footer = inflater.inflate(R.layout.frag_question_detail_footer, null, false);

			questionAnswerListView.addHeaderView(header);
			questionAnswerListView.addFooterView(footer);

			mQuestionAnswerAdapter = new QuestionAnswerAdapter(mMainActivity, R.layout.row_question_answer_list, mQuestionShowAnswerList);
			questionAnswerListView.setAdapter(mQuestionAnswerAdapter);

			mTitle = (TextView) header.findViewById(R.id.question_detail_title);
			mContents = (TextView) header.findViewById(R.id.question_detail_contents);
			mAnswerCount = (TextView) header.findViewById(R.id.question_detail_answer_count);

			mBtnPrev = (ImageButton) footer.findViewById(R.id.btn_question_detail_answer_prev);
			mBtnNext = (ImageButton) footer.findViewById(R.id.btn_question_detail_answer_next);
			mPageText = (TextView) footer.findViewById(R.id.question_detail_page);

			mBtnPrev.setSelected(false);
			mBtnPrev.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (mBtnPrev.isSelected()) {
						mCurrentPage--;
						showCommentList();
					}
				}
			});

			mBtnNext.setSelected(false);
			mBtnNext.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (mBtnNext.isSelected()) {
						mCurrentPage++;
						showCommentList();
					}
				}
			});

			btnBack.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					moveToQuestionFragment();
				}
			});

			getQuestionDetailInfo(mQuestionId);
		} catch(InflateException e) {
			e.printStackTrace();
		}

		return mView;
	}

	private void moveToQuestionFragment() {
		try {
			mMainActivity.mFragmentQuestion = new QuestionFragment();
			mMainActivity.replaceFragment(mMainActivity.mFragmentQuestion, R.id.mainFrameLayout, Global.FRAG_QUESTION_TAG, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void checkPageButtonEnable() {
		try {
			mBtnNext.setSelected(true);
			mBtnPrev.setSelected(true);

			if(mCurrentPage + 1 == mTotalPageCount) {
				mBtnNext.setSelected(false);
			}

			if(mCurrentPage == 0) {
				mBtnPrev.setSelected(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void getQuestionDetailInfo(String _questionId) {
		SVUtil.showProgressDialog(mMainActivity, "로딩중...");

		String url = Global.URL_QUESTION_DETAIL.replaceAll(":questionId", _questionId);
		NetClient.send(mMainActivity, url, "GET", null, new QuestionDetailAck(), new NetResponseCallback(new NetResponse() {
			@Override
			public void onResponse(NetAPI _netAPI) {
				SVUtil.hideProgressDialog();

				try {
					if (_netAPI != null && !mIsFinishFragment) {
						QuestionDetailAck retNetApi = (QuestionDetailAck) _netAPI;
						ResultData result = retNetApi.getmResultData();

						if (result.getmCode() != 100) {
							SVUtil.showSimpleDialog(mMainActivity, result.getmDetail());
						} else {
							QuestionData retQuestionData = retNetApi.getmQuestionDetailData();
							ArrayList<QuestionAnswerData> questionAnswerDataList = retNetApi.getmQuestionAnswerDataList();

							String answerCount = "답변(" + retQuestionData.getmAnswerCount() + ")";

							mTitle.setText(retQuestionData.getmTitle());
							mContents.setText(retQuestionData.getmContents());
							mAnswerCount.setText(answerCount);

							int listSize = questionAnswerDataList.size();
							for (int i = 0; i < listSize; i++) {
								mQuestionAnswerList.add(questionAnswerDataList.get(i));
							}

							showCommentList();
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}));
	}

	private void calculatePageCount() {
		try {
			int val = mQuestionAnswerList.size() % LIST_COUNT_PER_PAGE;
			val = val == 0 ? 0 : 1;
			mTotalPageCount = (mQuestionAnswerList.size() / LIST_COUNT_PER_PAGE) + val;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void showCommentList() {
		try {
			calculatePageCount();

			String page = "Page " + (mCurrentPage + 1) + " of " + mTotalPageCount;
			mPageText.setText(page);

			mQuestionShowAnswerList.clear();

			int start = mCurrentPage * LIST_COUNT_PER_PAGE;
			int commentListSize = mQuestionAnswerList.size();
			for(int i = start; i < (start + LIST_COUNT_PER_PAGE); i++) {
				if(i < commentListSize) {
					mQuestionShowAnswerList.add(mQuestionAnswerList.get(i));
				} else {
					break;
				}
			}

			mQuestionAnswerAdapter.notifyDataSetChanged();

			checkPageButtonEnable();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private class ViewHolderQuestionAnswer {
		public TextView mTextViewAnswer;
		public TextView mTextViewDate;
	}

	private class QuestionAnswerAdapter extends ArrayAdapter<QuestionAnswerData> {
		ArrayList<QuestionAnswerData> mQuestionAnswerList;
		private int mRowId;
		private ViewHolderQuestionAnswer mHolder;

		public QuestionAnswerAdapter(Context _context, int _rowId, ArrayList<QuestionAnswerData> _questionAnswerList) {
			super(_context, _rowId, _questionAnswerList);
			this.mQuestionAnswerList = _questionAnswerList;
			this.mRowId = _rowId;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;

			try {
				if (v == null) {
					LayoutInflater li = (LayoutInflater) mMainActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					v = li.inflate(mRowId, null);
					mHolder = new ViewHolderQuestionAnswer();
					mHolder.mTextViewAnswer = (TextView) v.findViewById(R.id.row_question_answer_contents);
					mHolder.mTextViewDate = (TextView) v.findViewById(R.id.row_question_answer_date);

					v.setTag(mHolder);
				} else {
					mHolder = (ViewHolderQuestionAnswer) v.getTag();
				}

				QuestionAnswerData questionAnswerData = mQuestionAnswerList.get(position);

				String answerDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(questionAnswerData.getmCreatedDate()));

				mHolder.mTextViewDate.setText(answerDate);
				mHolder.mTextViewAnswer.setText(questionAnswerData.getmAnswer());
			} catch (Exception e) {
				e.printStackTrace();
			}

			return v;
		}
	}

	@Override
	public void onStart() {
		super.onStart();

		mIsFinishFragment = false;
	}

	@Override
	public void onResume() {
		super.onResume();

		try {
			mMainActivity.changeButtonImage(mMainActivity.mImgBtnQuestion);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onPause() {
		super.onPause();

		mIsFinishFragment = true;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();

		try {
			if(mView != null){
				ViewGroup parent = (ViewGroup) mView.getParent();
				if(parent!=null){
					parent.removeView(mView);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		setTargetFragment(null, -1);
	}
}
