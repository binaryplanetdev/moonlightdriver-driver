package kr.moonlightdriver.driver.Network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

import kr.moonlightdriver.driver.Network.NetData.DriverPointData;
import kr.moonlightdriver.driver.Network.NetData.ResultData;

/**
 * Created by youngmin on 2015-10-13.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class NearDriverListAck extends NetAPI {
	@JsonProperty("result")
	private ResultData mResultData;
	@JsonProperty("driver_list")
	private ArrayList<DriverPointData> mDriverPointData;

	public NearDriverListAck() {}

	public NearDriverListAck(ArrayList<DriverPointData> mDriverPointData, ResultData mResultData) {
		this.mDriverPointData = mDriverPointData;
		this.mResultData = mResultData;
	}

	public ArrayList<DriverPointData> getmDriverPointData() {
		return mDriverPointData;
	}

	public void setmDriverPointData(ArrayList<DriverPointData> mDriverPointData) {
		this.mDriverPointData = mDriverPointData;
	}

	public ResultData getmResultData() {
		return mResultData;
	}

	public void setmResultData(ResultData mResultData) {
		this.mResultData = mResultData;
	}
}
