package kr.moonlightdriver.driver.Network.NetData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by youngmin on 2015-10-30.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DriverPointData {
	@JsonProperty("driverId")
	private String mDriverId;

	@JsonProperty("phone")
	private String mPhoneNumber;

	@JsonProperty("date")
	private long mDate;

	@JsonProperty("gcmId")
	private String mGcmId;

	@JsonProperty("location")
	private ShuttleLocationData mLocation;

	public DriverPointData() {}

	public DriverPointData(long mDate, String mDriverId, String mGcmId, ShuttleLocationData mLocation, String mPhoneNumber) {
		this.mDate = mDate;
		this.mDriverId = mDriverId;
		this.mGcmId = mGcmId;
		this.mLocation = mLocation;
		this.mPhoneNumber = mPhoneNumber;
	}

	public long getmDate() {
		return mDate;
	}

	public void setmDate(long mDate) {
		this.mDate = mDate;
	}

	public String getmDriverId() {
		return mDriverId;
	}

	public void setmDriverId(String mDriverId) {
		this.mDriverId = mDriverId;
	}

	public String getmGcmId() {
		return mGcmId;
	}

	public void setmGcmId(String mGcmId) {
		this.mGcmId = mGcmId;
	}

	public ShuttleLocationData getmLocation() {
		return mLocation;
	}

	public void setmLocation(ShuttleLocationData mLocation) {
		this.mLocation = mLocation;
	}

	public String getmPhoneNumber() {
		return mPhoneNumber;
	}

	public void setmPhoneNumber(String mPhoneNumber) {
		this.mPhoneNumber = mPhoneNumber;
	}
}
