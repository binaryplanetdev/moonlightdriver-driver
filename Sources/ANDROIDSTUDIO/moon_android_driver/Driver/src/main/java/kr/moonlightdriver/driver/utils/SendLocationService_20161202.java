package kr.moonlightdriver.driver.utils;

/**
 * Created by youngmin on 2016-09-02.
 */
public class SendLocationService_20161202 {

}
//public class SendLocationService_20161202 extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
//	private static final String TAG = "SendLocationService";
//	public static final String ACTION = "CHANGE_LOCATION";
//	public static final String CHECK_PERMISSION_REQ = "CHECK_PERMISSION_REQ";
//	private String mPhoneNumber = null;
//	private String mDriverId = "";
//	private LatLng mLastUpdateLocation;
//	private double mLastUpdateLocationTime = 0;
//	private Context mContext;
//
//	private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10 * 1000;
//	private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;
//	private static final float SMALLEST_DISPLACEMENT_IN_METERS = 0f;
//
//	private GoogleApiClient mGoogleApiClient;
//	private LocationRequest mLocationRequest;
//
//	private boolean mIsCheckGpsUpdateTime;
//
//	@Override
//	public void onConnected(@Nullable Bundle bundle) {
//		try {
//			if (checkLocationPermission()) {
//				Location currentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
//
//				if(currentLocation != null) {
//					sendDataToMain(currentLocation.getLatitude(), currentLocation.getLongitude(), currentLocation.getAltitude());
//				}
//			}
//
//			startLocationUpdates();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	@Override
//	public void onConnectionSuspended(int i) {
//		mGoogleApiClient.connect();
//	}
//
//	@Override
//	public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
//		SVUtil.log("error", TAG, "Connection failed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
//	}
//
//	@Override
//	public void onLocationChanged(Location location) {
//		try {
////			SVUtil.log("error", TAG, "onLocationChanged lat: " + location.getLatitude() + ", lng : " + location.getLongitude());
//
//			sendDataToMain(location.getLatitude(), location.getLongitude(), location.getAltitude());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	private void setLocationUpdateTime(LatLng _newLocation) {
//		try {
//			if(mLastUpdateLocation != null && mLastUpdateLocationTime >= 0) {
//				Calendar calendar = Calendar.getInstance();
//				double nowTime = calendar.getTimeInMillis();
//				double diffTimeInSeconds = ((nowTime - mLastUpdateLocationTime) / 1000d) / 3600d;
//
//				float[] dist = new float[3];
//				Location.distanceBetween(mLastUpdateLocation.latitude, mLastUpdateLocation.longitude, _newLocation.latitude, _newLocation.longitude, dist);
//
//				double distance = dist[0] / 1000;
//
//				double kmPerHour =  distance / diffTimeInSeconds;
//
//				long uploadTime = SVUtil.getLocationUpdateTime(kmPerHour);
//
////				SVUtil.log("error", TAG, "distance : " + distance + ", diffTimeInSeconds : " + diffTimeInSeconds + ", kmPerHour : " + kmPerHour + ", uploadTime : " + uploadTime + ", mIsCheckGpsUpdateTime : " + mIsCheckGpsUpdateTime);
//
//				if(uploadTime > 1000 && mIsCheckGpsUpdateTime) {
//					stopLocationUpdates();
//
//					new android.os.Handler().postDelayed(
//							new Runnable() {
//								public void run() {
//									startLocationUpdates();
//								}
//							}, uploadTime);
//				}
//			}
//
//			Calendar calendar1 = Calendar.getInstance();
//			mLastUpdateLocationTime = calendar1.getTimeInMillis();
//			mLastUpdateLocation = new LatLng(_newLocation.latitude, _newLocation.longitude);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	@Override
//	public IBinder onBind(Intent arg0) {
//		return null;
//	}
//
//	@Override
//	public int onStartCommand(Intent intent, int flags, int startId) {
//		super.onStartCommand(intent, flags, startId);
//
//		try {
//			mIsCheckGpsUpdateTime = true;
//			mContext = getApplicationContext();
//
//			if(intent == null) {
//				sendLogout();
//			} else {
//				mIsCheckGpsUpdateTime = false;
//
//				new android.os.Handler().postDelayed(
//					new Runnable() {
//						public void run() {
//							mIsCheckGpsUpdateTime = true;
//						}
//					}, 30000);
//			}
//
//			initializeLocationManager();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		return START_STICKY;
//	}
//
//	@Override
//	public void onCreate() {
//	}
//
//	private boolean checkLocationPermission() {
//		try {
//			if(Build.VERSION.SDK_INT >= 23) {
//				if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
//						|| ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//					return false;
//				}
//			}
//		} catch(Exception e) {
//			e.printStackTrace();
//			return false;
//		}
//
//		return true;
//	}
//
//	private boolean checkPhoneNumberPermission() {
//		try {
//			if(Build.VERSION.SDK_INT >= 23) {
//				if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED
//						|| ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED) {
//					return false;
//				}
//			}
//		} catch(Exception e) {
//			e.printStackTrace();
//			return false;
//		}
//
//		return true;
//	}
//
//	private String getDriverId() {
//		String driverId = "";
//
//		try {
//			SharedPreferences prefs = mContext.getSharedPreferences("moonlightdriver", Context.MODE_PRIVATE);
//			driverId = prefs.getString("driverId", "");
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		return driverId;
//	}
//
//	private void sendDataToMain(double _lat, double _lng, double _alt) {
//		try {
//			Intent intent = new Intent();
//			intent.setAction(SendLocationService_20161202.ACTION);
//			intent.putExtra("lat", _lat);
//			intent.putExtra("lng", _lng);
//			intent.putExtra("alt", _alt);
//
//			sendBroadcast(intent);
//
//			if(mDriverId.isEmpty()) {
//				mDriverId = getDriverId();
//			}
//
//			if(checkPhoneNumberPermission()) {
//				TelephonyManager tMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
//				mPhoneNumber = tMgr.getLine1Number();
//
//				if(!mDriverId.isEmpty() && mPhoneNumber != null && !mPhoneNumber.isEmpty()) {
//					updateCurrentLocation(new LatLng(_lat, _lng));
//				}
//			} else {
//				reqCheckPermissionToMain("phone_number_permission");
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	private void reqCheckPermissionToMain(String _permissionType) {
//		try {
//			Intent intent = new Intent();
//			intent.setAction(SendLocationService_20161202.CHECK_PERMISSION_REQ);
//			intent.putExtra("permissionType", _permissionType);
//
//			sendBroadcast(intent);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	private void updateCurrentLocation(LatLng _newLocation) {
//		try {
//			if(mLastUpdateLocation == null) {
//				updateDriverLocation(_newLocation);
//			} else {
//				float[] dist = new float[2];
//				Location.distanceBetween(mLastUpdateLocation.latitude, mLastUpdateLocation.longitude, _newLocation.latitude, _newLocation.longitude, dist);
//
////				if (dist[0] >= Global.UPDATE_DISTANCE) {
//				if (dist[0] >= 0) {
//					updateDriverLocation(_newLocation);
//				}
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	private void sendLogout() {
//		try {
//			if(mDriverId.isEmpty()) {
//				mDriverId = getDriverId();
//			}
//
//			if(mDriverId.isEmpty()) {
//				return;
//			}
//
//			RequestParams params = new RequestParams();
//			params.add("driverId", mDriverId);
//
//			NetClient.send(mContext, Global.URL_DRIVER_LOGOUT, "POST", params, new NoResponseDataAck(), new NetResponseCallback(new NetResponse() {
//				@Override
//				public void onResponse(NetAPI _netAPI) {
//				}
//			}));
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	private void updateDriverLocation(final LatLng _newLocation) {
//		try {
//			RequestParams params = new RequestParams();
//			params.add("driverId", mDriverId);
//			params.add("phone", mPhoneNumber);
//			params.add("lat", _newLocation.latitude + "");
//			params.add("lng", _newLocation.longitude + "");
//
//			NetClient.send(mContext, Global.URL_UPDATE_DRIVER_LOCATION, "POST", params, new NoResponseDataAck(), new NetResponseCallback(new NetResponse() {
//				@Override
//				public void onResponse(NetAPI _netAPI) {
//					try {
//						if (_netAPI != null) {
//							NoResponseDataAck retNetApi = (NoResponseDataAck) _netAPI;
//							ResultData result = retNetApi.getmResultData();
//
//							if (result.getmCode() == 100) {
//								setLocationUpdateTime(_newLocation);
//							} else {
//								SVUtil.log("error", TAG, result.getmDetail());
//							}
//						}
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//				}
//			}));
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	@Override
//	public void onDestroy() {
//		super.onDestroy();
//
//		try {
//			stopLocationUpdates();
//
//			mGoogleApiClient.disconnect();
//		} catch (Exception ex) {
//			ex.printStackTrace();
//		}
//	}
//
//	private void initializeLocationManager() {
//		try {
//			buildGoogleApiClient();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	private synchronized void buildGoogleApiClient() {
//		try {
//			mGoogleApiClient = new GoogleApiClient.Builder(mContext)
//					.addConnectionCallbacks(this)
//					.addOnConnectionFailedListener(this)
//					.addApi(LocationServices.API)
//					.build();
//
//			createLocationRequest();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	private void createLocationRequest() {
//		mLocationRequest = new LocationRequest();
//		mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
//		mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
//		mLocationRequest.setSmallestDisplacement(SMALLEST_DISPLACEMENT_IN_METERS);
//		mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
//
//		mGoogleApiClient.connect();
//	}
//
//	public void startLocationUpdates() {
//		try {
//			if(checkLocationPermission()) {
//				LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
//			} else {
//				reqCheckPermissionToMain("location_permission");
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	public void stopLocationUpdates() {
//		try {
//			LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
//			mContext.sendBroadcast(new Intent("com.skt.intent.action.GPS_TURN_OFF"));
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//}
