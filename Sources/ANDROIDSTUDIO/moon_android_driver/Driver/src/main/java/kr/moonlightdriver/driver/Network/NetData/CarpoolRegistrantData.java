package kr.moonlightdriver.driver.Network.NetData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by youngmin on 2015-10-30.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CarpoolRegistrantData {
	@JsonProperty("taxiId")
	private String mTaxiId;

	@JsonProperty("driverId")
	private String mDriverId;

	@JsonProperty("registrant")
	private String mRegistrant;

	@JsonProperty("location")
	private ShuttleLocationData mLocation;

	public CarpoolRegistrantData() {}

	public CarpoolRegistrantData(String mDriverId, ShuttleLocationData mLocation, String mRegistrant, String mTaxiId) {
		this.mDriverId = mDriverId;
		this.mLocation = mLocation;
		this.mRegistrant = mRegistrant;
		this.mTaxiId = mTaxiId;
	}

	public String getmDriverId() {
		return mDriverId;
	}

	public void setmDriverId(String mDriverId) {
		this.mDriverId = mDriverId;
	}

	public ShuttleLocationData getmLocation() {
		return mLocation;
	}

	public void setmLocation(ShuttleLocationData mLocation) {
		this.mLocation = mLocation;
	}

	public String getmRegistrant() {
		return mRegistrant;
	}

	public void setmRegistrant(String mRegistrant) {
		this.mRegistrant = mRegistrant;
	}

	public String getmTaxiId() {
		return mTaxiId;
	}

	public void setmTaxiId(String mTaxiId) {
		this.mTaxiId = mTaxiId;
	}
}
