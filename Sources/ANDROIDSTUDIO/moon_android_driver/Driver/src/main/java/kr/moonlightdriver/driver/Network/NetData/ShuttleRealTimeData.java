package kr.moonlightdriver.driver.Network.NetData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by youngmin on 2015-11-05.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShuttleRealTimeData {
	@JsonProperty("locations")
	private ShuttleLocationData mShuttleLocationData;
	@JsonProperty("categoryName")
	private String mRealTimeCategoryName;

	public ShuttleRealTimeData() {}

	public ShuttleRealTimeData(String mRealTimeCategoryName, ShuttleLocationData mShuttleLocationData) {
		this.mRealTimeCategoryName = mRealTimeCategoryName;
		this.mShuttleLocationData = mShuttleLocationData;
	}

	public String getmRealTimeCategoryName() {
		return mRealTimeCategoryName;
	}

	public void setmRealTimeCategoryName(String mRealTimeCategoryName) {
		this.mRealTimeCategoryName = mRealTimeCategoryName;
	}

	public ShuttleLocationData getmShuttleLocationData() {
		return mShuttleLocationData;
	}

	public void setmShuttleLocationData(ShuttleLocationData mShuttleLocationData) {
		this.mShuttleLocationData = mShuttleLocationData;
	}
}
