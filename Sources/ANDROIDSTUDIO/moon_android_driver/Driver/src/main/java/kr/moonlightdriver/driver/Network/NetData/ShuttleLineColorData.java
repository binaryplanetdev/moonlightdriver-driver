package kr.moonlightdriver.driver.Network.NetData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by youngmin on 2015-10-08.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShuttleLineColorData {
	@JsonProperty("name")
	private String mShuttleName;
	@JsonProperty("busId")
	private String mBusId;
	@JsonProperty("lineColor")
	private String mLineColor;
	@JsonProperty("firstTime")
	private String mFirstTime;
	@JsonProperty("lastTime")
	private String mLastTime;
	@JsonProperty("saturdayFirstTime")
	private String mSaturdayFirstTime;
	@JsonProperty("saturdayLastTime")
	private String mSaturdayLastTime;
	@JsonProperty("sundayFirstTime")
	private String mSundayFirstTime;
	@JsonProperty("sundayLastTime")
	private String mSundayLastTime;
	@JsonProperty("runFirstTime")
	private String mRunFirstTime;
	@JsonProperty("runLastTime")
	private String mRunLastTime;
	@JsonProperty("saturdayRunFirstTime")
	private String mSaturdayRunFirstTime;
	@JsonProperty("saturdayRunLastTime")
	private String mSaturdayRunLastTime;
	@JsonProperty("sundayRunFirstTime")
	private String mSundayRunFirstTime;
	@JsonProperty("sundayRunLastTime")
	private String mSundayRunLastTime;
	@JsonProperty("holidayDate")
	private String mHolidayDate;
	@JsonProperty("realTimeCategoryName")
	private String mRealTimeCategoryName;

	public ShuttleLineColorData() {}

	public ShuttleLineColorData(String mBusId, String mLineColor, String mShuttleName) {
		this.mBusId = mBusId;
		this.mLineColor = mLineColor;
		this.mShuttleName = mShuttleName;
	}

	public ShuttleLineColorData(String mBusId, String mFirstTime, String mHolidayDate, String mLastTime, String mLineColor, String mRealTimeCategoryName, String mRunFirstTime, String mRunLastTime, String mSaturdayFirstTime, String mSaturdayLastTime, String mSaturdayRunFirstTime, String mSaturdayRunLastTime, String mShuttleName, String mSundayFirstTime, String mSundayLastTime, String mSundayRunFirstTime, String mSundayRunLastTime) {
		this.mBusId = mBusId;
		this.mFirstTime = mFirstTime;
		this.mHolidayDate = mHolidayDate;
		this.mLastTime = mLastTime;
		this.mLineColor = mLineColor;
		this.mRealTimeCategoryName = mRealTimeCategoryName;
		this.mRunFirstTime = mRunFirstTime;
		this.mRunLastTime = mRunLastTime;
		this.mSaturdayFirstTime = mSaturdayFirstTime;
		this.mSaturdayLastTime = mSaturdayLastTime;
		this.mSaturdayRunFirstTime = mSaturdayRunFirstTime;
		this.mSaturdayRunLastTime = mSaturdayRunLastTime;
		this.mShuttleName = mShuttleName;
		this.mSundayFirstTime = mSundayFirstTime;
		this.mSundayLastTime = mSundayLastTime;
		this.mSundayRunFirstTime = mSundayRunFirstTime;
		this.mSundayRunLastTime = mSundayRunLastTime;
	}

	public String getmBusId() {
		return mBusId;
	}

	public void setmBusId(String mBusId) {
		this.mBusId = mBusId;
	}

	public String getmLineColor() {
		return mLineColor;
	}

	public void setmLineColor(String mLineColor) {
		this.mLineColor = mLineColor;
	}

	public String getmShuttleName() {
		return mShuttleName;
	}

	public void setmShuttleName(String mShuttleName) {
		this.mShuttleName = mShuttleName;
	}

	public String getmFirstTime() {
		return mFirstTime;
	}

	public void setmFirstTime(String mFirstTime) {
		this.mFirstTime = mFirstTime;
	}

	public String getmHolidayDate() {
		return mHolidayDate;
	}

	public void setmHolidayDate(String mHolidayDate) {
		this.mHolidayDate = mHolidayDate;
	}

	public String getmLastTime() {
		return mLastTime;
	}

	public void setmLastTime(String mLastTime) {
		this.mLastTime = mLastTime;
	}

	public String getmRunFirstTime() {
		return mRunFirstTime;
	}

	public void setmRunFirstTime(String mRunFirstTime) {
		this.mRunFirstTime = mRunFirstTime;
	}

	public String getmRunLastTime() {
		return mRunLastTime;
	}

	public void setmRunLastTime(String mRunLastTime) {
		this.mRunLastTime = mRunLastTime;
	}

	public String getmSaturdayFirstTime() {
		return mSaturdayFirstTime;
	}

	public void setmSaturdayFirstTime(String mSaturdayFirstTime) {
		this.mSaturdayFirstTime = mSaturdayFirstTime;
	}

	public String getmSaturdayLastTime() {
		return mSaturdayLastTime;
	}

	public void setmSaturdayLastTime(String mSaturdayLastTime) {
		this.mSaturdayLastTime = mSaturdayLastTime;
	}

	public String getmSaturdayRunFirstTime() {
		return mSaturdayRunFirstTime;
	}

	public void setmSaturdayRunFirstTime(String mSaturdayRunFirstTime) {
		this.mSaturdayRunFirstTime = mSaturdayRunFirstTime;
	}

	public String getmSaturdayRunLastTime() {
		return mSaturdayRunLastTime;
	}

	public void setmSaturdayRunLastTime(String mSaturdayRunLastTime) {
		this.mSaturdayRunLastTime = mSaturdayRunLastTime;
	}

	public String getmSundayFirstTime() {
		return mSundayFirstTime;
	}

	public void setmSundayFirstTime(String mSundayFirstTime) {
		this.mSundayFirstTime = mSundayFirstTime;
	}

	public String getmSundayLastTime() {
		return mSundayLastTime;
	}

	public void setmSundayLastTime(String mSundayLastTime) {
		this.mSundayLastTime = mSundayLastTime;
	}

	public String getmSundayRunFirstTime() {
		return mSundayRunFirstTime;
	}

	public void setmSundayRunFirstTime(String mSundayRunFirstTime) {
		this.mSundayRunFirstTime = mSundayRunFirstTime;
	}

	public String getmSundayRunLastTime() {
		return mSundayRunLastTime;
	}

	public void setmSundayRunLastTime(String mSundayRunLastTime) {
		this.mSundayRunLastTime = mSundayRunLastTime;
	}

	public String getmRealTimeCategoryName() {
		return mRealTimeCategoryName;
	}

	public void setmRealTimeCategoryName(String mRealTimeCategoryName) {
		this.mRealTimeCategoryName = mRealTimeCategoryName;
	}
}
