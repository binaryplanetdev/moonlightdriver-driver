package kr.moonlightdriver.driver.Network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import kr.moonlightdriver.driver.Network.NetData.ResultData;
import kr.moonlightdriver.driver.Network.NetData.ShuttlePathData;

/**
 * Created by youngmin on 2015-10-13.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShuttlePathAck extends NetAPI {
	@JsonProperty("result")
	private ResultData mResultData;
	@JsonProperty("shuttle")
	private ShuttlePathData mShuttlePathData;

	public ShuttlePathAck() {}

	public ShuttlePathAck(ResultData mResultData, ShuttlePathData mShuttlePathData) {
		this.mResultData = mResultData;
		this.mShuttlePathData = mShuttlePathData;
	}

	public ResultData getmResultData() {
		return mResultData;
	}

	public void setmResultData(ResultData mResultData) {
		this.mResultData = mResultData;
	}

	public ShuttlePathData getmShuttlePathData() {
		return mShuttlePathData;
	}

	public void setmShuttlePathData(ShuttlePathData mShuttlePathData) {
		this.mShuttlePathData = mShuttlePathData;
	}
}
