package kr.moonlightdriver.driver.utils;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.SupportMapFragment;

/**
 * Created by youngmin on 2015-12-06.
 */
public class TouchableSupportMapFragment extends SupportMapFragment {
	public View mOrgContentView;
	public TouchableWrapper mTouchableView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mOrgContentView = super.onCreateView(inflater, container, savedInstanceState);
		mTouchableView = new TouchableWrapper(getActivity());
		mTouchableView.addView(mOrgContentView);

		return mTouchableView;
	}

	@Nullable
	@Override
	public View getView() {
		return mOrgContentView;
	}
}
