package kr.moonlightdriver.driver.utils;

public class WebSocketUtil {
    public static String toJS(String str) {
        return str.replace("\\", "\\\\")
                .replace("\'", "\\\'")
                .replace("\"", "\\\"")
                .replace("\r\n", "\\n")
                .replace("\n", "\\n");
    }

    public static String toSt(String str) {
        return str.replace("\\\\", "\\")
                .replace("\\\'", "\'")
                .replace("\\\"", "\"")
                .replace("\\n", "\r\n")
                .replace("\\n", "\n");
    }
}