package kr.moonlightdriver.driver.Network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

import kr.moonlightdriver.driver.Network.NetData.ResultData;

/**
 * Created by youngmin on 2015-10-13.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShuttleCategoryListAck extends NetAPI {
	@JsonProperty("result")
	private ResultData mResultData;
	@JsonProperty("shuttle_category")
	private ArrayList<String> mShuttleCategoryList;

	public ShuttleCategoryListAck() {}

	public ShuttleCategoryListAck(ResultData mResultData, ArrayList<String> mShuttleCategoryList) {
		this.mResultData = mResultData;
		this.mShuttleCategoryList = mShuttleCategoryList;
	}

	public ResultData getmResultData() {
		return mResultData;
	}

	public void setmResultData(ResultData mResultData) {
		this.mResultData = mResultData;
	}

	public ArrayList<String> getmShuttleCategoryList() {
		return mShuttleCategoryList;
	}

	public void setmShuttleCategoryList(ArrayList<String> mShuttleCategoryList) {
		this.mShuttleCategoryList = mShuttleCategoryList;
	}
}
