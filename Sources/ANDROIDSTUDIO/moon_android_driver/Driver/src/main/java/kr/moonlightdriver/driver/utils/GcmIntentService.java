package kr.moonlightdriver.driver.utils;


import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.BufferedInputStream;
import java.net.URL;
import java.net.URLConnection;

import kr.moonlightdriver.driver.R;
import kr.moonlightdriver.driver.activity.MainActivity;

public class GcmIntentService extends IntentService {
	private static final String TAG = "GcmIntentService";
	public static final int NOTIFICATION_ID = 1;

	public GcmIntentService() {
		super(TAG);
	}

	private boolean isCarpoolPush() {
		try {
			SharedPreferences prefs = getSharedPreferences(GcmIntentService.class.getSimpleName(), Context.MODE_PRIVATE);
			String ynCarpoolPush = prefs.getString(Global.PROPERTY_YN_CARPOOL_PUSH, "Y");

			return ynCarpoolPush.equals("Y");
		} catch (Exception e) {
			e.printStackTrace();

			return false;
		}
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		try {
			Bundle extras = intent.getExtras();
			if(extras != null) {
				GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);

				String messageType = gcm.getMessageType(intent);

				if (!extras.isEmpty()) {
//			        SVUtil.log("error", TAG, "Received: " + extras.toString());

					if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
						sendNotification(extras);
					}
				}

				GcmBroadcastReceiver.completeWakefulIntent(intent);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	private String getNotificationTitle(String _messageType) {
		try {
			switch (_messageType) {
				case "create_taxi" :
					return "택시 카풀 등록";
				case "request_taxi" :
					return "택시 카풀 동승 신청";
				case "success_taxi" :
					return "택시 카풀 동승 신청 수락";
				case "decline_taxi" :
					return "택시 카풀 동승 신청 거절";
				case "question_answer" :
					return "문의사항 답변 등록";
				case "question_register" :
					return "신규 문의사항 등록";
				case "twoinone_answer" :
					return "2인1조 답변 등록";
				case "notice" :
					return "공지사항";
				default :
					return "";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	private boolean checkCarpoolPush(String _messageType) {
		try {
			switch (_messageType) {
				case "create_taxi" :
				case "request_taxi" :
				case "success_taxi" :
				case "decline_taxi" :
					return isCarpoolPush();
				default :
					return true;
			}
		} catch (Exception e) {
			e.printStackTrace();

			return false;
		}
	}

	private void sendNotification(Bundle _args) {
		try {
			String push_type = _args.getString("type", "");
			if(!checkCarpoolPush(push_type)) {
				return;
			}

			String title = getNotificationTitle(push_type);
			if(title.isEmpty()) {
				return;
			}

			String imgUrl = _args.getString("imgUrl", "");
			Bitmap notifyImg = null;
			if(push_type.equals("notice")) {
				title = _args.getString("title", "");
				if(!imgUrl.isEmpty()) {
					notifyImg = getNotificationImage(imgUrl);
				}
			}

			String content = _args.getString("message", "");
			Intent intent = getIntent(_args);

			PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
			NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
			builder.setSmallIcon(R.drawable.ic_launcher);
			builder.setTicker(title);
			builder.setWhen(System.currentTimeMillis());
			builder.setContentTitle(title);
			builder.setContentText(content);
			builder.setContentIntent(contentIntent);
			builder.setAutoCancel(true);
			Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher);
			builder.setLargeIcon(bm);
			builder.setVibrate(new long[] {50, 500, 50, 500});
			builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

			if(Build.VERSION.SDK_INT > 15) {
				builder.setPriority(Notification.PRIORITY_MAX);
			}

			NotificationCompat.BigTextStyle textStyle = new NotificationCompat.BigTextStyle();
			textStyle.setBigContentTitle(title);
			textStyle.bigText(content);
			textStyle.setSummaryText(content);
			builder.setStyle(textStyle);

			if(push_type.equals("notice") && notifyImg != null) {
				NotificationCompat.BigPictureStyle style = new NotificationCompat.BigPictureStyle();
				style.setBigContentTitle(title);
				style.setSummaryText(content);
				style.bigPicture(notifyImg);
				builder.setStyle(style);
			}

			NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
			if(Build.VERSION.SDK_INT > 15) {
				notificationManager.notify(NOTIFICATION_ID, builder.build());
			} else {
				notificationManager.notify(NOTIFICATION_ID, builder.getNotification());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Bitmap getNotificationImage(String _imgUrl) {
		Bitmap notifyImg = null;
		try {
			URL url = new URL(_imgUrl);
			URLConnection conn = url.openConnection();
			conn.connect();

			BufferedInputStream bis = new BufferedInputStream(conn.getInputStream());
			notifyImg = BitmapFactory.decodeStream(bis);
			bis.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return notifyImg;
	}

	private Intent getIntent(Bundle _args) {
		Intent intent = new Intent(this, MainActivity.class);

		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

		try {
			String type = _args.getString("type", "");

			if(!type.isEmpty()) {
				switch (type) {
					case "create_taxi":
					case "request_taxi":
					case "success_taxi":
					case "decline_taxi":
						String taxiId = _args.getString("taxiId", "");

						intent.putExtra("actionFragment", "CarpoolDetail");
						intent.putExtra("actionId", taxiId);
						break;
					case "question_answer":
					case "question_register":
						String questionId = _args.getString("questionId", "");

						intent.putExtra("actionFragment", "QuestionDetail");
						intent.putExtra("actionId", questionId);
						break;
					case "twoinone_answer" :
						String boardId = _args.getString("boardId", "");

						intent.putExtra("actionFragment", "TwoInOneDetail");
						intent.putExtra("actionId", boardId);
						break;
					case "notice" :
						intent = new Intent(Intent.ACTION_VIEW);
						intent.setData(Uri.parse(Global.APP_DOWNLOAD_URL));
						break;
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}

		return intent;
	}
}