package kr.moonlightdriver.driver.Network.NetData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * Created by youngmin on 2015-10-29.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DriverListData {
	@JsonProperty("max")
	private int mMax;

	@JsonProperty("list")
	private ArrayList<CarpoolDriverData> mDriverList;

	public DriverListData() {}

	public DriverListData(ArrayList<CarpoolDriverData> mDriverList, int mMax) {
		this.mDriverList = mDriverList;
		this.mMax = mMax;
	}

	public ArrayList<CarpoolDriverData> getmDriverList() {
		return mDriverList;
	}

	public void setmDriverList(ArrayList<CarpoolDriverData> mDriverList) {
		this.mDriverList = mDriverList;
	}

	public int getmMax() {
		return mMax;
	}

	public void setmMax(int mMax) {
		this.mMax = mMax;
	}

	public int getCurrentDriverCount() {
		return this.mDriverList.size();
	}
}
