package kr.moonlightdriver.driver.Network.NetData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by youngmin on 2015-12-31.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShuttleTransferPathData {
	@JsonProperty("seq")
	private int mSeq;

	@JsonProperty("geton")
	private ShuttleTransferLocationData mGetOnInfo;

	@JsonProperty("getoff")
	private ShuttleTransferLocationData mGetOffInfo;

	@JsonProperty("shuttleInfo")
	private TransferPathShuttleInfoData mTransferPathShuttleInfoData;

	public ShuttleTransferPathData() {}

	public ShuttleTransferPathData(int mSeq, ShuttleTransferLocationData mGetOnInfo, ShuttleTransferLocationData mGetOffInfo, TransferPathShuttleInfoData mTransferPathShuttleInfoData) {
		this.mSeq = mSeq;
		this.mGetOnInfo = mGetOnInfo;
		this.mGetOffInfo = mGetOffInfo;
		this.mTransferPathShuttleInfoData = mTransferPathShuttleInfoData;
	}

	public int getmSeq() {
		return mSeq;
	}

	public void setmSeq(int mSeq) {
		this.mSeq = mSeq;
	}

	public ShuttleTransferLocationData getmGetOnInfo() {
		return mGetOnInfo;
	}

	public void setmGetOnInfo(ShuttleTransferLocationData mGetOnInfo) {
		this.mGetOnInfo = mGetOnInfo;
	}

	public ShuttleTransferLocationData getmGetOffInfo() {
		return mGetOffInfo;
	}

	public void setmGetOffInfo(ShuttleTransferLocationData mGetOffInfo) {
		this.mGetOffInfo = mGetOffInfo;
	}

	public TransferPathShuttleInfoData getmTransferPathShuttleInfoData() {
		return mTransferPathShuttleInfoData;
	}

	public void setmTransferPathShuttleInfoData(TransferPathShuttleInfoData mTransferPathShuttleInfoData) {
		this.mTransferPathShuttleInfoData = mTransferPathShuttleInfoData;
	}
}
