package kr.moonlightdriver.driver.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.vividsolutions.jts.geom.Coordinate;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import kr.moonlightdriver.driver.BuildConfig;
import kr.moonlightdriver.driver.Network.NetData.ShuttleLineColorData;
import kr.moonlightdriver.driver.Network.NetData.ShuttlePathPointData;
import kr.moonlightdriver.driver.Network.NetData.StationInfoData;
import kr.moonlightdriver.driver.R;
import kr.moonlightdriver.driver.activity.MainActivity;

/**
 * Created by redjjol on 6/09/2014.
 */
public class SVUtil {
	public static final String TAG = "SVUtil";
	public static ProgressDialog mProgressDialog;
    private static final double KM_TO_MILE = 1.61;

	public static void showProgressDialog(Context cx, String msg) {
		hideProgressDialog();

		mProgressDialog = ProgressDialog.show(cx, "", msg, true, false);
	}

	public static void hideProgressDialog() {
		if(mProgressDialog != null) {
			mProgressDialog.dismiss();
		}
	}

	public static void showSimpleDialog(Context _context, String _message) {
		showDialogWithListener(_context, "알림", _message, null, "확인", null, null, null);
	}

	public static void showDialogWithListener(Context _context, String _message, String _buttonText, DialogInterface.OnClickListener _buttonListener) {
		showDialogWithListener(_context, "알림", _message, null, _buttonText, _buttonListener, null, null);
	}

	public static void showDialogWithListener(Context _context, String _title, String _message, String _positiveButtonText, DialogInterface.OnClickListener _positiveListener,
	                                                 String _negativeButtonText, DialogInterface.OnClickListener _negativeListener) {
		showDialogWithListener(_context, _title, _message, null, _positiveButtonText, _positiveListener, _negativeButtonText, _negativeListener);
	}

	public static void showDialogWithListener(Context _context, String _title, View _view, String _buttonText, DialogInterface.OnClickListener _buttonListener) {
		showDialogWithListener(_context, _title, null, _view, _buttonText, _buttonListener, null, null);
	}

	public static AlertDialog showDialogWithListener(Context _context, String _title, View _view, String _positiveButtonText, DialogInterface.OnClickListener _positiveListener,
	                                                 String _negativeButtonText, DialogInterface.OnClickListener _negativeListener) {
		return showDialogWithListener(_context, _title, null, _view, _positiveButtonText, _positiveListener, _negativeButtonText, _negativeListener);
	}

	public static AlertDialog showDialogWithListener(Context _context, String _title, String _message, View _view, String _positiveButtonText, DialogInterface.OnClickListener _positiveListener,
	                                          String _negativeButtonText, DialogInterface.OnClickListener _negativeListener) {
		try {
			if (_context == null) {
				return null;
			}

			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(_context);
			alertDialogBuilder.setTitle(_title);

			if(_message != null) {
				alertDialogBuilder.setMessage(_message);
			}

			if(_view != null) {
				alertDialogBuilder.setView(_view);
			}

			if(_positiveButtonText != null && !_positiveButtonText.isEmpty()) {
				alertDialogBuilder.setPositiveButton(_positiveButtonText, _positiveListener);
			}

			if(_negativeButtonText != null && !_negativeButtonText.isEmpty()) {
				alertDialogBuilder.setNegativeButton(_negativeButtonText, _negativeListener);
			}

			AlertDialog dialog = alertDialogBuilder.show();

			setDialogTitleColor(dialog, _context.getResources().getColor(R.color.main200));

			return dialog;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void showItemDialog(Context _context, String _title, CharSequence[] _items, DialogInterface.OnClickListener _itemClickListener) {
		try {
			if (_context == null) {
				return;
			}

			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(_context);
			alertDialogBuilder.setTitle(_title);

			alertDialogBuilder.setItems(_items, _itemClickListener);

			AlertDialog dialog = alertDialogBuilder.show();

			setDialogTitleColor(dialog, _context.getResources().getColor(R.color.main200));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void showAdapterDialog(Context _context, String _title, ListAdapter _adapter, DialogInterface.OnClickListener _itemClickListener, String _buttonText, DialogInterface.OnClickListener _buttonListener) {
		try {
			if (_context == null) {
				return;
			}

			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(_context);
			alertDialogBuilder.setTitle(_title);

			alertDialogBuilder.setAdapter(_adapter, _itemClickListener);

			if(_buttonText != null && !_buttonText.isEmpty()) {
				alertDialogBuilder.setNegativeButton(_buttonText, _buttonListener);
			}

			AlertDialog dialog = alertDialogBuilder.show();

			setDialogTitleColor(dialog, _context.getResources().getColor(R.color.main200));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void showSingleChoiceItemDialog(Context _context, String _title, CharSequence[] _items, int _checkedItem, DialogInterface.OnClickListener _itemClickListener) {
		showSingleChoiceItemDialog(_context, _title, _items, _checkedItem, _itemClickListener, null, null, null, null);
	}

	public static void showSingleChoiceItemDialog(Context _context, String _title, CharSequence[] _items, int _checkedItem, DialogInterface.OnClickListener _itemClickListener,
	                                              String _positiveButtonText, DialogInterface.OnClickListener _positiveListener, String _negativeButtonText, DialogInterface.OnClickListener _negativeListener) {
		try {
			if (_context == null) {
				return;
			}

			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(_context);
			alertDialogBuilder.setTitle(_title);

			alertDialogBuilder.setSingleChoiceItems(_items, _checkedItem, _itemClickListener);

			if(_positiveButtonText != null && !_positiveButtonText.isEmpty()) {
				alertDialogBuilder.setPositiveButton(_positiveButtonText, _positiveListener);
			}

			if(_negativeButtonText != null && !_negativeButtonText.isEmpty()) {
				alertDialogBuilder.setNegativeButton(_negativeButtonText, _negativeListener);
			}

			AlertDialog dialog = alertDialogBuilder.show();

			setDialogTitleColor(dialog, _context.getResources().getColor(R.color.main200));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void showTimeoutDialog(final Context _context) {
		showDialogWithListener(_context, "네트워크 연결이 원할하지 않습니다.\n다시 시도해 주십시요.", "확인", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				finishActivity((Activity) _context);
			}
		});
	}

	public static void SaveDriverId(Context cx, String driverId) {
		try {
			SharedPreferences prefs = cx.getSharedPreferences("moonlightdriver", Context.MODE_PRIVATE);
			SharedPreferences.Editor editor = prefs.edit();
			editor.putString("driverId", driverId);
			editor.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static int GetLocationServiceStatus(Context cx) {
		try {
			LocationManager lm = (LocationManager) cx.getSystemService(Context.LOCATION_SERVICE);
			if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER) && lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
				return 3;
			} else if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
				return 2;
			} else if (lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
				return 1;
			} else { // 위치 찾기 불가
				return 0;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	public static boolean GetWifiOnStatus(Context cx) {
		try {
			ConnectivityManager cm = (ConnectivityManager)cx.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

			return wifi.isAvailable();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static int dp(Context context, int dp) {
		return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
	}

	public static ArrayList<LatLng> getCircleCoordinatesLatLng(LatLng _center, double _radius, double _points) {
		ArrayList<LatLng> coordinates = new ArrayList<>();

		try {
			double d2r = Math.PI / 180;
			double r2d = 180 / Math.PI;
			double earthsradius = 3959;
			double start = 0;
			double end = _points + 1;
			double radius = _radius / KM_TO_MILE;

			double rLat = (radius / earthsradius) * r2d;
			double rLng = rLat / Math.cos(_center.latitude * d2r);
			for(double i = start; i < end; i++) {
				double theta = Math.PI * (i / (_points / 2));
				double resultLng = _center.longitude + (rLng * Math.cos(theta));
				double resultLat = _center.latitude + (rLat * Math.sin(theta));
				coordinates.add(new LatLng(resultLat, resultLng));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return coordinates;
	}

	public static Coordinate[] getCircleCoordinates(LatLng _center, double _radius, double _points) {
		try {
			double d2r = Math.PI / 180;
			double r2d = 180 / Math.PI;
			double earthsradius = 3959;
			double start = 0;
			double end = _points + 1;
			double radius = _radius / KM_TO_MILE;
			Coordinate[] coordinates = new Coordinate[(int)_points + 1];

			double rLat = (radius / earthsradius) * r2d;
			double rLng = rLat / Math.cos(_center.latitude * d2r);
			for(double i = start; i < end; i++) {
				double theta = Math.PI * (i / (_points / 2));
				double resultLng = _center.longitude + (rLng * Math.cos(theta));
				double resultLat = _center.latitude + (rLat * Math.sin(theta));
				coordinates[(int)i] = new Coordinate(resultLng, resultLat);
			}

			return coordinates;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

    public static String getTotalTimeString(int _totalTime) {
	    String ret = "";

	    try {
		    int hours = _totalTime / (60 * 60);
		    int minutes = _totalTime / 60;
		    int second = _totalTime % 60;
		    ret += hours > 0 ? ((hours < 10 ? "0" + hours : hours) + "시간 ") : "";
		    ret += minutes > 0 ? ((minutes < 10 ? "0" + minutes : minutes) + "분 ") : "";
		    ret += second > 0 ? ((second < 10 ? "0" + second : second) + "초") : "";
		} catch (Exception e) {
		    e.printStackTrace();
	    }

	    return ret;
    }

    public static BitmapDescriptor createShuttleArrowBitmap(int _width, int _height, String _color) {
	    try {
		    int rectLength = (int) (Math.sqrt(Math.pow(_width, 2) + Math.pow(_height, 2)));    //  93
		    float x = (rectLength - _width) / 2;  //  15.5
		    float y = (rectLength - _height) / 2; // 11.5

		    Bitmap resultBitmap = Bitmap.createBitmap(rectLength, rectLength, Bitmap.Config.ARGB_8888);

		    Paint paint = new Paint();
		    paint.setStrokeWidth(4);
		    paint.setColor(Color.parseColor(_color));
		    paint.setStyle(Paint.Style.FILL_AND_STROKE);

		    Path path = new Path();

		    path.moveTo(rectLength - x, rectLength / 2);
		    path.lineTo(x, rectLength - y);
		    path.lineTo((_width / 3) + x, rectLength / 2);
		    path.lineTo(x, y);

		    path.close();

		    Canvas canvas = new Canvas(resultBitmap);
		    canvas.save();

		    canvas.drawPath(path, paint);

		    BitmapDescriptor markerIcon = BitmapDescriptorFactory.fromBitmap(resultBitmap);
		    resultBitmap.recycle();

		    return markerIcon;
	    } catch (Exception e) {
		    e.printStackTrace();
	    }

	    return null;
    }

    public static float getRotationDegrees(LatLng _org, LatLng _dest) {
        float degree = (float)(((Math.atan2(_dest.latitude - _org.latitude, _dest.longitude - _org.longitude)) * 180) / Math.PI);
        return degree * -1;
    }

    public static void log(String _logType, String _tag, String _message) {
	    try {
		    if(BuildConfig.DEBUG) {
			    switch (_logType) {
				    case "verbose" :
					    Log.v(_tag, _message);
					    break;
				    case "debug" :
					    Log.d(_tag, _message);
					    break;
				    case "info" :
					    Log.i(_tag, _message);
					    break;
				    case "warning" :
					    Log.w(_tag, _message);
					    break;
				    case "error" :
					    Log.e(_tag, _message);
					    break;
			    }
		    }
	    } catch (Exception e) {
		    e.printStackTrace();
	    }
    }

    public static void hideSoftInput(MainActivity _mainActivity) {
	    try {
		    View view = _mainActivity.getCurrentFocus();
		    if(view != null) {
			    InputMethodManager imm = (InputMethodManager) _mainActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
			    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
		    }
	    } catch (Exception e) {
		    e.printStackTrace();
	    }
    }

	public static BitmapDescriptor getMarkerIcon(Resources _res, int _resourceId) {
		try {
			Bitmap markerDefaultIcon = BitmapFactory.decodeResource(_res, _resourceId);
			BitmapDescriptor marketDefaultIconDescriptor = BitmapDescriptorFactory.fromBitmap(markerDefaultIcon);

			markerDefaultIcon.recycle();

			return marketDefaultIconDescriptor;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public static int[] getFistShuttleTime(String _times) {
		try {
			int[] minimumTime = getLastShuttleTime(_times);
			if(minimumTime != null && minimumTime[0] > 4) {
				return minimumTime;
			}

			String[] times = _times.split(",");

			int listSize = times.length;
			for(int i = 0; i < listSize; i++) {
				String[] compTime = times[i].split(":");
				int compHours = Integer.parseInt(compTime[0]);
				int compMinutes = Integer.parseInt(compTime[1]);

				if(minimumTime == null) {
					minimumTime = new int[] { compHours, compMinutes };
				} else if((minimumTime[0] > compHours) || (minimumTime[0] == compHours && minimumTime[1] > compMinutes)) {
					minimumTime[0] = compHours;
					minimumTime[1] = compMinutes;
				}
			}

			return minimumTime;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public static int[] getLastShuttleTime(String _times) {
		int[] maximumTime = null;

		try {
			String[] times = _times.split(",");

			int listSize = times.length;
			for(int i = 0; i < listSize; i++) {
				String[] compTime = times[i].split(":");
				int compHours = Integer.parseInt(compTime[0]);
				int compMinutes = Integer.parseInt(compTime[1]);

				if(maximumTime == null) {
					maximumTime = new int[] { compHours, compMinutes };
				} else if((maximumTime[0] < compHours) || (maximumTime[0] == compHours && maximumTime[1] < compMinutes)) {
					maximumTime[0] = compHours;
					maximumTime[1] = compMinutes;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return maximumTime;
	}

	public static int[] getFistBusTime(String _times) {
		int[] minimumTime = null;

		try {
			String[] times = _times.split(",");

			int listSize = times.length;
			for(int i = 0; i < listSize; i++) {
				String[] compTime = times[i].split(":");
				int compHours = Integer.parseInt(compTime[0]);
				int compMinutes = Integer.parseInt(compTime[1]);

				if(minimumTime == null) {
					minimumTime = new int[] { compHours, compMinutes };
				} else if((minimumTime[0] > compHours) || (minimumTime[0] == compHours && minimumTime[1] > compMinutes)) {
					minimumTime[0] = compHours;
					minimumTime[1] = compMinutes;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return minimumTime;
	}

	public static int[] getLastBusTime(String _times) {
		try {
			int[] maximumTime = getFistBusTime(_times);
			if(maximumTime != null && maximumTime[0] <= Global.BUS_START_HOURS) {
				return maximumTime;
			}

			maximumTime = null;
			String[] times = _times.split(",");

			int listSize = times.length;
			for(int i = 0; i < listSize; i++) {
				String[] compTime = times[i].split(":");
				int compHours = Integer.parseInt(compTime[0]);
				int compMinutes = Integer.parseInt(compTime[1]);

				if(maximumTime == null) {
					maximumTime = new int[] { compHours, compMinutes };
				} else if((maximumTime[0] < compHours) || (maximumTime[0] == compHours && maximumTime[1] < compMinutes)) {
					maximumTime[0] = compHours;
					maximumTime[1] = compMinutes;
				}
			}

			return maximumTime;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public static void setDialogTitleColor(Dialog _dialog, int _color) {
		try {
			int textViewId = _dialog.getContext().getResources().getIdentifier("android:id/alertTitle", null, null);
			TextView tv = (TextView) _dialog.findViewById(textViewId);
			tv.setTextColor(_color);
			tv.setGravity(Gravity.CENTER);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static LatLngBounds getExtendedOutbackBounds(float _width, float _height, LatLng _center) {
		try {
			ArrayList<LatLng> extendedWidthCoordinates = SVUtil.getCircleCoordinatesLatLng(_center, (_width / 1000), 4);
			ArrayList<LatLng> extendedHeightCoordinates = SVUtil.getCircleCoordinatesLatLng(_center, (_height / 1000), 4);

			return new LatLngBounds(new LatLng(extendedHeightCoordinates.get(3).latitude, extendedWidthCoordinates.get(2).longitude), new LatLng(extendedHeightCoordinates.get(1).latitude, extendedWidthCoordinates.get(0).longitude));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public static float[] getScreenSize(LatLngBounds _currentScreen) {
		float[] screenSize = new float[2];

		try {
			float[] width = new float[2];
			Location.distanceBetween(_currentScreen.getCenter().latitude, _currentScreen.southwest.longitude, _currentScreen.getCenter().latitude, _currentScreen.northeast.longitude, width);

			screenSize[0] = width[0] < Global.MIN_SCREEN_WITH ? Global.MIN_SCREEN_WITH : width[0];

			float[] height = new float[2];
			Location.distanceBetween(_currentScreen.northeast.latitude, _currentScreen.getCenter().longitude, _currentScreen.southwest.latitude, _currentScreen.getCenter().longitude, height);

			screenSize[1] = height[0] < Global.MIN_SCREEN_HEIGHT ? Global.MIN_SCREEN_HEIGHT : height[0];
		} catch (Exception e) {
			e.printStackTrace();
		}

		return screenSize;
	}

	public static void finishActivity(Activity _activity) {
		try {
			_activity.moveTaskToBack(true);
			_activity.finish();
			android.os.Process.killProcess(android.os.Process.myPid());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String convertTime(String _time) {
		try {
			String convertedTime = _time;

			String[] timeArray = _time.split(":");
			int timeHours = Integer.parseInt(timeArray[0]);
			if(timeHours >= 24) {
				timeHours = timeHours - 24;
				if(timeHours < 10) {
					convertedTime = "0" + timeHours + ":" + timeArray[1];
				} else {
					convertedTime = timeHours + ":" + timeArray[1];
				}
			}

			return convertedTime;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "";
	}

	public static boolean checkIsStationRunning(String _startTime, String _endTime) {
		boolean isValidTime = false;

		try {
			if(_startTime == null || _startTime.isEmpty() || _endTime == null || _endTime.isEmpty()) {
				return true;
			}

			Calendar rightNow = Calendar.getInstance();
			int hours = rightNow.get(Calendar.HOUR_OF_DAY);
			int minutes = rightNow.get(Calendar.MINUTE);

			String[] startTimeSplit = _startTime.split(":");
			int startTimeHours = Integer.parseInt(startTimeSplit[0]);
			int startTimeMinutes = Integer.parseInt(startTimeSplit[1]);

			String[] endTimeSplit = _endTime.split(":");
			int endTimeHours = Integer.parseInt(endTimeSplit[0]);
			int endTimeMinutes = Integer.parseInt(endTimeSplit[1]);

			if((startTimeHours < hours || (startTimeHours == hours && startTimeMinutes <= minutes))
					&& (endTimeHours > hours || (endTimeHours == hours && endTimeMinutes >= minutes))) {
				isValidTime = true;
			}

			if(!isValidTime) {
				hours = hours + 24;

				if((startTimeHours < hours || (startTimeHours == hours && startTimeMinutes <= minutes))
						&& (endTimeHours > hours || (endTimeHours == hours && endTimeMinutes >= minutes))) {
					isValidTime = true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

//		SVUtil.log("error", TAG, "now : " + "0:22" + ", _startTime : " + _startTime + ", _endTIme : " + _endTime + ", isValidTime : " + isValidTime);

		return isValidTime;
	}

	public static boolean checkIsShuttleStationRunning(StationInfoData _stationInfoData) {
		boolean isValidTime = false;

		try {
			Calendar cal = Calendar.getInstance();
			int weekday = cal.get(Calendar.DAY_OF_WEEK);

			String firstTime = _stationInfoData.getmFirstTime();
			String lastTime = _stationInfoData.getmLastTime();
			boolean isHoliday = SVUtil.checkHoliday(_stationInfoData.getmHolidayDate());
			if(isHoliday) {
				weekday = 1;
			}

			if(weekday == 1) {      // 일요일
				firstTime = _stationInfoData.getmSundayFirstTime();
				lastTime = _stationInfoData.getmSundayLastTime();
			} else if(weekday == 7) {   // 토요일
				firstTime = _stationInfoData.getmSaturdayFirstTime();
				lastTime = _stationInfoData.getmSaturdayLastTime();
			}

//			SVUtil.log("error", TAG, "busId : " + _stationInfoData.getmBusId() + ", weekday : " + weekday + ", firstTime : " + firstTime + ", lastTime : " + lastTime);

			if(firstTime == null || firstTime.isEmpty() || lastTime == null || lastTime.isEmpty()) {
				return false;
			}

			isValidTime = checkIsStationRunning(firstTime, lastTime);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return isValidTime;
	}

	public static boolean checkIsShuttleStationRunning(ShuttleLineColorData _shuttleLineColorData) {
		boolean isValidTime = false;

		try {
			Calendar cal = Calendar.getInstance();
			int weekday = cal.get(Calendar.DAY_OF_WEEK);

			String firstTime = _shuttleLineColorData.getmRunFirstTime();
			String lastTime = _shuttleLineColorData.getmRunLastTime();
			boolean isHoliday = SVUtil.checkHoliday(_shuttleLineColorData.getmHolidayDate());
			if(isHoliday) {
				weekday = 1;
			}

			if(weekday == 1) {      // 일요일
				firstTime = _shuttleLineColorData.getmSundayRunFirstTime();
				lastTime = _shuttleLineColorData.getmSundayRunLastTime();
			} else if(weekday == 7) {   // 토요일
				firstTime = _shuttleLineColorData.getmSaturdayRunFirstTime();
				lastTime = _shuttleLineColorData.getmSaturdayRunLastTime();
			}

//			SVUtil.log("error", TAG, "busId : " + _shuttleLineColorData.getmBusId() + ", weekday : " + weekday + ", firstTime : " + firstTime + ", lastTime : " + lastTime);

			if(firstTime == null || firstTime.isEmpty() || lastTime == null || lastTime.isEmpty()) {
				return false;
			}

			isValidTime = checkIsStationRunning(firstTime, lastTime);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return isValidTime;
	}

	public static boolean checkHoliday(String _holidayDate) {
		try {
			if(_holidayDate == null || _holidayDate.isEmpty()) {
				return false;
			}

			Calendar cal = Calendar.getInstance();
			String year = cal.get(Calendar.YEAR) + "-";
			String month = (cal.get(Calendar.MONTH) + 1 < 10 ? "0" + (cal.get(Calendar.MONTH) + 1) : cal.get(Calendar.MONTH) + 1) + "-";
			String day = (cal.get(Calendar.DAY_OF_MONTH) < 10 ? "0" + (cal.get(Calendar.DAY_OF_MONTH)) : cal.get(Calendar.DAY_OF_MONTH)) + "";
			String today = year + month + day;

			ArrayList<String> holidayDateList = new ArrayList<>(Arrays.asList(_holidayDate.split(",")));
			if(holidayDateList.contains(today)) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	public static String Encrypt(String _str) {
		try {
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			byte[] keyBytes = new byte[16];
			byte[] bytes = Global.ENCRYPT_KEY.getBytes("UTF-8");
			int len = bytes.length;

			if(len > keyBytes.length) {
				len = keyBytes.length;
			}

			System.arraycopy(bytes, 0, keyBytes, 0, len);
			SecretKeySpec keyspec = new SecretKeySpec(keyBytes, "AES");
			IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);

			cipher.init(Cipher.ENCRYPT_MODE, keyspec, ivSpec);

			byte[] result = cipher.doFinal(_str.getBytes("UTF-8"));
			return Base64.encodeToString(result, 0);
		} catch (NoSuchAlgorithmException | NoSuchPaddingException | UnsupportedEncodingException | InvalidAlgorithmParameterException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
			e.printStackTrace();
			return "";
		}
	}

	public static  String Decrypt(String _str) {
		try {
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			byte[] keyBytes = new byte[16];
			byte[] bytes = Global.ENCRYPT_KEY.getBytes("UTF-8");
			int len = bytes.length;

			if(len > keyBytes.length) {
				len = keyBytes.length;
			}

			System.arraycopy(bytes, 0, keyBytes, 0, len);
			SecretKeySpec keyspec = new SecretKeySpec(keyBytes, "AES");
			IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);

			cipher.init(Cipher.DECRYPT_MODE, keyspec, ivSpec);

			byte[] result = cipher.doFinal(Base64.decode(_str, 0));
			return new String(result, "UTF-8");
		} catch (NoSuchAlgorithmException | NoSuchPaddingException | UnsupportedEncodingException | InvalidAlgorithmParameterException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
			e.printStackTrace();
			return "";
		}
	}

	public static String getShuttleStartStation(ArrayList<ShuttlePathPointData> _shuttlePathPointData) {
		String stationName = "";

		try {
			int listSize = _shuttlePathPointData.size();

			for(int i = 0; i < listSize; i++) {
				if(!_shuttlePathPointData.get(i).getmShuttlePointName().equals("__point") && stationName.equals("")) {
					stationName = _shuttlePathPointData.get(i).getmShuttlePointName();
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return stationName;
	}

	public static String getShuttleEndStation(ArrayList<ShuttlePathPointData> _shuttlePathPointData) {
		String stationName = "";

		try {
			int listSize = _shuttlePathPointData.size();

			for(int i = listSize - 1; i >= 0; i--) {
				if(!_shuttlePathPointData.get(i).getmShuttlePointName().equals("__point") && stationName.equals("")) {
					stationName = _shuttlePathPointData.get(i).getmShuttlePointName();
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return stationName;
	}

	public static int dpFromPx(final Context context, final float px) {
		return Math.round(px / context.getResources().getDisplayMetrics().density);
	}

	public static int pxFromDp(final Context context, final float dp) {
		return Math.round(dp * context.getResources().getDisplayMetrics().density);
	}

	public static long getLocationUpdateTime(double _kmPerHour) {
		try {
			if(_kmPerHour >= 40) {
				return 1000;
			} else if(_kmPerHour >= 30) {
				return 1200;
			} else if(_kmPerHour >= 25) {
				return 1400;
			} else if(_kmPerHour >= 20) {
				return 1800;
			} else if(_kmPerHour >= 19) {
				return 1900;
			} else if(_kmPerHour >= 18) {
				return 2000;
			} else if(_kmPerHour >= 17) {
				return 2100;
			} else if(_kmPerHour >= 16) {
				return 2200;
			} else if(_kmPerHour >= 15) {
				return 2400;
			} else if(_kmPerHour >= 14) {
				return 2600;
			} else if(_kmPerHour >= 13) {
				return 2800;
			} else if(_kmPerHour >= 12) {
				return 3000;
			} else if(_kmPerHour >= 11) {
				return 3200;
			} else if(_kmPerHour >= 10) {
				return 3600;
			} else if(_kmPerHour >= 9) {
				return 4000;
			} else if(_kmPerHour >= 8) {
				return 4500;
			} else if(_kmPerHour >= 7) {
				return 5000;
			} else if(_kmPerHour >= 6) {
				return 6000;
			} else if(_kmPerHour >= 5) {
				return 7000;
			} else if(_kmPerHour >= 4) {
				return 9000;
			} else if(_kmPerHour >= 3) {
				return 12000;
			} else if(_kmPerHour >= 2) {
				return 18000;
			} else if(_kmPerHour >= 1) {
				return 36000;
			} else {
				return 60000;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return 60000;
		}
	}

//	public void exportDatabase(String databaseName) {
//		try {
//			File sd = Environment.getExternalStorageDirectory();
//			File data = Environment.getDataDirectory();
//
//			if (sd.canWrite()) {
//				String currentDBPath = "//data//"+getPackageName()+"//databases//"+databaseName+"";
//				String backupDBPath = "backup_bus_data.db";
//				File currentDB = new File(data, currentDBPath);
//				File backupDB = new File(sd, backupDBPath);
//
//				if (currentDB.exists()) {
//					FileChannel src = new FileInputStream(currentDB).getChannel();
//					FileChannel dst = new FileOutputStream(backupDB).getChannel();
//					dst.transferFrom(src, 0, src.size());
//					src.close();
//					dst.close();
//				}
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

}
