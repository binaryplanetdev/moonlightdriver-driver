package kr.moonlightdriver.driver.fragment;

import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.kevinsawicki.http.HttpRequest;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import kr.moonlightdriver.driver.Network.NetApi.NightBusRouteListAck;
import kr.moonlightdriver.driver.Network.NetApi.ShuttleListAck;
import kr.moonlightdriver.driver.Network.NetData.ResultData;
import kr.moonlightdriver.driver.Network.NetData.ShuttleListData;
import kr.moonlightdriver.driver.R;
import kr.moonlightdriver.driver.activity.MainActivity;
import kr.moonlightdriver.driver.utils.Global;
import kr.moonlightdriver.driver.utils.SVUtil;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * Created by redjjol on 3/08/15.
 */
public class ShuttleFragment extends Fragment implements View.OnClickListener {
	private static final String TAG = "ShuttleFragment";
	private final float[] mDistanceList = { 500f, 1000f, 2000f, 3000f, 5000f, 10000f };
	private String[] mDistanceItem;

	private Button mBtnCategory;
	private Button mBtnDistance;
	private int mSelectedCategory;
	private int mSelectingCategory;
	private int mSelectedDistance;
	private int mSelectingDistance;

	private ArrayList<ShuttleListData> mShuttleTotalList;
	private ArrayList<ShuttleListData> mCurrentShowList;
	private ShuttleListAdapter mShuttleListAdapter;

	private GetShuttleAndNightBusListTask mShuttleAndNightBusListTask;

	private MainActivity mMainActivity;

	private View mView;

	private boolean mIsFinishFragment;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		try {
			mView = inflater.inflate(R.layout.frag_shuttle, container, false);

			mMainActivity = (MainActivity) getActivity();

			mIsFinishFragment = false;

			initDistance();

			mBtnCategory = (Button) mView.findViewById(R.id.frag_sb_shuttle_categoryBtn);
			mBtnDistance = (Button) mView.findViewById(R.id.frag_sb_shuttle_distBtn);
			StickyListHeadersListView listViewShuttle = (StickyListHeadersListView) mView.findViewById(R.id.frag_sb_shuttle_listView);

			mBtnCategory.setOnClickListener(this);
			mBtnDistance.setOnClickListener(this);
			listViewShuttle.setOnItemClickListener(new OnItemClick_path());

			mCurrentShowList = new ArrayList<>();
			mShuttleTotalList = new ArrayList<>();
			mShuttleListAdapter = new ShuttleListAdapter(mMainActivity, R.layout.row_shuttle_list, mCurrentShowList);
			listViewShuttle.setAdapter(mShuttleListAdapter);

			mSelectedCategory = 0;
			mSelectedDistance = 1; // 1km

			if(mShuttleAndNightBusListTask != null) {
				mShuttleAndNightBusListTask.cancel(true);
				mShuttleAndNightBusListTask = null;
			}

			mShuttleAndNightBusListTask = new GetShuttleAndNightBusListTask();
			mShuttleAndNightBusListTask.execute();
		} catch(InflateException e) {
			e.printStackTrace();
		}

		return mView;
	}

	private void initDistance() {
		try {
			int listLength = mDistanceList.length;

			mDistanceItem = new String[listLength];
			for(int i = 0; i < listLength; i++) {
				float distance = (mDistanceList[i] / 1000);
				if(distance < 1) {
					mDistanceItem[i] = distance + "km";
				} else {
					mDistanceItem[i] = ((int) distance) + "km";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private class GetShuttleAndNightBusListTask extends AsyncTask<Void, Void, String> {
		private boolean mIsCanceled = false;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			mIsCanceled = false;

			SVUtil.showProgressDialog(mMainActivity, "로딩중...");
		}

		@Override
		protected String doInBackground(Void... params) {
			double minimumDistance = mDistanceList[mSelectedDistance];
			float searchRange = mDistanceList[mDistanceList.length - 1];

			try {
				if (!this.mIsCanceled) {
					String url = Global.URL_SHUTTLE_LIST + "?lat=" + mMainActivity.mCurrentLocation.latitude + "&lng=" + mMainActivity.mCurrentLocation.longitude + "&range=" + searchRange;
//					String url = Global.URL_SHUTTLE_LIST + "?lat=35.826386&lng=128.725648&range=" + searchRange;
					ShuttleListAck retNetApi = getShuttleList(url);

					if (retNetApi != null) {
						ResultData result = retNetApi.getmResultData();
						if (result.getmCode() != 100) {
							SVUtil.showSimpleDialog(mMainActivity, result.getmDetail());
							return null;
						}

						for(ShuttleListData shuttleListData : retNetApi.getmShuttleListData()) {
							if (mIsFinishFragment || this.mIsCanceled) {
								return null;
							}

							if (minimumDistance > shuttleListData.getmDistance()) {
								minimumDistance = shuttleListData.getmDistance();
							}

							shuttleListData.setmFirstTime(SVUtil.convertTime(shuttleListData.getmFirstTime()));
							shuttleListData.setmLastTime(SVUtil.convertTime(shuttleListData.getmLastTime()));
							shuttleListData.setmIsShuttle(shuttleListData.getmRouteType().equals("shuttle"));

							mShuttleTotalList.add(shuttleListData);
						}

						calculateSelectedDistance(minimumDistance);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				Collections.sort(mShuttleTotalList, ShuttleDistanceSort);
			}

			return null;
		}

		@Override
		protected void onPostExecute(String _ret) {
			super.onPostExecute(_ret);

			try {
				SVUtil.hideProgressDialog();

				if (!updateCurrentShowingList()) {
					SVUtil.showSimpleDialog(mMainActivity, "10km 범위 이내에 셔틀/심야 노선이 없습니다.");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private ShuttleListAck getShuttleList(String _url) {
		ShuttleListAck retShuttleNetApi = null;
		Reader reader = null;
		JsonParser jp = null;

		try {
			HttpRequest request = HttpRequest.get(_url);
//			SVUtil.log("error", TAG, "shuttle station url : " + _url);
			request.acceptGzipEncoding().uncompress(true);
			reader = request.reader();
			ObjectMapper mapper = new ObjectMapper();

			jp = mapper.getFactory().createParser(reader);
			retShuttleNetApi = mapper.readValue(jp, ShuttleListAck.class);

			reader.close();
			reader = null;
			jp.close();
			jp = null;
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				if(reader != null) {
					reader.close();
				}

				if(jp != null && !jp.isClosed()) {
					jp.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return retShuttleNetApi;
	}

	private NightBusRouteListAck getNightBusRouteList(LatLng _center, float _maxRange) {
		NightBusRouteListAck retNightBusRouteListNetApi = null;
		Reader reader = null;
		JsonParser jp = null;

		try {
			String url = Global.URL_NIGHT_BUS_ROUTE_LIST + "?lat=" + _center.latitude + "&lng=" + _center.longitude + "&range=" + _maxRange;
//			SVUtil.log("error", TAG, "bus route detail url : " + url);
			HttpRequest request = HttpRequest.get(url);
			request.acceptGzipEncoding().uncompress(true);
			reader = request.reader();
			ObjectMapper mapper = new ObjectMapper();

			jp = mapper.getFactory().createParser(reader);
			retNightBusRouteListNetApi = mapper.readValue(jp, NightBusRouteListAck.class);

			reader.close();
			reader = null;
			jp.close();
			jp = null;
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				if(reader != null) {
					reader.close();
				}

				if(jp != null && !jp.isClosed()) {
					jp.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return retNightBusRouteListNetApi;
	}

	private void calculateSelectedDistance(double _minDistance) {
		try {
			int listLength = mDistanceList.length;
			for(int i = 1; i < listLength; i++) {
				if(_minDistance <= mDistanceList[i]) {
					mSelectedDistance = i;
					mSelectingDistance = i;

					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private boolean updateCurrentShowingList() {
		try {
			boolean hasShuttleList = false;
			boolean hasNightBusList = false;

			mCurrentShowList.clear();

			for (ShuttleListData shuttleItem : mShuttleTotalList) {
				if (mSelectedCategory == 0) {
					if (shuttleItem.getmDistance() <= mDistanceList[mSelectedDistance]) {
						mCurrentShowList.add(shuttleItem);

						if(shuttleItem.ismIsShuttle()) {
							hasShuttleList = true;
						} else {
							hasNightBusList = true;
						}
					}
				} else {
					if (shuttleItem.getmCategory() == mSelectedCategory && shuttleItem.getmDistance() <= mDistanceList[mSelectedDistance]) {
						mCurrentShowList.add(shuttleItem);

						if(shuttleItem.ismIsShuttle()) {
							hasShuttleList = true;
						} else {
							hasNightBusList = true;
						}
					}
				}
			}

			if(!hasShuttleList) {
				mCurrentShowList.add(new ShuttleListData(true, "", -1, "shuttle", "", -1, "", "", "", 0, 0));
			}

			if(!hasNightBusList) {
				mCurrentShowList.add(new ShuttleListData(false, "", -1, "nightbus", "", -1, "", "", "", 0, 0));
			}

			mShuttleListAdapter.notifyDataSetChanged();

			if(hasShuttleList || hasNightBusList) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	/**
	 * 셔틀 목록 중 하나 선택했을 때
	 */
	private class OnItemClick_path implements AdapterView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
			try {
				ShuttleListData selectedShuttle = mCurrentShowList.get(pos);

				if(!selectedShuttle.getmBusId().isEmpty()) {
					mMainActivity.checkDriverConfirmData(selectedShuttle.getmBusId(), "main");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onClick(View view) {
		try {
			if (view == mBtnCategory) {
				SVUtil.showSingleChoiceItemDialog(mMainActivity, "방향 선택", mMainActivity.mShuttleCategoryList, mSelectedCategory, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						mSelectingCategory = whichButton;
					}
				}, "확인", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						mSelectedCategory = mSelectingCategory;
						mBtnCategory.setText(mMainActivity.mShuttleCategoryList[mSelectedCategory]);

						if (!updateCurrentShowingList()) {
							SVUtil.showSimpleDialog(mMainActivity, mMainActivity.mShuttleCategoryList[mSelectedCategory] + " 방향의 셔틀 노선이 없습니다.");
						}
					}
				}, "취소", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						dialog.dismiss();
					}
				});
			} else if (view == mBtnDistance) {
				SVUtil.showSingleChoiceItemDialog(mMainActivity, "검색 범위 선택", mDistanceItem, mSelectedDistance, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						mSelectingDistance = whichButton;
					}
				}, "확인", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						mSelectedDistance = mSelectingDistance;
						mBtnDistance.setText(mDistanceItem[mSelectedDistance]);

						if (!updateCurrentShowingList()) {
							SVUtil.showSimpleDialog(mMainActivity, mDistanceItem[mSelectedDistance] + "범위내에 셔틀 노선이 없습니다.");
						}
					}
				}, "취소", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						dialog.dismiss();
					}
				});
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private class ShuttleListViewHolder {
		public LinearLayout mShuttleListLayout;
		public TextView mTextViewShuttleName;
		public TextView mTextViewDistance;
		public TextView mTextViewStartTime;
		public TextView mTextViewEndTime;
		public TextView mTextViewDescription;
		public TextView mTextViewNoItem;
	}

	private class ShuttleListHeaderViewHolder {
		public TextView mTextViewHeaderTitle;
	}

	private class ShuttleListAdapter extends ArrayAdapter<ShuttleListData> implements StickyListHeadersAdapter {
		ArrayList<ShuttleListData> mShuttleListData;

		private int cellID;
		ShuttleListViewHolder mShuttleListViewHolder;

		public ShuttleListAdapter(Context cxt, int cellID, ArrayList<ShuttleListData> data) {
			super(cxt, cellID, data);
			this.mShuttleListData = data;
			this.cellID = cellID;
		}

		@Override
		public View getHeaderView(int position, View convertView, ViewGroup parent) {
			try {
				ShuttleListHeaderViewHolder headerViewHolder;
				if (convertView == null) {
					headerViewHolder = new ShuttleListHeaderViewHolder();
					convertView = mMainActivity.getLayoutInflater().inflate(R.layout.row_shuttle_list_header, parent, false);
					headerViewHolder.mTextViewHeaderTitle = (TextView) convertView.findViewById(R.id.row_shuttle_list_headerTV);

					convertView.setTag(headerViewHolder);
				} else {
					headerViewHolder = (ShuttleListHeaderViewHolder) convertView.getTag();
				}

				if (getHeaderId(position) == 0) {
					headerViewHolder.mTextViewHeaderTitle.setText("셔틀버스");
				} else if (getHeaderId(position) == 1) {
					headerViewHolder.mTextViewHeaderTitle.setText("심야버스");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return convertView;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;

			try {
				if (v == null) {
					LayoutInflater li = (LayoutInflater) mMainActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					v = li.inflate(cellID, null);
					mShuttleListViewHolder = new ShuttleListViewHolder();
					mShuttleListViewHolder.mShuttleListLayout = (LinearLayout) v.findViewById(R.id.row_shuttle_list_layout);
					mShuttleListViewHolder.mTextViewShuttleName = (TextView) v.findViewById(R.id.row_shuttle_list_shuttle_name);
					mShuttleListViewHolder.mTextViewDistance = (TextView) v.findViewById(R.id.row_shuttle_list_distance);
					mShuttleListViewHolder.mTextViewDescription = (TextView) v.findViewById(R.id.row_shuttle_list_description);
					mShuttleListViewHolder.mTextViewStartTime = (TextView) v.findViewById(R.id.row_shuttle_list_start_time);
					mShuttleListViewHolder.mTextViewEndTime = (TextView) v.findViewById(R.id.row_shuttle_list_end_time);
					mShuttleListViewHolder.mTextViewNoItem = (TextView) v.findViewById(R.id.row_shuttle_list_no_item_text_view);

					v.setTag(mShuttleListViewHolder);
				} else {
					mShuttleListViewHolder = (ShuttleListViewHolder) v.getTag();
				}

				ShuttleListData shuttleItem = mShuttleListData.get(position);

				if(shuttleItem.getmBusId().isEmpty()) {
					mShuttleListViewHolder.mTextViewNoItem.setVisibility(View.VISIBLE);
					mShuttleListViewHolder.mShuttleListLayout.setVisibility(View.GONE);
				} else {
					mShuttleListViewHolder.mTextViewNoItem.setVisibility(View.GONE);
					mShuttleListViewHolder.mShuttleListLayout.setVisibility(View.VISIBLE);

					if (shuttleItem.ismIsShuttle()) { // 셔틀
						double distance = shuttleItem.getmDistance();
						String strDist;
						if(distance >= 1000) {
							distance = distance / 1000.0;
							strDist = String.format("%.2fkm", distance);
						} else {
							strDist = String.format("%.0fm", distance);
						}

						mShuttleListViewHolder.mTextViewDistance.setVisibility(View.VISIBLE);
						mShuttleListViewHolder.mTextViewDistance.setText(strDist);
						mShuttleListViewHolder.mTextViewShuttleName.setTextColor(Color.parseColor("#fff1b735"));
					} else { // 심야
						mShuttleListViewHolder.mTextViewDistance.setVisibility(View.GONE);
						mShuttleListViewHolder.mTextViewShuttleName.setTextColor(Color.parseColor("#ff704005"));
					}

					String startTime = "첫차 - " + shuttleItem.getmFirstTime();
					String endTime = "막차 - " + shuttleItem.getmLastTime();

					mShuttleListViewHolder.mTextViewShuttleName.setText(shuttleItem.getmShuttleName());
					mShuttleListViewHolder.mTextViewDescription.setText(shuttleItem.getmDescription());
					mShuttleListViewHolder.mTextViewStartTime.setText(startTime);
					mShuttleListViewHolder.mTextViewEndTime.setText(endTime);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return v;
		}

		@Override
		public long getHeaderId(int position) {
			return mCurrentShowList.get(position).ismIsShuttle() ? 0 : 1;
		}
	}

	private final Comparator<ShuttleListData> ShuttleDistanceSort = new Comparator<ShuttleListData>() {
		@Override
		public int compare(ShuttleListData box, ShuttleListData box2) {
			int comp1 = box.ismIsShuttle() ? 0 : 1;
			int comp2 = box2.ismIsShuttle() ? 0 : 1;
			int comp_id = comp1 - comp2;

			if(comp_id == 0) {
				return (int)(box.getmDistance() - box2.getmDistance());
			}

			return comp_id;
		}
	};

	@Override
	public void onStart() {
		super.onStart();

		mIsFinishFragment = false;
	}

	@Override
	public void onResume() {
		super.onResume();

		try {
			mMainActivity.changeButtonImage(mMainActivity.mImgBtnShuttle);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onPause() {
		super.onPause();

		try {
			SVUtil.hideProgressDialog();

			mIsFinishFragment = true;

			if(mShuttleAndNightBusListTask != null) {
				mShuttleAndNightBusListTask.cancel(true);
				mShuttleAndNightBusListTask = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();

		try {
			if(mView != null) {
				ViewGroup parent = (ViewGroup) mView.getParent();
				if(parent!=null) {
					parent.removeView(mView);
				}
			}

			mMainActivity.mFragmentShuttle = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		setTargetFragment(null, -1);
	}
}
