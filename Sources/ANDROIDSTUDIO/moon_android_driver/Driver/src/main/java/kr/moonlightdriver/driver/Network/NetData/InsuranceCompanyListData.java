package kr.moonlightdriver.driver.Network.NetData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by youngmin on 2015-10-29.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class InsuranceCompanyListData {
	@JsonProperty("insuranceCompanyId")
	private String mInsuranceCompanyId;

	@JsonProperty("companyName")
	private String mCompanyName;

	public InsuranceCompanyListData() {}

	public InsuranceCompanyListData(String mCompanyName, String mInsuranceCompanyId) {
		this.mCompanyName = mCompanyName;
		this.mInsuranceCompanyId = mInsuranceCompanyId;
	}

	public String getmCompanyName() {
		return mCompanyName;
	}

	public void setmCompanyName(String mCompanyName) {
		this.mCompanyName = mCompanyName;
	}

	public String getmInsuranceCompanyId() {
		return mInsuranceCompanyId;
	}

	public void setmInsuranceCompanyId(String mInsuranceCompanyId) {
		this.mInsuranceCompanyId = mInsuranceCompanyId;
	}
}
