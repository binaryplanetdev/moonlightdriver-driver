package kr.moonlightdriver.driver.Network.NetData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by youngmin on 2015-10-28.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TwoInOneListData {
	@JsonProperty("boardId")
	private String mBoardId;

	@JsonProperty("nickName")
	private String mNickName;

	@JsonProperty("title")
	private String mTitle;

	@JsonProperty("category")
	private String mCategory;

	@JsonProperty("commentCount")
	private int mCommentCount;

	@JsonProperty("createdDate")
	private Long mCreatedDate;

	public TwoInOneListData() {}

	public TwoInOneListData(String mBoardId, String mCategory, int mCommentCount, Long mCreatedDate, String mNickName, String mTitle) {
		this.mBoardId = mBoardId;
		this.mCategory = mCategory;
		this.mCommentCount = mCommentCount;
		this.mCreatedDate = mCreatedDate;
		this.mNickName = mNickName;
		this.mTitle = mTitle;
	}

	public String getmBoardId() {
		return mBoardId;
	}

	public void setmBoardId(String mBoardId) {
		this.mBoardId = mBoardId;
	}

	public String getmCategory() {
		return mCategory;
	}

	public void setmCategory(String mCategory) {
		this.mCategory = mCategory;
	}

	public int getmCommentCount() {
		return mCommentCount;
	}

	public void setmCommentCount(int mCommentCount) {
		this.mCommentCount = mCommentCount;
	}

	public Long getmCreatedDate() {
		return mCreatedDate;
	}

	public void setmCreatedDate(Long mCreatedDate) {
		this.mCreatedDate = mCreatedDate;
	}

	public String getmNickName() {
		return mNickName;
	}

	public void setmNickName(String mNickName) {
		this.mNickName = mNickName;
	}

	public String getmTitle() {
		return mTitle;
	}

	public void setmTitle(String mTitle) {
		this.mTitle = mTitle;
	}
}
