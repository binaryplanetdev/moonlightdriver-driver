package kr.moonlightdriver.driver.Network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

import kr.moonlightdriver.driver.Network.NetData.ResultData;
import kr.moonlightdriver.driver.Network.NetData.TwoInOneListData;

/**
 * Created by youngmin on 2015-10-13.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TwoInOneListAck extends NetAPI {
	@JsonProperty("result")
	private ResultData mResultData;
	@JsonProperty("list")
	private ArrayList<TwoInOneListData> mTwoInOneListData;

	public TwoInOneListAck() {}

	public TwoInOneListAck(ResultData mResultData, ArrayList<TwoInOneListData> mTwoInOneListData) {
		this.mResultData = mResultData;
		this.mTwoInOneListData = mTwoInOneListData;
	}

	public ArrayList<TwoInOneListData> getmTwoInOneListData() {
		return mTwoInOneListData;
	}

	public void setmTwoInOneListData(ArrayList<TwoInOneListData> mTwoInOneListData) {
		this.mTwoInOneListData = mTwoInOneListData;
	}

	public ResultData getmResultData() {
		return mResultData;
	}

	public void setmResultData(ResultData mResultData) {
		this.mResultData = mResultData;
	}
}
