package kr.moonlightdriver.driver.Network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

import kr.moonlightdriver.driver.Network.NetData.ResultData;
import kr.moonlightdriver.driver.Network.NetData.ShuttleLineColorData;
import kr.moonlightdriver.driver.Network.NetData.ShuttleLineData;
import kr.moonlightdriver.driver.Network.NetData.StationInfoData;

/**
 * Created by youngmin on 2015-10-13.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class NetNearShuttleStationsAck extends NetAPI {
	@JsonProperty("result")
	private ResultData mResultData;

	@JsonProperty("shuttleMarkerPoints")
	private ArrayList<StationInfoData> mShuttleMarkerPoints;

	@JsonProperty("shuttleLinePoints")
	private ArrayList<ShuttleLineData> mShuttleLinePoints;

	@JsonProperty("shuttles")
	private ArrayList<ShuttleLineColorData> mShuttleLineColorData;

	public NetNearShuttleStationsAck() {}

	public NetNearShuttleStationsAck(ResultData mResultData, ArrayList<ShuttleLineColorData> mShuttleLineColorData, ArrayList<ShuttleLineData> mShuttleLinePoints, ArrayList<StationInfoData> mShuttleMarkerPoints) {
		this.mResultData = mResultData;
		this.mShuttleLineColorData = mShuttleLineColorData;
		this.mShuttleLinePoints = mShuttleLinePoints;
		this.mShuttleMarkerPoints = mShuttleMarkerPoints;
	}

	public ResultData getmResultData() {
		return mResultData;
	}

	public void setmResultData(ResultData mResultData) {
		this.mResultData = mResultData;
	}

	public ArrayList<ShuttleLineColorData> getmShuttleLineColorData() {
		return mShuttleLineColorData;
	}

	public void setmShuttleLineColorData(ArrayList<ShuttleLineColorData> mShuttleLineColorData) {
		this.mShuttleLineColorData = mShuttleLineColorData;
	}

	public ArrayList<ShuttleLineData> getmShuttleLinePoints() {
		return mShuttleLinePoints;
	}

	public void setmShuttleLinePoints(ArrayList<ShuttleLineData> mShuttleLinePoints) {
		this.mShuttleLinePoints = mShuttleLinePoints;
	}

	public ArrayList<StationInfoData> getmShuttleMarkerPoints() {
		return mShuttleMarkerPoints;
	}

	public void setmShuttleMarkerPoints(ArrayList<StationInfoData> mShuttleMarkerPoints) {
		this.mShuttleMarkerPoints = mShuttleMarkerPoints;
	}
}
