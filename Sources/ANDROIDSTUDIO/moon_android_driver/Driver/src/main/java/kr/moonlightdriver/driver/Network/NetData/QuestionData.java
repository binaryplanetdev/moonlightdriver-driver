package kr.moonlightdriver.driver.Network.NetData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by youngmin on 2015-10-28.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class QuestionData {
	@JsonProperty("questionId")
	private String mQuestionId;

	@JsonProperty("title")
	private String mTitle;

	@JsonProperty("contents")
	private String mContents;

	@JsonProperty("seq")
	private int mSeq;

	@JsonProperty("driverId")
	private String mDriverId;

	@JsonProperty("phone")
	private String mPhone;

	@JsonProperty("answerCount")
	private int mAnswerCount;

	@JsonProperty("locations")
	private ShuttleLocationData mLocations;

	@JsonProperty("createdDate")
	private Long mCreatedDate;

	@JsonProperty("updatedDate")
	private Long mUpdatedDate;

	public QuestionData() {}

	public QuestionData(int mAnswerCount, String mContents, Long mCreatedDate, String mDriverId, ShuttleLocationData mLocations, String mPhone, String mQuestionId, int mSeq, String mTitle, Long mUpdatedDate) {
		this.mAnswerCount = mAnswerCount;
		this.mContents = mContents;
		this.mCreatedDate = mCreatedDate;
		this.mDriverId = mDriverId;
		this.mLocations = mLocations;
		this.mPhone = mPhone;
		this.mQuestionId = mQuestionId;
		this.mSeq = mSeq;
		this.mTitle = mTitle;
		this.mUpdatedDate = mUpdatedDate;
	}

	public int getmAnswerCount() {
		return mAnswerCount;
	}

	public void setmAnswerCount(int mAnswerCount) {
		this.mAnswerCount = mAnswerCount;
	}

	public String getmContents() {
		return mContents;
	}

	public void setmContents(String mContents) {
		this.mContents = mContents;
	}

	public Long getmCreatedDate() {
		return mCreatedDate;
	}

	public void setmCreatedDate(Long mCreatedDate) {
		this.mCreatedDate = mCreatedDate;
	}

	public String getmDriverId() {
		return mDriverId;
	}

	public void setmDriverId(String mDriverId) {
		this.mDriverId = mDriverId;
	}

	public ShuttleLocationData getmLocations() {
		return mLocations;
	}

	public void setmLocations(ShuttleLocationData mLocations) {
		this.mLocations = mLocations;
	}

	public String getmPhone() {
		return mPhone;
	}

	public void setmPhone(String mPhone) {
		this.mPhone = mPhone;
	}

	public String getmQuestionId() {
		return mQuestionId;
	}

	public void setmQuestionId(String mQuestionId) {
		this.mQuestionId = mQuestionId;
	}

	public int getmSeq() {
		return mSeq;
	}

	public void setmSeq(int mSeq) {
		this.mSeq = mSeq;
	}

	public String getmTitle() {
		return mTitle;
	}

	public void setmTitle(String mTitle) {
		this.mTitle = mTitle;
	}

	public Long getmUpdatedDate() {
		return mUpdatedDate;
	}

	public void setmUpdatedDate(Long mUpdatedDate) {
		this.mUpdatedDate = mUpdatedDate;
	}
}
