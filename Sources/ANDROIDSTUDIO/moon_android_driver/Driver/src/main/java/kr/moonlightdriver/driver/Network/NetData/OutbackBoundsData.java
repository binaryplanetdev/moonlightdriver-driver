package kr.moonlightdriver.driver.Network.NetData;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.operation.union.CascadedPolygonUnion;

import java.util.ArrayList;

import kr.moonlightdriver.driver.utils.SVUtil;

/**
 * Created by youngmin on 2016-01-08.
 */
public class OutbackBoundsData {
	private static final String TAG = "OutbackBoundsData";
	private int mOutbackBoundsNumber;
	private int mOutbackScreenBoundsNumber;
	private float mOutbackBoundsWidth;
	private float mOutbackBoundsHeight;
	private LatLngBounds[] mOutbackScreenBounds;
	private OutbackBoundsAreaData[][] mOutbackBoundsAreaDataList;
	private LatLngBounds mMaxExtendOutbackCheckBounds;
	private int mTotalOutbackCheckStationCount;

	public OutbackBoundsData(int _outbackBoundsNumber, int _outbackScreenBoundsNumber) {
		this.mOutbackBoundsNumber = _outbackBoundsNumber;
		this.mOutbackScreenBoundsNumber = _outbackScreenBoundsNumber;

		initDefaultData();
	}

	public void initDefaultData() {
		this.mOutbackBoundsWidth = 0f;
		this.mOutbackBoundsHeight = 0f;
		this.mOutbackScreenBounds = new LatLngBounds[this.mOutbackScreenBoundsNumber];
		this.mOutbackBoundsAreaDataList = new OutbackBoundsAreaData[this.mOutbackBoundsNumber][this.mOutbackBoundsNumber];

		this.mMaxExtendOutbackCheckBounds = null;
		this.mTotalOutbackCheckStationCount = 0;
	}

	public void createOutbackCheckBounds(LatLngBounds _currentScreen) {
		initDefaultData();

		initializeOutbackScreenBounds(_currentScreen);

		initializeNearStationBounds();

		mMaxExtendOutbackCheckBounds = mOutbackScreenBounds[mOutbackScreenBoundsNumber - 3];
	}

	private void initializeNearStationBounds() {
		for(int y = 0; y < mOutbackBoundsNumber; y++) {
			double sw_lat = getLatByIndexY(y, true);
			double ne_lat = getLatByIndexY(y, false);

			for(int x = 0; x < mOutbackBoundsNumber; x++) {
				double sw_lng = getLngByIndexX(x, true);
				double ne_lng = getLngByIndexX(x, false);

				mOutbackBoundsAreaDataList[x][y] = new OutbackBoundsAreaData(new LatLng(sw_lat, sw_lng), new LatLng(ne_lat, ne_lng));
			}
		}
	}

	private void initializeOutbackScreenBounds(LatLngBounds _currentScreen) {
		float[] screenSize = SVUtil.getScreenSize(_currentScreen);
		for(int i = 0; i < mOutbackScreenBoundsNumber; i++) {
			mOutbackBoundsWidth = (screenSize[0] / 2) * ((i * 2) + 1);
			mOutbackBoundsHeight = (screenSize[1] / 2) * ((i * 2) + 1);

			mOutbackScreenBounds[i] = SVUtil.getExtendedOutbackBounds(mOutbackBoundsWidth, mOutbackBoundsHeight, _currentScreen.getCenter());
		}
	}

	public boolean isShowOutbackPolygon() {
		int positiveCount = 0;
		int negativeCount = 0;

		boolean isShow = false;
		for(int y = 0; y < mOutbackBoundsNumber; y++) {
			for (int x = 0; x < mOutbackBoundsNumber; x++) {
				if(!mOutbackBoundsAreaDataList[x][y].ismIsStationInBounds()) {
					negativeCount++;
					isShow = true;
					addShowOutbackBoundPoint(x, y);
				} else {
					positiveCount++;
				}
			}
		}

//		SVUtil.log("error", TAG, "isShowOutbackPolygon() : true(" + positiveCount + "); false(" + negativeCount + ");");

		return isShow;
	}

	private void addShowOutbackBoundPoint(int _x, int _y) {
		for(int dy = _y - 1; dy <= _y + 1; dy++) {
			if(dy < 0 || dy >= this.mOutbackBoundsNumber) {
				continue;
			}

			for(int dx = _x - 1; dx <= _x + 1; dx++) {
				if(dx < 0 || dx >=  this.mOutbackBoundsNumber) {
					continue;
				}

				mOutbackBoundsAreaDataList[dx][dy].setmIsShowingPolygonArea(true);
			}
		}
	}

	public void addToOutbackBoundsAreaList(LatLng _stationPosition) {
		if(_stationPosition == null) {
			return;
		}

		for(int y = 0; y < mOutbackBoundsNumber; y++) {
			for(int x = 0; x < mOutbackBoundsNumber; x++) {
				if(mOutbackBoundsAreaDataList[x][y].getmOutbackCheckBounds().contains(_stationPosition)) {
					mTotalOutbackCheckStationCount++;
					mOutbackBoundsAreaDataList[x][y].addOutbackCheckStation(_stationPosition);
					break;
				}
			}
		}
	}

	public Geometry getPolygonsByStation(double _radius, double _holePoints) {
		long startTime = System.currentTimeMillis();

		ArrayList<Polygon> retPolygonList = new ArrayList<>();
		Coordinate[] coordinatesArray;
		GeometryFactory fact = new GeometryFactory();

		for(int y = 0; y < mOutbackBoundsNumber; y++) {
			for(int x = 0; x < mOutbackBoundsNumber; x++) {
				if(mOutbackBoundsAreaDataList[x][y].ismIsShowingPolygonArea()) {
					int listSize = mOutbackBoundsAreaDataList[x][y].getmOutbackCheckStationList().size();
					for(int i = 0; i < listSize; i++) {
						coordinatesArray = SVUtil.getCircleCoordinates(mOutbackBoundsAreaDataList[x][y].getmOutbackCheckStationList().get(i), _radius, _holePoints); // circle radius
						retPolygonList.add(new Polygon(new GeometryFactory().createLinearRing(coordinatesArray), null, fact));
					}
				} else if(!mOutbackBoundsAreaDataList[x][y].ismIsDoneDrawingPolygonArea()){
					coordinatesArray = mOutbackBoundsAreaDataList[x][y].getPolygonCoordinates();
					retPolygonList.add(new Polygon(new GeometryFactory().createLinearRing(coordinatesArray), null, fact));
					mOutbackBoundsAreaDataList[x][y].setmIsDoneDrawingPolygonArea(true);
				}
			}
		}

		long execTime = System.currentTimeMillis() - startTime;
		int seconds = (int) Math.floor(execTime / 1000);
		int milliseconds = (int) Math.floor(execTime % 1000);

//		SVUtil.log("error", TAG, "getPolygonsByStation() execution time : " + (seconds + "." + milliseconds));

		if(retPolygonList.size() > 0) {
			return CascadedPolygonUnion.union(retPolygonList);
		}

		return null;
	}

	public boolean isScreenInMaxOutbackCheckBounds(int _index, LatLngBounds _screenBounds) {
		if(mOutbackScreenBounds == null || mOutbackScreenBounds.length <= _index) {
			return false;
		}

		LatLngBounds checkBounds = mOutbackScreenBounds[mOutbackScreenBounds.length - _index];

		if(checkBounds == null) {
			return false;
		}

		LatLng[] screenVertex = new LatLng[] {
			_screenBounds.northeast,
			new LatLng(_screenBounds.northeast.latitude, _screenBounds.southwest.longitude),
			_screenBounds.southwest,
			new LatLng(_screenBounds.southwest.latitude, _screenBounds.northeast.longitude)
		};

		boolean isScreenInBounds = true;
		for(LatLng vertex : screenVertex) {
			if(!checkBounds.contains(vertex)) {
				isScreenInBounds = false;
				break;
			}
		}

		return isScreenInBounds;
	}

	private double getLatByIndexY(int _y, boolean _isSW) {
		int index = (mOutbackBoundsNumber / 2);

		if(_isSW) {
			index = index - 1;
		}

		index = index - _y;

		boolean isFromNE = true;
		if(index < 0) {
			index = Math.abs(index + 1);
			isFromNE = false;
		}

		if(index >= mOutbackBoundsNumber || index < 0) {
			return 0;
		}

		if(isFromNE) {
			return mOutbackScreenBounds[index].northeast.latitude;
		}

		return mOutbackScreenBounds[index].southwest.latitude;
	}

	private double getLngByIndexX(int _x, boolean _isSW) {
		int index = (mOutbackBoundsNumber / 2);

		if(!_isSW) {
			index = index - 1;
		}

		index = index - _x;

		boolean isFromSW = true;
		if(index < 0) {
			index = Math.abs(index + 1);
			isFromSW = false;
		}

		if(index >= mOutbackBoundsNumber || index < 0) {
			return 0;
		}

		if(isFromSW) {
			return mOutbackScreenBounds[index].southwest.longitude;
		}

		return mOutbackScreenBounds[index].northeast.longitude;
	}

	public float getMaxSearchRange() {
		return this.mOutbackBoundsWidth;
	}

	public LatLngBounds getCheckExtendOutbackCheckBounds() {
		return mOutbackScreenBounds[mOutbackScreenBoundsNumber - 1];
	}

	public LatLngBounds getmMaxExtendOutbackCheckBounds() {
		return mMaxExtendOutbackCheckBounds;
	}

	public void setmMaxExtendOutbackCheckBounds(LatLngBounds mMaxExtendOutbackCheckBounds) {
		this.mMaxExtendOutbackCheckBounds = mMaxExtendOutbackCheckBounds;
	}

	public OutbackBoundsAreaData[][] getmOutbackBoundsAreaDataList() {
		return mOutbackBoundsAreaDataList;
	}

	public void setmOutbackBoundsAreaDataList(OutbackBoundsAreaData[][] mOutbackBoundsAreaDataList) {
		this.mOutbackBoundsAreaDataList = mOutbackBoundsAreaDataList;
	}

	public float getmOutbackBoundsHeight() {
		return mOutbackBoundsHeight;
	}

	public void setmOutbackBoundsHeight(float mOutbackBoundsHeight) {
		this.mOutbackBoundsHeight = mOutbackBoundsHeight;
	}

	public int getmOutbackBoundsNumber() {
		return mOutbackBoundsNumber;
	}

	public void setmOutbackBoundsNumber(int mOutbackBoundsNumber) {
		this.mOutbackBoundsNumber = mOutbackBoundsNumber;
	}

	public float getmOutbackBoundsWidth() {
		return mOutbackBoundsWidth;
	}

	public void setmOutbackBoundsWidth(float mOutbackBoundsWidth) {
		this.mOutbackBoundsWidth = mOutbackBoundsWidth;
	}

	public LatLngBounds[] getmOutbackScreenBounds() {
		return mOutbackScreenBounds;
	}

	public void setmOutbackScreenBounds(LatLngBounds[] mOutbackScreenBounds) {
		this.mOutbackScreenBounds = mOutbackScreenBounds;
	}

	public int getmOutbackScreenBoundsNumber() {
		return mOutbackScreenBoundsNumber;
	}

	public void setmOutbackScreenBoundsNumber(int mOutbackScreenBoundsNumber) {
		this.mOutbackScreenBoundsNumber = mOutbackScreenBoundsNumber;
	}

	public int getmTotalOutbackCheckStationCount() {
		return mTotalOutbackCheckStationCount;
	}

	public void setmTotalOutbackCheckStationCount(int mTotalOutbackCheckStationCount) {
		this.mTotalOutbackCheckStationCount = mTotalOutbackCheckStationCount;
	}
}
