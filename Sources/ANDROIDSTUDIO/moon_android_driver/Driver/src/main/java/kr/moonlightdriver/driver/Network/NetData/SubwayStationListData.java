package kr.moonlightdriver.driver.Network.NetData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by youngmin on 2015-10-30.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SubwayStationListData {
	@JsonProperty("stationCode")
	private String mStationCode;

	@JsonProperty("stationName")
	private String mStationName;

	@JsonProperty("location")
	private ShuttleLocationData mLocation;

	@JsonProperty("stationCodeDaum")
	private String mStationCodeDaum;

	@JsonProperty("distance")
	private double mDistance;

	@JsonProperty("firstTime")
	private String mFirstTime;

	@JsonProperty("lastTime")
	private String mLastTime;

	public SubwayStationListData() {}

	public SubwayStationListData(double mDistance, String mFirstTime, String mLastTime, ShuttleLocationData mLocation, String mStationCode, String mStationCodeDaum, String mStationName) {
		this.mDistance = mDistance;
		this.mFirstTime = mFirstTime;
		this.mLastTime = mLastTime;
		this.mLocation = mLocation;
		this.mStationCode = mStationCode;
		this.mStationCodeDaum = mStationCodeDaum;
		this.mStationName = mStationName;
	}

	public double getmDistance() {
		return mDistance;
	}

	public void setmDistance(double mDistance) {
		this.mDistance = mDistance;
	}

	public String getmFirstTime() {
		return mFirstTime;
	}

	public void setmFirstTime(String mFirstTime) {
		this.mFirstTime = mFirstTime;
	}

	public String getmLastTime() {
		return mLastTime;
	}

	public void setmLastTime(String mLastTime) {
		this.mLastTime = mLastTime;
	}

	public ShuttleLocationData getmLocation() {
		return mLocation;
	}

	public void setmLocation(ShuttleLocationData mLocation) {
		this.mLocation = mLocation;
	}

	public String getmStationCode() {
		return mStationCode;
	}

	public void setmStationCode(String mStationCode) {
		this.mStationCode = mStationCode;
	}

	public String getmStationCodeDaum() {
		return mStationCodeDaum;
	}

	public void setmStationCodeDaum(String mStationCodeDaum) {
		this.mStationCodeDaum = mStationCodeDaum;
	}

	public String getmStationName() {
		return mStationName;
	}

	public void setmStationName(String mStationName) {
		this.mStationName = mStationName;
	}
}
