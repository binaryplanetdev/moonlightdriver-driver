package kr.moonlightdriver.driver.Network.NetData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by youngmin on 2015-11-05.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShuttlePathPointData {
	@JsonProperty("shuttlePointsId")
	private String mShuttlePointId;
	@JsonProperty("busId")
	private String mBusId;
	@JsonProperty("index")
	private int mShuttlePointSequence;
	@JsonProperty("text")
	private String mShuttlePointName;
	@JsonProperty("arsId")
	private String mArsId;
	@JsonProperty("firstTime")
	private String mFirstTime;
	@JsonProperty("lastTime")
	private String mLastTime;
	@JsonProperty("exceptOutback")
	private int mExceptOutback;
	@JsonProperty("locations")
	private ShuttleLocationData mShuttleLocationData;

	public ShuttlePathPointData() {}

	public ShuttlePathPointData(String mArsId, String mBusId, int mExceptOutback, String mFirstTime, String mLastTime, ShuttleLocationData mShuttleLocationData, String mShuttlePointId, String mShuttlePointName, int mShuttlePointSequence) {
		this.mArsId = mArsId;
		this.mBusId = mBusId;
		this.mExceptOutback = mExceptOutback;
		this.mFirstTime = mFirstTime;
		this.mLastTime = mLastTime;
		this.mShuttleLocationData = mShuttleLocationData;
		this.mShuttlePointId = mShuttlePointId;
		this.mShuttlePointName = mShuttlePointName;
		this.mShuttlePointSequence = mShuttlePointSequence;
	}

	public int getmExceptOutback() {
		return mExceptOutback;
	}

	public void setmExceptOutback(int mExceptOutback) {
		this.mExceptOutback = mExceptOutback;
	}

	public String getmArsId() {
		return mArsId;
	}

	public void setmArsId(String mArsId) {
		this.mArsId = mArsId;
	}

	public String getmBusId() {
		return mBusId;
	}

	public void setmBusId(String mBusId) {
		this.mBusId = mBusId;
	}

	public String getmFirstTime() {
		return mFirstTime;
	}

	public void setmFirstTime(String mFirstTime) {
		this.mFirstTime = mFirstTime;
	}

	public String getmLastTime() {
		return mLastTime;
	}

	public void setmLastTime(String mLastTime) {
		this.mLastTime = mLastTime;
	}

	public ShuttleLocationData getmShuttleLocationData() {
		return mShuttleLocationData;
	}

	public void setmShuttleLocationData(ShuttleLocationData mShuttleLocationData) {
		this.mShuttleLocationData = mShuttleLocationData;
	}

	public String getmShuttlePointId() {
		return mShuttlePointId;
	}

	public void setmShuttlePointId(String mShuttlePointId) {
		this.mShuttlePointId = mShuttlePointId;
	}

	public String getmShuttlePointName() {
		return mShuttlePointName;
	}

	public void setmShuttlePointName(String mShuttlePointName) {
		this.mShuttlePointName = mShuttlePointName;
	}

	public int getmShuttlePointSequence() {
		return mShuttlePointSequence;
	}

	public void setmShuttlePointSequence(int mShuttlePointSequence) {
		this.mShuttlePointSequence = mShuttlePointSequence;
	}
}
