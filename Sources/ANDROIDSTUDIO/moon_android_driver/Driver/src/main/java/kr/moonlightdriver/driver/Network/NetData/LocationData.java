package kr.moonlightdriver.driver.Network.NetData;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by youngmin on 2015-12-31.
 */
public class LocationData {
	private String mLocationName;
	private LatLng mPosition;

	public LocationData() {}

	public LocationData(String _locationName, LatLng _position) {
		this.mLocationName = _locationName;
		this.mPosition = _position;
	}

	public String getmLocationName() {
		return mLocationName;
	}

	public void setmLocationName(String mLocationName) {
		this.mLocationName = mLocationName;
	}

	public LatLng getmPosition() {
		return mPosition;
	}

	public void setmPosition(LatLng mPosition) {
		this.mPosition = mPosition;
	}
}
