package kr.moonlightdriver.driver.Network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

import kr.moonlightdriver.driver.Network.NetData.BusRouteInfoData;
import kr.moonlightdriver.driver.Network.NetData.BusStationListData;
import kr.moonlightdriver.driver.Network.NetData.ResultData;

/**
 * Created by youngmin on 2015-10-13.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BusStationListByBusRouteIdAck extends NetAPI {
	@JsonProperty("result")
	private ResultData mResultData;

	@JsonProperty("busRouteInfo")
	private BusRouteInfoData mBusRouteDetailData;

	@JsonProperty("busStationList")
	private ArrayList<BusStationListData> mBusStationDataList;

	public BusStationListByBusRouteIdAck() {}

	public BusStationListByBusRouteIdAck(BusRouteInfoData mBusRouteDetailData, ArrayList<BusStationListData> mBusStationDataList, ResultData mResultData) {
		this.mBusRouteDetailData = mBusRouteDetailData;
		this.mBusStationDataList = mBusStationDataList;
		this.mResultData = mResultData;
	}

	public ResultData getmResultData() {
		return mResultData;
	}

	public void setmResultData(ResultData mResultData) {
		this.mResultData = mResultData;
	}

	public BusRouteInfoData getmBusRouteDetailData() {
		return mBusRouteDetailData;
	}

	public void setmBusRouteDetailData(BusRouteInfoData mBusRouteDetailData) {
		this.mBusRouteDetailData = mBusRouteDetailData;
	}

	public ArrayList<BusStationListData> getmBusStationDataList() {
		return mBusStationDataList;
	}

	public void setmBusStationDataList(ArrayList<BusStationListData> mBusStationDataList) {
		this.mBusStationDataList = mBusStationDataList;
	}
}
