package kr.moonlightdriver.driver.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.MacAddress;
import com.estimote.sdk.Region;
import com.google.android.gms.maps.model.LatLng;

import org.java_websocket.exceptions.WebsocketNotConnectedException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import kr.moonlightdriver.driver.Network.NetApi.BeaconDataAck;
import kr.moonlightdriver.driver.Network.NetApi.NetAPI;
import kr.moonlightdriver.driver.Network.NetApi.NetResponse;
import kr.moonlightdriver.driver.Network.NetApi.NetResponseCallback;
import kr.moonlightdriver.driver.Network.NetClient.NetClient;
import kr.moonlightdriver.driver.Network.NetData.BeaconData;
import kr.moonlightdriver.driver.Network.NetData.ResultData;
import kr.moonlightdriver.driver.R;
import kr.moonlightdriver.driver.activity.MainActivity;

public class Application extends android.app.Application {

    private BeaconManager beaconManager;
    private Region region;
    private int status = 0;
    public static LatLng mNewlocation = null;
    private LatLng mLastLocation = null;

    private SockJSImpl sockJS;
    private Timer timer;
    private String mShuttleId;
    private String mBusNumId;
    private String driverId;
    private MacAddress beaconMacAddress;

    public void onCreate() {
        super.onCreate();

        TelephonyManager tMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        driverId = tMgr.getLine1Number();

        //connectSockJS();
        beaconManager = new BeaconManager(getApplicationContext());
        region = new Region(
                "beacon",
                UUID.fromString("fda50693-a4e2-4fb1-afcf-c6eb07647825"), // 본인이 연결할 Beacon의 ID와 Major / Minor Code를 알아야 한다.
                null, null);

        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                beaconManager.startMonitoring(region);
            }
        });


        beaconManager.setRangingListener(new BeaconManager.RangingListener() {
            @Override
            public void onBeaconsDiscovered(Region region, List<Beacon> list) {
                if (!list.isEmpty()) {
                    Beacon nearestBeacon = list.get(0);
                    Log.d("Airport", "Nearest places: " + nearestBeacon.getRssi()+ " / major = " + list.get(0).getMajor() + " / minor" + list.get(0).getMinor() + " / " + nearestBeacon.getMacAddress());

                    //Log.d("aa","lat=" + mLastLocation.latitude + "&lng=" + mLastLocation.longitude);
                    if (list.get(0).getRssi() > -80) {
                        if(!beaconMacAddress.toStandardString().equals(nearestBeacon.getMacAddress().toStandardString())) {
                            beaconMacAddress = nearestBeacon.getMacAddress();
                            getBeaconData(beaconMacAddress.toStandardString());
                        }
                        if (status == 0) {
                            showNotification("달빛기사", "탑승중입니다.");
                            //connectSockJS();
                        }
                        status = 1;
                        checkGPSChange();

                    } else {
                        if (status == 1)
                            showNotification("달빛기사", "하차하셨습니다.");
                        status = 0;
                    }
                }
            }
        });

        beaconManager.setMonitoringListener(new BeaconManager.MonitoringListener() {

            @Override
            public void onEnteredRegion(Region r, List<Beacon> list) {
                showNotification("달빛기사", "승차알림 - 근처에 버스가 있습니다.");
                getBeaconData(list.get(0).getMacAddress().toStandardString());
                beaconMacAddress = list.get(0).getMacAddress();
                beaconManager.startRanging(region);
//              getApplicationContext().startActivity(new Intent(getApplicationContext(), PopupActivity.class).putExtra("uuid", String.valueOf(list.get(0).getProximityUUID()) ).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }

            @Override
            public void onExitedRegion(Region r) {
                showNotification("달빛기사", "하차하셨습니다.");
                beaconManager.stopRanging(region);
                try {
                    timer.cancel();
                    sockJS.closeSession();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

    }

    private void checkGPSChange() {
        if(mNewlocation == null) {

        }else {
            if (mLastLocation == null) {
                mLastLocation = mNewlocation;
                Log.e("aa", "lat=" + mLastLocation.latitude + "&lng=" + mLastLocation.longitude + " / " + mBusNumId + "/" + driverId);

            } else if (!mNewlocation.equals(mLastLocation)) {
                mLastLocation = mNewlocation;
                Log.e("aa", "lat=" + mLastLocation.latitude + "&lng=" + mLastLocation.longitude + " / " + mBusNumId + "/" + driverId);
                try {
                    if (sockJS != null)
                        sockJS.send(sendGPS());
                }catch (Exception e){
                    try {
                        timer.cancel();
                    }catch (NullPointerException e2){
                        sockJS = null;
                    }
                    e.printStackTrace();
                }
            }
        }
    }

    public void showNotification(String title, String message) {
        Intent notifyIntent = new Intent(this, MainActivity.class);
        notifyIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivities(this, 0,
                new Intent[]{notifyIntent}, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification notification = new Notification.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .build();
//        notification.defaults |= Notification.DEFAULT_SOUND;
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(100, notification);
    }

    private void connectSockJS() {
        SVUtil.log("error", "SockJS","connectSockJS");
        if(sockJS != null){
            try {
                timer.cancel();
                sockJS.closeSession();
            }catch (NullPointerException e){
                e.printStackTrace();
            }
        }
        try {
            sockJS = new SockJSImpl(Global.WOKITIKI_SERVER_HOST + "/eventbus", "shuttlepoint", mBusNumId, false) {
                @Override
                public void scheduleHeartbeat() {
                    try {
                        timer = new Timer();
                        timer.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                String ping = "[\"{\\\"type\\\":\\\"ping\\\"}\"]";
                                try {
                                    checkGPSChange();
                                    send(ping);
                                    scheduleHeartbeat();
                                } catch (WebsocketNotConnectedException e) {
                                    e.printStackTrace();
                                    sockJS = null;
                                    timer.cancel();
                                    connectSockJS();
                                }
                            }
                        }, 3000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void parseSockJS(String s) {
                    // ignode;
                    Log.e("Application", s);
                }

            };

            sockJS.connectBlocking();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public JSONObject sendGPS() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("type", "publish");
            obj.put("address", "to.server.channel");
            JSONObject body = new JSONObject();
            body.put("type", "shuttlePoint");
            body.put("channel_id", mShuttleId);
            body.put("senderId", driverId);
            body.put("senderNick", driverId);
            body.put("isPrivateMsg", false);
            body.put("beaconMac", beaconMacAddress.toStandardString());
            body.put("msg", mLastLocation.latitude + "&" + mLastLocation.longitude + "&"+mBusNumId);
            obj.put("body", body);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("send");

        return obj;
    }


    public void getBeaconData(String beaconMac){
        SVUtil.log("error", "TAG", "정보 요청");
        try {
            NetClient.send(getApplicationContext(), Global.URL_SHUTTLE_BEACON + "/" + beaconMac, "GET", null, new BeaconDataAck(), new NetResponseCallback(new NetResponse() {
                @Override
                public void onResponse(NetAPI _netAPI) {
                    try {
                        if (_netAPI != null) {
                            BeaconDataAck retNetApi = (BeaconDataAck) _netAPI;
                            ResultData result = retNetApi.getmResultData();

                            if (result.getmCode() == 100) {
                                BeaconData beaconData = retNetApi.getmBeaconData();
                                //setLocationUpdateTime(_newLocation);
                                SVUtil.log("error", "TAG", beaconData.getmBusId() + " // " + beaconData.getmBusId());

                                mShuttleId = beaconData.getmBusId();
                                mBusNumId = beaconData.getmBusNum();
                                connectSockJS();
                            } else {
                                SVUtil.log("error", "TAG", result.getmDetail());
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
