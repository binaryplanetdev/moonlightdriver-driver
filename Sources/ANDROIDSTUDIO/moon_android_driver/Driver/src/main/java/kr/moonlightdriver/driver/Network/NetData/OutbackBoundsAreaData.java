package kr.moonlightdriver.driver.Network.NetData;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.vividsolutions.jts.geom.Coordinate;

import java.util.ArrayList;

import kr.moonlightdriver.driver.utils.SVUtil;

/**
 * Created by youngmin on 2016-01-08.
 */
public class OutbackBoundsAreaData {
	private static final String TAG = "OutbackBoundsAreaData";
	private LatLngBounds mOutbackCheckBounds;
	private boolean mIsStationInBounds;
	private boolean mIsShowingPolygonArea;
	private boolean mIsDoneDrawingPolygonArea;
	private ArrayList<LatLng> mOutbackCheckStationList;

	public OutbackBoundsAreaData(LatLng _southWest, LatLng _northEast) {
		this.mOutbackCheckBounds = new LatLngBounds(_southWest, _northEast);
		this.mIsStationInBounds = false;
		this.mIsDoneDrawingPolygonArea = false;
		this.mIsShowingPolygonArea = false;
		this.mOutbackCheckStationList = new ArrayList<>();
	}

	public void addOutbackCheckStation(LatLng _stationPoint) {
		mOutbackCheckStationList.add(_stationPoint);
		mIsStationInBounds = true;
	}

	public Coordinate[] getPolygonCoordinates() {
		return new Coordinate[] {
			new Coordinate(mOutbackCheckBounds.northeast.longitude, mOutbackCheckBounds.northeast.latitude),
			new Coordinate(mOutbackCheckBounds.southwest.longitude, mOutbackCheckBounds.northeast.latitude),
			new Coordinate(mOutbackCheckBounds.southwest.longitude, mOutbackCheckBounds.southwest.latitude),
			new Coordinate(mOutbackCheckBounds.northeast.longitude, mOutbackCheckBounds.southwest.latitude),
			new Coordinate(mOutbackCheckBounds.northeast.longitude, mOutbackCheckBounds.northeast.latitude)
		};
	}

	public boolean ismIsShowingPolygonArea() {
		return mIsShowingPolygonArea;
	}

	public void setmIsShowingPolygonArea(boolean mIsShowingPolygonArea) {
		this.mIsShowingPolygonArea = mIsShowingPolygonArea;
	}

	public boolean ismIsDoneDrawingPolygonArea() {
		return mIsDoneDrawingPolygonArea;
	}

	public void setmIsDoneDrawingPolygonArea(boolean mIsDoneDrawingPolygonArea) {
		this.mIsDoneDrawingPolygonArea = mIsDoneDrawingPolygonArea;
	}

	public boolean ismIsStationInBounds() {
		return mIsStationInBounds;
	}

	public void setmIsStationInBounds(boolean mIsStationInBounds) {
		this.mIsStationInBounds = mIsStationInBounds;
	}

	public LatLngBounds getmOutbackCheckBounds() {
		return mOutbackCheckBounds;
	}

	public void setmOutbackCheckBounds(LatLngBounds mOutbackCheckBounds) {
		this.mOutbackCheckBounds = mOutbackCheckBounds;
	}

	public ArrayList<LatLng> getmOutbackCheckStationList() {
		return mOutbackCheckStationList;
	}

	public void setmOutbackCheckStationList(ArrayList<LatLng> mOutbackCheckStationList) {
		this.mOutbackCheckStationList = mOutbackCheckStationList;
	}
}
