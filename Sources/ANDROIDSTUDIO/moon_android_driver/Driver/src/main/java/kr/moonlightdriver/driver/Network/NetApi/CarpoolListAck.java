package kr.moonlightdriver.driver.Network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

import kr.moonlightdriver.driver.Network.NetData.CarpoolData;
import kr.moonlightdriver.driver.Network.NetData.DriverPointData;
import kr.moonlightdriver.driver.Network.NetData.ResultData;

/**
 * Created by youngmin on 2015-10-13.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CarpoolListAck extends NetAPI {
	@JsonProperty("result")
	private ResultData mResultData;
	@JsonProperty("list")
	private ArrayList<CarpoolData> mCarpoolList;
	@JsonProperty("driverPoints")
	private ArrayList<DriverPointData> mDriverPointData;
	@JsonProperty("driverList")
	private ArrayList<DriverPointData> mNearDriverList;

	public CarpoolListAck() {}

	public CarpoolListAck(ArrayList<CarpoolData> mCarpoolList, ArrayList<DriverPointData> mDriverPointData, ArrayList<DriverPointData> mNearDriverList, ResultData mResultData) {
		this.mCarpoolList = mCarpoolList;
		this.mDriverPointData = mDriverPointData;
		this.mNearDriverList = mNearDriverList;
		this.mResultData = mResultData;
	}

	public ArrayList<CarpoolData> getmCarpoolList() {
		return mCarpoolList;
	}

	public void setmCarpoolList(ArrayList<CarpoolData> mCarpoolList) {
		this.mCarpoolList = mCarpoolList;
	}

	public ArrayList<DriverPointData> getmDriverPointData() {
		return mDriverPointData;
	}

	public void setmDriverPointData(ArrayList<DriverPointData> mDriverPointData) {
		this.mDriverPointData = mDriverPointData;
	}

	public ArrayList<DriverPointData> getmNearDriverList() {
		return mNearDriverList;
	}

	public void setmNearDriverList(ArrayList<DriverPointData> mNearDriverList) {
		this.mNearDriverList = mNearDriverList;
	}

	public ResultData getmResultData() {
		return mResultData;
	}

	public void setmResultData(ResultData mResultData) {
		this.mResultData = mResultData;
	}
}
