package kr.moonlightdriver.driver.Network.NetData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by youngmin on 2015-10-30.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BusRouteListDataByStationId {
	@JsonProperty("busRouteId")
	private String mBusRouteId;

	@JsonProperty("stationSequence")
	private int mStationSequence;

	@JsonProperty("direction")
	private String mDirection;

	@JsonProperty("busRouteName")
	private String mBusRouteName;

	@JsonProperty("busRouteType")
	private int mBusRouteType;

	@JsonProperty("busRouteTypeName")
	private String mBusRouteTypeName;

	@JsonProperty("localBusRouteId")
	private String mLocalBusRouteId;

	@JsonProperty("busRouteArea")
	private String mBusRouteArea;

	@JsonProperty("nextStation")
	private String mNextStation;

	public BusRouteListDataByStationId() {}

	public BusRouteListDataByStationId(String mBusRouteArea, String mBusRouteId, String mBusRouteName, int mBusRouteType, String mBusRouteTypeName, String mDirection, String mLocalBusRouteId, String mNextStation, int mStationSequence) {
		this.mBusRouteArea = mBusRouteArea;
		this.mBusRouteId = mBusRouteId;
		this.mBusRouteName = mBusRouteName;
		this.mBusRouteType = mBusRouteType;
		this.mBusRouteTypeName = mBusRouteTypeName;
		this.mDirection = mDirection;
		this.mLocalBusRouteId = mLocalBusRouteId;
		this.mNextStation = mNextStation;
		this.mStationSequence = mStationSequence;
	}

	public String getmBusRouteId() {
		return mBusRouteId;
	}

	public void setmBusRouteId(String mBusRouteId) {
		this.mBusRouteId = mBusRouteId;
	}

	public String getmBusRouteName() {
		return mBusRouteName;
	}

	public void setmBusRouteName(String mBusRouteName) {
		this.mBusRouteName = mBusRouteName;
	}

	public String getmBusRouteTypeName() {
		return mBusRouteTypeName;
	}

	public void setmBusRouteTypeName(String mBusRouteTypeName) {
		this.mBusRouteTypeName = mBusRouteTypeName;
	}

	public String getmDirection() {
		return mDirection;
	}

	public void setmDirection(String mDirection) {
		this.mDirection = mDirection;
	}

	public String getmNextStation() {
		return mNextStation;
	}

	public void setmNextStation(String mNextStation) {
		this.mNextStation = mNextStation;
	}

	public int getmStationSequence() {
		return mStationSequence;
	}

	public void setmStationSequence(int mStationSequence) {
		this.mStationSequence = mStationSequence;
	}

	public int getmBusRouteType() {
		return mBusRouteType;
	}

	public void setmBusRouteType(int mBusRouteType) {
		this.mBusRouteType = mBusRouteType;
	}

	public String getmLocalBusRouteId() {
		return mLocalBusRouteId;
	}

	public void setmLocalBusRouteId(String mLocalBusRouteId) {
		this.mLocalBusRouteId = mLocalBusRouteId;
	}

	public String getmBusRouteArea() {
		return mBusRouteArea;
	}

	public void setmBusRouteArea(String mBusRouteArea) {
		this.mBusRouteArea = mBusRouteArea;
	}
}
