package kr.moonlightdriver.driver.fragment;

import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import kr.moonlightdriver.driver.Network.NetApi.NetAPI;
import kr.moonlightdriver.driver.Network.NetApi.NetResponse;
import kr.moonlightdriver.driver.Network.NetApi.NetResponseCallback;
import kr.moonlightdriver.driver.Network.NetApi.NoResponseDataAck;
import kr.moonlightdriver.driver.Network.NetApi.TwoInOneContentDetailAck;
import kr.moonlightdriver.driver.Network.NetClient.NetClient;
import kr.moonlightdriver.driver.Network.NetData.ResultData;
import kr.moonlightdriver.driver.Network.NetData.TwoInOneCommentData;
import kr.moonlightdriver.driver.Network.NetData.TwoInOneContentDetailData;
import kr.moonlightdriver.driver.R;
import kr.moonlightdriver.driver.activity.MainActivity;
import kr.moonlightdriver.driver.utils.Global;
import kr.moonlightdriver.driver.utils.SVUtil;

public class TwoInOneDetailFragment extends Fragment {
	private static final String TAG = "ContentInfoActivity";
	private static final int LIST_COUNT_PER_PAGE = 10;

	private ImageView mCategoryIcon;
	private TextView mTitle;
	private TextView mDate;
	private TextView mCommentCount;
	private TextView mNickName;
	private TextView mSex;
	private TextView mAge;
	private TextView mCareer;
	private TextView mDivide;
	private TextView mRange;
	private TextView mStart;
	private TextView mTime;
	private TextView mTarget;
	private TextView mCar;
	private TextView mContact;
	private TextView mEtc;
	private TextView mBottomCommentCount;
	private EditText mETNickName;
	private EditText mETComment;
	private CommentAdapter m2in1CommentAdapter;
	private ArrayList<TwoInOneCommentData> m2in1CommentList;
	private ArrayList<TwoInOneCommentData> m2in1ShowCommentList;
	private ImageButton mBtnPrev;
	private ImageButton mBtnNext;
	private TextView mPageText;
	private LinearLayout mButtonWrapper;
	private Button mBtnEdit;
	private Button mBtnDelete;

	public String mBoardId;

	private MainActivity mMainActivity;

	private int mTotalPageCount;
	private int mCurrentPage;

	private View mView;

	private boolean mIsFinishFragment;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		try {
			mView = inflater.inflate(R.layout.frag_2in1_detail, container, false);

			mMainActivity = (MainActivity) getActivity();

			Bundle bundle = TwoInOneDetailFragment.this.getArguments();
			mBoardId = bundle.getString("boardId", "");

			mIsFinishFragment = false;
			mTotalPageCount = 0;
			mCurrentPage = 0;
			m2in1CommentList = new ArrayList<>();
			m2in1ShowCommentList = new ArrayList<>();

			ListView commentListView = (ListView) mView.findViewById(R.id.content_info_comment_list);
			ImageButton btnBack = (ImageButton) mView.findViewById(R.id.content_info_btn_back);

			View header = inflater.inflate(R.layout.frag_2in1_detail_header, null, false);
			View footer = inflater.inflate(R.layout.frag_2in1_detail_footer, null, false);

			commentListView.addHeaderView(header);
			commentListView.addFooterView(footer);

			m2in1CommentAdapter = new CommentAdapter(mMainActivity, R.layout.row_2in1_comment, m2in1ShowCommentList);
			commentListView.setAdapter(m2in1CommentAdapter);

			mCategoryIcon = (ImageView) header.findViewById(R.id.icon_2in1_content_info);
			mTitle = (TextView) header.findViewById(R.id.title_2in1_content_info);
			mDate = (TextView) header.findViewById(R.id.date_2in1_content_info);
			mCommentCount = (TextView) header.findViewById(R.id.comment_2in1_content_info);
			mNickName = (TextView) header.findViewById(R.id.content_info_nickname);
			mSex = (TextView) header.findViewById(R.id.content_info_sex);
			mAge = (TextView) header.findViewById(R.id.content_info_age);
			mCareer = (TextView) header.findViewById(R.id.content_info_career);
			mDivide = (TextView) header.findViewById(R.id.content_info_divide);
			mRange = (TextView) header.findViewById(R.id.content_info_range);
			mStart = (TextView) header.findViewById(R.id.content_info_start);
			mTime = (TextView) header.findViewById(R.id.content_info_time);
			mTarget = (TextView) header.findViewById(R.id.content_info_target);
			mCar = (TextView) header.findViewById(R.id.content_info_car);
			mContact = (TextView) header.findViewById(R.id.content_info_contact);
			mEtc = (TextView) header.findViewById(R.id.content_info_etc);
			mBottomCommentCount = (TextView) header.findViewById(R.id.content_info_comment_count);
			mButtonWrapper = (LinearLayout) header.findViewById(R.id.detail_2in1_button_wrapper);
			mBtnEdit = (Button) header.findViewById(R.id.detail_2in1_editBtn);
			mBtnDelete = (Button) header.findViewById(R.id.detail_2in1_deleteBtn);

			mETNickName = (EditText) footer.findViewById(R.id.et_content_nick_name);
			mETComment = (EditText) footer.findViewById(R.id.et_content_comment);
			Button btnRegisterComment = (Button) footer.findViewById(R.id.btn_content_info_comment);
			mBtnPrev = (ImageButton) footer.findViewById(R.id.btn_content_info_prev);
			mBtnNext = (ImageButton) footer.findViewById(R.id.btn_content_info_next);
			mPageText = (TextView) footer.findViewById(R.id.content_info_page);

			mBtnPrev.setSelected(false);
			mBtnPrev.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						if (mBtnPrev.isSelected()) {
							mCurrentPage--;
							showCommentList();
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			mBtnNext.setSelected(false);
			mBtnNext.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						if (mBtnNext.isSelected()) {
							mCurrentPage++;
							showCommentList();
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			btnBack.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					SVUtil.hideSoftInput(mMainActivity);
					moveToTwoInOneFragment();
				}
			});

			btnRegisterComment.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					SVUtil.hideSoftInput(mMainActivity);

					registerComment();
				}
			});

			mBtnEdit.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					moveToTwoInOneWriteFragment();
				}
			});

			mBtnDelete.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					SVUtil.showDialogWithListener(mMainActivity, "알림", "2인1조 등록 내용을 삭제 하시겠습니까?", "삭제", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							deleteTwoInOne();
						}
					}, "취소", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});
				}
			});

			get2in1ContentInfo(mBoardId);
		} catch(InflateException e) {
			e.printStackTrace();
		}

		return mView;
	}

	private void moveToTwoInOneFragment() {
		try {
			mMainActivity.mFragmentTwoInOne = new TwoInOneFragment();
			mMainActivity.replaceFragment(mMainActivity.mFragmentTwoInOne, R.id.mainFrameLayout, Global.FRAG_TWOINONE_TAG, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private void moveToTwoInOneWriteFragment() {
		try {
			TwoInOneWriteFragment frag = new TwoInOneWriteFragment();

			Bundle args = new Bundle();
			args.putString("boardId", mBoardId);

			frag.setArguments(args);

			mMainActivity.replaceFragment(frag, R.id.mainFrameLayout, Global.FRAG_TWOINONE_WRITE_TAG, false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void checkPageButtonEnable() {
		try {
			mBtnNext.setSelected(true);
			mBtnPrev.setSelected(true);

			if(mCurrentPage + 1 == mTotalPageCount) {
				mBtnNext.setSelected(false);
			}

			if(mCurrentPage == 0) {
				mBtnPrev.setSelected(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void get2in1ContentInfo(String _boardId) {
		SVUtil.showProgressDialog(mMainActivity, "로딩중...");

		String url = Global.URL_BOARD_INFO.replaceAll(":boardId", _boardId);
		NetClient.send(mMainActivity, url, "GET", null, new TwoInOneContentDetailAck(), new NetResponseCallback(new NetResponse() {
			@Override
			public void onResponse(NetAPI _netAPI) {
				try {
					SVUtil.hideProgressDialog();

					if (_netAPI != null && !mIsFinishFragment) {
						TwoInOneContentDetailAck retNetApi = (TwoInOneContentDetailAck) _netAPI;
						ResultData result = retNetApi.getmResultData();

						if (result.getmCode() != 100) {
							SVUtil.showSimpleDialog(mMainActivity, result.getmDetail());
						} else {
							TwoInOneContentDetailData contentDetailInfo = retNetApi.getmTwoInOneContentDetailData();
							int commentListSize = contentDetailInfo.getmTwoInOneCommentDataList().size();
							String dateString = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(contentDetailInfo.getmCreatedDate())) + " | " + contentDetailInfo.getmNickName();
							String commentCount = commentListSize + "";
							String bottomCommentCount = "댓글(" + commentListSize + ")";

							if (contentDetailInfo.getmCategory().equals("find")) {
								mCategoryIcon.setImageResource(R.drawable.detail_gp_category02);
							} else {
								mCategoryIcon.setImageResource(R.drawable.detail_gp_category01);
							}

							if (contentDetailInfo.getmSex().equals("male")) {
								mSex.setText("남자");
							} else {
								mSex.setText("여자");
							}

							mTitle.setText(contentDetailInfo.getmTitle());
							mDate.setText(dateString);
							mCommentCount.setText(commentCount);
							mNickName.setText(contentDetailInfo.getmNickName());
							mAge.setText(contentDetailInfo.getmAge());
							mCareer.setText(contentDetailInfo.getmCareer());
							mDivide.setText(contentDetailInfo.getmDivide());
							mRange.setText(contentDetailInfo.getmRange());
							mStart.setText(contentDetailInfo.getmStart());
							mTime.setText(contentDetailInfo.getmTime());
							mTarget.setText(contentDetailInfo.getmTarget());
							mCar.setText(contentDetailInfo.getmCar());
							mContact.setText(contentDetailInfo.getmContact());
							mEtc.setText(contentDetailInfo.getmEtc());
							mBottomCommentCount.setText(bottomCommentCount);

							for (int i = 0; i < commentListSize; i++) {
								TwoInOneCommentData commentData = contentDetailInfo.getmTwoInOneCommentDataList().get(i);
								m2in1CommentList.add(commentData);
							}

							showCommentList();

							String phoneNumber = mMainActivity.mDriverInfo.getmPhone().substring(mMainActivity.mDriverInfo.getmPhone().length() - 4);
							mETNickName.setText(phoneNumber);

							if(mMainActivity.mDriverInfo.getmDriverId().equals(contentDetailInfo.getmDriverId())) {
								mButtonWrapper.setVisibility(View.VISIBLE);
							} else {
								mButtonWrapper.setVisibility(View.GONE);
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}));
	}

	private void deleteTwoInOne() {
		RequestParams params = new RequestParams();
		params.add("boardId", mBoardId);
		params.add("driverId", mMainActivity.mDriverInfo.getmDriverId());

		NetClient.send(mMainActivity, Global.URL_BOARD_DELETE, "POST", params, new NoResponseDataAck(), new NetResponseCallback(new NetResponse() {
			@Override
			public void onResponse(NetAPI _netAPI) {
				try {
					if (_netAPI != null && !mIsFinishFragment) {
						NoResponseDataAck retNetApi = (NoResponseDataAck) _netAPI;
						ResultData result = retNetApi.getmResultData();
						String message = "2인1조 삭제를 완료 했습니다.";
						if (result.getmCode() != 100) {
							message = result.getmDetail();
						}

						SVUtil.showDialogWithListener(mMainActivity, message, "확인", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								moveToTwoInOneFragment();
							}
						});
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}));
	}

	private void calculatePageCount() {
		try {
			int val = m2in1CommentList.size() % LIST_COUNT_PER_PAGE;
			val = val == 0 ? 0 : 1;
			mTotalPageCount = (m2in1CommentList.size() / LIST_COUNT_PER_PAGE) + val;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void showCommentList() {
		try {
			calculatePageCount();

			String page = "Page " + (mCurrentPage + 1) + " of " + mTotalPageCount;
			mPageText.setText(page);

			m2in1ShowCommentList.clear();

			int start = mCurrentPage * LIST_COUNT_PER_PAGE;
			int commentListSize = m2in1CommentList.size();
			for(int i = start; i < (start + LIST_COUNT_PER_PAGE); i++) {
				if(i < commentListSize) {
					m2in1ShowCommentList.add(m2in1CommentList.get(i));
				} else {
					break;
				}
			}

			m2in1CommentAdapter.notifyDataSetChanged();
//		getListViewSize();

			checkPageButtonEnable();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void registerComment() {
		try {
			final String nickName = mETNickName.getText().toString();
			final String comment = mETComment.getText().toString();

			if(nickName.isEmpty()) {
				SVUtil.showSimpleDialog(mMainActivity, "닉네임을 입력해주세요.");
				return;
			}

			if(comment.isEmpty()) {
				SVUtil.showSimpleDialog(mMainActivity, "댓글을 입력해주세요.");
				return;
			}

			SVUtil.showProgressDialog(mMainActivity, "등록중...");

			RequestParams params = new RequestParams();
			params.add("driverId", mMainActivity.mDriverInfo.getmDriverId());
			params.add("boardId", mBoardId);
			params.add("nickName", nickName);
			params.add("comment", comment);

			NetClient.send(mMainActivity, Global.URL_BOARD_WRITE_COMMENT, "POST", params, new NoResponseDataAck(), new NetResponseCallback(new NetResponse() {
				@Override
				public void onResponse(NetAPI _netAPI) {
					SVUtil.hideProgressDialog();
					try {
						if (_netAPI != null && !mIsFinishFragment) {
							NoResponseDataAck retNetApi = (NoResponseDataAck) _netAPI;
							ResultData result = retNetApi.getmResultData();

							if (result.getmCode() != 100) {
								SVUtil.showSimpleDialog(mMainActivity, result.getmDetail());
							} else {
								mETComment.setText("");

								SVUtil.showSimpleDialog(mMainActivity, "등록 되었습니다.");

								long nowTime = System.currentTimeMillis();

								m2in1CommentList.add(new TwoInOneCommentData(comment, nowTime, mMainActivity.mDriverInfo.getmDriverId(), nickName));

								showCommentList();
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private class ViewHolder2in1Comment {
		public TextView mTextViewNickName;
		public TextView mTextViewComment;
		public TextView mTextViewDate;
	}

	private class CommentAdapter extends ArrayAdapter<TwoInOneCommentData> {
		ArrayList<TwoInOneCommentData> mCommentList;
		private int mRowId;
		private ViewHolder2in1Comment mHolder;

		public CommentAdapter(Context _context, int _rowId, ArrayList<TwoInOneCommentData> _commentList) {
			super(_context, _rowId, _commentList);
			this.mCommentList = _commentList;
			this.mRowId = _rowId;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;

			try {
				if (v == null) {
					LayoutInflater li = (LayoutInflater) mMainActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					v = li.inflate(mRowId, null);
					mHolder = new ViewHolder2in1Comment();
					mHolder.mTextViewNickName = (TextView) v.findViewById(R.id.row_2in1_comment_nick_name);
					mHolder.mTextViewComment = (TextView) v.findViewById(R.id.row_2in1_comment_comment);
					mHolder.mTextViewDate = (TextView) v.findViewById(R.id.row_2in1_comment_date);

					v.setTag(mHolder);
				} else {
					mHolder = (ViewHolder2in1Comment) v.getTag();
				}

				TwoInOneCommentData commentBox = mCommentList.get(position);

				String nickName = commentBox.getmNickName() + " : ";

				mHolder.mTextViewNickName.setText(nickName);
				mHolder.mTextViewDate.setText(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(commentBox.getmCreatedAt())));
				mHolder.mTextViewComment.setText(commentBox.getmComment());
			} catch (Exception e) {
				e.printStackTrace();
			}

			return v;
		}
	}

	@Override
	public void onStart() {
		super.onStart();

		mIsFinishFragment = false;
	}

	@Override
	public void onResume() {
		super.onResume();

		try {
			mMainActivity.changeButtonImage(mMainActivity.mImgBtn2in1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onPause() {
		super.onPause();

		mIsFinishFragment = true;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();

		try {
			if(mView != null){
				ViewGroup parent = (ViewGroup) mView.getParent();
				if(parent!=null){
					parent.removeView(mView);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		setTargetFragment(null, -1);
	}
}
