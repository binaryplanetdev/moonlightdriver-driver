package kr.moonlightdriver.driver.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import kr.moonlightdriver.driver.Network.NetData.ChatJoinedDriverData;
import kr.moonlightdriver.driver.R;

public class WokitokiEntryListActivity extends Activity {

    public static ArrayList<ChatJoinedDriverData> mChatJoinedDriverDataList;
    private ListView mEntryList;
    private String myDriverId;
    private ImageButton mExitButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_entrylist);

            mChatJoinedDriverDataList = new ArrayList<>();

            Intent intent = getIntent();

            int cnt = intent.getIntExtra("cnt", 0);
            for(int i=0; i<cnt; i++){
                try {
                    mChatJoinedDriverDataList.add(new ChatJoinedDriverData(intent.getStringExtra("driverId_" + i), intent.getStringExtra("nickName" + i), intent.getStringExtra("joinedDate" + i)));
                    Log.i("aaa", intent.getStringExtra("joinedDate" + i));
                }catch (RuntimeException e){
                    e.printStackTrace();
                }
            }
            myDriverId = intent.getStringExtra("myDriverId");

            mEntryList = (ListView) findViewById(R.id.content_chatjoined_listview);
            mEntryList.setAdapter(new EntryInfoAdapter());

            mExitButton = (ImageButton) findViewById(R.id.btn_list_exit);
            mExitButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class EntryInfoAdapter extends BaseAdapter {
        ViewHolder holder;

        @Override
        public int getCount() {
            return mChatJoinedDriverDataList.size();
        }

        @Override
        public Object getItem(int position) {
            return mChatJoinedDriverDataList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;

            try {
                if (v == null) {
                    LayoutInflater li = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    v = li.inflate(R.layout.row_entry, parent, false);
                    holder = new ViewHolder();

                    holder.mEntryName = (TextView) v.findViewById(R.id.tv_entryName);
                    holder.mEntranceTime = (TextView) v.findViewById(R.id.tv_EntranceTime);
                    holder.mOfferPrivateWokitoki = (ImageButton) v.findViewById(R.id.iv_offerprivatewokitoki);

                    v.setTag(holder);
                } else {
                    holder = (ViewHolder) v.getTag();
                }

                final String driveId = mChatJoinedDriverDataList.get(position).getmDriverId();
                String nickName = mChatJoinedDriverDataList.get(position).getmNickName();
                String joinDate = mChatJoinedDriverDataList.get(position).getmJoinDate();

                Log.i("aa", joinDate);

                holder.mEntryName.setText(nickName);
                try {
                    SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                    Date to = transFormat.parse(joinDate);
                    transFormat = new SimpleDateFormat("HH:mm", Locale.KOREA);
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(to);
                    cal.add(Calendar.HOUR, +9);
                    holder.mEntranceTime.setText(transFormat.format(cal.getTime())+"");
                }catch (StringIndexOutOfBoundsException e){
                    e.printStackTrace();
                    holder.mEntranceTime.setText("00:00");
                }

                if(driveId.equals(myDriverId)){
                    holder.mOfferPrivateWokitoki.setImageDrawable(getResources().getDrawable(R.drawable.btn_me));
                }else{
                    holder.mOfferPrivateWokitoki.setImageDrawable(getResources().getDrawable(R.drawable.btn_privatewokitokioffer));
                    holder.mOfferPrivateWokitoki.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = getIntent();
                            intent.putExtra("driverId", driveId);
                            setResult(100, intent);
                            finish();
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return v;
        }
    }

    private class ViewHolder {
        public TextView mEntryName;
        public TextView mEntranceTime;
        public ImageButton mOfferPrivateWokitoki;
    }
}
