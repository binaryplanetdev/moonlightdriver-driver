package kr.moonlightdriver.driver.Network.NetData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by youngmin on 2015-12-31.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BusRouteInfoData {
	@JsonProperty("busRouteId")
	private String mBusRouteId;

	@JsonProperty("busRouteName")
	private String mBusRouteName;

	@JsonProperty("busRouteArea")
	private String mBusRouteArea;

	@JsonProperty("startStationName")
	private String mStartStationName;

	@JsonProperty("endStationName")
	private String mEndStationName;

	@JsonProperty("firstTime")
	private String mFirstTime;

	@JsonProperty("lastTime")
	private String mLastTime;

	@JsonProperty("term")
	private String mTerm;

	@JsonProperty("localBusRouteId")
	private String mLocalBusRouteId;

	public BusRouteInfoData() {}

	public BusRouteInfoData(String _busRouteId, String _busRouteName, String _busRouteArea, String _startStationName, String _endStationName, String _firstTime, String _lastTime, String _term, String _localBusRouteId) {
		this.mBusRouteId = _busRouteId;
		this.mBusRouteName = _busRouteName;
		this.mBusRouteArea = _busRouteArea;
		this.mStartStationName = _startStationName;
		this.mEndStationName = _endStationName;
		this.mFirstTime = _firstTime;
		this.mLastTime = _lastTime;
		this.mTerm = _term;
		this.mLocalBusRouteId = _localBusRouteId;
	}

	public String getmLocalBusRouteId() {
		return mLocalBusRouteId;
	}

	public void setmLocalBusRouteId(String mLocalBusRouteId) {
		this.mLocalBusRouteId = mLocalBusRouteId;
	}

	public String getmBusRouteArea() {
		return mBusRouteArea;
	}

	public void setmBusRouteArea(String mBusRouteArea) {
		this.mBusRouteArea = mBusRouteArea;
	}

	public String getmBusRouteId() {
		return mBusRouteId;
	}

	public void setmBusRouteId(String mBusRouteId) {
		this.mBusRouteId = mBusRouteId;
	}

	public String getmBusRouteName() {
		return mBusRouteName;
	}

	public void setmBusRouteName(String mBusRouteName) {
		this.mBusRouteName = mBusRouteName;
	}

	public String getmEndStationName() {
		return mEndStationName;
	}

	public void setmEndStationName(String mEndStationName) {
		this.mEndStationName = mEndStationName;
	}

	public String getmFirstTime() {
		return mFirstTime;
	}

	public void setmFirstTime(String mFirstTime) {
		this.mFirstTime = mFirstTime;
	}

	public String getmLastTime() {
		return mLastTime;
	}

	public void setmLastTime(String mLastTime) {
		this.mLastTime = mLastTime;
	}

	public String getmStartStationName() {
		return mStartStationName;
	}

	public void setmStartStationName(String mStartStationName) {
		this.mStartStationName = mStartStationName;
	}

	public String getmTerm() {
		return mTerm;
	}

	public void setmTerm(String mTerm) {
		this.mTerm = mTerm;
	}
}
