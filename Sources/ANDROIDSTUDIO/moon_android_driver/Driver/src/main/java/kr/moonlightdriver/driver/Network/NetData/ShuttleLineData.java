package kr.moonlightdriver.driver.Network.NetData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * Created by youngmin on 2015-10-07.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShuttleLineData {
	@JsonProperty("_id")
	private ShuttleLineIdData mShuttleLineIdData;
	@JsonProperty("shuttle_point")
	private ArrayList<StationInfoData> mShuttleLinePointDataList;

	public ShuttleLineData() {}

	public ShuttleLineData(ShuttleLineIdData mShuttleLineIdData, ArrayList<StationInfoData> mShuttleLinePointDataList) {
		this.mShuttleLineIdData = mShuttleLineIdData;
		this.mShuttleLinePointDataList = mShuttleLinePointDataList;
	}

	public ShuttleLineIdData getmShuttleLineIdData() {
		return mShuttleLineIdData;
	}

	public void setmShuttleLineIdData(ShuttleLineIdData mShuttleLineIdData) {
		this.mShuttleLineIdData = mShuttleLineIdData;
	}

	public ArrayList<StationInfoData> getmShuttleLinePointDataList() {
		return mShuttleLinePointDataList;
	}

	public void setmShuttleLinePointDataList(ArrayList<StationInfoData> mShuttleLinePointDataList) {
		this.mShuttleLinePointDataList = mShuttleLinePointDataList;
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public class ShuttleLineIdData {
		@JsonProperty("busId")
		private String mBusId;

		public ShuttleLineIdData() {}

		public ShuttleLineIdData(String mBusId) {
			this.mBusId = mBusId;
		}

		public String getmBusId() {
			return mBusId;
		}

		public void setmBusId(String mBusId) {
			this.mBusId = mBusId;
		}
	}
}
