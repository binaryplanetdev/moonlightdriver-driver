package kr.moonlightdriver.drunkendrivingcheck;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;

/**
 * Created by youngmin on 2016-12-05.
 */

public class CrackdownDataService extends Service {
    private static final String TAG = "CrackdownDataService";
    private static final String UPLOAD_SERVER = "http://115.68.122.108/ajax/crackdown.php";
//    private static final String UPLOAD_SERVER = "http://dev.sncinteractive.com:8889/ajax/crackdown.php";
    private static final String mSite_1_URL = "https://du3.shjey.com/api_v19/initMap.php";      // 더더더

    private static final int CRACKDOWN_DATA_REFRESH_TIME = 5 * 60 * 1000;  // 음주단속 지점 갱신 시간 5분
    private static final int MSG_1 = 1;
    public static final String ACTION = "UPDATE_CRACKDOWN";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        try {
            getCrackdownData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean checkRunningTime() {
        try {
            Calendar calendar = Calendar.getInstance(Locale.KOREA);
            int hours = calendar.get(Calendar.HOUR_OF_DAY);

            return hours >= 18 || hours < 6;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            try {
                switch (msg.what) {
                    case MSG_1 :
                        getCrackdownData();
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private void getCrackdownData() {
        try {
            if(!checkRunningTime()) {
                handler.sendEmptyMessageDelayed(MSG_1, CRACKDOWN_DATA_REFRESH_TIME);
                return;
            }

            AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
            asyncHttpClient.setTimeout(30000);
            asyncHttpClient.get(mSite_1_URL, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    try {
                        String response = new String(responseBody);

                        if(response.isEmpty()) {
                            sendDataToMain("Failure", 0);
                        } else {
                            JSONObject responseObj = new JSONObject(response);
                            JSONArray crackdowns = responseObj.optJSONArray("crackdowns");
                            if(crackdowns != null && crackdowns.length() > 0) {
                                JSONArray ret_params = new JSONArray();
                                String seqList = "";
                                int listSize = crackdowns.length();
                                for (int i = 0; i < listSize; i++) {
                                    JSONObject ret = crackdowns.getJSONObject(i);
                                    if(ret.getString("category").equals("POLICE")) {
                                        JSONObject params = new JSONObject();
                                        params.put("seq", ret.getLong("seq"));
                                        params.put("sido", ret.getString("sido"));
                                        params.put("sigugun", ret.getString("sigugun"));
                                        params.put("address", ret.getString("address"));
                                        params.put("latitude", ret.getDouble("latitude"));
                                        params.put("longitude", ret.getDouble("longitude"));
                                        params.put("createTime", ret.getLong("createTime"));
                                        params.put("from", "더더더");

                                        ret_params.put(params);

                                        seqList += ret.getLong("seq") + ",";
                                    }
                                }

                                updateCrackdownData(ret_params.toString(), seqList);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        sendDataToMain("Failure", 0);
                    }

                    handler.sendEmptyMessageDelayed(MSG_1, CRACKDOWN_DATA_REFRESH_TIME);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    try {
                        sendDataToMain("Failure", 0);
                        error.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    handler.sendEmptyMessageDelayed(MSG_1, CRACKDOWN_DATA_REFRESH_TIME);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendDataToMain(String _status, int _resultCount) {
        try {
            Intent intent = new Intent();
            intent.setAction(CrackdownDataService.ACTION);
            intent.putExtra("resultCount", _resultCount);
            intent.putExtra("status", _status);

            sendBroadcast(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateCrackdownData(String _retData, String _seqList) {
        try {
            RequestParams post_params = new RequestParams();
            post_params.add("type", "update_crackdown_data");
            post_params.add("crackdown_data", _retData);
            post_params.add("seq_list", _seqList);

            AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
            asyncHttpClient.setTimeout(30000);
            asyncHttpClient.post(UPLOAD_SERVER, post_params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    try {
                        String response = new String(responseBody);
                        JSONObject responseObj = new JSONObject(response);
                        if(responseObj.getString("result").equals("OK")) {
                            int retCount = responseObj.getInt("retCount");
                            sendDataToMain("Success", retCount);
                        } else {
                            sendDataToMain("Failure", 0);
                        }
                    } catch (Exception e) {
                        sendDataToMain("Failure", 0);
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    try {
                        sendDataToMain("Failure", 0);
                        error.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
