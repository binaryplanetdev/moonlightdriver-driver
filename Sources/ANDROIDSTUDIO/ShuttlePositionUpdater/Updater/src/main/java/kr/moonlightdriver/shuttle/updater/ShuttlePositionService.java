package kr.moonlightdriver.shuttle.updater;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.util.Calendar;

import cz.msebera.android.httpclient.Header;

/**
 * Created by youngmin on 2016-09-21.
 */

public class ShuttlePositionService extends Service {
	private static final String TAG = "ShuttlePositionService";
	private static final int SHUTTLE_POSITION_REFRESH_TIME = 5 * 1000;  // 셔틀 위치 갱신 시간 : 5초
	private static final int MSG_1 = 1;
	public static final String ACTION = "UPDATE_POSITION";
	public static final int START_TIME = 22;
	public static final int END_TIME = 6;
	public static final String RETRIEVING_URL = "http://121.254.229.91:1544/GetCarGps";
//	public static final String SERVER_URL = "http://115.68.122.108/ajax/shuttle_position.php";	// 실서버 URL
	public static final String SERVER_URL = "http://218.36.4.85:8090/ajax/shuttle_position.php";	// 개발서버 URL

	private boolean mIsContinue;

	@Nullable
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		super.onStartCommand(intent, flags, startId);
		return START_STICKY;
	}

	@Override
	public void onCreate() {
		try {
			mIsContinue = true;
			getRealTimeShuttlePosition();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			try {
				switch (msg.what) {
					case MSG_1 :
						getRealTimeShuttlePosition();
						break;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};

	private boolean isContinue() {
		boolean isContinue = false;

		try {
			Calendar cal = Calendar.getInstance();
			int nowHour = cal.get(Calendar.HOUR_OF_DAY);

			if(!(nowHour < START_TIME && nowHour >= END_TIME)) {
				isContinue = true;
			} else {
				Log.e(TAG, "isContinue() not working time : ");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return isContinue;
	}

	private void getRealTimeShuttlePosition() {
		try {
			if(!mIsContinue) {
				return;
			}

			if(!isContinue()) {
				sendDataToMain("InvalidTIme", 0);
				handler.sendEmptyMessageDelayed(MSG_1, SHUTTLE_POSITION_REFRESH_TIME);
				return;
			}

			AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
			asyncHttpClient.get(RETRIEVING_URL, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					try {
						String retData = new String(responseBody);
						Log.e(TAG, "getRealTimeShuttlePosition() retData : " + retData);
						if(!retData.isEmpty()) {
							updateShuttlePosition(retData);
						} else {
							sendDataToMain("Failure", 0);
						}

						handler.sendEmptyMessageDelayed(MSG_1, SHUTTLE_POSITION_REFRESH_TIME);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					try {
						sendDataToMain("Failure", 0);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void sendDataToMain(String _status, int _resultCount) {
		try {
			Log.e(TAG, "_status : " + _status + ", _resultCount: " + _resultCount);

			Intent intent = new Intent();
			intent.setAction(ShuttlePositionService.ACTION);
			intent.putExtra("resultCount", _resultCount);
			intent.putExtra("status", _status);

			sendBroadcast(intent);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private void updateShuttlePosition(String _retData) {
		try {
//			_retData = "{\"CarGpsList\":[{\"Lat\":\"35.799692\",\"Lon\":\"128.623369\",\"UserID\":\"가창*\"},{\"Lat\":\"35.840081\",\"Lon\":\"128.6203\",\"UserID\":\"경산1*\"},{\"Lat\":\"35.841672\",\"Lon\":\"128.623903\",\"UserID\":\"경산2*\"},{\"Lat\":\"35.846633\",\"Lon\":\"128.671819\",\"UserID\":\"경산3*\"},{\"Lat\":\"35.825358\",\"Lon\":\"128.726203\",\"UserID\":\"경산4*\"},{\"Lat\":\"35.817608\",\"Lon\":\"128.761322\",\"UserID\":\"경산5*\"},{\"Lat\":\"35.837567\",\"Lon\":\"128.755064\",\"UserID\":\"경산6*\"},{\"Lat\":\"35.852144\",\"Lon\":\"128.486536\",\"UserID\":\"다사1*\"},{\"Lat\":\"35.864597\",\"Lon\":\"128.457519\",\"UserID\":\"다사2*\"},{\"Lat\":\"35.876556\",\"Lon\":\"128.496775\",\"UserID\":\"다사3*\"},{\"Lat\":\"35.791556\",\"Lon\":\"128.474525\",\"UserID\":\"다산1*\"},{\"Lat\":\"35.802883\",\"Lon\":\"128.496117\",\"UserID\":\"다산2*\"},{\"Lat\":\"35.841703\",\"Lon\":\"128.536872\",\"UserID\":\"대곡1*\"},{\"Lat\":\"35.851278\",\"Lon\":\"128.492478\",\"UserID\":\"대곡2*\"},{\"Lat\":\"35.813150\",\"Lon\":\"128.512178\",\"UserID\":\"대곡3*\"},{\"Lat\":\"35.819353\",\"Lon\":\"128.5371\",\"UserID\":\"대곡4*\"},{\"Lat\":\"35.877983\",\"Lon\":\"128.649147\",\"UserID\":\"반야월1*\"},{\"Lat\":\"35.8602\",\"Lon\":\"128.622378\",\"UserID\":\"반야월2*\"},{\"Lat\":\"35.890003\",\"Lon\":\"128.642278\",\"UserID\":\"반야월3*\"},{\"Lat\":\"35.863397\",\"Lon\":\"128.698244\",\"UserID\":\"반야월4*\"},{\"Lat\":\"35.870611\",\"Lon\":\"128.732136\",\"UserID\":\"반야월5*\"},{\"Lat\":\"35.808161\",\"Lon\":\"128.758650\",\"UserID\":\"백천*\"},{\"Lat\":\"35.822047\",\"Lon\":\"128.652736\",\"UserID\":\"범물1*\"},{\"Lat\":\"35.834372\",\"Lon\":\"128.727583\",\"UserID\":\"범물2*\"},{\"Lat\":\"35.884831\",\"Lon\":\"128.502861\",\"UserID\":\"사수*\"},{\"Lat\":\"35.922025\",\"Lon\":\"128.599211\",\"UserID\":\"서변1*\"},{\"Lat\":\"35.875294\",\"Lon\":\"128.587906\",\"UserID\":\"서변2*\"},{\"Lat\":\"35.891089\",\"Lon\":\"128.590072\",\"UserID\":\"서변3*\"},{\"Lat\":\"35.864653\",\"Lon\":\"128.582103\",\"UserID\":\"중부1*\"},{\"Lat\":\"35.851261\",\"Lon\":\"128.493939\",\"UserID\":\"중부2*\"},{\"Lat\":\"35.835342\",\"Lon\":\"128.497672\",\"UserID\":\"중부3*\"},{\"Lat\":\"35.863650\",\"Lon\":\"128.601506\",\"UserID\":\"중부4*\"},{\"Lat\":\"35.905508\",\"Lon\":\"128.612247\",\"UserID\":\"지묘3*\"},{\"Lat\":\"35.938933\",\"Lon\":\"128.645014\",\"UserID\":\"지묘4*\"},{\"Lat\":\"35.840328\",\"Lon\":\"128.7593\",\"UserID\":\"진량1*\"},{\"Lat\":\"35.883583\",\"Lon\":\"128.825244\",\"UserID\":\"진량2*\"},{\"Lat\":\"35.883325\",\"Lon\":\"128.817928\",\"UserID\":\"진량3*\"},{\"Lat\":\"35.913975\",\"Lon\":\"128.819281\",\"UserID\":\"진량4*\"},{\"Lat\":\"35.931489\",\"Lon\":\"128.554914\",\"UserID\":\"칠곡1*\"},{\"Lat\":\"35.943272\",\"Lon\":\"128.589978\",\"UserID\":\"칠곡3*\"},{\"Lat\":\"35.878167\",\"Lon\":\"128.554794\",\"UserID\":\"칠곡4*\"},{\"Lat\":\"35.930181\",\"Lon\":\"128.547914\",\"UserID\":\"칠곡5*\"},{\"Lat\":\"35.939639\",\"Lon\":\"128.569072\",\"UserID\":\"칠곡6*\"},{\"Lat\":\"35.837717\",\"Lon\":\"128.557822\",\"UserID\":\"화원1*\"},{\"Lat\":\"35.844375\",\"Lon\":\"128.588358\",\"UserID\":\"화원2*\"},{\"Lat\":\"35.839083\",\"Lon\":\"128.575378\",\"UserID\":\"화원3*\"},{\"Lat\":\"35.818825\",\"Lon\":\"128.537503\",\"UserID\":\"화원4*\"},{\"Lat\":\"35.801442\",\"Lon\":\"128.494025\",\"UserID\":\"화원5*\"},{\"Lat\":\"35.840864\",\"Lon\":\"128.536878\",\"UserID\":\"화원7*\"},{\"Lat\":\"35.820036\",\"Lon\":\"128.523644\",\"UserID\":\"화원8*\"}],\"Code\":\"OK\"}";

			RequestParams params = new RequestParams();
			params.add("type", "update_shuttle_position");
			params.add("shuttle_position_data", _retData);

			AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
			asyncHttpClient.post(SERVER_URL, params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					try {
						String retData = new String(responseBody);

						Log.e(TAG, "updateShuttlePosition() retData : " + retData);

						JSONObject responseObj = new JSONObject(retData);

						if(responseObj.getString("result").equals("OK")) {
							int retCount = responseObj.getInt("retCount");
							sendDataToMain("Success", retCount);
						} else {
							sendDataToMain("Failure", 0);
						}

					} catch (Exception e) {
						e.printStackTrace();
					}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					try {
						sendDataToMain("Failure", 0);
						error.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mIsContinue = false;
	}
}
